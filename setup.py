﻿from setuptools import setup, Extension
from Cython.Build import cythonize
import time

start_time = time.clock()

USE_CYTHON = True  # TODO: detect when to compile from .c instead

ext = '.pyx' if USE_CYTHON else '.cpp'

module_names = [ "Color", "BulletType", "EventType", "Flag", "GameType",
    "Input", "Order", "UnitSizeType",
    "UnitCommandType", "UnitCommand", "DamageType", "ExplosionType",
    # massive dependencies below
    "WeaponType", "UpgradeType", "TechType",
    "Force", "Forceset", "Race",  "Unitset",
      "BWAPI",
    # Incorporated into GameClient below
    # "Unit", "Game", "Region", "Position", "Bullet", "Player", "Event",
    # "Playerset", "Regionset", "UnitType", "Bulletset",
]

module_link_options = [
    ["Color", "/LTCG"],
    ["BulletType", "/LTCG"],
    ["EventType", "/LTCG"],
    ["Flag", "/LTCG"],
    ["GameType","/LTCG"],
    ["Input", "/LTCG"],
    ["Order", "/LTCG"],
    ["UnitSizeType","/LTCG"],
    ["UnitCommandType", "/LTCG"],
    ["UnitCommand", "/LTCG"],
    ["DamageType", "/LTCG"],
    ["ExplosionType","/LTCG"],
    ["WeaponType", "/LTCG"],
    ["UpgradeType", "/LTCG"],
    ["TechType","/LTCG"],
    ["Force", ""],
    ["Forceset", "/LTCG"],
    ["Race", "/LTCG"],
    ["Unitset", ""],
    ["BWAPI", "/LTCG"],
]

client = Extension("cybw.GameClient",
      sources=["cybw/GameClient{}".format(ext)],
      language="c++",
      include_dirs=["../include/"],  # path to .h file(s)
      library_dirs=["../lib/"],  # path to .a or .so file(s)
      libraries=['BWAPI'],
      extra_objects=['../lib/BWAPI.lib', '../lib/BWAPIClient.lib'],
      extra_compile_args=["/EHsc", "/wd 4190", "/wd 4800",
        "/MP8",  # multicore
        ],
      extra_link_args=["/LTCG"],
    )


def generate_extension(name_options):
    name, link_args = name_options
    return Extension("cybw.{}".format(name),
      sources=["cybw/{}{}".format(name, ext)],
      language="c++",
      include_dirs=["../include/"],  # path to .h file(s)
      library_dirs=["../lib/"],  # path to .a or .so file(s)
      libraries=['BWAPI'],
      extra_objects=['../lib/BWAPI.lib', '../lib/BWAPIClient.lib'],
      extra_compile_args=["/EHsc", "/wd 4190", "/wd 4800",
        "/MP8",  # multicore
        "/Od"   # disable optimization
        ],
      extra_link_args=[link_args],
    )

ext_modules = []

for name_options in module_link_options:
    ext_modules.append(generate_extension(name_options))

ext_modules.append(client)

if USE_CYTHON:
    extensions = cythonize(ext_modules, compiler_directives={
        'language_level': 3,
        'embedsignature': True,
        'optimize.use_switch': True,
        'optimize.unpack_method_calls': True
    })
else:
    extensions = ext_modules

setup(
  name='cybw',
  version='0.7.1b',
  description='Python wrapper for BWAPI_4.1.2',
  url='https://bitbucket.org/ratiotile/cybw',
  license='BSD',
  classifiers=[
    'Development Status :: Beta',
    'Intended Audience :: Developers',
    'Topic :: Software Development :: Wrappers',
    'Programming Language :: Python :: 3.5',
  ],
  keywords='bwapi wrapper',
  ext_modules=extensions,
  packages=['cybw'],
  # package_data={
  #  'cybw': ['BWAPI.pyd']  # compiled dll extension (windows)
  # },
  data_files=[
    ("example_ai", ["example_ai/example.py"])
  ]
)
print("Built {} modules in {} s.".format(
    len(ext_modules), time.clock()-start_time))
