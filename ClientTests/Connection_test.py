import pytest
import cybw
from time import sleep

client = cybw.BWAPIClient
Broodwar = cybw.Broodwar


def reconnect():
    while not client.connect():
        sleep(0.5)

@pytest.fixture(scope="module")
def connection(request):
    def fin():
        # Broodwar.leaveGame()
        print("Tests finished!")

    print("Connecting...")
    request.addfinalizer(fin)
    reconnect()
    while True:
        print("waiting to enter match")
        while not Broodwar.isInGame():
            client.update()
            if not client.isConnected():
                print("Reconnecting...")
                reconnect()
        print("starting match!")
        return

def test_inGame(connection):
    assert Broodwar.isInGame() == True

def test_hasModules(connection):
    assert hasattr(cybw, 'Position')
    assert hasattr(cybw, 'WalkPosition')
    assert hasattr(cybw, 'TilePosition')
    assert hasattr(cybw, 'GameType')
    assert hasattr(cybw, 'Race')
    assert hasattr(cybw, 'UnitType')
    assert hasattr(cybw, 'Unit')
    assert hasattr(cybw, 'TechType')
    assert hasattr(cybw, 'UpgradeType')
    assert hasattr(cybw, 'WeaponType')
    assert hasattr(cybw, 'DamageType')

def test_offmap_position_invalid(connection):
    """ when Position and Game were wrapped independantly, there was a bug
    where obviously invalid off-map positions would return True for isValid()
    """
    position = cybw.Position(-1, -1)
    assert position.isValid() == False

def test_offmap_region_crash(connection):
    """ There was also a crash caused by region being wrapped separately. After
    getting an off-map region from Broodwar, calling any methods on it would
    crash python. NOTE this issue is with BWAPI itself! """
    #position = cybw.Position(-1, 489)
    #region = cybw.Broodwar.getRegionAt(position)
    #assert region.isAccessible() == False