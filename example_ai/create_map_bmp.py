﻿import cybw
from cybw import Unit
from time import sleep
from cybw import Colors, Color
from cybw import Position
from PIL import Image, ImageDraw

client = cybw.BWAPIClient
Broodwar = cybw.Broodwar
all_regions = set()
r_id_color = {}
region_colors = [
    (100,0,0),
    (0,80,0),
    (60,60,60),
    (80,80,80),
    (100,100,100),
    (120,120,120),
    (140,140,140),
]
choke_colors = [
    (255,00,00),  # 2
    (000,255,00), # 3
    (0,0,255),     # 4
    (0,255,255),     # 5
]
im = None
draw = None

def create_image(x, y):
    global im, draw
    im = Image.new("RGB",(x, y))
    draw = ImageDraw.Draw(im)

def image_draw_point(x, y, color):
    global draw
    draw.point((x, y), color)

def save_image(pathname):
    global draw, im
    del draw
    try:
        im.save(pathname)

    except Exception as e:
        print("error, {}".format(str(e)))

def draw_map(pathname):
    # assume all regions have colors assigned
    for x in range(Broodwar.mapWidth()):
        for y in range(Broodwar.mapHeight()):
            pos = Position(x*32+16, y*32+16)
            region = Broodwar.getRegionAt(pos)
            if region.isAccessible():
                if False and region.getDefensePriority() > 1:
                    c = choke_colors[region.getDefensePriority()-1]
                else:
                    c = r_id_color[region.getID()]
            else:
                c = (0,0,0)
            image_draw_point(x, y, c )

    draw_resources()

    save_image(pathname)

def draw_resources():
    geysers = Broodwar.getStaticGeysers()
    minerals = Broodwar.getStaticMinerals()
    for mineral in minerals:
        tp = mineral.getTilePosition()
        x = tp.x()
        y = tp.y()
        for xi in range(x, x+2):
            image_draw_point(xi, y, (100,150,255))

    for geyser in geysers:
        tp = geyser.getTilePosition()
        x = tp.x()
        y = tp.y()
        for xi in range(x, x+3):
            for yi in range(y, y+2):
                image_draw_point(xi, yi, (200,150,40))

def reconnect():
    while not client.connect():
        sleep(1)


def colorRegions(regions):
    for region in regions:
        r_id_color[region.getID()] = (0,0,0)

    for region in regions:
        n_colors = [r_id_color[r.getID()] for r in region.getNeighbors()]
        for color in region_colors:
            if color not in n_colors:
                r_id_color[region.getID()] = color
                break
            r_id_color[region.getID()] = (255,100,255)

print("Connecting...")
reconnect()
while True:
    print("waiting to enter match")
    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")
    Broodwar.sendText( "Hello world from python 1!");
    Broodwar.printf( "Hello world from python 2!");

    # need newline to flush buffer
    Broodwar << "The map is " << Broodwar.mapName() << ", a " \
        << len(Broodwar.getStartLocations()) << " player map" << " \n"

    # Enable some cheat flags
    Broodwar.enableFlag(cybw.Flag.UserInput);

    show_regions = True;
    show_visibility_data = False;

    if Broodwar.isReplay():
        Broodwar << "The following players are in this replay:\n"
        players = Broodwar.getPlayers()
        #TODO add rest of replay actions

    else:
        ## test section
        all_regions = Broodwar.getAllRegions()
        rs = "There are {} regions.".format(len(all_regions))
        print(rs)
        Broodwar.printf(rs)
        colorRegions(all_regions)
        Broodwar.sendText("black sheep wall")
        create_image(Broodwar.mapWidth(), Broodwar.mapHeight())
        mapname = Broodwar.mapFileName()
        print(mapname)
        #import ipdb; ipdb.set_trace()
        draw_map("mapimg/{}_bregion.bmp".format(mapname))
        Broodwar.leaveGame()
        ## test section
        Broodwar << "The matchup is " << Broodwar.self().getRace() << " vs " << Broodwar.enemy().getRace() << "\n"
        #send each worker to the mineral field that is closest to it
        units    = Broodwar.self().getUnits();
        minerals  = Broodwar.getMinerals();
        print("got",len(units),"units")
        print("got",len(minerals),"minerals")
        for unit in units:
            if unit.getType().isWorker():
                closestMineral = None
                #print("worker")
                for mineral in minerals:
                    if closestMineral is None or unit.getDistance(mineral) < unit.getDistance(closestMineral):
                        closestMineral = mineral
                if closestMineral:
                    unit.rightClick(closestMineral)
            elif unit.getType().isResourceDepot():
                unit.train(Broodwar.self().getRace().getWorker())
        events = Broodwar.getEvents()
        print(len(events))

    while Broodwar.isInGame():
        events = Broodwar.getEvents()
        for e in events:
            eventtype = e.getType()
            if eventtype == cybw.EventType.SendText:
                if e.getText() == "/show regions":
                    show_regions = not show_regions;
                elif e.getText()=="/show visibility":
                    show_visibility_data = not show_visibility_data
                else:
                    Broodwar.sendText(e.getText())

        Broodwar.drawTextScreen(cybw.Position(300,0),"FPS: " + str(Broodwar.getAverageFPS()))
        client.update()

