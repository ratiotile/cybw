﻿import cybw
from cybw import Unit
from time import sleep
from cybw import Colors, Color
from cybw import Position, TilePosition, WalkPosition
from PIL import Image, ImageDraw

client = cybw.BWAPIClient
Broodwar = cybw.Broodwar

im = None
draw = None

groundlevel_colors = [
    '#CC0000',
    '#CC6600',
    '#336600',
    '#339900',
    '#33CC00',
    '#33FF00'
]
impassable_color = '#000000'

def create_image(x, y):
    global im, draw
    im = Image.new("RGB",(x, y))
    draw = ImageDraw.Draw(im)

def image_draw_point(x, y, color):
    global draw
    draw.point((x, y), color)

def save_image(pathname):
    global draw, im
    del draw
    try:
        im.save(pathname)

    except Exception as e:
        print("error, {}".format(str(e)))

def draw_map(pathname):
    # just do impassable, high/low
    global groundlevel_colors
    for x in range(Broodwar.mapWidth()*4):
        for y in range(Broodwar.mapHeight()*4):
            walk = WalkPosition(x,y)
            if Broodwar.isWalkable(walk):
                height = Broodwar.getGroundHeight(x=x/4, y=y/4)
                c = groundlevel_colors[height]
            else:
                c = impassable_color

            image_draw_point(x, y, c )

    draw_resources()

    save_image(pathname)

def draw_resources():
    geysers = Broodwar.getStaticGeysers()
    minerals = Broodwar.getStaticMinerals()
    for mineral in minerals:
        tp = mineral.getTilePosition()
        wp = WalkPosition(tp)
        x = wp.x()
        y = wp.y()
        for xi in range(x, x+2*4):
            for yi in range(y, y+1*4):
                image_draw_point(xi, yi, (100,150,255))

    for geyser in geysers:
        tp = geyser.getTilePosition()
        wp = WalkPosition(tp)
        x = wp.x()
        y = wp.y()
        for xi in range(x, x+3*4):
            for yi in range(y, y+2*4):
                image_draw_point(xi, yi, (200,150,40))

def reconnect():
    while not client.connect():
        sleep(1)

print("Connecting...")
reconnect()
while True:
    print("waiting to enter match")
    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")

    show_regions = True;
    show_visibility_data = False;

    ## test section
    Broodwar.sendText("black sheep wall")
    create_image(Broodwar.mapWidth()*4, Broodwar.mapHeight()*4)
    mapname = Broodwar.mapFileName()
    print(mapname)
    #import ipdb; ipdb.set_trace()
    draw_map("mapimg/{}_hgw.bmp".format(mapname))
    Broodwar.leaveGame()
    ## test section
    while Broodwar.isInGame():
        client.update()