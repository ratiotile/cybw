﻿import cybw
from cybw import Unit
from time import sleep
from cybw import Colors
from cybw import Position

client = cybw.BWAPIClient
Broodwar = cybw.Broodwar
all_regions = set()
r_id_color = {}
region_colors = [
    Colors.Red,
    Colors.Green,
    Colors.Blue,
    Colors.Orange,
    Colors.White,
    Colors.Purple,
    Colors.Grey,
    Colors.Brown
]


def reconnect():
    while not client.connect():
        sleep(1)


def drawVisibilityData():
    wid = Broodwar.mapWidth()
    hgt = Broodwar.mapHeight()
    for x in range(wid):
        for y in range(hgt):
            drawColor = cybw.Colors.Red
            if Broodwar.isExplored(tileX=x,tileY=y):
                if Broodwar.isVisible(tileX=x,tileY=y):
                    drawColor = cybw.Colors.Green
                else:
                    drawColor = cybw.Colors.Blue

            Broodwar.drawBoxMap(cybw.Position(x*32,y*32),
                                cybw.Position(x*32+31,y*32+31), drawColor)

def drawRegions():
    # assume all regions have colors assigned
    for x in range(Broodwar.mapWidth()):
        for y in range(Broodwar.mapHeight()):
            pos = Position(x*32+16, y*32+16)
            region = Broodwar.getRegionAt(pos)
            c = r_id_color[region.getID()]
            Broodwar.drawBoxMap(cybw.Position(x*32,y*32),
                                cybw.Position(x*32+31,y*32+31), c)


def colorRegions(regions):
    for region in regions:
        r_id_color[region.getID()] = Colors.Black

    for region in regions:
        n_colors = [r_id_color[r.getID()] for r in region.getNeighbors()]
        for color in region_colors:
            if color not in n_colors:
                r_id_color[region.getID()] = color
                break

print("Connecting...")
reconnect()
while True:
    print("waiting to enter match")
    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")
    Broodwar.sendText( "Hello world from python 1!");
    Broodwar.printf( "Hello world from python 2!");

    # need newline to flush buffer
    Broodwar << "The map is " << Broodwar.mapName() << ", a " \
        << len(Broodwar.getStartLocations()) << " player map" << " \n"

    # Enable some cheat flags
    Broodwar.enableFlag(cybw.Flag.UserInput);

    show_regions = True;
    show_visibility_data = False;

    if Broodwar.isReplay():
        Broodwar << "The following players are in this replay:\n"
        players = Broodwar.getPlayers()
        #TODO add rest of replay actions

    else:
        ## test section
        all_regions = Broodwar.getAllRegions()
        rs = "There are {} regions.".format(len(all_regions))
        print(rs)
        Broodwar.printf(rs)
        colorRegions(all_regions)
        Broodwar.sendText("black sheep wall")
        ## test section
        Broodwar << "The matchup is " << Broodwar.self().getRace() << " vs " << Broodwar.enemy().getRace() << "\n"
        #send each worker to the mineral field that is closest to it
        units    = Broodwar.self().getUnits();
        minerals  = Broodwar.getMinerals();
        print("got",len(units),"units")
        print("got",len(minerals),"minerals")
        for unit in units:
            if unit.getType().isWorker():
                closestMineral = None
                #print("worker")
                for mineral in minerals:
                    if closestMineral is None or unit.getDistance(mineral) < unit.getDistance(closestMineral):
                        closestMineral = mineral
                if closestMineral:
                    unit.rightClick(closestMineral)
            elif unit.getType().isResourceDepot():
                unit.train(Broodwar.self().getRace().getWorker())
        events = Broodwar.getEvents()
        print(len(events))

    while Broodwar.isInGame():
        events = Broodwar.getEvents()
        for e in events:
            eventtype = e.getType()
            if eventtype == cybw.EventType.SendText:
                if e.getText() == "/show regions":
                    show_regions = not show_regions;
                elif e.getText()=="/show visibility":
                    show_visibility_data = not show_visibility_data
                else:
                    Broodwar.sendText(e.getText())

        if show_visibility_data:
            drawVisibilityData()
        if show_regions:
            drawRegions()

        Broodwar.drawTextScreen(cybw.Position(300,0),"FPS: " + str(Broodwar.getAverageFPS()))
        client.update()

