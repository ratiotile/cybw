﻿import cybw
from cybw import Unit
from time import sleep
from cybw import Colors, Color
from cybw import Position, TilePosition, WalkPosition
from PIL import Image, ImageDraw

client = cybw.BWAPIClient
Broodwar = cybw.Broodwar

im = None
draw = None

groundlevel_colors = [
    '#CC0000',
    '#CC6600',
    '#336600',
    '#339900',
    '#33CC00',
    '#33FF00'
]
impassable_color = '#000000'

def create_image(x, y):
    global im, draw
    im = Image.new("RGB",(x, y))
    draw = ImageDraw.Draw(im)

def image_draw_point(x, y, color):
    global draw
    draw.point((x, y), color)

def save_image(pathname):
    global draw, im
    del draw
    try:
        im.save(pathname)

    except Exception as e:
        print("error, {}".format(str(e)))

def draw_map(pathname):
    # just do impassable, high/low
    global groundlevel_colors
    for x in range(Broodwar.mapWidth()):
        for y in range(Broodwar.mapHeight()):
            tile = TilePosition(x,y)
            # fudge it a little to get it to center of tile
            #walk = WalkPosition(x*4+2, y*4+2)
            # region gives cleaner result
            pos = Position(x*32+16, y*32+16)
            region = Broodwar.getRegionAt(pos)
            if region.isAccessible():
                height = Broodwar.getGroundHeight(x=x, y=y)
                c = groundlevel_colors[height]
            else:
                c = impassable_color

            image_draw_point(x, y, c )

    draw_resources()

    save_image(pathname)

def draw_resources():
    geysers = Broodwar.getStaticGeysers()
    minerals = Broodwar.getStaticMinerals()
    for mineral in minerals:
        tp = mineral.getTilePosition()
        x = tp.x()
        y = tp.y()
        for xi in range(x, x+2):
            image_draw_point(xi, y, (100,150,255))

    for geyser in geysers:
        tp = geyser.getTilePosition()
        x = tp.x()
        y = tp.y()
        for xi in range(x, x+3):
            for yi in range(y, y+2):
                image_draw_point(xi, yi, (200,150,40))

def reconnect():
    while not client.connect():
        sleep(1)

print("Connecting...")
reconnect()
while True:
    print("waiting to enter match")
    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")

    show_regions = True;
    show_visibility_data = False;

    ## test section
    Broodwar.sendText("black sheep wall")
    create_image(Broodwar.mapWidth(), Broodwar.mapHeight())
    mapname = Broodwar.mapFileName()
    print(mapname)
    #import ipdb; ipdb.set_trace()
    draw_map("mapimg/{}_hgr.bmp".format(mapname))
    Broodwar.leaveGame()
    ## test section
    while Broodwar.isInGame():
        client.update()