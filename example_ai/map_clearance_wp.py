﻿import cybw
from cybw import Unit
from time import sleep
from cybw import Colors, Color
from cybw import Position, TilePosition, WalkPosition
from tabw.TerrainAnalyzer import TerrainAnalyzer




client = cybw.BWAPIClient
Broodwar = cybw.Broodwar

im = None
draw = None





TA = None

def reconnect():
    while not client.connect():
        sleep(0.5)

print("Connecting...")
reconnect()
while True:
    print("waiting to enter match")
    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")

    ## test section
    Broodwar.sendText("black sheep wall")
    print(Broodwar.mapFileName())
    TA = TerrainAnalyzer(Broodwar)
    TA.analyze()
    Broodwar.leaveGame()
    ## test section

    while Broodwar.isInGame():
        client.update()
