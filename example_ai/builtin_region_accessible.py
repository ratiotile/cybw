﻿import cybw
from cybw import Unit
from time import sleep
from cybw import Colors, Color
from cybw import Position
from tabw.Draw import MapImage

client = cybw.BWAPIClient
Broodwar = cybw.Broodwar
all_regions = set()
r_id_color = {}
region_colors = [
    (100,0,0),
    (0,80,0),
    (60,60,60),
    (80,80,80),
    (100,100,100),
    (120,120,120),
    (140,140,140),
]
choke_colors = [
    (255,00,00),  # 2
    (000,255,00), # 3
    (0,0,255),     # 4
    (0,255,255),     # 5
]


def draw_map(pathname):
    # assume all regions have colors assigned
    image = MapImage(8, Broodwar)
    for x in range(Broodwar.mapWidth() * 4):
        for y in range(Broodwar.mapHeight() * 4):
            pos = Position(x*8+4, y*8+4)
            region = Broodwar.getRegionAt(pos)
            if region.isAccessible():
                c = r_id_color[region.getID()]
            else:
                c = (0, 0, 0)
            image.draw_point(x, y, c )

    image.draw_resources()

    image.save_image(pathname)

def reconnect():
    while not client.connect():
        sleep(1)


def colorRegions(regions):
    for region in regions:
        r_id_color[region.getID()] = (0,0,0)

    for region in regions:
        n_colors = [r_id_color[r.getID()] for r in region.getNeighbors()]
        for color in region_colors:
            if color not in n_colors:
                r_id_color[region.getID()] = color
                break
            r_id_color[region.getID()] = (255,100,255)

print("Connecting...")
reconnect()
while True:
    print("waiting to enter match")
    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")
    Broodwar.sendText( "Hello world from python 1!");
    Broodwar.printf( "Hello world from python 2!");

    # need newline to flush buffer
    Broodwar << "The map is " << Broodwar.mapName() << ", a " \
        << len(Broodwar.getStartLocations()) << " player map" << " \n"

    # Enable some cheat flags
    Broodwar.enableFlag(cybw.Flag.UserInput);

    show_regions = True;
    show_visibility_data = False;

    if Broodwar.isReplay():
        Broodwar << "The following players are in this replay:\n"
        players = Broodwar.getPlayers()
        #TODO add rest of replay actions

    else:
        ## test section
        all_regions = Broodwar.getAllRegions()
        rs = "There are {} regions.".format(len(all_regions))
        print(rs)
        Broodwar.printf(rs)
        colorRegions(all_regions)
        Broodwar.sendText("black sheep wall")
        mapname = Broodwar.mapFileName()
        print(mapname)
        draw_map("mapimg/{}_bregion.gif".format(mapname))
        Broodwar.leaveGame()
        ## test section


    while Broodwar.isInGame():
        events = Broodwar.getEvents()
        for e in events:
            eventtype = e.getType()
            if eventtype == cybw.EventType.SendText:
                if e.getText() == "/show regions":
                    show_regions = not show_regions;
                elif e.getText()=="/show visibility":
                    show_visibility_data = not show_visibility_data
                else:
                    Broodwar.sendText(e.getText())

        client.update()

