﻿from cybw import BWAPI
from cybw.BWAPI import Unit
from time import sleep

client = BWAPI.BWAPIClient
Broodwar = BWAPI.Broodwar

def reconnect():
    while not client.connect():
        sleep(1)

print("Connecting...")
reconnect()
while True:
    print("waiting to enter match")
    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")
    Broodwar.printf( "Hello world from python!");
    # onStart section
    # run Position tests

    # Enable some cheat flags
    Broodwar.enableFlag(BWAPI.Flag.UserInput);

    while Broodwar.isInGame():
        events = Broodwar.getEvents()
        for e in events:
            eventtype = e.getType()
            if eventtype == BWAPI.EventType.MatchEnd:
                pass

            elif eventtype == BWAPI.EventType.SendText:
                pass

            elif eventtype == BWAPI.EventType.ReceiveText:
                Broodwar << e.getPlayer().getName() << " said \"" << e.getText() << "\"\n"

            elif eventtype == BWAPI.EventType.PlayerLeft:
                Broodwar << e.getPlayer().getName() << " left the game.\n"

            elif eventtype == BWAPI.EventType.NukeDetect:
                if e.getPosition() is not BWAPI.Positions.Unknown:
                    Broodwae.drawCircleMap(e.getPosition(), 40, BWAPI.Colors.Red, True)
                    Broodwar << "Nuclear Launch Detected at " << e.getPosition() << "\n"
                else:
                    Broodwar << "Nuclear Launch Detected.\n"

            elif eventtype == BWAPI.EventType.UnitCreate:
                if not Broodwar.isReplay():
                    Broodwar << "A " << e.getUnit() << " has been created at " << e.getUnit().getPosition() << "\n"
                else:
                    if e.getUnit().getType().isBuilding() and (e.getUnit().getPlayer().isNeutral() == false):
                        seconds = Broodwar.getFrameCount()/24
                        minutes = seconds/60
                        seconds %= 60
                        Broodwar.sendText(str(minutes)+":"+str(seconds)+": "+e.getUnit().getPlayer().getName()+" creates a "+str(e.getUnit().getType())+"\n")

            elif eventtype == BWAPI.EventType.UnitDestroy:
                if not Broodwar.isReplay():
                    Broodwar << "A " << e.getUnit() << " has been destroyed at " << e.getUnit().getPosition() << "\n"

            elif eventtype == BWAPI.EventType.UnitMorph:
                if not Broodwar.isReplay():
                    Broodwar << "A " << e.getUnit() << " has been morphed at " << e.getUnit().getPosition() << "\n"
                else:
                    #if we are in a replay, then we will print out the build order
                    #(just of the buildings, not the units).
                    if e.getUnit().getType().isBuilding() and not e.getUnit().getPlayer().isNeutral():
                        seconds = Broodwar.getFrameCount()/24
                        minutes = seconds/60
                        seconds %= 60
                        Broodwar << str(minutes) << ":" << str(seconds) << ": " << e.getUnit().getPlayer().getName() << " morphs a " << e.getUnit().getType() << "\n"

            elif eventtype == BWAPI.EventType.UnitShow:
                if not Broodwar.isReplay():
                    Broodwar << e.getUnit() << " spotted at " << e.getUnit().getPosition() << "\n"
            elif eventtype == BWAPI.EventType.UnitHide:
                if not Broodwar.isReplay():
                    Broodwar << e.getUnit() << " was last seen at " << e.getUnit().getPosition() << "\n"
            elif eventtype == BWAPI.EventType.UnitRenegade:
                if not Broodwar.isReplay():
                    Broodwar << e.getUnit() << " is now owned by " << e.getUnit().getPlayer() << "\n"
            elif eventtype == BWAPI.EventType.SaveGame:
                Broodwar << "The game was saved to " << e.getText() << "\n"

        client.update()