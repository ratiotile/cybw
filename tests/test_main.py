import cybw

def test_BWAPI_getRevision():
	rev = cybw.BWAPI_getRevision()
	assert rev == 4708

def test_BWAPI_isDebug():
	assert cybw.BWAPI_isDebug() == False