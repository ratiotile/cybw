from cybw import UpgradeTypes, UnitTypes, Races

def test_getRace():
    assert UpgradeTypes.Terran_Infantry_Weapons.getRace() == Races.Terran

def test_mineralPrice():
    assert UpgradeTypes.Terran_Infantry_Armor.mineralPrice() == 100

def test_mineralPrice_level():
    assert UpgradeTypes.Terran_Infantry_Armor.mineralPrice(2) == 175

def test_mineralPriceFactor():
    assert UpgradeTypes.Terran_Vehicle_Weapons.mineralPriceFactor() == 75

def test_gasPrice():
    assert UpgradeTypes.Terran_Vehicle_Plating.gasPrice() == 100

def test_gasPriceFactor():
    assert UpgradeTypes.Terran_Ship_Weapons.gasPriceFactor() == 50

def test_upgradeTime():
    assert UpgradeTypes.Terran_Ship_Plating.upgradeTime() == 4000

def test_upgradeTimeFactor():
    assert UpgradeTypes.Zerg_Carapace.upgradeTimeFactor() == 480

def test_maxRepeats():
    assert UpgradeTypes.Zerg_Missile_Attacks.maxRepeats() == 3

def test_whatUpgrades():
    assert UpgradeTypes.Zerg_Melee_Attacks.whatUpgrades() == (
        UnitTypes.Zerg_Evolution_Chamber)

def test_whatsRequired():
    assert UpgradeTypes.Terran_Infantry_Armor.whatsRequired(3) == (
        UnitTypes.Terran_Science_Facility)

def test_getName():
    assert UpgradeTypes.Protoss_Plasma_Shields.getName() == (
        "Protoss_Plasma_Shields")

def test_allUpgradeTypes():
    uts = UpgradeTypes.allUpgradeTypes()
    assert len(uts) == 52
    assert UpgradeTypes.U_238_Shells in uts