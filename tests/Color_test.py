from cybw import Colors, Text

def test_TextColors():
	assert Text.Previous 	== 1;
	assert Text.Blue 		== 14;
	assert Text.Turquoise	== 31;

def test_TextSizes():
	assert Text.Size.Small == 0;
	assert Text.Size.Huge == 3;

def test_ConstColors():
	assert Colors.Red.red() == 244;
	assert Colors.Blue.blue() == 204;
	assert Colors.Green.green() == 252;
	assert Colors.White.red() == 240;
	assert Colors.White.blue() == 240;
	assert Colors.White.green() == 240;
	assert Colors.Black.red() == 0;
	assert Colors.Black.blue() == 0;
	assert Colors.Black.green() == 0;