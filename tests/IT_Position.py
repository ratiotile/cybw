﻿from ..cybw import BWAPI
from ..cybw.BWAPI import Position, WalkPosition, TilePosition
import math

def test_inGame():
    assert BWAPI.Broodwar.isInGame() == True

def test_Exists():
	assert hasattr(BWAPI,'Position')
	assert hasattr(BWAPI,'WalkPosition')
	assert hasattr(BWAPI,'TilePosition')