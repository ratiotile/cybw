from cybw import WeaponTypes

def test_damageAmount():
    assert WeaponTypes.Arclite_Shock_Cannon.damageAmount() == 70

def test_damageBonus():
    assert WeaponTypes.Arclite_Shock_Cannon.damageBonus() == 5

def test_damageCooldown():
    assert WeaponTypes.Arclite_Shock_Cannon.damageCooldown() == 75

def test_damageFactor():
    assert WeaponTypes.Arclite_Shock_Cannon.damageFactor() == 1

def test_damageType():
    assert WeaponTypes.Arclite_Shock_Cannon.damageType().getName() == \
           "Explosive"

def test_explosionType():
    assert WeaponTypes.Arclite_Shock_Cannon.explosionType().getName() == \
           "Radial_Splash"

def test_getID():
    assert WeaponTypes.Arclite_Shock_Cannon.getID() == 27

def test_getName():
    assert WeaponTypes.Arclite_Shock_Cannon.getName() == "Arclite_Shock_Cannon"

def test_getTech():
    assert WeaponTypes.Arclite_Shock_Cannon.getTech().getName() == "None"

def test_innerSplashRadius():
    assert WeaponTypes.Arclite_Shock_Cannon.innerSplashRadius() == 10

def test_maxRange():
    assert WeaponTypes.Arclite_Shock_Cannon.maxRange() == 384

def test_medianSplashRadius():
    assert WeaponTypes.Arclite_Shock_Cannon.medianSplashRadius() == 25

def test_minRange():
    assert WeaponTypes.Arclite_Shock_Cannon.minRange() == 64

def test_outerSplashRadius():
    assert WeaponTypes.Arclite_Shock_Cannon.outerSplashRadius() == 40

def test_targetsAir():
    assert WeaponTypes.Suicide_Scourge.targetsAir() == True

def test_targetsMechanical():
    assert WeaponTypes.Lockdown.targetsMechanical() == True

def test_targetsNonBuilding():
    assert WeaponTypes.Consume.targetsOrganic() == True

def test_targetsNonRobotic():
    assert WeaponTypes.Spawn_Broodlings.targetsNonRobotic() == True

def test_targetsOrgOrMech():
    assert WeaponTypes.Spawn_Broodlings.targetsOrgOrMech() == True

def test_targetsOrganic():
    assert WeaponTypes.Arclite_Shock_Cannon.targetsOrganic() == False
    assert WeaponTypes.Consume.targetsOrganic() == True

def test_targetsOwn():
    assert WeaponTypes.Arclite_Shock_Cannon.targetsOwn() == False
    assert WeaponTypes.Consume.targetsOwn() == True

def test_targetsTerrain():
    assert WeaponTypes.EMP_Shockwave.targetsTerrain() == True

def test_upgradeType():
    assert WeaponTypes.Arclite_Shock_Cannon.upgradeType().getName() == \
           "Terran_Vehicle_Weapons"

def test_whatUses():
    assert WeaponTypes.Arclite_Shock_Cannon.whatUses().getName() == \
           "Terran_Siege_Tank_Siege_Mode"