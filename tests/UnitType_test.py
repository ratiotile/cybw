from cybw import UnitTypes, Races

Terran = Races.Terran

def test_getName():
    assert UnitTypes.Terran_Marine.getName() == "Terran_Marine"
    assert str(UnitTypes.Terran_Marine) == "Terran_Marine"


def test_getID():
    assert UnitTypes.Terran_Marine.getID() == 0


def test_getRace():
    assert UnitTypes.Terran_Marine.getRace() == Terran


def test_requiredUnits():
    a = UnitTypes.Zerg_Guardian.requiredUnits()
    b = [x for x in a.keys()]
    assert b[1].getName() == "Zerg_Mutalisk"
    assert b[0].getName() == "Zerg_Greater_Spire"


def test_maxHitPoints():
    assert UnitTypes.Terran_Marine.maxHitPoints() == 40


def test_maxShields():
    assert UnitTypes.Protoss_Zealot.maxShields() == 60


def test_maxEnergy():
    assert UnitTypes.Terran_Science_Vessel.maxEnergy() == 200


def test_armor():
    assert UnitTypes.Terran_Siege_Tank_Tank_Mode.armor() == 1


def test_mineralPrice():
    assert UnitTypes.Terran_SCV.mineralPrice() == 50


def test_gasPrice():
    assert UnitTypes.Terran_Firebat.gasPrice() == 25

def test_allUnitTypes():
    uts = UnitTypes.allUnitTypes()
    assert len(uts) == 207
    assert UnitTypes.Terran_Goliath in uts

def test_researchesWhat():
    assert len(UnitTypes.Terran_Medic.researchesWhat()) == 0
    assert len(UnitTypes.Terran_Engineering_Bay.researchesWhat()) == 0
    assert len(UnitTypes.Terran_Engineering_Bay.upgradesWhat()) == 2
    assert len(UnitTypes.Terran_Academy.researchesWhat()) == 3
    assert len(UnitTypes.Terran_Academy.upgradesWhat()) == 2