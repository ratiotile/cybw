from cybw import TechTypes, Races, WeaponTypes, Orders, TechType

def test_Race():
    assert TechTypes.Stim_Packs.getRace() == Races.Terran

def test_Minerals():
    assert TechTypes.Spider_Mines.mineralPrice() == 100

def test_Gas():
    assert TechTypes.Tank_Siege_Mode.gasPrice() == 150

def test_Time():
    assert TechTypes.Irradiate.researchTime() == 1200

def test_Energy():
    assert TechTypes.Scanner_Sweep.energyCost() == 50

def test_Weapon():
    assert TechTypes.Lockdown.getWeapon() == WeaponTypes.Lockdown

def test_targetsUnit():
    assert TechTypes.Defensive_Matrix.targetsUnit() == True

def test_targetsPosition():
    assert TechTypes.Plague.targetsPosition() == True

def test_Order():
    assert TechTypes.EMP_Shockwave.getOrder() == Orders.CastEMPShockwave

def test_Name():
    assert TechTypes.Yamato_Gun.getName() == "Yamato_Gun"

def test_allTechTypes():
    tts = TechTypes.allTechTypes()
    assert len(tts) == 36
    assert TechTypes.Personnel_Cloaking in tts