﻿import cybw
from cybw import Position, WalkPosition, TilePosition
import math

def test_Exists():
    assert hasattr(cybw, 'Position')
    assert hasattr(cybw, 'WalkPosition')
    assert hasattr(cybw, 'TilePosition')

class TestPosition:
    # Position tests
    def test_properties(self):
        tp = Position(1, 2)
        assert tp.x == 1
        assert tp.y == 2

    def test_length(self):
        tp = Position(1, 2)
        assert tp.getLength() == math.sqrt(math.pow(tp.x, 2)+math.pow(tp.y, 2))

    def test_isValid(self):
        pos = Position(1, 1)
        assert pos.isValid() == True
        invalidPos = Position(-100, -100)
        # need to test in-game since map is not defined

    def test_position_distance(self):
        p1 = Position(0, 0)
        p2 = Position(10, 0)
        assert p1.getDistance(p2) == 10
        assert p1.getApproxDistance(p2) == 10

    def test_Position_fromTile_conversion(self):
        p1 = TilePosition(1, 2)
        p2 = Position(p1)
        assert p2.x == 32
        assert p2.y == 64

    def test_Position_fromWalk_conversion(self):
        p1 = WalkPosition(1, 2)
        p2 = Position(p1)
        assert p2.x == 8
        assert p2.y == 16

    def test_Position_fromPos_conversion(self):
        p1 = Position(32, 64)
        p2 = Position(p1)
        assert p2.x == 32
        assert p2.y == 64

    def test_division(self):
        p1 = Position(10, 10)
        p2 = p1 / 2
        assert p2.x == 5
        assert p2.y == 5
        p1 = Position(11, 11)
        p2 = p1 / 2
        assert p2.x == 5
        assert p2.y == 5


class TestWalkPosition:
    # WalkPosition tests
    def test_walkPositionProperties(self):
        tw = WalkPosition(3, 4)
        assert tw.x == 3
        assert tw.y == 4

    def test_walkPosition_length(self):
        tw = WalkPosition(1, 2)
        assert tw.getLength() == math.sqrt(math.pow(tw.x, 2)+math.pow(tw.y, 2))

    def test_walkPosition_isValid(self):
        pos = WalkPosition(1, 1)
        assert pos.isValid() == True
        invalidPos = WalkPosition(-1, -1)
        #assert invalidPos.isValid() == False

    def test_walkPosition_distance(self):
        p1 = WalkPosition(0, 0)
        p2 = WalkPosition(10, 0)
        assert p1.getDistance(p2) == 10
        assert p1.getApproxDistance(p2) == 10

    def test_walkPosition_fromTile_conversion(self):
        p1 = TilePosition(1, 2)
        p2 = WalkPosition(p1)
        assert p2.x == 4
        assert p2.y == 8

    def test_walkPosition_fromWalk_conversion(self):
        p1 = WalkPosition(1, 2)
        p2 = WalkPosition(p1)
        assert p2.x == 1
        assert p2.y == 2

    def test_walkPosition_fromPos_conversion(self):
        p1 = Position(32, 16)
        p2 = WalkPosition(p1)
        assert p2.x == 4
        assert p2.y == 2

    def test_addition(self):
        p1 = WalkPosition(1, 2)
        assert p1.x == 1
        assert p1.y ==2
        p2 = WalkPosition(10, 10)
        p3 = p1 + p2
        assert p3.x == 11
        assert p3.y == 12

    def test_division(self):
        p1 = WalkPosition(10, 10)
        p2 = p1 / 2
        assert p2.x == 5
        assert p2.y == 5


# TilePosition tests
class TestTilePosition:

    def test_tilePositionProperties(self):
        tt = TilePosition(5, 6)
        assert tt.x == 5
        assert tt.y == 6

    def test_tilePosition_length(self):
        tt = TilePosition(5, 6)
        assert tt.getLength() == math.sqrt(math.pow(tt.x, 2)+math.pow(tt.y, 2))

    def test_tilePosition_isValid(self):
        pos = WalkPosition(1, 1)
        assert pos.isValid() == True
        invalidPos = WalkPosition(-1, -1)
        #assert invalidPos.isValid() == False

    def test_tilePosition_distance(self):
        p1 = TilePosition(0, 0)
        p2 = TilePosition(10, 0)
        assert p1.getDistance(p2) == 10
        assert p1.getApproxDistance(p2) == 10

    def test_TilePosition_fromTile_conversion(self):
        p1 = TilePosition(1, 2)
        p2 = TilePosition(p1)
        assert p2.x == 1
        assert p2.y == 2

    def test_TilePosition_fromWalk_conversion(self):
        p1 = WalkPosition(32, 16)
        p2 = TilePosition(p1)
        assert p2.x == 8
        assert p2.y == 4

    def test_TilePosition_fromPos_conversion(self):
        p1 = Position(32, 64)
        p2 = TilePosition(p1)
        assert p2.x == 1
        assert p2.y == 2

    def test_addition(self):
        p1 = TilePosition(1, 2)
        p2 = TilePosition(10, 10)
        p3 = p1 + p2
        assert p3.x == 11
        assert p3.y == 12

    def test_division(self):
        pt = TilePosition(4, 4)
        p2 = pt / 2
        assert p2.x == 2
        assert p2.y == 2

