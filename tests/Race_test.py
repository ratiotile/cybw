import cybw
from cybw import Race, Races, UnitTypes

def test_Exists():
    assert hasattr(cybw, 'Race')
    assert hasattr(cybw, 'Races')
    assert hasattr(cybw, 'UnitTypes')

def test_Names():
    assert Races.Terran.getName() == "Terran"
    assert Races.Zerg.getName() == "Zerg"
    assert Races.Protoss.getName() == "Protoss"

def test_Center():
    assert Races.Terran.getCenter() == UnitTypes.Terran_Command_Center
    assert Races.Zerg.getCenter() == UnitTypes.Zerg_Hatchery
    assert Races.Protoss.getCenter() == UnitTypes.Protoss_Nexus

def test_Refinery():
    assert Races.Terran.getRefinery() == UnitTypes.Terran_Refinery

def test_SupplyProvider():
    assert Races.Terran.getSupplyProvider() == UnitTypes.Terran_Supply_Depot

def test_Transport():
    assert Races.Terran.getTransport() == UnitTypes.Terran_Dropship

def test_Worker():
    assert Races.Terran.getWorker() == UnitTypes.Terran_SCV

def test_comparison():
    assert Races.Terran > Races.Zerg
    assert Races.Protoss > Races.Terran
    assert Races.Zerg == Races.Zerg
    assert Races.Zerg < Races.Protoss

def test_allRaces():
    all_races = Races.allRaces()
    assert len(all_races) > 3

