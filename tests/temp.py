﻿def pig_latin(data): 
	words = data.split() 
	piglatin = [] 
	vowels = ["a", "i", "e", "u", "o", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

	for word in words:
		if word[0] in vowels:
			word = word + "way"
		else:
			word = word.replace(word[0],"") + word[0] + "ay"
			word = word.lower()
		piglatin.append(word)

	piglatin = " ".join(piglatin)
	return piglatin