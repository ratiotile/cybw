cybw 0.7.1 beta
For BWAPI 4.1.2 r4708

##Description##
CyBW is a Python language wrapper for BWAPI implemented in Cython.
Most of the BWAPI functions use the exact same interface,
so read the [BWAPI documentation.](http://bwapi.github.io/)

BWAPI, an interface for building AI in Starcraft:
https://github.com/bwapi/bwapi

Cython, interface between C/C++ and Python.
http://cython.org/

For more information on Starcraft AI, visit:
http://www.starcraftai.com/

IRC: ratiotile on `irc.freenode.net` channel `#BWAPI`

----------------------------------

### == Changes ==###
#####0.7.1 beta#####
- Unit.getTrainingQueue() works again
- Return None instead of raising NullPointerException on null Units

#####0.7#####
- Now works with official Python 3.5 distribution, with custom built bwapi 
static libraries using v140
- implemented Races.allRaces(), WeaponTypes, UnitSizeTypes, DamageTypes, 
ExplosionType
- fixed bug in Races
- Region: is now hashable, equality operator
- Game.drawLineMap now takes overloaded parameters

#####0.6.2#####
- Fixed allTypes() for Tech, Upgrades, UnitTypes
- Added missing `__hash__` to Player
- implemented UnitType.upgradesWhat, .researchesWhat.
- cleaned up implementation of SetContainer

#####0.6.1#####
- Fixed error on division in Position, TilePosition, and WalkPosition.
- updated to target BWAPI 4.1.2

#####0.6#####
- BulletSet reimplemented
- UnitType has better __repr__ output
- reimplemented setTextSize
- implemented more functions in Unit
- implemented most functions in Player
- implemented __str__ for TilePosition and WalkPosition
- fixed a bug that broke Unit.getDistance to other Units
- attempted to increase threaded performace by declaring nogil on some functions

#####0.5#####
- throws NullPointerException instead of crashing Starcraft on null Units, Players, Bullets, and Regions.
- Players are now comparable.

------------------------------------

###Requirements###
- BWAPI v4.1.2 installed
- Python 3.5 32-bit [standard distribution](https://www.python.org/downloads/)
- Python 3.4.3 (vs2013, x86) [in the downloads section](https://bitbucket.org/ratiotile/cybw/downloads).

###Setup - Python 3.5 (windows)###
1. download the binary cybw wheel
2. pip install cybw-version.whl

###Setup - Python 3.4###
[video] (https://youtu.be/eMg120b_ny4)

1. Extract Python34vs2013 to C:\python34vs2013 for example.

2. make cybw directory below BWAPI_4.1.2
    `BWAPI_4.1.2\cybw`

3. make venv directory inside cybw
    `BWAPI_4.1.2\cybw\venv\`

4. start cmd shell and navigate into `cybw/venv/`

5. create the virtualenv:
    `virtualenv cybw --system-site-packages --always-copy -p C:\python34vs2013\python.exe`

6. this adds all the system packages, so run virtualenv again to remove them:
    `virtualenv cybw --always-copy -p C:\python34vs2013\python.exe`

7. activate the virtualenv - you should see (cybw) in your shell
    `venv\cybw\Scripts\activate`

8. go back to top level cybw folder. Install the cybw package from wheel.
    `pip install cybw_for_BWAPI_4.1.0-0.4-cp33-none-win32.whl`

9. go into venv\cybw\ and move the example_ai directory up into the main -
the python wheel install puts it here for some reason.
    `cybw\venv\cybw\example_ai -> cybw\example_ai`

###Troubleshooting###
In case of a DistributionNotFound error launching the virtualenv, it can be resolved by reinstalling pip via get-pip.py, then reinstalling all the required packages.

###To Run###
- Set BWAPI to client mode - make sure `ai=` line in bwapi.ini is commented out
- run example client with: `python example_ai\example.py`
    assuming that you are cybw and setup as so: `cybw\example_ai\example.py`

- start Starcraft using ChaosLauncher injecting BWAPI release mode.

------------------------------------------------------------------------------
####For Developers####

###Additional Requirements###
- Cython 0.23.4+
- BWAPI [libraries compiled with v140](https://bitbucket.org/ratiotile/cybw/downloads) (MSVC 2015)

###To Build###
- python setup.py build_ext
- python setup.py bdist_wheel (to build release)

###Programming Bots###
- Check out the .pyx files to see what functions of BWAPI have been implemented. There is still a lot missing, but the basic functionality is there.

###Development Setup###
- install all of requirements.txt in venv folder. ipdb and ipython are tools to aid in debugging. Wheel and setuptools are for distribution and packaging.

