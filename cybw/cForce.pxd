﻿from libcpp.string cimport string
from cybw.cInterface cimport Interface

cdef extern from "BWAPI/Force.h" namespace "BWAPI":
    cdef cppclass Playerset:
        pass

    ctypedef ForceInterface *Force

    cdef cppclass ForceInterface(Interface[Force]):
        int getID() const
        string getName() const
        Playerset getPlayers() const