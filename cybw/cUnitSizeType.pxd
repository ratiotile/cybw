﻿from cybw.cType cimport Type


cdef extern from "BWAPI/UnitSizeType.h" namespace "BWAPI":
    cdef cppclass UnitSizeType(Type[UnitSizeType,int]):
        UnitSizeType()
        UnitSizeType(int id)
        
    cdef cppclass UnitSizeTypeCSet "UnitSizeType::const_set":
        pass

cdef extern from "BWAPI/UnitSizeType.h" namespace "BWAPI::UnitSizeType":
    cdef cppclass set:
        cppclass iterator:
            UnitSizeType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)
        iterator begin()
        iterator end()

        cppclass const_iterator:
            UnitSizeType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)
        const_iterator cbegin()
        const_iterator cend()


cdef extern from "BWAPI/UnitSizeType.h" namespace "BWAPI::UnitSizeTypes":
    const set& allUnitSizeTypes()
    
    const UnitSizeType Independent
    const UnitSizeType Small
    const UnitSizeType Medium
    const UnitSizeType Large
    const UnitSizeType None
    const UnitSizeType Unknown