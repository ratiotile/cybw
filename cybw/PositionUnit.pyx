﻿cimport cybw.cPositionUnit as cPositionUnit

cdef class PositionUnitConverter:
    cdef cPositionUnit.PositionOrUnit getPositionOrUnit(self):
        raise NotImplemented("This method MUST be overridden!")

cdef class PositionOrUnit(PositionUnitConverter):

    def __init__(self):
        pass

    cdef cPositionUnit.PositionOrUnit getPositionOrUnit(self):
        return self.thisobj

    def isUnit(self):
        return self.isUnit()

    def isPosition(self):
        return self.isPosition()

    def getUnit(self):
        return UnitFactory(self.thisobj.getUnit())

    def getPosition(self):
        return PositionFactory(self.thisobj.getPosition())


cdef PositionOrUnit PositionOrUnitFactory(cPositionUnit.PositionOrUnit cPoU):
    pou = PositionOrUnit()
    pou.thisobj = cPoU
    return pou