from libcpp cimport bool
from libcpp.string cimport string
from cython.operator cimport preincrement as inc, dereference as deref

class NullPointerException(Exception):
    def __init__(self, value):
        self.value = "BWAPI returned nullptr for {}.".format(value)
    def __str__(self):
        return repr(self.value)

#from cybw.UnitType import UnitTypes

include "PositionUnit.pyx"
include "Position.pyx"
include "UnitType.pyx"
include "Unit.pyx"
include "Region.pyx"
include "Regionset.pyx"
include "Bullet.pyx"
include "Bulletset.pyx"
include "Player.pyx"
include "Playerset.pyx"
include "Game.pxi"
include "Event.pyx"
include "Client/Client.pxi"

include "utility.pxi"
