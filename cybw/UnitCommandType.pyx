﻿cimport cybw.cUnitCommandType as cUnitCommandType

cdef class UnitCommandType:
    def __init__(self):
        pass

    #def __str__(self):
    #   return self.getName()

    #def getName(self):
    #    name = self.thisobj.getName()
    #    pystr = name.decode()
    #    return pystr

cdef UnitCommandTypeFactory(cUnitCommandType.UnitCommandType ct):
    cdef UnitCommandType commandtype = UnitCommandType()
    commandtype.thisobj = ct
    return commandtype