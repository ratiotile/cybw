﻿from cybw.cSetContainer cimport SetContainer
from cybw.cPosition cimport Position




cdef extern from "BWAPI/Regionset.h" namespace "BWAPI":
    # forward declarations
    cdef cppclass RegionInterface:
        pass
    ctypedef  RegionInterface *Region

    cdef cppclass Unitset:
        pass

    #ctypedef std_hash "std::hash<void*>"

    cdef cppclass Regionset(SetContainer[Region]):

        Position getCenter() const

        #Unitset getUnits(const UnitFilter &pred) const
        Unitset getUnits() const

        cppclass iterator:
            Region operator*()  # removed the Region& to get it to work
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)