﻿cdef extern from "BWAPI/EventType.h" namespace "BWAPI::EventType":
    cdef enum Enum:
        MatchStart,
        MatchEnd,
        MatchFrame,
        MenuFrame,
        SendText,
        ReceiveText,
        PlayerLeft,
        NukeDetect,
        UnitDiscover,
        UnitEvade,
        UnitShow,
        UnitHide,
        UnitCreate,
        UnitDestroy,
        UnitMorph,
        UnitRenegade,
        SaveGame,
        UnitComplete,
        None