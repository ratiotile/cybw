﻿from cybw.cUnit cimport Unit
from cybw.cPlayer cimport Player
from cybw.cPosition cimport Position
from libcpp cimport bool

cdef extern from "<string>" namespace "std":
    ctypedef char* stdstringr "const string"
    cdef cppclass string:
        void string()
        char *c_str()



cdef extern from "BWAPI/Event.h" namespace "BWAPI":
    cdef cppclass PlayerInterface:
        pass

    ctypedef PlayerInterface *Player

    cdef cppclass EventTypeEnum "EventType::Enum":
        pass



    cdef cppclass Event:
        Event()
        Event(const Event& other)
        Event(Event&& other)

        Event& opAssign "operator=" (const Event& other)
        Event& opAssignMove "operator=" (Event &&other)
        bool operator==(const Event& other) const

        #int is EventYype::Enum
        int getType() const

        Position getPosition() const
        const string& getText() const
        Unit getUnit() const
        Player getPlayer() const
        bool isWinner() const

        Event& setType(EventTypeEnum type)
        Event& setPosition(Position position)
        Event& setText(const char* text)
        Event& setUnit(Unit unit)
        Event& setPlayer(Player player)
        Event& setWinner(bool isWinner)

cdef extern from "BWAPI/Event.h" namespace "BWAPI::Event":
    #methods of Event
    Event MatchStart()
    Event MatchEnd(bool isWinner)
    Event MatchFrame()
    Event MenuFrame()
    Event SendText(const char* text)
    Event SendText() #fake for default value
    Event ReceiveText(Player player, const char* text)
    Event ReceiveText(Player player) #fake for default value
    Event PlayerLeft(Player player)
    Event NukeDetect(Position target)
    Event UnitDiscover(Unit unit)
    Event UnitEvade(Unit unit)
    Event UnitShow(Unit unit)
    Event UnitHide(Unit unit)
    Event UnitCreate(Unit unit)
    Event UnitDestroy(Unit unit)
    Event UnitMorph(Unit unit)
    Event UnitRenegade(Unit unit)
    Event SaveGame(const char* gameName)
    Event SaveGame() #fake for default value
    Event UnitComplete(Unit unit)