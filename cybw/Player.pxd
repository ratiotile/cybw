﻿from cybw cimport cPlayer

cdef class Player:
    cdef cPlayer.Player thisptr

cdef PlayerFactory(cPlayer.Player pointer)

cdef const_PlayerFactory(const cPlayer.Player pointer)