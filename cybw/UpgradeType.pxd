﻿cimport cybw.cUpgradeType as cUpgradeType
from .cSetContainer cimport SetContainer

cdef class UpgradeType:
    cdef cUpgradeType.UpgradeType thisobj


cdef UpgradeTypeFactory(cUpgradeType.UpgradeType ctt)

cdef UpgradeTypeSetFactory(const SetContainer[cUpgradeType.UpgradeType]& cset)
