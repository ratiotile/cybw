﻿cdef extern from "BWAPI/Filters.h" namespace "BWAPI":
    cdef cppclass PlayerInterface:
        pass
        
    ctypedef PlayerInterface *Player
    
    cdef cppclass UnitType:
        pass
        
    cdef cppclass UnitInterface:
        pass
    
    ctypedef UnitInterface *Unit
    
    cdef cppclass UnitFilter:
        pass
        
    cdef cppclass PtrUnitFilter:
        pass
        
    cdef cppclass BestUnitFilter:
        pass
        
    cdef cppclass PtrIntCompareUnitFilter:
        pass
        
    cdef cppclass DoubleCompareFilter "CompareFilter<Unit ,double,double (*)(Unit )>":
        pass
        
    cdef cppclass UpgradeCompareFilter "CompareFilter<Unit ,UpgradeType,UpgradeType (*)(Unit )>":
        pass
    
    cdef cppclass SizeCompareFilter "CompareFilter<Unit ,UnitSizeType,UnitSizeType (*)(Unit )>":
        pass
        
    cdef cppclass WeaponCompareFilter "CompareFilter<Unit ,WeaponType,WeaponType (*)(Unit )>":
        pass
    
    cdef cppclass UnitTypeCompareFilter "CompareFilter<Unit ,UnitType,UnitType (*)(Unit )>":
        pass
        
    cdef cppclass RaceCompareFilter "CompareFilter<Unit ,Race,Race (*)(Unit )>":
        pass
        
    cdef cppclass PlayerCompareFilter "CompareFilter<Unit ,Player,Player (*)(Unit )>":
        pass
        
    cdef cppclass UnitCompareFilter "CompareFilter<Unit ,Unit ,Unit (*)(Unit )>":
        pass
    
    cdef cppclass OrderCompareFilter "CompareFilter<Unit ,Order,Order (*)(Unit )>":
        pass
    
cdef extern from "BWAPI/Filters.h" namespace "BWAPI::Filter":
    const PtrUnitFilter IsTransport
    const PtrUnitFilter CanProduce
    const PtrUnitFilter CanAttack
    const PtrUnitFilter CanMove
    const PtrUnitFilter IsFlyer
    const PtrUnitFilter IsFlying
    const PtrUnitFilter RegeneratesHP
    const PtrUnitFilter IsSpellcaster
    const PtrUnitFilter HasPermanentCloak
    const PtrUnitFilter IsOrganic
    const PtrUnitFilter IsMechanical
    const PtrUnitFilter IsRobotic
    const PtrUnitFilter IsDetector
    const PtrUnitFilter IsResourceContainer
    const PtrUnitFilter IsResourceDepot
    const PtrUnitFilter IsRefinery
    const PtrUnitFilter IsWorker
    const PtrUnitFilter RequiresPsi
    const PtrUnitFilter RequiresCreep
    const PtrUnitFilter IsBurrowable
    const PtrUnitFilter IsCloakable
    const PtrUnitFilter IsBuilding
    const PtrUnitFilter IsAddon
    const PtrUnitFilter IsFlyingBuilding
    const PtrUnitFilter IsNeutral
    const PtrUnitFilter IsHero
    const PtrUnitFilter IsPowerup
    const PtrUnitFilter IsBeacon
    const PtrUnitFilter IsFlagBeacon
    const PtrUnitFilter IsSpecialBuilding
    const PtrUnitFilter IsSpell
    const PtrUnitFilter ProducesLarva
    const PtrUnitFilter IsMineralField
    const PtrUnitFilter IsCritter
    const PtrUnitFilter CanBuildAddon
  
    const PtrIntCompareUnitFilter HP
    const PtrIntCompareUnitFilter MaxHP
    const PtrIntCompareUnitFilter HP_Percent
  
    const PtrIntCompareUnitFilter Shields
    const PtrIntCompareUnitFilter MaxShields
    const PtrIntCompareUnitFilter Shields_Percent
  
    const PtrIntCompareUnitFilter Energy
    const PtrIntCompareUnitFilter MaxEnergy
    const PtrIntCompareUnitFilter Energy_Percent

    const PtrIntCompareUnitFilter Armor
    const UpgradeCompareFilter ArmorUpgrade

    const PtrIntCompareUnitFilter MineralPrice
    const PtrIntCompareUnitFilter GasPrice
    const PtrIntCompareUnitFilter BuildTime

    const PtrIntCompareUnitFilter SupplyRequired
    const PtrIntCompareUnitFilter SupplyProvided

    const PtrIntCompareUnitFilter SpaceRequired
    const PtrIntCompareUnitFilter SpaceRemaining
    const PtrIntCompareUnitFilter SpaceProvided

    const PtrIntCompareUnitFilter BuildScore
    const PtrIntCompareUnitFilter DestroyScore

    const DoubleCompareFilter TopSpeed
    const PtrIntCompareUnitFilter SightRange
    const PtrIntCompareUnitFilter WeaponCooldown
    const SizeCompareFilter SizeType

    const WeaponCompareFilter GroundWeapon
    const WeaponCompareFilter AirWeapon

    const UnitTypeCompareFilter GetType
    const RaceCompareFilter GetRace
    const PlayerCompareFilter GetPlayer
  
    const PtrIntCompareUnitFilter Resources
    const PtrIntCompareUnitFilter ResourceGroup
    const PtrIntCompareUnitFilter AcidSporeCount
    const PtrIntCompareUnitFilter InterceptorCount
    const PtrIntCompareUnitFilter ScarabCount
    const PtrIntCompareUnitFilter SpiderMineCount
    const PtrIntCompareUnitFilter MaxWeaponCooldown
    const PtrIntCompareUnitFilter SpellCooldown

    const PtrIntCompareUnitFilter DefenseMatrixPoints
    const PtrIntCompareUnitFilter DefenseMatrixTime
    const PtrIntCompareUnitFilter EnsnareTime
    const PtrIntCompareUnitFilter IrradiateTime
    const PtrIntCompareUnitFilter LockdownTime
    const PtrIntCompareUnitFilter MaelstromTime
    const PtrIntCompareUnitFilter OrderTime
    const PtrIntCompareUnitFilter PlagueTimer
    const PtrIntCompareUnitFilter RemoveTime
    const PtrIntCompareUnitFilter StasisTime
    const PtrIntCompareUnitFilter StimTime
    const UnitTypeCompareFilter BuildType
    const PtrIntCompareUnitFilter RemainingBuildTime
    const PtrIntCompareUnitFilter RemainingTrainTime
    const UnitCompareFilter Target
    const OrderCompareFilter CurrentOrder
    const OrderCompareFilter SecondaryOrder
    const UnitCompareFilter OrderTarget
    const PtrIntCompareUnitFilter GetLeft
    const PtrIntCompareUnitFilter GetTop
    const PtrIntCompareUnitFilter GetRight
    const PtrIntCompareUnitFilter GetBottom

    const PtrUnitFilter Exists
    const PtrUnitFilter IsAttacking
    const PtrUnitFilter IsBeingConstructed
    const PtrUnitFilter IsBeingGathered
    const PtrUnitFilter IsBeingHealed
    const PtrUnitFilter IsBlind
    const PtrUnitFilter IsBraking
    const PtrUnitFilter IsBurrowed
    const PtrUnitFilter IsCarryingGas
    const PtrUnitFilter IsCarryingMinerals
    const PtrUnitFilter IsCarryingSomething
    const PtrUnitFilter IsCloaked
    const PtrUnitFilter IsCompleted
    const PtrUnitFilter IsConstructing
    const PtrUnitFilter IsDefenseMatrixed
    const PtrUnitFilter IsDetected
    const PtrUnitFilter IsEnsnared
    const PtrUnitFilter IsFollowing
    const PtrUnitFilter IsGatheringGas
    const PtrUnitFilter IsGatheringMinerals
    const PtrUnitFilter IsHallucination
    const PtrUnitFilter IsHoldingPosition
    const PtrUnitFilter IsIdle
    const PtrUnitFilter IsInterruptible
    const PtrUnitFilter IsInvincible
    const PtrUnitFilter IsIrradiated
    const PtrUnitFilter IsLifted
    const PtrUnitFilter IsLoaded
    const PtrUnitFilter IsLockedDown
    const PtrUnitFilter IsMaelstrommed
    const PtrUnitFilter IsMorphing
    const PtrUnitFilter IsMoving
    const PtrUnitFilter IsParasited
    const PtrUnitFilter IsPatrolling
    const PtrUnitFilter IsPlagued
    const PtrUnitFilter IsRepairing
    const PtrUnitFilter IsResearching
    const PtrUnitFilter IsSieged
    const PtrUnitFilter IsStartingAttack
    const PtrUnitFilter IsStasised
    const PtrUnitFilter IsStimmed
    const PtrUnitFilter IsStuck
    const PtrUnitFilter IsTraining
    const PtrUnitFilter IsUnderAttack
    const PtrUnitFilter IsUnderDarkSwarm
    const PtrUnitFilter IsUnderDisruptionWeb
    const PtrUnitFilter IsUnderStorm
    const PtrUnitFilter IsPowered
    const PtrUnitFilter IsVisible
  
    const PtrUnitFilter IsEnemy
    const PtrUnitFilter IsAlly
    const PtrUnitFilter IsOwned