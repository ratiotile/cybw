﻿from cybw cimport cWeaponType
from cybw cimport cWeaponTypesEnum
from cybw.DamageType cimport DamageType
from cybw.TechType cimport TechTypeFactory
from cybw.UpgradeType cimport UpgradeTypeFactory
from cybw.GameClient cimport UnitTypeFactory
from cybw.ExplosionType cimport ExplosionType

cdef class WeaponType:
    def __init__(self, id = -1):
        if id >= 0 and id < cWeaponTypesEnum.MAX:
            self.thisobj = cWeaponType.WeaponType(id)
        else:
            self.thisobj = cWeaponType.Unknown

    def __str__(self):
        return self.getName()

    def __richcmp__(x, y, int op):
        if op == 0:     # lt
            return x.getID() < y.getID()
        elif op == 2:   # eq
            return x.getID() == y.getID()
        elif op == 4:   # gt
            return x.getID() > y.getID()
        elif op == 1:   # lte
            return x.getID() <= y.getID()
        elif op == 3:   # ne
            return x.getID() != y.getID()
        elif op == 5:   # gte
            return x.getID() >= y.getID()
        return False

    def __hash__(self):
        return self.getID()

    def getName(self):
        name = self.thisobj.getName()
        pystr = name.decode()
        return pystr

    def getID(self):
        return self.thisobj.getID()

    def damageAmount(self):
        return self.thisobj.damageAmount()

    def damageBonus(self):
        return self.thisobj.damageBonus()

    def damageCooldown(self):
        return self.thisobj.damageCooldown()

    def damageFactor(self):
        return self.thisobj.damageFactor()

    def damageType(self):
        return DamageType.create(self.thisobj.damageType())

    def explosionType(self):
        return ExplosionType.create(self.thisobj.explosionType())

    def getTech(self):
        return TechTypeFactory(self.thisobj.getTech())

    def innerSplashRadius(self):
        return self.thisobj.innerSplashRadius()

    def medianSplashRadius(self):
        return self.thisobj.medianSplashRadius()

    def outerSplashRadius(self):
        return self.thisobj.outerSplashRadius()

    def minRange(self):
        return self.thisobj.minRange()

    def maxRange(self):
        return self.thisobj.maxRange()

    def targetsAir(self):
        return self.thisobj.targetsAir()

    def targetsMechanical(self):
        return self.thisobj.targetsMechanical()

    def targetsOrganic(self):
        return self.thisobj.targetsOrganic()

    def targetsNonBuilding(self):
        return self.thisobj.targetsNonBuilding()

    def targetsNonRobotic(self):
        return self.thisobj.targetsNonRobotic()

    def targetsTerrain(self):
        return self.thisobj.targetsTerrain()

    def targetsOrgOrMech(self):
        return self.thisobj.targetsOrgOrMech()

    def targetsOwn(self):
        return self.thisobj.targetsOwn()

    def upgradeType(self):
        return UpgradeTypeFactory(self.thisobj.upgradeType())

    def whatUses(self):
        return UnitTypeFactory(self.thisobj.whatUses())


cdef WeaponTypeFactory(cWeaponType.WeaponType cweap):
    weaptype = WeaponType()
    weaptype.thisobj = cweap
    return weaptype

cdef class WeaponTypeSet(set):
    def __init__(self):
        super().__init__()

cdef ConstWeaponTypeSetFactory(const cWeaponType.set &cset):
    wt_set = WeaponTypeSet()
    wt_set.thisobj = cset
    for i in cset:
        wt_set.add(WeaponTypeFactory(i))
    return wt_set

cdef class WeaponTypes:
    @staticmethod
    def allWeaponTypes():
        return ConstWeaponTypeSetFactory(cWeaponType.allWeaponTypes())

    Gauss_Rifle = WeaponType(cWeaponTypesEnum.Gauss_Rifle)
    Gauss_Rifle_Jim_Raynor = WeaponType(cWeaponTypesEnum.Gauss_Rifle_Jim_Raynor)
    C_10_Canister_Rifle = WeaponType(cWeaponTypesEnum.C_10_Canister_Rifle)
    C_10_Canister_Rifle_Sarah_Kerrigan = WeaponType(cWeaponTypesEnum.C_10_Canister_Rifle_Sarah_Kerrigan)
    C_10_Canister_Rifle_Samir_Duran = WeaponType(cWeaponTypesEnum.C_10_Canister_Rifle_Samir_Duran)
    C_10_Canister_Rifle_Infested_Duran = WeaponType(cWeaponTypesEnum.C_10_Canister_Rifle_Infested_Duran)
    C_10_Canister_Rifle_Alexei_Stukov = WeaponType(cWeaponTypesEnum.C_10_Canister_Rifle_Alexei_Stukov)
    Fragmentation_Grenade = WeaponType(cWeaponTypesEnum.Fragmentation_Grenade)
    Fragmentation_Grenade_Jim_Raynor = WeaponType(cWeaponTypesEnum.Fragmentation_Grenade_Jim_Raynor)
    Spider_Mines = WeaponType(cWeaponTypesEnum.Spider_Mines)
    Twin_Autocannons = WeaponType(cWeaponTypesEnum.Twin_Autocannons)
    Twin_Autocannons_Alan_Schezar = WeaponType(cWeaponTypesEnum.Twin_Autocannons_Alan_Schezar)
    Hellfire_Missile_Pack = WeaponType(cWeaponTypesEnum.Hellfire_Missile_Pack)
    Hellfire_Missile_Pack_Alan_Schezar = WeaponType(cWeaponTypesEnum.Hellfire_Missile_Pack_Alan_Schezar)
    Arclite_Cannon = WeaponType(cWeaponTypesEnum.Arclite_Cannon)
    Arclite_Cannon_Edmund_Duke = WeaponType(cWeaponTypesEnum.Arclite_Cannon_Edmund_Duke)
    Fusion_Cutter = WeaponType(cWeaponTypesEnum.Fusion_Cutter)
    Gemini_Missiles = WeaponType(cWeaponTypesEnum.Gemini_Missiles)
    Gemini_Missiles_Tom_Kazansky = WeaponType(cWeaponTypesEnum.Gemini_Missiles_Tom_Kazansky)
    Burst_Lasers = WeaponType(cWeaponTypesEnum.Burst_Lasers)
    Burst_Lasers_Tom_Kazansky = WeaponType(cWeaponTypesEnum.Burst_Lasers_Tom_Kazansky)
    ATS_Laser_Battery = WeaponType(cWeaponTypesEnum.ATS_Laser_Battery)
    ATS_Laser_Battery_Hero = WeaponType(cWeaponTypesEnum.ATS_Laser_Battery_Hero)
    ATS_Laser_Battery_Hyperion = WeaponType(cWeaponTypesEnum.ATS_Laser_Battery_Hyperion)
    ATA_Laser_Battery = WeaponType(cWeaponTypesEnum.ATA_Laser_Battery)
    ATA_Laser_Battery_Hero = WeaponType(cWeaponTypesEnum.ATA_Laser_Battery_Hero)
    ATA_Laser_Battery_Hyperion = WeaponType(cWeaponTypesEnum.ATA_Laser_Battery_Hyperion)
    Flame_Thrower = WeaponType(cWeaponTypesEnum.Flame_Thrower)
    Flame_Thrower_Gui_Montag = WeaponType(cWeaponTypesEnum.Flame_Thrower_Gui_Montag)
    Arclite_Shock_Cannon = WeaponType(cWeaponTypesEnum.Arclite_Shock_Cannon)
    Arclite_Shock_Cannon_Edmund_Duke = WeaponType(cWeaponTypesEnum.Arclite_Shock_Cannon_Edmund_Duke)
    Longbolt_Missile = WeaponType(cWeaponTypesEnum.Longbolt_Missile)
    Claws = WeaponType(cWeaponTypesEnum.Claws)
    Claws_Devouring_One = WeaponType(cWeaponTypesEnum.Claws_Devouring_One)
    Claws_Infested_Kerrigan = WeaponType(cWeaponTypesEnum.Claws_Infested_Kerrigan)
    Needle_Spines = WeaponType(cWeaponTypesEnum.Needle_Spines)
    Needle_Spines_Hunter_Killer = WeaponType(cWeaponTypesEnum.Needle_Spines_Hunter_Killer)
    Kaiser_Blades = WeaponType(cWeaponTypesEnum.Kaiser_Blades)
    Kaiser_Blades_Torrasque = WeaponType(cWeaponTypesEnum.Kaiser_Blades_Torrasque)
    Toxic_Spores = WeaponType(cWeaponTypesEnum.Toxic_Spores)
    Spines = WeaponType(cWeaponTypesEnum.Spines)
    Acid_Spore = WeaponType(cWeaponTypesEnum.Acid_Spore)
    Acid_Spore_Kukulza = WeaponType(cWeaponTypesEnum.Acid_Spore_Kukulza)
    Glave_Wurm = WeaponType(cWeaponTypesEnum.Glave_Wurm)
    Glave_Wurm_Kukulza = WeaponType(cWeaponTypesEnum.Glave_Wurm_Kukulza)
    Seeker_Spores = WeaponType(cWeaponTypesEnum.Seeker_Spores)
    Subterranean_Tentacle = WeaponType(cWeaponTypesEnum.Subterranean_Tentacle)
    Suicide_Infested_Terran = WeaponType(cWeaponTypesEnum.Suicide_Infested_Terran)
    Suicide_Scourge = WeaponType(cWeaponTypesEnum.Suicide_Scourge)
    Particle_Beam = WeaponType(cWeaponTypesEnum.Particle_Beam)
    Psi_Blades = WeaponType(cWeaponTypesEnum.Psi_Blades)
    Psi_Blades_Fenix = WeaponType(cWeaponTypesEnum.Psi_Blades_Fenix)
    Phase_Disruptor = WeaponType(cWeaponTypesEnum.Phase_Disruptor)
    Phase_Disruptor_Fenix = WeaponType(cWeaponTypesEnum.Phase_Disruptor_Fenix)
    Psi_Assault = WeaponType(cWeaponTypesEnum.Psi_Assault)
    Psionic_Shockwave = WeaponType(cWeaponTypesEnum.Psionic_Shockwave)
    Psionic_Shockwave_TZ_Archon = WeaponType(cWeaponTypesEnum.Psionic_Shockwave_TZ_Archon)
    Dual_Photon_Blasters = WeaponType(cWeaponTypesEnum.Dual_Photon_Blasters)
    Dual_Photon_Blasters_Mojo = WeaponType(cWeaponTypesEnum.Dual_Photon_Blasters_Mojo)
    Dual_Photon_Blasters_Artanis = WeaponType(cWeaponTypesEnum.Dual_Photon_Blasters_Artanis)
    Anti_Matter_Missiles = WeaponType(cWeaponTypesEnum.Anti_Matter_Missiles)
    Anti_Matter_Missiles_Mojo = WeaponType(cWeaponTypesEnum.Anti_Matter_Missiles_Mojo)
    Anti_Matter_Missiles_Artanis = WeaponType(cWeaponTypesEnum.Anti_Matter_Missiles_Artanis)
    Phase_Disruptor_Cannon = WeaponType(cWeaponTypesEnum.Phase_Disruptor_Cannon)
    Phase_Disruptor_Cannon_Danimoth = WeaponType(cWeaponTypesEnum.Phase_Disruptor_Cannon_Danimoth)
    Pulse_Cannon = WeaponType(cWeaponTypesEnum.Pulse_Cannon)
    STS_Photon_Cannon = WeaponType(cWeaponTypesEnum.STS_Photon_Cannon)
    STA_Photon_Cannon = WeaponType(cWeaponTypesEnum.STA_Photon_Cannon)
    Scarab = WeaponType(cWeaponTypesEnum.Scarab)
    Neutron_Flare = WeaponType(cWeaponTypesEnum.Neutron_Flare)
    Halo_Rockets = WeaponType(cWeaponTypesEnum.Halo_Rockets)
    Corrosive_Acid = WeaponType(cWeaponTypesEnum.Corrosive_Acid)
    Subterranean_Spines = WeaponType(cWeaponTypesEnum.Subterranean_Spines)
    Warp_Blades = WeaponType(cWeaponTypesEnum.Warp_Blades)
    Warp_Blades_Hero = WeaponType(cWeaponTypesEnum.Warp_Blades_Hero)
    Warp_Blades_Zeratul = WeaponType(cWeaponTypesEnum.Warp_Blades_Zeratul)
    Independant_Laser_Battery = WeaponType(cWeaponTypesEnum.Independant_Laser_Battery)
    Twin_Autocannons_Floor_Trap = WeaponType(cWeaponTypesEnum.Twin_Autocannons_Floor_Trap)
    Hellfire_Missile_Pack_Wall_Trap = WeaponType(cWeaponTypesEnum.Hellfire_Missile_Pack_Wall_Trap)
    Flame_Thrower_Wall_Trap = WeaponType(cWeaponTypesEnum.Flame_Thrower_Wall_Trap)
    Hellfire_Missile_Pack_Floor_Trap = WeaponType(cWeaponTypesEnum.Hellfire_Missile_Pack_Floor_Trap)

    Yamato_Gun = WeaponType(cWeaponTypesEnum.Yamato_Gun)
    Nuclear_Strike = WeaponType(cWeaponTypesEnum.Nuclear_Strike)
    Lockdown = WeaponType(cWeaponTypesEnum.Lockdown)
    EMP_Shockwave = WeaponType(cWeaponTypesEnum.EMP_Shockwave)
    Irradiate = WeaponType(cWeaponTypesEnum.Irradiate)
    Parasite = WeaponType(cWeaponTypesEnum.Parasite)
    Spawn_Broodlings = WeaponType(cWeaponTypesEnum.Spawn_Broodlings)
    Ensnare = WeaponType(cWeaponTypesEnum.Ensnare)
    Dark_Swarm = WeaponType(cWeaponTypesEnum.Dark_Swarm)
    Plague = WeaponType(cWeaponTypesEnum.Plague)
    Consume = WeaponType(cWeaponTypesEnum.Consume)
    Stasis_Field = WeaponType(cWeaponTypesEnum.Stasis_Field)
    Psionic_Storm = WeaponType(cWeaponTypesEnum.Psionic_Storm)
    Disruption_Web = WeaponType(cWeaponTypesEnum.Disruption_Web)
    Restoration = WeaponType(cWeaponTypesEnum.Restoration)
    Mind_Control = WeaponType(cWeaponTypesEnum.Mind_Control)
    Feedback = WeaponType(cWeaponTypesEnum.Feedback)
    Optical_Flare = WeaponType(cWeaponTypesEnum.Optical_Flare)
    Maelstrom = WeaponType(cWeaponTypesEnum.Maelstrom)

    none = WeaponType(cWeaponTypesEnum.None)
    Unknown = WeaponType(cWeaponTypesEnum.Unknown)