﻿cimport cybw.cBWAPI as cBWAPI

def BWAPI_getRevision():
	return cBWAPI.BWAPI_getRevision()

def BWAPI_isDebug():
	return cBWAPI.BWAPI_isDebug()
