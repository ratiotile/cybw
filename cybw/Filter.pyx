﻿cimport cFilter

cdef class UnitFilter:
    cdef cFilter.UnitFilter* thisptr
    
    def __init__(self):
        self.thisptr = NULL
        

cdef UnitFilterFactory(cFilter.UnitFilter* cfilter):
    filter = UnitFilter()
    filter.thisptr = cfilter
    return filter