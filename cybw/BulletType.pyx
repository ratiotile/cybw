﻿cimport cybw.cBulletType as cBulletType

cdef class BulletType:
    def __init__(self):
        pass
    def __str__(self):
        return self.getName()
    def getName(self):
        name = self.thisobj.getName()
        return name.decode()

cdef BulletTypeFactory(cBulletType.BulletType cbt):
    bt = BulletType()
    bt.thisobj = cbt
    return bt