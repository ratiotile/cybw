﻿from cybw cimport cBullet
from cybw.BulletType cimport BulletTypeFactory

cdef class Bullet:
    cdef cBullet.Bullet thisptr

    def __init__(self):
        pass

    def getID(self):
        return self.thisptr.getID()

    def exists(self):
        return self.thisptr.exists()

    def getPlayer(self):
        return PlayerFactory(self.thisptr.getPlayer())

    def getType(self):
        return BulletTypeFactory(self.thisptr.getType())

    def getSource(self):
        return UnitFactory(self.thisptr.getSource())

    def getPosition(self):
        return PositionFactory(self.thisptr.getPosition())

    def getAngle(self):
        return self.thisptr.getAngle()

    def getVelocityX(self):
        return self.thisptr.getVelocityX()

    def getVelocityY(self):
        return self.thisptr.getVelocityY()

    def getTarget(self):
        return UnitFactory(self.thisptr.getTarget())

    def getTargetPosition(self):
        return PositionFactory(self.thisptr.getTargetPosition())

    def getRemoveTimer(self):
        return self.getRemoveTimer()
    """
    def isVisible(self, player = None):
        if player is None:
            return self.thisptr.isVisible()
        elif player is Player:
            return self.thisptr.isVisible((<cPlayer.Player>player).thisptr)
        else:
            raise TypeError
    """
cdef BulletFactory(cBullet.Bullet cb):
    if not cb:
        raise NullPointerException("Bullet")
    bullet = Bullet()
    bullet.thisptr = cb
    return bullet