﻿cimport cybw.cGameType as cGameType

cdef class GameType:
    def __init__(self):
        pass

    def __str__(self):
        return self.getName()

    def __richcmp__(x, y, int op):
        if op == 0:     # lt
            return x.getID() < y.getID()
        elif op == 2:   # eq
            return x.getID() == y.getID()
        elif op == 4:   # gt
            return x.getID() > y.getID()
        elif op == 1:   # lte
            return x.getID() <= y.getID()
        elif op == 3:   # ne
            return x.getID() != y.getID()
        elif op == 5:   # gte
            return x.getID() >= y.getID()
        return False

    def __hash__(self):
        return self.getID()

    def getID(self):
        return self.thisobj.getID()

    def isValid(self):
        return self.thisobj.isValid()

    def getName(self):
        return self.thisobj.getName()

cdef GameTypeFactory(cGameType.GameType cgt):
    gt = GameType()
    gt.thisobj = cgt
    return gt

class GameTypes:
    Melee = GameTypeFactory(cGameType.Melee)
    Free_For_All = GameTypeFactory(cGameType.Free_For_All)
    One_on_One = GameTypeFactory(cGameType.One_on_One)
    Capture_The_Flag = GameTypeFactory(cGameType.Capture_The_Flag)
    Greed = GameTypeFactory(cGameType.Greed)
    Slaughter = GameTypeFactory(cGameType.Slaughter)
    Sudden_Death = GameTypeFactory(cGameType.Sudden_Death)
    Ladder = GameTypeFactory(cGameType.Ladder)
    Use_Map_Settings = GameTypeFactory(cGameType.Use_Map_Settings)
    Team_Melee = GameTypeFactory(cGameType.Team_Melee)
    Team_Free_For_All = GameTypeFactory(cGameType.Team_Free_For_All)
    Team_Capture_The_Flag = GameTypeFactory(cGameType.Team_Capture_The_Flag)
    Top_vs_Bottom = GameTypeFactory(cGameType.Top_vs_Bottom)
    none = GameTypeFactory(cGameType.None)
    Unknown = GameTypeFactory(cGameType.Unknown)