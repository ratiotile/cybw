﻿

cdef class Unit(PositionUnitConverter):
    """ A Python wrapper around the BWAPI Unit class. Holds a pointer to the C++ object hidden from Python code. """
    cdef cUnit.Unit thisptr

cdef UnitFactory(cUnit.Unit unit)