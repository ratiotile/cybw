﻿from cybw.cSetContainer cimport SetContainer

cdef extern from "BWAPI/UnitType.h" namespace "BWAPI::UnitType":
    ctypedef SetContainer[UnitType] set