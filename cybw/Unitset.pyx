﻿cimport cybw.cUnitset as cUnitset
from cybw.cUnitset cimport Unitset as c_UnitSet
cimport cybw.cUnit as cUnit
from cybw.GameClient cimport UnitFactory
from cython.operator import dereference as deref

cdef class ConstUnitset(set):
    def __init__(self):
        pass

    #def __iter__(self):
    #    return ConstUnitset_iterator(self.thisptr)

    #def size(self):
    #    return deref(self.thisptr).size()

cdef class Unitset(set):
    pass

cdef ConstUnitsetFactory(const cUnitset.Unitset &cus):
    unitset = ConstUnitset()
    unitset.thisptr = &cus
    for u in cus:
        unitset.add(UnitFactory(u))
    return unitset



cdef UnitsetFactory(cUnitset.Unitset &us):
    unitset = Unitset()
    unitset.thisptr = &us
    for u in us:
        unitset.add(UnitFactory(u))
    return unitset

"""
cdef class ConstUnitset_iterator:
    cdef __init__(self, c_UnitSet* us):
        self.it = deref(us).begin()
        self.set = us
    def __next__(self):
        from cybw.Unit import UnitFactory
        for unit in deref(self.set):
            yield UnitFactory(unit)
        '''
        inc(self.iterator)
        return UnitFactory(deref(self.iterator))
        if self.iterator == deref(self.set).end():
            raise StopIteration
            '''
    def __iter__(self):
        return self
"""
"""
cdef class Unitset_iterator:
    cdef __init__(self, c_UnitSet* us):
        self.it = deref(us).begin()
        self.set = us
    def __next__(self):
        from cybw.Unit import UnitFactory
        for unit in deref(self.set):
            yield UnitFactory(unit)
        '''
        inc(self.iterator)
        return UnitFactory(deref(self.iterator))
        if self.iterator == deref(self.set).end():
            raise StopIteration
            '''
    def __iter__(self):
        return self
"""