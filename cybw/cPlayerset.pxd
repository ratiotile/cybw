﻿from libcpp cimport bool
from cybw.cSetContainer cimport SetContainer

cdef extern from "BWAPI/Playerset.h" namespace "BWAPI":
    cdef cppclass PlayerInterface:
        pass

    ctypedef PlayerInterface *Player

    cdef cppclass Unitset:
        pass

    cdef cppclass Playerset(SetContainer[Player]):
        Unitset getUnits() const

        #Race::set getRaces() const TODO

        void setAlliance(bool allies = true, bool alliedVictory = true)

        # no refs here. Player is already a pointer
        cppclass iterator:
            Player operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)

        cppclass const_iterator:
            const Player operator*()
            const_iterator operator++()
            const_iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)