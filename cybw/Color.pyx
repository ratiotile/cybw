﻿cimport cybw.cColor as cColor
cimport cybw.cText as cText
from cybw.Namespace import as_namespace

cdef class Color:
    def __init__(self, r_id=0, g=None, b=None):
        if(g is not None and b is not None):
            self.thisptr = new cColor.Color(int(r_id), int(g), int(b))
        else:
            self.thisptr = new cColor.Color(int(r_id))
    def red(self):
        return self.thisptr.red()
    def green(self):
        return self.thisptr.green()
    def blue(self):
        return self.thisptr.blue()

    def rgb(self):
        return (self.thisptr.red(), self.thisptr.green(), self.thisptr.blue())

cdef ColorFactory(cColor.Color *c_color):
    cdef Color c = Color()
    c.thisptr = c_color
    return c

class Colors:
    Red = Color(cColor.Red.red(),       cColor.Red.green(),     cColor.Red.blue())
    Blue = Color(cColor.Blue.red(),     cColor.Blue.green(),    cColor.Blue.blue())
    Teal = Color(cColor.Teal.red(),     cColor.Teal.green(),    cColor.Teal.blue())
    Purple = Color(cColor.Purple.red(), cColor.Purple.green(),  cColor.Purple.blue())
    Orange = Color(cColor.Orange.red(), cColor.Orange.green(),  cColor.Orange.blue())
    Brown = Color(cColor.Brown.red(),   cColor.Brown.green(),   cColor.Brown.blue())
    White = Color(cColor.White.red(),   cColor.White.green(),   cColor.White.blue())
    Yellow = Color(cColor.Yellow.red(), cColor.Yellow.green(),  cColor.Yellow.blue())
    Green = Color(cColor.Green.red(),   cColor.Green.green(),   cColor.Green.blue())
    Black = Color(cColor.Black.red(),   cColor.Black.green(),   cColor.Black.blue())
    Grey = Color(cColor.Grey.red(),     cColor.Grey.green(),    cColor.Grey.blue())

from enum import IntEnum


cdef class TextWrapper:
    def __init__(self, cText.Enum ctext):
        self.thisval = ctext

    def __richcmp__(TextWrapper self, TextWrapper other, int optype):
        if optype   == 0: # <
            return self.thisval < other.thisval
        elif optype == 2: # ==
            return self.thisval == other.thisval
        elif optype == 4: # >
            return self.thisval > other.thisval
        elif optype == 1: # <=
            return self.thisval <= other.thisval
        elif optype == 3: # !=
            return self.thisval != other.thisval
        elif optype == 5: # >=
            return self.thisval >= other.thisval

@as_namespace
class Text:
    Previous     = cText.Previous
    Default      = cText.Default
    Yellow       = cText.Yellow
    White        = cText.White
    Grey         = cText.Grey
    Red          = cText.Red
    Green        = cText.Green
    BrightRed    = cText.BrightRed
    Invisible    = cText.Invisible
    Blue         = cText.Blue
    Teal         = cText.Teal
    Purple       = cText.Purple
    Orange       = cText.Orange
    Align_Right  = cText.Align_Right
    Align_Center = cText.Align_Center
    Invisible2   = cText.Invisible2
    Brown        = cText.Brown
    PlayerWhite  = cText.PlayerWhite
    PlayerYellow = cText.PlayerYellow
    DarkGreen    = cText.DarkGreen
    LightYellow  = cText.LightYellow
    Cyan         = cText.Cyan
    Tan          = cText.Tan
    GreyBlue     = cText.GreyBlue
    GreyGreen    = cText.GreyGreen
    GreyCyan     = cText.GreyCyan
    Turquoise    = cText.Turquoise

    class Size(IntEnum):
        Small   = cText.Small
        Default = cText.Default
        Large   = cText.Large
        Huge    = cText.Huge
