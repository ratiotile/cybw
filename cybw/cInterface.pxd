﻿cdef extern from "BWAPI/Interface.h" namespace "BWAPI":
    cdef cppclass Interface[T]:
        void* getClientInfo(int key) const
        void* getClientInfo() const

        CT getClientInfo[CT](int key) const
        CT getClientInfo[CT]() const

        void setClientInfo[V](const V &clientInfo, int key) const
        void setClientInfo[V](const V &clientInfo) const