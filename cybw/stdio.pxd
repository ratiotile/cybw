cdef extern from * namespace "std":
    cdef cppclass ostream:
        pass
    ctypedef ostream (*ostream_manipulator)(ostream)

cdef extern from * namespace "std":
    ostream& endl(ostream& os)