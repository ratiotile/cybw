﻿cimport cybw.cRegionset as cRegionset
from cybw.cRegionset cimport Regionset as c_crs
from cybw.Unitset cimport UnitsetFactory

cdef class Regionset:
    def __init__(self):
        pass

    def getCenter(self):
        """ returns a Position that is the center node of this region """
        return PositionFactory(self.thisptr.getCenter())

    def getUnits(self):
        """ get a Unitset of all units in the region """
        return UnitsetFactory(self.thisptr.getUnits())

cdef ConstRegionSetFactory(const cRegionset.Regionset& crs):
    regionset = set()
    cdef c_crs.iterator it = crs.begin()
    while it != crs.end():
        regionset.add(RegionFactory(deref(it)))
        inc(it)

    return regionset

cdef RegionSetFactory(cRegionset.Regionset& rs):
    regionset = set()
    cdef c_crs.iterator it = rs.begin()
    while it != rs.end():
        regionset.add(RegionFactory(deref(it)))
        inc(it)
    return regionset