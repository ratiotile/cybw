﻿from libcpp cimport bool
from libcpp.string cimport string
from libcpp.map cimport map
from libcpp.pair cimport pair
from cybw.cPosition cimport TilePosition
from cybw.cType cimport Type
from cybw.cSetContainer cimport SetContainer


cdef extern from "BWAPI/UnitType.h" namespace "BWAPI":
    cdef cppclass TechType:
        pass
    cdef cppclass UpgradeType:
        pass
    cdef cppclass WeaponType:
        pass
    cdef cppclass UnitSizeType:
        pass
    cdef cppclass Race:
        pass


    ctypedef int enum_unknown "UnitTypes::Enum::Unknown"

    cdef cppclass UnitType(Type[UnitType,int]):
        UnitType()
        UnitType(int id)
        Race getRace() const
        pair[UnitType, int] whatBuilds() const
        #const std::map< UnitType, int >& requiredUnits() const
        map[UnitType, int] requiredUnits() const
        TechType requiredTech() const
        TechType cloakingTech() const
        #const ConstVectorset<TechType>& abilities() const
        #const ConstVectorset<UpgradeType>& upgrades() const
        UpgradeType armorUpgrade() const
        int maxHitPoints() const
        int maxShields() const
        int maxEnergy() const
        int armor() const
        int mineralPrice() const
        int gasPrice() const
        int buildTime() const
        int supplyRequired() const
        int supplyProvided() const
        int spaceRequired() const
        int spaceProvided() const
        int buildScore() const
        int destroyScore() const
        UnitSizeType size() const
        int tileWidth() const
        int tileHeight() const
        TilePosition tileSize() const
        int dimensionLeft() const
        int dimensionUp() const
        int dimensionRight() const
        int dimensionDown() const
        int width() const
        int height() const
        int seekRange() const
        int sightRange() const
        WeaponType groundWeapon() const
        int maxGroundHits() const
        WeaponType airWeapon() const
        int maxAirHits() const
        double topSpeed() const
        int acceleration() const
        int haltDistance() const
        int turnRadius() const
        bool canProduce() const
        bool canAttack() const
        bool canMove() const
        bool isFlyer() const
        bool regeneratesHP() const
        bool isSpellcaster() const
        bool hasPermanentCloak() const
        bool isInvincible() const
        bool isOrganic() const
        bool isMechanical() const
        bool isRobotic() const
        bool isDetector() const
        bool isResourceContainer() const
        bool isResourceDepot() const
        bool isRefinery() const
        bool isWorker() const
        bool requiresPsi() const
        bool requiresCreep() const
        bool isTwoUnitsInOneEgg() const
        bool isBurrowable() const
        bool isCloakable() const
        bool isBuilding() const
        bool isAddon() const
        bool isFlyingBuilding() const
        bool isNeutral() const
        bool isHero() const
        bool isPowerup() const
        bool isBeacon() const
        bool isFlagBeacon() const
        bool isSpecialBuilding() const
        bool isSpell() const
        bool producesLarva() const
        bool isMineralField() const
        bool isCritter() const
        bool canBuildAddon() const
        const SetContainer[TechType]& researchesWhat() const
        const SetContainer[UpgradeType]& upgradesWhat() const
        #inherited
        #const string &getName() const
        #int getID() const

cdef extern from "BWAPI/UnitType.h" namespace "BWAPI::UnitType":
    cdef cppclass set:
        cppclass iterator:
            UnitType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)
        iterator begin()
        iterator end()

        cppclass const_iterator:
            UnitType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)
        const_iterator cbegin()
        const_iterator cend()

    #ctypedef const_set "const set"
    cdef cppclass list:
        cppclass iterator:
            UnitType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)
        iterator begin()
        iterator end()

        cppclass const_iterator:
            UnitType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)
        const_iterator cbegin()
        const_iterator cend()


cdef extern from "BWAPI/UnitType.h" namespace "BWAPI::UnitTypes":
    int maxUnitWidth()
    int maxUnitHeight()
    const set& allUnitTypes()
    const set& allMacroTypes()

    const UnitType Terran_Marine
    const UnitType Terran_Ghost
    const UnitType Terran_Vulture
    const UnitType Terran_Goliath
    # goliath turret 4
    const UnitType Terran_Siege_Tank_Tank_Mode
    # siege tank turret 6
    const UnitType Terran_SCV
    const UnitType Terran_Wraith
    const UnitType Terran_Science_Vessel
    const UnitType Hero_Gui_Montag
    const UnitType Terran_Dropship
    const UnitType Terran_Battlecruiser
    const UnitType Terran_Vulture_Spider_Mine
    const UnitType Terran_Nuclear_Missile
    const UnitType Terran_Civilian
    const UnitType Hero_Sarah_Kerrigan
    const UnitType Hero_Alan_Schezar
    # alan turret 18
    const UnitType Hero_Jim_Raynor_Vulture
    const UnitType Hero_Jim_Raynor_Marine
    const UnitType Hero_Tom_Kazansky
    const UnitType Hero_Magellan
    const UnitType Hero_Edmund_Duke_Tank_Mode
    # edmund duke turret 24
    const UnitType Hero_Edmund_Duke_Siege_Mode
    # edmund duke turret siege mode 26
    const UnitType Hero_Arcturus_Mengsk
    const UnitType Hero_Hyperion
    const UnitType Hero_Norad_II
    const UnitType Terran_Siege_Tank_Siege_Mode
    # siege tank siege mode turret 31
    const UnitType Terran_Firebat
    const UnitType Spell_Scanner_Sweep
    const UnitType Terran_Medic
    const UnitType Zerg_Larva
    const UnitType Zerg_Egg
    const UnitType Zerg_Zergling
    const UnitType Zerg_Hydralisk
    const UnitType Zerg_Ultralisk
    const UnitType Zerg_Broodling
    const UnitType Zerg_Drone
    const UnitType Zerg_Overlord
    const UnitType Zerg_Mutalisk
    const UnitType Zerg_Guardian
    const UnitType Zerg_Queen
    const UnitType Zerg_Defiler
    const UnitType Zerg_Scourge
    const UnitType Hero_Torrasque
    const UnitType Hero_Matriarch
    const UnitType Zerg_Infested_Terran
    const UnitType Hero_Infested_Kerrigan
    const UnitType Hero_Unclean_One
    const UnitType Hero_Hunter_Killer
    const UnitType Hero_Devouring_One
    const UnitType Hero_Kukulza_Mutalisk
    const UnitType Hero_Kukulza_Guardian
    const UnitType Hero_Yggdrasill
    const UnitType Terran_Valkyrie
    const UnitType Zerg_Cocoon
    const UnitType Protoss_Corsair
    const UnitType Protoss_Dark_Templar
    const UnitType Zerg_Devourer
    const UnitType Protoss_Dark_Archon
    const UnitType Protoss_Probe
    const UnitType Protoss_Zealot
    const UnitType Protoss_Dragoon
    const UnitType Protoss_High_Templar
    const UnitType Protoss_Archon
    const UnitType Protoss_Shuttle
    const UnitType Protoss_Scout
    const UnitType Protoss_Arbiter
    const UnitType Protoss_Carrier
    const UnitType Protoss_Interceptor
    const UnitType Hero_Dark_Templar
    const UnitType Hero_Zeratul
    const UnitType Hero_Tassadar_Zeratul_Archon
    const UnitType Hero_Fenix_Zealot
    const UnitType Hero_Fenix_Dragoon
    const UnitType Hero_Tassadar
    const UnitType Hero_Mojo
    const UnitType Hero_Warbringer
    const UnitType Hero_Gantrithor
    const UnitType Protoss_Reaver
    const UnitType Protoss_Observer
    const UnitType Protoss_Scarab
    const UnitType Hero_Danimoth
    const UnitType Hero_Aldaris
    const UnitType Hero_Artanis
    const UnitType Critter_Rhynadon
    const UnitType Critter_Bengalaas
    const UnitType Special_Cargo_Ship
    const UnitType Special_Mercenary_Gunship
    const UnitType Critter_Scantid
    const UnitType Critter_Kakaru
    const UnitType Critter_Ragnasaur
    const UnitType Critter_Ursadon
    const UnitType Zerg_Lurker_Egg
    const UnitType Hero_Raszagal
    const UnitType Hero_Samir_Duran
    const UnitType Hero_Alexei_Stukov
    const UnitType Special_Map_Revealer
    const UnitType Hero_Gerard_DuGalle
    const UnitType Zerg_Lurker
    const UnitType Hero_Infested_Duran
    const UnitType Spell_Disruption_Web
    const UnitType Terran_Command_Center
    const UnitType Terran_Comsat_Station
    const UnitType Terran_Nuclear_Silo
    const UnitType Terran_Supply_Depot
    const UnitType Terran_Refinery
    const UnitType Terran_Barracks
    const UnitType Terran_Academy
    const UnitType Terran_Factory
    const UnitType Terran_Starport
    const UnitType Terran_Control_Tower
    const UnitType Terran_Science_Facility
    const UnitType Terran_Covert_Ops
    const UnitType Terran_Physics_Lab
    # starbase 119
    const UnitType Terran_Machine_Shop
    # repair bay 121
    const UnitType Terran_Engineering_Bay
    const UnitType Terran_Armory
    const UnitType Terran_Missile_Turret
    const UnitType Terran_Bunker
    const UnitType Special_Crashed_Norad_II
    const UnitType Special_Ion_Cannon
    const UnitType Powerup_Uraj_Crystal
    const UnitType Powerup_Khalis_Crystal
    const UnitType Zerg_Infested_Command_Center
    const UnitType Zerg_Hatchery
    const UnitType Zerg_Lair
    const UnitType Zerg_Hive
    const UnitType Zerg_Nydus_Canal
    const UnitType Zerg_Hydralisk_Den
    const UnitType Zerg_Defiler_Mound
    const UnitType Zerg_Greater_Spire
    const UnitType Zerg_Queens_Nest
    const UnitType Zerg_Evolution_Chamber
    const UnitType Zerg_Ultralisk_Cavern
    const UnitType Zerg_Spire
    const UnitType Zerg_Spawning_Pool
    const UnitType Zerg_Creep_Colony
    const UnitType Zerg_Spore_Colony
    # unused zerg 1 145
    const UnitType Zerg_Sunken_Colony
    const UnitType Special_Overmind_With_Shell
    const UnitType Special_Overmind
    const UnitType Zerg_Extractor
    const UnitType Special_Mature_Chrysalis
    const UnitType Special_Cerebrate
    const UnitType Special_Cerebrate_Daggoth
    # unused zerg 2 153
    const UnitType Protoss_Nexus
    const UnitType Protoss_Robotics_Facility
    const UnitType Protoss_Pylon
    const UnitType Protoss_Assimilator
    # unused protoss 1 158
    const UnitType Protoss_Observatory
    const UnitType Protoss_Gateway
    # unused protoss 2 161
    const UnitType Protoss_Photon_Cannon
    const UnitType Protoss_Citadel_of_Adun
    const UnitType Protoss_Cybernetics_Core
    const UnitType Protoss_Templar_Archives
    const UnitType Protoss_Forge
    const UnitType Protoss_Stargate
    const UnitType Special_Stasis_Cell_Prison
    const UnitType Protoss_Fleet_Beacon
    const UnitType Protoss_Arbiter_Tribunal
    const UnitType Protoss_Robotics_Support_Bay
    const UnitType Protoss_Shield_Battery
    const UnitType Special_Khaydarin_Crystal_Form
    const UnitType Special_Protoss_Temple
    const UnitType Special_XelNaga_Temple
    const UnitType Resource_Mineral_Field
    const UnitType Resource_Mineral_Field_Type_2
    const UnitType Resource_Mineral_Field_Type_3
    # cave 179
    # cave-in 180
    # cantina 181
    # mining platform 182
    # independant command center 183
    const UnitType Special_Independant_Starport
    # independant jump gate 185
    # ruins 186
    # unused khaydarin crystal formation 187
    const UnitType Resource_Vespene_Geyser
    const UnitType Special_Warp_Gate
    const UnitType Special_Psi_Disrupter
    # zerg marker 191
    # terran marker 192
    # protoss marker 193
    const UnitType Special_Zerg_Beacon
    const UnitType Special_Terran_Beacon
    const UnitType Special_Protoss_Beacon
    const UnitType Special_Zerg_Flag_Beacon
    const UnitType Special_Terran_Flag_Beacon
    const UnitType Special_Protoss_Flag_Beacon
    const UnitType Special_Power_Generator
    const UnitType Special_Overmind_Cocoon
    const UnitType Spell_Dark_Swarm
    const UnitType Special_Floor_Missile_Trap
    const UnitType Special_Floor_Hatch
    const UnitType Special_Upper_Level_Door
    const UnitType Special_Right_Upper_Level_Door
    const UnitType Special_Pit_Door
    const UnitType Special_Right_Pit_Door
    const UnitType Special_Floor_Gun_Trap
    const UnitType Special_Wall_Missile_Trap
    const UnitType Special_Wall_Flame_Trap
    const UnitType Special_Right_Wall_Missile_Trap
    const UnitType Special_Right_Wall_Flame_Trap
    const UnitType Special_Start_Location
    const UnitType Powerup_Flag
    const UnitType Powerup_Young_Chrysalis
    const UnitType Powerup_Psi_Emitter
    const UnitType Powerup_Data_Disk
    const UnitType Powerup_Khaydarin_Crystal
    const UnitType Powerup_Mineral_Cluster_Type_1
    const UnitType Powerup_Mineral_Cluster_Type_2
    const UnitType Powerup_Protoss_Gas_Orb_Type_1
    const UnitType Powerup_Protoss_Gas_Orb_Type_2
    const UnitType Powerup_Zerg_Gas_Sac_Type_1
    const UnitType Powerup_Zerg_Gas_Sac_Type_2
    const UnitType Powerup_Terran_Gas_Tank_Type_1
    const UnitType Powerup_Terran_Gas_Tank_Type_2

    const UnitType None
    const UnitType AllUnits
    const UnitType Men
    const UnitType Buildings
    const UnitType Factories
    const UnitType Unknown