﻿from cybw.cForceset cimport Forceset as cForceset
from cybw.Force cimport const_ForceFactory
from cython.operator cimport preincrement as inc, dereference as deref

cdef class Forceset:
    def __init__(self):
        pass

cdef ForceListFactory(const cForceset& cfs):
    ''' make a list of Players from a Forceset pointer'''
    force_set = set()
    cdef cForceset.const_iterator it = cfs.cbegin()
    cdef cForceset.const_iterator end = cfs.cend()
    while it != end:
        force_set.add(const_ForceFactory(deref(it)))
        inc(it)
    return force_set

    """
    # failed attempts
    for f in cfs:
        force_set.add(ForceFactory(f))
    return force_set
    """