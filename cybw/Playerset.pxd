﻿cimport cybw.cPlayerset as cPlayerset

cdef class Playerset(set):
    cdef cPlayerset.Playerset *thisptr

cdef class const_Playerset(set):
    cdef const cPlayerset.Playerset *thisptr

cdef ConstPlayerSetFactory(const cPlayerset.Playerset& cps)

cdef PlayerSetFactory(cPlayerset.Playerset& ps)