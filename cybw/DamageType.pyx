cimport cybw.cDamageType as cDamageType


cdef class DamageType:

    @staticmethod
    cdef create(cDamageType.DamageType o_DamageType):
        d = DamageType()
        d.thisobj = o_DamageType
        return d

    def __int__(self):
        return self.thisobj.getID()

    def __richcmp__(DamageType self, DamageType other, op):
        if op == 2:
            return int(self) == int(other)
        else:
            raise NotImplementedError

    def getName(self):
        name = self.thisobj.getName()
        return name.decode()

    def getID(self):
        return self.thisobj.getID()


cdef DamageTypeSetFactory(const cDamageType.set &cset):
    dts = set()
    for i in cset:
        dts.add(DamageType.create(i))
    return dts

cdef class DamageTypes:
    @staticmethod
    def allDamageTypes():
        return DamageTypeSetFactory(cDamageType.allDamageTypes())

    Independent = DamageType.create(cDamageType.Independent)
    Explosive = DamageType.create(cDamageType.Explosive)
    Concussive = DamageType.create(cDamageType.Concussive)
    Normal = DamageType.create(cDamageType.Normal)
    Ignore_Armor = DamageType.create(cDamageType.Ignore_Armor)
    none = DamageType.create(cDamageType.None)
    Unknown = DamageType.create(cDamageType.Unknown)





