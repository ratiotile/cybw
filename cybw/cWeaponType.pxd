﻿from libcpp cimport bool
from libcpp.string cimport string
from cybw.cType cimport Type

cdef extern from "BWAPI/WeaponType.h" namespace "BWAPI":
    cdef cppclass TechType:
        pass

    cdef cppclass UpgradeType:
        pass

    cdef cppclass ExplosionType:
        pass

    cdef cppclass DamageType:
        pass

    cdef cppclass UnitType:
        pass

    ctypedef int enum_unknown "WeaponTypes::Enum::Unknown"

    cdef cppclass WeaponType(Type[WeaponType, enum_unknown]):
        WeaponType(int id)
        WeaponType()

        TechType getTech() const

        UnitType whatUses() const

        int damageAmount() const

        int damageBonus() const

        int damageCooldown() const

        int damageFactor() const

        UpgradeType upgradeType() const

        DamageType damageType() const

        ExplosionType explosionType() const

        int minRange() const

        int maxRange() const

        int innerSplashRadius() const

        int medianSplashRadius() const

        int outerSplashRadius() const

        bool targetsAir() const

        bool targetsGround() const
        bool targetsMechanical() const
        bool targetsOrganic() const
        bool targetsNonBuilding() const
        bool targetsNonRobotic() const
        bool targetsTerrain() const
        bool targetsOrgOrMech() const
        bool targetsOwn() const

cdef extern from "BWAPI/WeaponType.h" namespace "BWAPI::WeaponType":
    cdef cppclass set:
        cppclass iterator:
            WeaponType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)
        iterator begin()
        iterator end()

        cppclass const_iterator:
            WeaponType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)
        const_iterator cbegin()
        const_iterator cend()

cdef extern from "BWAPI/WeaponType.h" namespace "BWAPI::WeaponType":
    cdef cppclass set:
        pass
    cdef cppclass const_set:
        pass

cdef extern from "BWAPI/WeaponType.h" namespace "BWAPI::WeaponTypes":
    const set& allWeaponTypes()
    #const const_set& normalWeaponTypes()
    #const const_set& specialWeaponTypes()


    const WeaponType Gauss_Rifle
    const WeaponType Gauss_Rifle_Jim_Raynor
    const WeaponType C_10_Canister_Rifle
    const WeaponType C_10_Canister_Rifle_Sarah_Kerrigan
    const WeaponType C_10_Canister_Rifle_Samir_Duran
    const WeaponType C_10_Canister_Rifle_Infested_Duran
    const WeaponType C_10_Canister_Rifle_Alexei_Stukov
    const WeaponType Fragmentation_Grenade
    const WeaponType Fragmentation_Grenade_Jim_Raynor
    const WeaponType Spider_Mines
    const WeaponType Twin_Autocannons
    const WeaponType Twin_Autocannons_Alan_Schezar
    const WeaponType Hellfire_Missile_Pack
    const WeaponType Hellfire_Missile_Pack_Alan_Schezar
    const WeaponType Arclite_Cannon
    const WeaponType Arclite_Cannon_Edmund_Duke
    const WeaponType Fusion_Cutter
    const WeaponType Gemini_Missiles
    const WeaponType Gemini_Missiles_Tom_Kazansky
    const WeaponType Burst_Lasers
    const WeaponType Burst_Lasers_Tom_Kazansky
    const WeaponType ATS_Laser_Battery
    const WeaponType ATS_Laser_Battery_Hero
    const WeaponType ATS_Laser_Battery_Hyperion
    const WeaponType ATA_Laser_Battery
    const WeaponType ATA_Laser_Battery_Hero
    const WeaponType ATA_Laser_Battery_Hyperion
    const WeaponType Flame_Thrower
    const WeaponType Flame_Thrower_Gui_Montag
    const WeaponType Arclite_Shock_Cannon
    const WeaponType Arclite_Shock_Cannon_Edmund_Duke
    const WeaponType Longbolt_Missile
    const WeaponType Claws
    const WeaponType Claws_Devouring_One
    const WeaponType Claws_Infested_Kerrigan
    const WeaponType Needle_Spines
    const WeaponType Needle_Spines_Hunter_Killer
    const WeaponType Kaiser_Blades
    const WeaponType Kaiser_Blades_Torrasque
    const WeaponType Toxic_Spores
    const WeaponType Spines
    const WeaponType Acid_Spore
    const WeaponType Acid_Spore_Kukulza
    const WeaponType Glave_Wurm
    const WeaponType Glave_Wurm_Kukulza
    const WeaponType Seeker_Spores
    const WeaponType Subterranean_Tentacle
    const WeaponType Suicide_Infested_Terran
    const WeaponType Suicide_Scourge
    const WeaponType Particle_Beam
    const WeaponType Psi_Blades
    const WeaponType Psi_Blades_Fenix
    const WeaponType Phase_Disruptor
    const WeaponType Phase_Disruptor_Fenix
    const WeaponType Psi_Assault
    const WeaponType Psionic_Shockwave
    const WeaponType Psionic_Shockwave_TZ_Archon
    const WeaponType Dual_Photon_Blasters
    const WeaponType Dual_Photon_Blasters_Mojo
    const WeaponType Dual_Photon_Blasters_Artanis
    const WeaponType Anti_Matter_Missiles
    const WeaponType Anti_Matter_Missiles_Mojo
    const WeaponType Anti_Matter_Missiles_Artanis
    const WeaponType Phase_Disruptor_Cannon
    const WeaponType Phase_Disruptor_Cannon_Danimoth
    const WeaponType Pulse_Cannon
    const WeaponType STS_Photon_Cannon
    const WeaponType STA_Photon_Cannon
    const WeaponType Scarab
    const WeaponType Neutron_Flare
    const WeaponType Halo_Rockets
    const WeaponType Corrosive_Acid
    const WeaponType Subterranean_Spines
    const WeaponType Warp_Blades
    const WeaponType Warp_Blades_Hero
    const WeaponType Warp_Blades_Zeratul
    const WeaponType Independant_Laser_Battery
    const WeaponType Twin_Autocannons_Floor_Trap
    const WeaponType Hellfire_Missile_Pack_Wall_Trap
    const WeaponType Flame_Thrower_Wall_Trap
    const WeaponType Hellfire_Missile_Pack_Floor_Trap

    const WeaponType Yamato_Gun
    const WeaponType Nuclear_Strike
    const WeaponType Lockdown
    const WeaponType EMP_Shockwave
    const WeaponType Irradiate
    const WeaponType Parasite
    const WeaponType Spawn_Broodlings
    const WeaponType Ensnare
    const WeaponType Dark_Swarm
    const WeaponType Plague
    const WeaponType Consume
    const WeaponType Stasis_Field
    const WeaponType Psionic_Storm
    const WeaponType Disruption_Web
    const WeaponType Restoration
    const WeaponType Mind_Control
    const WeaponType Feedback
    const WeaponType Optical_Flare
    const WeaponType Maelstrom

    const WeaponType None
    const WeaponType Unknown