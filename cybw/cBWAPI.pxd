﻿from libcpp cimport bool

cdef extern from "../../include/bwapi.h" namespace "BWAPI":
	int BWAPI_getRevision()
	bool BWAPI_isDebug()