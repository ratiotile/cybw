﻿cimport cybw.cUpgradeType as cUpgradeType
cimport cybw.cUpgradeTypeEnum as cUpgradeTypeEnum
from enum import IntEnum
from cybw.Race cimport Race, RaceFactory
from cybw.GameClient cimport UnitType, UnitTypeFactory

cdef class UpgradeType:
    def __init__(self, id =-1):
        if(id >= 0 and id < cUpgradeTypeEnum.MAX):
            self.thisobj = cUpgradeType.UpgradeType(id)

    def __str__(self):
        return self.getName()

    def __richcmp__(x, y, int op):
        if op == 0:     # lt
            return x.getID() < y.getID()
        elif op == 2:   # eq
            return x.getID() == y.getID()
        elif op == 4:   # gt
            return x.getID() > y.getID()
        elif op == 1:   # lte
            return x.getID() <= y.getID()
        elif op == 3:   # ne
            return x.getID() != y.getID()
        elif op == 5:   # gte
            return x.getID() >= y.getID()
        return False

    def __hash__(self):
        return self.getID()

    """ Returns the race the upgrade is for. For example, UpgradeTypes::Terran_Infantry_Armor.getRace()
     * will return Races::Terran. """
    def getRace(self):  # const, returns Race
        return RaceFactory(self.thisobj.getRace())

    """ Returns the mineral price for the first upgrade. """
    def mineralPrice(self, int level = 1):  # const, returns int
        return self.thisobj.mineralPrice(level)

    """ Returns the amount that the mineral price increases for each additional upgrade. """
    def mineralPriceFactor(self):  # const, returns int
        return self.thisobj.mineralPriceFactor()

    """ Returns the vespene gas price for the first upgrade. """
    def gasPrice(self, int level = 1):  # const, returns int
        return self.thisobj.gasPrice(level)

    """ Returns the amount that the vespene gas price increases for each additional upgrade. """
    def gasPriceFactor(self):  # const, returns int
        return self.thisobj.gasPriceFactor()

    """ Returns the number of frames needed to research the first upgrade. """
    def upgradeTime(self, int level = 1):  # const, returns int
        return self.thisobj.upgradeTime(level)

    """ Returns the number of frames that the upgrade time increases for each additional upgrade. """
    def upgradeTimeFactor(self):  # const, returns int
        return self.thisobj.upgradeTimeFactor()

    """ Returns the maximum number of times the upgrade can be researched. """
    def maxRepeats(self):  # const, returns int
        return self.thisobj.maxRepeats()

    """ Returns the type of unit that researches the upgrade. """
    def whatUpgrades(self):  # const, returns UnitType
        return UnitTypeFactory(self.thisobj.whatUpgrades())

    """ Returns the type of unit that is additionally required for the upgrade. """
    def whatsRequired(self, int level = 1):  # const, returns UnitType
        return UnitTypeFactory(self.thisobj.whatsRequired(level))

    """ Returns the set of units that are affected by this upgrade. """
    #def whatUses(self):  # const, returns const UnitType::const_set&

    # inherited
    def getName(self):
        name = self.thisobj.getName()
        pystr = name.decode()
        return pystr

    def getID(self):
        return self.thisobj.getID()

cdef UpgradeTypeSet(set):
    cdef cUpgradeType.set thisobj
    def __init__(self):
        pass


cdef UpgradeTypeFactory(cUpgradeType.UpgradeType ctt):
    upgrade = UpgradeType()
    upgrade.thisobj = ctt
    return upgrade

cdef UpgradeTypeSetFactory(const SetContainer[cUpgradeType.UpgradeType]& cset):
    utset = set()
    for ut in cset:
        utset.add(UpgradeTypeFactory(ut))
    return utset

class UpgradeTypes:

    @staticmethod
    def allUpgradeTypes():
        return UpgradeTypeSetFactory(cUpgradeType.allUpgradeTypes())

    Terran_Infantry_Armor = UpgradeType(cUpgradeTypeEnum.Terran_Infantry_Armor)
    Terran_Vehicle_Plating = UpgradeType(cUpgradeTypeEnum.Terran_Vehicle_Plating)
    Terran_Ship_Plating = UpgradeType(cUpgradeTypeEnum.Terran_Ship_Plating)
    Zerg_Carapace = UpgradeType(cUpgradeTypeEnum.Zerg_Carapace)
    Zerg_Flyer_Carapace = UpgradeType(cUpgradeTypeEnum.Zerg_Flyer_Carapace)
    Protoss_Ground_Armor = UpgradeType(cUpgradeTypeEnum.Protoss_Ground_Armor)
    Protoss_Air_Armor = UpgradeType(cUpgradeTypeEnum.Protoss_Air_Armor)
    Terran_Infantry_Weapons = UpgradeType(cUpgradeTypeEnum.Terran_Infantry_Weapons)
    Terran_Vehicle_Weapons = UpgradeType(cUpgradeTypeEnum.Terran_Vehicle_Weapons)
    Terran_Ship_Weapons = UpgradeType(cUpgradeTypeEnum.Terran_Ship_Weapons)
    Zerg_Melee_Attacks = UpgradeType(cUpgradeTypeEnum.Zerg_Melee_Attacks)
    Zerg_Missile_Attacks = UpgradeType(cUpgradeTypeEnum.Zerg_Missile_Attacks)
    Zerg_Flyer_Attacks = UpgradeType(cUpgradeTypeEnum.Zerg_Flyer_Attacks)
    Protoss_Ground_Weapons = UpgradeType(cUpgradeTypeEnum.Protoss_Ground_Weapons)
    Protoss_Air_Weapons = UpgradeType(cUpgradeTypeEnum.Protoss_Air_Weapons)
    Protoss_Plasma_Shields = UpgradeType(cUpgradeTypeEnum.Protoss_Plasma_Shields)
    U_238_Shells = UpgradeType(cUpgradeTypeEnum.U_238_Shells)
    Ion_Thrusters = UpgradeType(cUpgradeTypeEnum.Ion_Thrusters)
    Titan_Reactor = UpgradeType(cUpgradeTypeEnum.Titan_Reactor)
    Ocular_Implants = UpgradeType(cUpgradeTypeEnum.Ocular_Implants)
    Moebius_Reactor = UpgradeType(cUpgradeTypeEnum.Moebius_Reactor)
    Apollo_Reactor = UpgradeType(cUpgradeTypeEnum.Apollo_Reactor)
    Colossus_Reactor = UpgradeType(cUpgradeTypeEnum.Colossus_Reactor)
    Ventral_Sacs = UpgradeType(cUpgradeTypeEnum.Ventral_Sacs)
    Antennae = UpgradeType(cUpgradeTypeEnum.Antennae)
    Pneumatized_Carapace = UpgradeType(cUpgradeTypeEnum.Pneumatized_Carapace)
    Metabolic_Boost = UpgradeType(cUpgradeTypeEnum.Metabolic_Boost)
    Adrenal_Glands = UpgradeType(cUpgradeTypeEnum.Adrenal_Glands)
    Muscular_Augments = UpgradeType(cUpgradeTypeEnum.Muscular_Augments)
    Grooved_Spines = UpgradeType(cUpgradeTypeEnum.Grooved_Spines)
    Gamete_Meiosis = UpgradeType(cUpgradeTypeEnum.Gamete_Meiosis)
    Metasynaptic_Node = UpgradeType(cUpgradeTypeEnum.Metasynaptic_Node)
    Singularity_Charge = UpgradeType(cUpgradeTypeEnum.Singularity_Charge)
    Leg_Enhancements = UpgradeType(cUpgradeTypeEnum.Leg_Enhancements)
    Scarab_Damage = UpgradeType(cUpgradeTypeEnum.Scarab_Damage)
    Reaver_Capacity = UpgradeType(cUpgradeTypeEnum.Reaver_Capacity)
    Gravitic_Drive = UpgradeType(cUpgradeTypeEnum.Gravitic_Drive)
    Sensor_Array = UpgradeType(cUpgradeTypeEnum.Sensor_Array)
    Gravitic_Boosters = UpgradeType(cUpgradeTypeEnum.Gravitic_Boosters)
    Khaydarin_Amulet = UpgradeType(cUpgradeTypeEnum.Khaydarin_Amulet)
    Apial_Sensors = UpgradeType(cUpgradeTypeEnum.Apial_Sensors)
    Gravitic_Thrusters = UpgradeType(cUpgradeTypeEnum.Gravitic_Thrusters)
    Carrier_Capacity = UpgradeType(cUpgradeTypeEnum.Carrier_Capacity)
    Khaydarin_Core = UpgradeType(cUpgradeTypeEnum.Khaydarin_Core)
    Argus_Jewel = UpgradeType(cUpgradeTypeEnum.Argus_Jewel)
    Argus_Talisman = UpgradeType(cUpgradeTypeEnum.Argus_Talisman)
    Caduceus_Reactor = UpgradeType(cUpgradeTypeEnum.Caduceus_Reactor)
    Chitinous_Plating = UpgradeType(cUpgradeTypeEnum.Chitinous_Plating)
    Anabolic_Synthesis = UpgradeType(cUpgradeTypeEnum.Anabolic_Synthesis)
    Charon_Boosters = UpgradeType(cUpgradeTypeEnum.Charon_Boosters)
    none = UpgradeType(cUpgradeTypeEnum.None)
    Unknown = UpgradeType(cUpgradeTypeEnum.Unknown)

    class Enum(IntEnum):
        Terran_Infantry_Armor = cUpgradeTypeEnum.Terran_Infantry_Armor
        Terran_Vehicle_Plating = cUpgradeTypeEnum.Terran_Vehicle_Plating
        Terran_Ship_Plating = cUpgradeTypeEnum.Terran_Ship_Plating
        Zerg_Carapace = cUpgradeTypeEnum.Zerg_Carapace
        Zerg_Flyer_Carapace = cUpgradeTypeEnum.Zerg_Flyer_Carapace
        Protoss_Ground_Armor = cUpgradeTypeEnum.Protoss_Ground_Armor
        Protoss_Air_Armor = cUpgradeTypeEnum.Protoss_Air_Armor
        Terran_Infantry_Weapons = cUpgradeTypeEnum.Terran_Infantry_Weapons
        Terran_Vehicle_Weapons = cUpgradeTypeEnum.Terran_Vehicle_Weapons
        Terran_Ship_Weapons = cUpgradeTypeEnum.Terran_Ship_Weapons
        Zerg_Melee_Attacks = cUpgradeTypeEnum.Zerg_Melee_Attacks
        Zerg_Missile_Attacks = cUpgradeTypeEnum.Zerg_Missile_Attacks
        Zerg_Flyer_Attacks = cUpgradeTypeEnum.Zerg_Flyer_Attacks
        Protoss_Ground_Weapons = cUpgradeTypeEnum.Protoss_Ground_Weapons
        Protoss_Air_Weapons = cUpgradeTypeEnum.Protoss_Air_Weapons
        Protoss_Plasma_Shields = cUpgradeTypeEnum.Protoss_Plasma_Shields
        U_238_Shells = cUpgradeTypeEnum.U_238_Shells
        Ion_Thrusters = cUpgradeTypeEnum.Ion_Thrusters
        Titan_Reactor = cUpgradeTypeEnum.Titan_Reactor
        Ocular_Implants = cUpgradeTypeEnum.Ocular_Implants
        Moebius_Reactor = cUpgradeTypeEnum.Moebius_Reactor
        Apollo_Reactor = cUpgradeTypeEnum.Apollo_Reactor
        Colossus_Reactor = cUpgradeTypeEnum.Colossus_Reactor
        Ventral_Sacs = cUpgradeTypeEnum.Ventral_Sacs
        Antennae = cUpgradeTypeEnum.Antennae
        Pneumatized_Carapace = cUpgradeTypeEnum.Pneumatized_Carapace
        Metabolic_Boost = cUpgradeTypeEnum.Metabolic_Boost
        Adrenal_Glands = cUpgradeTypeEnum.Adrenal_Glands
        Muscular_Augments = cUpgradeTypeEnum.Muscular_Augments
        Grooved_Spines = cUpgradeTypeEnum.Grooved_Spines
        Gamete_Meiosis = cUpgradeTypeEnum.Gamete_Meiosis
        Metasynaptic_Node = cUpgradeTypeEnum.Metasynaptic_Node
        Singularity_Charge = cUpgradeTypeEnum.Singularity_Charge
        Leg_Enhancements = cUpgradeTypeEnum.Leg_Enhancements
        Scarab_Damage = cUpgradeTypeEnum.Scarab_Damage
        Reaver_Capacity = cUpgradeTypeEnum.Reaver_Capacity
        Gravitic_Drive = cUpgradeTypeEnum.Gravitic_Drive
        Sensor_Array = cUpgradeTypeEnum.Sensor_Array
        Gravitic_Boosters = cUpgradeTypeEnum.Gravitic_Boosters
        Khaydarin_Amulet = cUpgradeTypeEnum.Khaydarin_Amulet
        Apial_Sensors = cUpgradeTypeEnum.Apial_Sensors
        Gravitic_Thrusters = cUpgradeTypeEnum.Gravitic_Thrusters
        Carrier_Capacity = cUpgradeTypeEnum.Carrier_Capacity
        Khaydarin_Core = cUpgradeTypeEnum.Khaydarin_Core
        Argus_Jewel = cUpgradeTypeEnum.Argus_Jewel
        Argus_Talisman = cUpgradeTypeEnum.Argus_Talisman
        Caduceus_Reactor = cUpgradeTypeEnum.Caduceus_Reactor
        Chitinous_Plating = cUpgradeTypeEnum.Chitinous_Plating
        Anabolic_Synthesis = cUpgradeTypeEnum.Anabolic_Synthesis
        Charon_Boosters = cUpgradeTypeEnum.Charon_Boosters
        none = cUpgradeTypeEnum.None
        Unknown = cUpgradeTypeEnum.Unknown
