﻿from libcpp cimport bool
from cybw.cInterface cimport Interface

cdef extern from "BWAPI/Bullet.h" namespace "BWAPI":
    cdef cppclass PlayerInterface:
        pass
    cdef cppclass BulletType:
        pass
    cdef cppclass UnitInterface:
        pass
    ctypedef PlayerInterface *Player
    ctypedef UnitInterface *Unit;
    ctypedef BulletInterface *Bullet
    cdef cppclass Position:
        pass

    cdef cppclass BulletInterface(Interface[BulletInterface]):
        int getID() const
        bool exists() const
        Player getPlayer() const
        BulletType getType() const
        Unit getSource() const
        Position getPosition() const
        double getAngle() const
        double getVelocityX() const
        double getVelocityY() const
        Unit getTarget() const
        Position getTargetPosition() const
        int getRemoveTimer() const
        bool isVisible(Player player) const
        bool isVisible() const