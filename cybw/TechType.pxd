﻿cimport cybw.cTechType as cTechType
from .cSetContainer cimport SetContainer

cdef class TechType:
    cdef cTechType.TechType thisobj

cdef TechTypeFactory(cTechType.TechType ctt)

cdef TechTypeSetFactory(const SetContainer[cTechType.TechType]& cset)
