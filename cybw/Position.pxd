﻿cimport cybw.cPosition as cPosition

cdef class Position(PositionUnitConverter):
    cdef cPosition.Position thisobj

cdef class WalkPosition:
    cdef cPosition.WalkPosition thisobj

cdef class TilePosition:
    cdef cPosition.TilePosition thisobj

cdef PositionFactory(cPosition.Position positionptr)

cdef WalkPositionFactory(cPosition.WalkPosition walkptr)

cdef TilePositionFactory(cPosition.TilePosition tileptr)

cdef ConstPositionListFactory(const cPosition.Position_list &c_set)

cdef PositionListFactory(cPosition.Position_list &c_set)

cdef TilePositionListFactory(cPosition.TilePosition_list &c_set)
