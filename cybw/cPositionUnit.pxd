﻿from libcpp cimport bool
from cybw cimport cPosition
from cybw cimport cUnit

cdef extern from "BWAPI/PositionUnit.h" namespace "BWAPI":
    #cdef cppclass UnitInterface:
    #    pass

    #ctypedef UnitInterface *Unit

    cdef cppclass PositionOrUnit:
        PositionOrUnit() #fake for nullptr unit
        PositionOrUnit(cUnit.Unit unit)
        PositionOrUnit(cPosition.Position pos)

        PositionOrUnit opAssign "&operator =" (cUnit.Unit pUnit)
        PositionOrUnit opAssign "&operator =" (cPosition.Position pos)

        bool isUnit() const
        cUnit.Unit getUnit() const

        bool isPosition() const
        cPosition.Position getPosition() const