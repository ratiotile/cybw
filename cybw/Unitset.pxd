﻿cimport cybw.cUnitset as cUnitset

'''
cdef class ConstUnitset_iterator:
    cdef cUnitset.Unitset.iterator it
    cdef c_UnitSet* set

cdef class Unitset_iterator:
    cdef cUnitset.Unitset.iterator it
    cdef c_UnitSet* set
'''

cdef class ConstUnitset(set):
    cdef const cUnitset.Unitset* thisptr

cdef class Unitset(set):
    cdef cUnitset.Unitset* thisptr

cdef UnitsetFactory(cUnitset.Unitset &cunitset)

cdef ConstUnitsetFactory(const cUnitset.Unitset &cunitset)