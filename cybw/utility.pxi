#from cybw.Unit cimport Unit

def getBoundingBox(Unit rUnit, int mapWidth, int mapHeight, int offset=0, bool crop=True):
    unitX = rUnit.getPosition().x
    unitY = rUnit.getPosition().y
    left = unitX - offset - rUnit.getType().dimensionLeft()
    right = unitX + offset + rUnit.getType().dimensionRight()
    top = unitY - offset - rUnit.getType().dimensionUp()
    bottom = unitY + offset + rUnit.getType().dimensionDown()
    if crop:
        (left, top, right, bottom) = cropToMap(left, top, right, bottom, mapWidth, mapHeight)
    return (left, top, right, bottom)

def getTileBounds(int left, int top, int right, int bottom, bool floor=True):
    left = floorTile( left )
    top = floorTile( top )
    if floor == True:
        right = floorTile( right )
        bottom = floorTile( bottom )
    else:
        right = ceilTile( right )
        bottom = ceilTile( bottom )
    return (left, top, right, bottom)

def cropToMap(int left, int top, int right, int bottom,
              mapWidth, mapHeight):
    if (left < 0):
        left = 0
    if (top < 0):
        top = 0
    if (right >= mapWidth):
        right = mapWidth - 1
    if (bottom >= mapHeight):
        bottom = mapHeight - 1
    return (left, top, right, bottom)

def floorTile(int pixels):
    return pixels // 32


def ceilTile(int pixels):
    return ( pixels + 31 ) // 32

def BtoP(int tile):
    return tile << 5

def PtoB(int tile):
    return tile >> 5

# 8 pixels in a walk tile
def PtoW(int pixel):
    return pixel >> 3

# 4 walk tiles in a build tile
def BtoW(int tile):
    return tile << 2

#/ convert walk tile coordinate to Build Tile
def WtoB(int walk):
    return walk >> 2

#/ convert walktile coordinate to Position
def WtoP(int walk):
    return walk << 3