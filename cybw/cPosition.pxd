﻿# from cVectorset cimport Vectorset
from cybw.libcpp.deque cimport deque

cdef extern from *:  # trick to make constant template parameter work
    ctypedef int pScale "1"
    ctypedef int wScale "8"
    ctypedef int bScale "32"

cdef extern from "BWAPI/Position.h" namespace "BWAPI":
    cdef cppclass Point[T,Scale]:
        Point()
        Point(T _x, T _y)
        # new, no idea what this does...
        _NT Point[_NT](const Point[_NT, Scale] &pt)

        # conversion constructor TODO
        #Point[_NT] Point(const Point[_NT,0] &pt)
        # manually expand this meta-template
        #point[NT, NScale] Point[NT, NScale](const Point[NT,NScale] &pt)
        Point[T, Scale] Point(const Point[T,pScale] &pt)
        Point[T, Scale] FromWP "Point"(const Point[T,wScale] &pt)
        Point[T, Scale] FromTP "Point"(const Point[T,bScale] &pt)
        #operators
        bint opbool "operator bool" () nogil
        bint operator == (const Point[T,Scale] &pos) const
        bint operator != (const Point[T,Scale] &pos) const
        bint operator  < (const Point[T,Scale] &position) const

        # Point[T, Scale] &operator += (const Point[T, Scale] &p)
        Point operator + (const Point &pos) const
        # Point[T, Scale] &operator -= (const Point[T, Scale] &p)
        Point operator - (const Point &pos) const
        # Point[T, Scale] &operator *= (const Point[T, Scale] &p)
        Point operator * (const T &v) const

        # Point[T, Scale] &operator |= (const Point[T, Scale] &p)
        Point operator | (const T &v) const
        # Point[T, Scale] &operator &= (const Point[T, Scale] &p)
        Point operator & (const T &v) const
        # Point[T, Scale] &operator ^= (const Point[T, Scale] &p)
        Point operator ^ (const T &v) const
        #Point[T, Scale] &operator /= (const Point[T, Scale] &p)
        Point operator / (const T &v) const

        Point operator % (const T &v) const
        # Point[T, Scale] &operator %= (const Point[T, Scale] &p)

        #friend std::ostream &operator << (std::ostream &out, const Point<T,Scale> &pt)
        #friend std::istream &operator >> (std::istream &in, Point<T,Scale> &pt)

        bint isValid() const
        Point &makeValid()
        double getDistance(const Point[T,Scale] &position) const
        double getLength() const
        int getApproxDistance(const Point[T,Scale] &position) const
        Point &setMax(T max_x, T max_y)
        Point &setMax(const Point[T,Scale] &max)
        Point &setMin(T min_x, T min_y)
        Point &setMin(const Point[T,Scale] &min)
        T x
        T y

    ctypedef Point[int,pScale] Position
    ctypedef Point[int,wScale] WalkPosition
    ctypedef Point[int,bScale] TilePosition


cdef extern from * namespace "std":
    cdef cppclass deque[T]:
        pass

cdef extern from "BWAPI/Position.h" namespace "BWAPI::Position":
    ctypedef deque[Point[int,pScale]] Position_list "BWAPI::Position::list"

cdef extern from "BWAPI/Position.h" namespace "BWAPI::WalkPosition":
    ctypedef deque[Point[int,wScale]] WalkPosition_list "BWAPI::WalkPosition::list"

cdef extern from "BWAPI/Position.h" namespace "BWAPI::TilePosition":
    ctypedef deque[Point[int,bScale]] TilePosition_list "BWAPI::TilePosition::list"

cdef extern from "BWAPI/Position.h":
    Position Positions_Invalid "BWAPI::Positions::Invalid"
    Position Positions_None "BWAPI::Positions::None"
    Position Positions_Unknown "BWAPI::Positions::Unknown"
    Position Positions_Origin "BWAPI::Positions::Origin"

    WalkPosition WalkPositions_Invalid "BWAPI::WalkPositions::Invalid"
    WalkPosition WalkPositions_None "BWAPI::WalkPositions::None"
    WalkPosition WalkPositions_Unknown "BWAPI::WalkPositions::Unknown"
    WalkPosition WalkPositions_Origin "BWAPI::WalkPositions::Origin"

    TilePosition TilePositions_Invalid "BWAPI::TilePositions::Invalid"
    TilePosition TilePositions_None "BWAPI::TilePositions::None"
    TilePosition TilePositions_Unknown "BWAPI::TilePositions::Unknown"
    TilePosition TilePositions_Origin "BWAPI::TilePositions::Origin"