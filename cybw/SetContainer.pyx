cimport cybw.cSetContainer as cSetContainer
from cython.operator cimport dereference as deref


cdef class SetContainer:

    class iterator:
        def __next__():

    # required to make iterable
    def __len__(self):
        return self.size()

    def __contains__(self, v):

    def __iter__():
