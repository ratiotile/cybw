﻿cimport cybw.cGame as cGame


from cybw cimport cPosition
from cybw cimport cUnitset

from cybw.Color cimport Color
from cybw.cText cimport TextSizeEnum

from cybw.TechType cimport TechType
from cybw.UpgradeType cimport UpgradeType
from cybw.GameType cimport GameType, GameTypeFactory
from cybw.Force cimport Force, ForceFactory
from cybw.Forceset cimport ForceListFactory

from cybw.Input cimport MouseButton, Key

from cybw.Unitset cimport Unitset, UnitsetFactory, ConstUnitsetFactory
from cybw.UnitCommand cimport UnitCommand

def checkThisPtr(func):
    def _checkThisPtr(self,*args,**kwargs):
        if self.isNull():
            print("thisptr is NULL!")
            return None
        else:
            return func(self,*args,**kwargs)
    return _checkThisPtr


cdef class BroodwarClass:
    cdef cGame.Game* thisptr

    def __cinit__(self):
        self.thisptr = cGame.Broodwar.opStructDereference()

    def isNull(self):
        if self.thisptr == NULL and cGame.BroodwarPtr != NULL:
            self.updatePtr()
            return False
        return self.thisptr == NULL

    def updatePtr(self):
        self.thisptr = cGame.Broodwar.opStructDereference()

    def __lshift__(self, text): # << stream operator
        if not isinstance(text,str):
            text = str(text)
        # split on newlines, if '' is left
        tokens = text.split('\n')
        length = len(tokens)
        cdef string s
        while length > 0:
            token = tokens.pop(0)
            length = len(tokens)
            s = token.encode('utf-8')
            cGame.Broodwar << s
            # check for case of empty string at end, signifies newline
            if ((length == 0 and token == '')
            or (length > 1) ):
                cGame.Broodwar << cGame.endl

        return self

    @checkThisPtr
    def getForces(self):
        return ForceListFactory(self.thisptr.getForces())

    @checkThisPtr
    def getPlayers(self):
        ''' Playerset from BWAPI and translate it into a List of Players '''
        return ConstPlayerSetFactory(self.thisptr.getPlayers())

    @checkThisPtr
    def getAllUnits(self):
        return ConstUnitsetFactory(self.thisptr.getAllUnits())

    @checkThisPtr
    def getMinerals(self):
        return ConstUnitsetFactory(self.thisptr.getMinerals())

    @checkThisPtr
    def getGeysers(self):
        return ConstUnitsetFactory(self.thisptr.getGeysers())

    @checkThisPtr
    def getNeutralUnits(self):
        return ConstUnitsetFactory(self.thisptr.getNeutralUnits())

    @checkThisPtr
    def getStaticMinerals(self):
        return ConstUnitsetFactory(self.thisptr.getStaticMinerals())

    @checkThisPtr
    def getStaticGeysers(self):
        return ConstUnitsetFactory(self.thisptr.getStaticGeysers())

    @checkThisPtr
    def getStaticNeutralUnits(self):
        return ConstUnitsetFactory(self.thisptr.getStaticNeutralUnits())

    @checkThisPtr
    def getBullets(self):
        return ConstBulletSetFactory(self.thisptr.getBullets())

    @checkThisPtr
    def getNukeDots(self):
        return ConstPositionListFactory(self.thisptr.getNukeDots())

    @checkThisPtr
    def getEvents(self):
        ''' Transcribe to a Python list of Events '''
        return EventListFactory(self.thisptr.getEvents())

    @checkThisPtr
    def getForce(self, int forceID):
        return ForceFactory(self.thisptr.getForce(forceID))

    @checkThisPtr
    def getPlayer(self, int playerID):
        return PlayerFactory(self.thisptr.getPlayer(playerID))

    @checkThisPtr
    def getUnit(self, int unitID):
        return UnitFactory(self.thisptr.getUnit(unitID))

    @checkThisPtr
    def indexToUnit(self, int unitIndex):
        return UnitFactory(self.thisptr.indexToUnit(unitIndex))

    @checkThisPtr
    def getRegion(self, int regionID):
        return RegionFactory(self.thisptr.getRegion(regionID))

    @checkThisPtr
    def getGameType(self):
        return GameTypeFactory(self.thisptr.getGameType())

    @checkThisPtr
    def getLatency(self):
        return self.thisptr.getLatency()

    @checkThisPtr
    def getFrameCount(self):
        return self.thisptr.getFrameCount()

    @checkThisPtr
    def getReplayFrameCount(self):
        return self.thisptr.getReplayFrameCount()

    @checkThisPtr
    def getFPS(self):
        return self.thisptr.getFPS()

    @checkThisPtr
    def getAverageFPS(self):
        return self.thisptr.getAverageFPS()

    @checkThisPtr
    def getMousePosition(self):
        return PositionFactory(self.thisptr.getMousePosition())

    @checkThisPtr
    def getMouseState(self, MouseButton button):
        return self.thisptr.getMouseState(button.thisenum)

    @checkThisPtr
    def getKeyState(self, Key key):
        return self.thisptr.getKeyState(key.thisenum)

    @checkThisPtr
    def getScreenPosition(self):
        return PositionFactory(self.thisptr.getScreenPosition())

    @checkThisPtr
    def setScreenPosition(self, Position p, int x = -1, int y = -1):
        if p is not Positions.none:
            self.thisptr.setScreenPosition(p.thisobj)
        elif x>=0 and y>=0:
            self.thisptr.setScreenPosition(x, y)
        else:
            raise TypeError

    @checkThisPtr
    def pingMinimap(self, int x, int y):
        self.thisptr.pingMinimap(x,y)

    @checkThisPtr
    def isFlagEnabled(self, int flag):
        return self.thisptr.isFlagEnabled(flag)

    @checkThisPtr
    def enableFlag(self, int flag):
        self.thisptr.enableFlag(flag)


    @checkThisPtr
    def getUnitsOnTile(self, TilePosition tile = TilePositions.none, int tileX = -1, int tileY = -1):
        if tile is not TilePositions.none:
            return UnitsetFactory(self.thisptr.getUnitsOnTile(tile.thisobj))
        elif tileX>=0 and tileY>=0:
            return UnitsetFactory(self.thisptr.getUnitsOnTile(tileX, tileY))
        else:
            raise TypeError


    @checkThisPtr
    def getUnitsInRectangle(self, Position topLeft = None, Position bottomRight = None, int left = -1, int top = -1, int right = -1, int bottom = -1):
        if (topLeft is not None) and (bottomRight is not None):
            return UnitsetFactory(self.thisptr.getUnitsInRectangle(topLeft.thisobj, bottomRight.thisobj))
        elif left>=0 and top>=0 and right>=0 and bottom>=0:
            return UnitsetFactory(self.thisptr.getUnitsInRectangle(left, top, right, bottom))
        else:
            raise TypeError

    @checkThisPtr
    def getUnitsInRadius(self, int radius, Position center = None, int x = -1, int y = -1):
        if center is not None:
            return UnitsetFactory(self.thisptr.getUnitsInRadius(center.thisobj, radius))
        elif x>=0 and y>=0:
            return UnitsetFactory(self.thisptr.getUnitsInRadius(x, y, radius))
        else:
            raise TypeError

    @checkThisPtr
    def getClosestUnit(self, Position center):
        return UnitFactory(self.thisptr.getClosestUnit(center.thisobj))

    @checkThisPtr
    def mapWidth(self):
        return self.thisptr.mapWidth()

    @checkThisPtr
    def mapHeight(self):
        return self.thisptr.mapHeight()

    @checkThisPtr
    def mapFileName(self):
        return self.thisptr.mapFileName().decode()

    @checkThisPtr
    def mapPathName(self):
        return self.thisptr.mapPathName().decode()

    @checkThisPtr
    def mapName(self):
        return self.thisptr.mapName()

    @checkThisPtr
    def mapHash(self):
        return self.thisptr.mapHash().decode()

    @checkThisPtr
    def isWalkable(self, x=None, y=None):
        if isinstance(x, WalkPosition):
            return self.thisptr.isWalkable((<WalkPosition>x).thisobj)
        else:
            return self.thisptr.isWalkable(x, y)

    @checkThisPtr
    def getGroundHeight(self, TilePosition tile = TilePositions.none, int x = -1, int y = -1):
        if tile is not TilePositions.none:
            return self.thisptr.getGroundHeight(tile.thisobj)
        elif x>=0 and y>=0:
            return self.thisptr.getGroundHeight(x, y)
        else:
            raise TypeError

    @checkThisPtr
    def isBuildable(self, TilePosition position = TilePositions.none, int tileX = -1, int tileY = -1, bool includeBuildings = False):
        if position is not TilePositions.none:
            return self.thisptr.isBuildable(position.thisobj, includeBuildings)
        elif tileX>=0 and tileY>=0:
            return self.thisptr.isBuildable(tileX, tileY, includeBuildings)
        else:
            raise TypeError

    @checkThisPtr
    def isVisible(self, TilePosition position = TilePositions.none, int tileX = -1, int tileY = -1):
        if position is not TilePositions.none:
            return self.thisptr.isVisible(position.thisobj)
        elif tileX>=0 and tileY>=0:
            return self.thisptr.isVisible(tileX, tileY)
        else:
            raise TypeError

    @checkThisPtr
    def isExplored(self, TilePosition position = TilePositions.none, int tileX = -1, int tileY = -1):
        if position is not TilePositions.none:
            return self.thisptr.isExplored(position.thisobj)
        elif tileX>=0 and tileY>=0:
            return self.thisptr.isExplored(tileX, tileY)
        else:
            raise TypeError

    @checkThisPtr
    def hasCreep(self, TilePosition position = TilePositions.none, int tileX = -1, int tileY = -1):
        if position is not TilePositions.none:
            return self.thisptr.hasCreep(position.thisobj)
        elif tileX>=0 and tileY>=0:
            return self.thisptr.hasCreep(tileX, tileY)
        else:
            raise TypeError

    @checkThisPtr
    def hasPowerPrecise(self, Position position = Positions.none, int X = -1, int Y = -1, UnitType unitType = UnitTypes.none):
        if position is not Positions.none:
            return self.thisptr.hasPowerPrecise(position.thisobj, unitType.thisobj)
        elif X>=0 and Y>=0:
            return self.thisptr.hasPowerPrecise(X, Y, unitType.thisobj)
        else:
            raise TypeError

    @checkThisPtr
    def hasPower(self, TilePosition position = TilePositions.none, int tileX = -1, int tileY = -1, int tileWidth = -1, int tileHeight = -1, UnitType unitType = UnitTypes.none):
        if position is not TilePositions.none:
            if tileWidth>=0 and tileHeight>=0:
                return self.thisptr.hasPower(position.thisobj, tileWidth, tileHeight, unitType.thisobj)
            else:
                return self.thisptr.hasPower(position.thisobj, unitType.thisobj)
        elif tileX>=0 and tileY>=0:
            if tileWidth>=0 and tileHeight>=0:
                return self.thisptr.hasPower(tileX, tileY, tileWidth, tileHeight, unitType.thisobj)
            else:
                return self.thisptr.hasPower(tileX, tileY, unitType.thisobj)
        else:
            raise TypeError

    @checkThisPtr
    def canBuildHere(self, TilePosition position, UnitType unitType, Unit builder = None, bool checkExplored = False):
        if builder is not None:
            return self.thisptr.canBuildHere(position.thisobj, unitType.thisobj, builder.thisptr, checkExplored)
        else:
            return self.thisptr.canBuildHere(position.thisobj, unitType.thisobj, NULL, checkExplored)

    @checkThisPtr
    def canMake(self, UnitType type, Unit builder = None):
        if builder is not None:
            return self.thisptr.canMake(type.thisobj, builder.thisptr)
        else:
            return self.thisptr.canMake(type.thisobj)

    @checkThisPtr
    def canResearch(self, TechType type, Unit unit = None, bool checkCanIssueCommandType = True):
        if unit is None:
            self.thisptr.canResearch(type.thisobj, NULL, checkCanIssueCommandType)
        else:
            self.thisptr.canResearch(type.thisobj, unit.thisptr, checkCanIssueCommandType)

    @checkThisPtr
    def canUpgrade(self, UpgradeType type, Unit unit = None, bool checkCanIssueCommandType = True):
        if unit is None:
            self.thisptr.canUpgrade(type.thisobj, NULL, checkCanIssueCommandType)
        else:
            self.thisptr.canUpgrade(type.thisobj, unit.thisptr, checkCanIssueCommandType)

    @checkThisPtr
    def getStartLocations(self):
        cdef cPosition.TilePosition_list cStartLocs = self.thisptr.getStartLocations()
        return TilePositionListFactory(cStartLocs)

    @checkThisPtr
    def printf(self, text): #print is taken!
        pyByteStr = text.encode('utf-8')
        cdef char* c_str = pyByteStr
        cGame.Broodwar.opStructDereference().printf(c_str);

    @checkThisPtr
    def sendText(self, text):
        pyByteStr = text.encode('UTF-8')
        cdef char* c_str = pyByteStr
        self.thisptr.sendText(c_str)

    @checkThisPtr
    def sendTextEx(self, bool toAllies, text):
        pyByteStr = text.encode('UTF-8')
        cdef char* c_str = pyByteStr
        self.thisptr.sendTextEx(toAllies, c_str)

    @checkThisPtr
    def isInGame(self):
        return self.thisptr.isInGame()

    @checkThisPtr
    def isMultiplayer(self):
        return self.thisptr.isMultiplayer()

    @checkThisPtr
    def isBattleNet(self):
        return self.thisptr.isBattleNet()

    @checkThisPtr
    def isPaused(self):
        return self.thisptr.isPaused()

    @checkThisPtr
    def isReplay(self):
        return self.thisptr.isReplay()

    @checkThisPtr
    def pauseGame(self):
        self.thisptr.pauseGame()

    @checkThisPtr
    def resumeGame(self):
        self.thisptr.resumeGame()

    @checkThisPtr
    def leaveGame(self):
        self.thisptr.leaveGame()

    @checkThisPtr
    def restartGame(self):
        self.thisptr.restartGame()

    @checkThisPtr
    def setLocalSpeed(self, int speed):
        self.thisptr.setLocalSpeed(speed)

    @checkThisPtr
    def issueCommand(self, Unitset units, UnitCommand command):
        return self.thisptr.issueCommand(<const cUnitset.Unitset>deref(units.thisptr), command.thisobj)

    @checkThisPtr
    def getSelectedUnits(self):
        return ConstUnitsetFactory(self.thisptr.getSelectedUnits())

    @checkThisPtr
    def self(self):
        p = PlayerFactory(self.thisptr.self())
        return p

    @checkThisPtr
    def enemy(self):
        return PlayerFactory(self.thisptr.enemy())

    @checkThisPtr
    def neutral(self):
        return PlayerFactory(self.thisptr.neutral())

    @checkThisPtr
    def allies(self):
        ''' Playerset from BWAPI and translate it into a List of Players '''
        return PlayerSetFactory(self.thisptr.allies())

    @checkThisPtr
    def enemies(self):
        ''' Playerset from BWAPI and translate it into a List of Players '''
        return PlayerSetFactory(self.thisptr.enemies())

    @checkThisPtr
    def observers(self):
        ''' Playerset from BWAPI and translate it into a List of Players '''
        return PlayerSetFactory(self.thisptr.observers())

    @checkThisPtr
    def setTextSize(self, size = None):
        if size is None:
            self.thisptr.setTextSize()
        cdef TextSizeEnum isize = int(size)
        self.thisptr.setTextSize(isize)

    @checkThisPtr
    def drawTextMap(self, Position p, text):
        pyByteStr = text.encode('utf-8')
        cdef char* c_str = pyByteStr
        with nogil:
            self.thisptr.drawTextMap(p.thisobj, c_str)

    @checkThisPtr
    def drawTextMouse(self, Position p, text):
        pyByteStr = text.encode('utf-8')
        cdef char* c_str = pyByteStr
        with nogil:
            self.thisptr.drawTextMouse(p.thisobj, c_str)

    @checkThisPtr
    def drawTextScreen(self, Position p, text):
        pyByteStr = text.encode('utf-8')
        cdef char* c_str = pyByteStr
        with nogil:
            self.thisptr.drawTextScreen(p.thisobj, c_str)

    @checkThisPtr
    def drawBoxMap(self, Position leftTop, Position rightBottom, Color color, bool isSolid = False):
        with nogil:
            self.thisptr.drawBoxMap(leftTop.thisobj, rightBottom.thisobj, deref(color.thisptr), isSolid)

    @checkThisPtr
    def drawBoxMouse(self, Position leftTop, Position rightBottom, Color color, bool isSolid = False):
        with nogil:
            self.thisptr.drawBoxMouse(leftTop.thisobj, rightBottom.thisobj, deref(color.thisptr), isSolid)

    @checkThisPtr
    def drawBoxScreen(self, Position leftTop, Position rightBottom, Color color, bool isSolid=False):
        with nogil:
            self.thisptr.drawBoxScreen(leftTop.thisobj, rightBottom.thisobj, deref(color.thisptr),isSolid)

    @checkThisPtr
    def drawTriangleMap(self, Position a, Position b, Position c, Color color, bool isSolid = False):
        with nogil:
            self.thisptr.drawTriangleMap(a.thisobj, b.thisobj, c.thisobj, deref(color.thisptr), isSolid)

    @checkThisPtr
    def drawTriangleMouse(self, Position a, Position b, Position c, Color color, bool isSolid = False):
        with nogil:
            self.thisptr.drawTriangleMouse(a.thisobj, b.thisobj, c.thisobj, deref(color.thisptr), isSolid)

    @checkThisPtr
    def drawTriangleScreen(self, Position a, Position b, Position c, Color color, bool isSolid=False):
        with nogil:
            self.thisptr.drawTriangleScreen(a.thisobj, b.thisobj, c.thisobj, deref(color.thisptr),isSolid)

    @checkThisPtr
    def drawCircleMap2(self, int x, int y, int radius, Color color, bool isSolid = False):
        with nogil:
            self.thisptr.drawCircleMap(x, y, radius, deref(color.thisptr), isSolid)

    @checkThisPtr
    def drawCircleMap(self, Position p, int radius, Color color, bool isSolid = False):
        with nogil:
            self.thisptr.drawCircleMap(p.thisobj, radius, deref(color.thisptr), isSolid)

    @checkThisPtr
    def drawCircleMouse(self, Position p, int radius, Color color, bool isSolid=False):
        with nogil:
            self.thisptr.drawCircleMouse(p.thisobj,radius,deref(color.thisptr),isSolid)

    @checkThisPtr
    def drawCircleScreen(self, Position p, int radius, Color color, bool isSolid=False):
        with nogil:
            self.thisptr.drawCircleScreen(p.thisobj,radius,deref(color.thisptr),isSolid)

    @checkThisPtr
    def drawEllipseMap(self, Position p, int xrad, int yrad, Color color, bool isSolid=False):
        with nogil:
            self.thisptr.drawEllipseMap(p.thisobj,xrad,yrad,deref(color.thisptr),isSolid)

    @checkThisPtr
    def drawEllipseMouse(self, Position p, int xrad, int yrad, Color color, bool isSolid=False):
        with nogil:
            self.thisptr.drawEllipseMouse(p.thisobj,xrad,yrad,deref(color.thisptr),isSolid)

    @checkThisPtr
    def drawEllipseScreen(self, Position p, int xrad, int yrad, Color color, bool isSolid=False):
        with nogil:
            self.thisptr.drawEllipseScreen(p.thisobj,xrad,yrad,deref(color.thisptr),isSolid)

    @checkThisPtr
    def drawDotMap(self, Position p, Color color):
        with nogil:
            self.thisptr.drawDotMap(p.thisobj,deref(color.thisptr))

    @checkThisPtr
    def drawDotMouse(self, Position p, Color color):
        with nogil:
            self.thisptr.drawDotMouse(p.thisobj,deref(color.thisptr))

    @checkThisPtr
    def drawDotScreen(self, Position p, Color color):
        with nogil:
            self.thisptr.drawDotScreen(p.thisobj,deref(color.thisptr))

    @checkThisPtr
    def _drawLineMapP(self, Position a, Position b, Color color):
        with nogil:
            self.thisptr.drawLineMap(a.thisobj,b.thisobj,deref(color.thisptr))

    @checkThisPtr
    def _drawLineMapC(self, int x1, int y1, int x2, int y2, Color color):
        with nogil:
            self.thisptr.drawLineMap(x1, y1, x2, y2, deref(color.thisptr))

    def drawLineMap(self, p_x1, p_y1, c_x2, _y2=None, _c=None):
        if _y2 is None:
            self._drawLineMapP(p_x1, p_y1, c_x2)
        else:
            self._drawLineMapC(p_x1, p_y1, c_x2, _y2, _c)

    @checkThisPtr
    def drawLineMouse(self, Position a, Position b, Color color):
        with nogil:
            self.thisptr.drawLineMouse(a.thisobj,b.thisobj,deref(color.thisptr))

    @checkThisPtr
    def drawLineScreen(self, Position a, Position b, Color color):
        with nogil:
            self.thisptr.drawLineScreen(a.thisobj,b.thisobj,deref(color.thisptr))

    @checkThisPtr
    def getLatencyFrames(self):
        return self.thisptr.getLatencyFrames()

    @checkThisPtr
    def getLatencyTime(self):
        return self.thisptr.getLatencyTime()

    @checkThisPtr
    def getRemainingLatencyFrames(self):
        return self.thisptr.getRemainingLatencyFrames()

    @checkThisPtr
    def getRemainingLatencyTime(self):
        return self.thisptr.getRemainingLatencyTime()

    @checkThisPtr
    def getRevision(self):
        return self.thisptr.getRevision()

    @checkThisPtr
    def isDebug(self):
        return self.thisptr.isDebug()

    @checkThisPtr
    def isLatComEnabled(self):
        return self.thisptr.isLatComEnabled()

    @checkThisPtr
    def setLatCom(self, bool isEnabled):
        self.thisptr.setLatCom(isEnabled)

    @checkThisPtr
    def isGUIEnabled(self):
        return self.thisptr.isGUIEnabled()

    @checkThisPtr
    def setGUI(self, bool enabled):
        self.thisptr.setGUI(enabled)

    @checkThisPtr
    def getInstanceNumber(self):
        return self.thisptr.getInstanceNumber()

    @checkThisPtr
    def getAPM(self, bool includeSelects):
        return self.thisptr.getAPM(includeSelects)

    @checkThisPtr
    def setMap(self, string mapFileName):
        return self.thisptr.setMap(mapFileName)

    @checkThisPtr
    def setFrameSkip(self, int frameskip):
        self.thisptr.setFrameSkip(frameskip)

    @checkThisPtr
    def hasPath(self, Position source, Position destination):
        return self.thisptr.hasPath(source.thisobj, destination.thisobj)

    @checkThisPtr
    def setAlliance(self, Player player, bool allied = True, bool alliedVictory = True):
        return self.thisptr.setAlliance(player.thisptr, allied, alliedVictory)

    @checkThisPtr
    def setVision(self, Player player, bool enabled = True):
        return self.thisptr.setVision(player.thisptr, enabled)

    @checkThisPtr
    def elapsedTime(self):
        return self.thisptr.elapsedTime()

    @checkThisPtr
    def setCommandOptimizationLevel(self, int level):
        self.thisptr.setCommandOptimizationLevel(level)

    @checkThisPtr
    def countdownTimer(self):
        return self.thisptr.countdownTimer()

    @checkThisPtr
    def getAllRegions(self):
        return ConstRegionSetFactory(self.thisptr.getAllRegions())

    @checkThisPtr
    def getRegionAt(self, Position position = Positions.none,
                    int x = -1, int y = -1):
        if position is not Positions.none:
            return RegionFactory(self.thisptr.getRegionAt(position.thisobj))
        elif x >= 0 and y >= 0:
            return RegionFactory(self.thisptr.getRegionAt(x,y))
        else:
            raise TypeError

    @checkThisPtr
    def getLastEventTime(self):
        return self.thisptr.getLastEventTime()

    @checkThisPtr
    def setRevealAll(self, bool reveal = True):
        self.thisptr.setRevealAll(reveal)

    @checkThisPtr
    def getBuildLocation(self, UnitType unittype, TilePosition desiredPosition, int maxRange = 64, bool creep = False):
        return TilePositionFactory(self.thisptr.getBuildLocation(unittype.thisobj, desiredPosition.thisobj, maxRange, creep))

    @checkThisPtr
    def getDamageFrom(self, UnitType fromType, UnitType toType, Player fromPlayer = None, Player toPlayer = None):
        if (fromPlayer is None) and (toPlayer is None):
            return self.thisptr.getDamageFrom(fromType.thisobj, toType.thisobj, NULL, NULL)
        elif toPlayer is None:
            return self.thisptr.getDamageFrom(fromType.thisobj, toType.thisobj, fromPlayer.thisptr, NULL)
        elif fromPlayer is None:
            return self.thisptr.getDamageFrom(fromType.thisobj, toType.thisobj, NULL, toPlayer.thisptr)
        else:
            return self.thisptr.getDamageFrom(fromType.thisobj, toType.thisobj, fromPlayer.thisptr, toPlayer.thisptr)

    @checkThisPtr
    def getDamageTo(self, UnitType toType, UnitType fromType, Player toPlayer = None, Player fromPlayer = None):
        if (fromPlayer is None) and (toPlayer is None):
            return self.thisptr.getDamageTo(fromType.thisobj, toType.thisobj, NULL, NULL)
        elif toPlayer is None:
            return self.thisptr.getDamageTo(fromType.thisobj, toType.thisobj, fromPlayer.thisptr, NULL)
        elif fromPlayer is None:
            return self.thisptr.getDamageTo(fromType.thisobj, toType.thisobj, NULL, toPlayer.thisptr)
        else:
            return self.thisptr.getDamageTo(fromType.thisobj, toType.thisobj, fromPlayer.thisptr, toPlayer.thisptr)

Broodwar = BroodwarClass()
