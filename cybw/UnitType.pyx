﻿from cybw.cUnitType cimport UnitType as cUnitType
from cybw cimport cUnitType as m_cUnitType
cimport cybw.cUnitTypesEnum as cUnitTypesEnum
from enum import IntEnum
from cybw.Race cimport Race, RaceFactory
from cybw.TechType cimport TechTypeFactory, TechTypeSetFactory
from cybw.UpgradeType cimport UpgradeTypeFactory, UpgradeTypeSetFactory
from cybw.UnitSizeType cimport UnitSizeType, UnitSizeTypeFactory
from cybw.WeaponType cimport WeaponType, WeaponTypeFactory


cdef class UnitType:
    def __cinit__(self,id = -1):
        if(id >= 0 and id < cUnitTypesEnum.MAX):
            self.thisobj = cUnitType(id)

    def __init__(self,id = -1):
        pass #wtf cUnitType undefined
        #if(id >= 0 and id < cUnitType.MAX):
        #    self.thisobj = cUnitType.UnitType(id)

    def __str__(self):
        return self.getName()

    def __repr__(self):
        return "<cybw.UnitType {}>".format(self.getName())

    def __richcmp__(x, y, int op):
        if op == 0:     # lt
            return x.getID() < y.getID()
        elif op == 2:   # eq
            return x.getID() == y.getID()
        elif op == 4:   # gt
            return x.getID() > y.getID()
        elif op == 1:   # lte
            return x.getID() <= y.getID()
        elif op == 3:   # ne
            return x.getID() != y.getID()
        elif op == 5:   # gte
            return x.getID() >= y.getID()
        return False

    def __hash__(self):
        return self.getID()

    def getName(self):
        name = self.thisobj.getName()
        pystr = name.decode()
        return pystr

    def getID(self):
        return self.thisobj.getID()

    def getRace(self):
        return RaceFactory(self.thisobj.getRace())

    def requiredTech(self):
        return TechTypeFactory(self.thisobj.requiredTech())

    def requiredUnits(self):
        t_map = self.thisobj.requiredUnits()
        cdef cUnitType ut
        temp_dict = {}
        for pair in t_map:
            ut = pair.first
            newUT = UnitType(ut.getID())
            num = pair.second
            temp_dict[newUT] = num
        return temp_dict

    def whatBuilds(self):
        t_pair = self.thisobj.whatBuilds()
        cdef cUnitType ut = t_pair.first
        num = t_pair.second
        return (UnitTypeFactory(ut), num)

    def cloakingTech(self):
        return TechTypeFactory(self.thisobj.cloakingTech())

    def armorUpgrade(self):
        return UpgradeTypeFactory(self.thisobj.armorUpgrade())

    def maxHitPoints(self):
        return self.thisobj.maxHitPoints()

    def maxShields(self):
        return self.thisobj.maxShields()

    def maxEnergy(self):
        return self.thisobj.maxEnergy()

    def armor(self):
        return self.thisobj.armor()

    def mineralPrice(self):
        return self.thisobj.mineralPrice()

    def gasPrice(self):
        return self.thisobj.gasPrice()

    def buildTime(self):
        return self.thisobj.buildTime()

    def supplyRequired(self):
        return self.thisobj.supplyRequired()

    def supplyProvided(self):
        return self.thisobj.supplyProvided()

    def spaceRequired(self):
        return self.thisobj.spaceRequired()

    def spaceProvided(self):
        return self.thisobj.spaceProvided()

    def buildScore(self):
        return self.thisobj.buildScore()

    def destroyScore(self):
        return self.thisobj.destroyScore()

    def size(self):
        return UnitSizeTypeFactory(self.thisobj.size())

    def tileWidth(self):
        return self.thisobj.tileWidth()

    def tileHeight(self):
        return self.thisobj.tileHeight()

    def tileSize(self):
        return TilePositionFactory(self.thisobj.tileSize())

    def dimensionLeft(self):
        return self.thisobj.dimensionLeft()

    def dimensionUp(self):
        return self.thisobj.dimensionUp()

    def dimensionRight(self):
        return self.thisobj.dimensionRight()

    def dimensionDown(self):
        return self.thisobj.dimensionDown()

    def width(self):
        return self.thisobj.width()

    def height(self):
        return self.thisobj.height()

    def seekRange(self):
        return self.thisobj.seekRange()

    def sightRange(self):
        return self.thisobj.sightRange()

    def groundWeapon(self):
        return WeaponTypeFactory(self.thisobj.groundWeapon())

    def maxGroundHits(self):
        return self.thisobj.maxGroundHits()

    def airWeapon(self):
        return WeaponTypeFactory(self.thisobj.airWeapon())

    def maxAirHits(self):
        return self.thisobj.maxAirHits()

    def topSpeed(self):
        return self.thisobj.topSpeed()

    def acceleration(self):
        return self.thisobj.acceleration()

    def haltDistance(self):
        return self.thisobj.haltDistance()

    def turnRadius(self):
        return self.thisobj.turnRadius()

    def canProduce(self):
        return self.thisobj.canProduce()

    def canAttack(self):
        return self.thisobj.canAttack()

    def canMove(self):
        return self.thisobj.canMove()

    def isFlyer(self):
        return self.thisobj.isFlyer()

    def regeneratesHP(self):
        return self.thisobj.regeneratesHP()

    def isSpellcaster(self):
        return self.thisobj.isSpellcaster()

    def hasPermanentCloak(self):
        return self.thisobj.hasPermanentCloak()

    def isInvincible(self):
        return self.thisobj.isInvincible()

    def isOrganic(self):
        return self.thisobj.isOrganic()

    def isMechanical(self):
        return self.thisobj.isMechanical()

    def isRobotic(self):
        return self.thisobj.isRobotic()

    def isDetector(self):
        return self.thisobj.isDetector()

    def isResourceContainer(self):
        return self.thisobj.isResourceContainer()

    def isResourceDepot(self):
        return self.thisobj.isResourceDepot()

    def isRefinery(self):
        return self.thisobj.isRefinery()

    def isWorker(self):
        return self.thisobj.isWorker()

    def requiresPsi(self):
        return self.thisobj.requiresPsi()

    def requiresCreep(self):
        return self.thisobj.requiresCreep()

    def isTwoUnitsInOneEgg(self):
        return self.thisobj.isTwoUnitsInOneEgg()

    def isBurrowable(self):
        return self.thisobj.isBurrowable()

    def isCloakable(self):
        return self.thisobj.isCloakable()

    def isBuilding(self):
        return self.thisobj.isBuilding()

    def isAddon(self):
        return self.thisobj.isAddon()

    def isFlyingBuilding(self):
        return self.thisobj.isFlyingBuilding()

    def isNeutral(self):
        return self.thisobj.isNeutral()

    def isHero(self):
        return self.thisobj.isHero()

    def isPowerup(self):
        return self.thisobj.isPowerup()

    def isBeacon(self):
        return self.thisobj.isBeacon()

    def isFlagBeacon(self):
        return self.thisobj.isFlagBeacon()

    def isSpecialBuilding(self):
        return self.thisobj.isSpecialBuilding()

    def isSpell(self):
        return self.thisobj.isSpell()

    def producesLarva(self):
        return self.thisobj.producesLarva()

    def isMineralField(self):
        return self.thisobj.isMineralField()

    def isCritter(self):
        return self.thisobj.isCritter()

    def canBuildAddon(self):
        return self.thisobj.canBuildAddon()

    def researchesWhat(self):
        return TechTypeSetFactory(self.thisobj.researchesWhat())

    def upgradesWhat(self):
        return UpgradeTypeSetFactory(self.thisobj.upgradesWhat())



cdef UnitType UnitTypeFactory(cUnitType unittype):
    newUnitType = UnitType()
    newUnitType.thisobj = unittype
    #print("UTF created UnitType", newUnitType.getName())
    return newUnitType

cdef class UnitTypeSet(set):
    def __init__(self):
        super().__init__()

cdef UnitTypeSet UnitTypeSetFactory(cUnitType.set &cset):
    unittypeset = UnitTypeSet()
    unittypeset.thisobj = cset
    for i in cset:
        unittypeset.add(UnitTypeFactory(i))
    return unittypeset

cdef ConstUnitTypeSetFactory(const cUnitType.set &cset):
    ut_set = UnitTypeSet()
    ut_set.thisobj = cset
    for i in cset:
        ut_set.add(UnitTypeFactory(i))
    return ut_set

cdef UnitTypeListFactory(cUnitType.list &utlist):
    unittypelist = []
    for i in utlist:
        unittypelist.append(UnitTypeFactory(i))
    return unittypelist


#Enum UnitTypes
cdef class UnitTypes:
    @staticmethod
    def maxUnitWidth():
        return m_cUnitType.maxUnitWidth()

    @staticmethod
    def maxUnitHeight():
        return m_cUnitType.maxUnitHeight()

    @staticmethod
    def allUnitTypes():
        return ConstUnitTypeSetFactory(m_cUnitType.allUnitTypes())
    '''
    class Enum(IntEnum):
        Terran_Marine = cUnitTypesEnum.Terran_Marine
        Terran_Ghost = cUnitTypesEnum.Terran_Ghost
        Terran_Vulture = cUnitTypesEnum.Terran_Vulture
        Terran_Goliath = cUnitTypesEnum.Terran_Goliath
        Terran_Goliath_Turret = cUnitTypesEnum.Terran_Goliath_Turret
        Terran_Siege_Tank_Tank_Mode = cUnitTypesEnum.Terran_Siege_Tank_Tank_Mode
        Terran_Siege_Tank_Tank_Mode_Turret = cUnitTypesEnum.Terran_Siege_Tank_Tank_Mode_Turret
        Terran_SCV = cUnitTypesEnum.Terran_SCV
        Terran_Wraith = cUnitTypesEnum.Terran_Wraith
        Terran_Science_Vessel = cUnitTypesEnum.Terran_Science_Vessel
        Hero_Gui_Montag = cUnitTypesEnum.Hero_Gui_Montag
        Terran_Dropship = cUnitTypesEnum.Terran_Dropship
        Terran_Battlecruiser = cUnitTypesEnum.Terran_Battlecruiser
        Terran_Vulture_Spider_Mine = cUnitTypesEnum.Terran_Vulture_Spider_Mine
        Terran_Nuclear_Missile = cUnitTypesEnum.Terran_Nuclear_Missile
        Terran_Civilian = cUnitTypesEnum.Terran_Civilian
        Hero_Sarah_Kerrigan = cUnitTypesEnum.Hero_Sarah_Kerrigan
        Hero_Alan_Schezar = cUnitTypesEnum.Hero_Alan_Schezar
        Hero_Alan_Schezar_Turret = cUnitTypesEnum.Hero_Alan_Schezar_Turret
        Hero_Jim_Raynor_Vulture = cUnitTypesEnum.Hero_Jim_Raynor_Vulture
        Hero_Jim_Raynor_Marine = cUnitTypesEnum.Hero_Jim_Raynor_Marine
        Hero_Tom_Kazansky = cUnitTypesEnum.Hero_Tom_Kazansky
        Hero_Magellan = cUnitTypesEnum.Hero_Magellan
        Hero_Edmund_Duke_Tank_Mode = cUnitTypesEnum.Hero_Edmund_Duke_Tank_Mode
        Hero_Edmund_Duke_Tank_Mode_Turret = cUnitTypesEnum.Hero_Edmund_Duke_Tank_Mode_Turret
        Hero_Edmund_Duke_Siege_Mode = cUnitTypesEnum.Hero_Edmund_Duke_Siege_Mode
        Hero_Edmund_Duke_Siege_Mode_Turret = cUnitTypesEnum.Hero_Edmund_Duke_Siege_Mode_Turret
        Hero_Arcturus_Mengsk = cUnitTypesEnum.Hero_Arcturus_Mengsk
        Hero_Hyperion = cUnitTypesEnum.Hero_Hyperion
        Hero_Norad_II = cUnitTypesEnum.Hero_Norad_II
        Terran_Siege_Tank_Siege_Mode = cUnitTypesEnum.Terran_Siege_Tank_Siege_Mode
        Terran_Siege_Tank_Siege_Mode_Turret = cUnitTypesEnum.Terran_Siege_Tank_Siege_Mode_Turret
        Terran_Firebat = cUnitTypesEnum.Terran_Firebat
        Spell_Scanner_Sweep = cUnitTypesEnum.Spell_Scanner_Sweep
        Terran_Medic = cUnitTypesEnum.Terran_Medic
        Zerg_Larva = cUnitTypesEnum.Zerg_Larva
        Zerg_Egg = cUnitTypesEnum.Zerg_Egg
        Zerg_Zergling = cUnitTypesEnum.Zerg_Zergling
        Zerg_Hydralisk = cUnitTypesEnum.Zerg_Hydralisk
        Zerg_Ultralisk = cUnitTypesEnum.Zerg_Ultralisk
        Zerg_Broodling = cUnitTypesEnum.Zerg_Broodling
        Zerg_Drone = cUnitTypesEnum.Zerg_Drone
        Zerg_Overlord = cUnitTypesEnum.Zerg_Overlord
        Zerg_Mutalisk = cUnitTypesEnum.Zerg_Mutalisk
        Zerg_Guardian = cUnitTypesEnum.Zerg_Guardian
        Zerg_Queen = cUnitTypesEnum.Zerg_Queen
        Zerg_Defiler = cUnitTypesEnum.Zerg_Defiler
        Zerg_Scourge = cUnitTypesEnum.Zerg_Scourge
        Hero_Torrasque = cUnitTypesEnum.Hero_Torrasque
        Hero_Matriarch = cUnitTypesEnum.Hero_Matriarch
        Zerg_Infested_Terran = cUnitTypesEnum.Zerg_Infested_Terran
        Hero_Infested_Kerrigan = cUnitTypesEnum.Hero_Infested_Kerrigan
        Hero_Unclean_One = cUnitTypesEnum.Hero_Unclean_One
        Hero_Hunter_Killer = cUnitTypesEnum.Hero_Hunter_Killer
        Hero_Devouring_One = cUnitTypesEnum.Hero_Devouring_One
        Hero_Kukulza_Mutalisk = cUnitTypesEnum.Hero_Kukulza_Mutalisk
        Hero_Kukulza_Guardian = cUnitTypesEnum.Hero_Kukulza_Guardian
        Hero_Yggdrasill = cUnitTypesEnum.Hero_Yggdrasill
        Terran_Valkyrie = cUnitTypesEnum.Terran_Valkyrie
        Zerg_Cocoon = cUnitTypesEnum.Zerg_Cocoon
        Protoss_Corsair = cUnitTypesEnum.Protoss_Corsair
        Protoss_Dark_Templar = cUnitTypesEnum.Protoss_Dark_Templar
        Zerg_Devourer = cUnitTypesEnum.Zerg_Devourer
        Protoss_Dark_Archon = cUnitTypesEnum.Protoss_Dark_Archon
        Protoss_Probe = cUnitTypesEnum.Protoss_Probe
        Protoss_Zealot = cUnitTypesEnum.Protoss_Zealot
        Protoss_Dragoon = cUnitTypesEnum.Protoss_Dragoon
        Protoss_High_Templar = cUnitTypesEnum.Protoss_High_Templar
        Protoss_Archon = cUnitTypesEnum.Protoss_Archon
        Protoss_Shuttle = cUnitTypesEnum.Protoss_Shuttle
        Protoss_Scout = cUnitTypesEnum.Protoss_Scout
        Protoss_Arbiter = cUnitTypesEnum.Protoss_Arbiter
        Protoss_Carrier = cUnitTypesEnum.Protoss_Carrier
        Protoss_Interceptor = cUnitTypesEnum.Protoss_Interceptor
        Hero_Dark_Templar = cUnitTypesEnum.Hero_Dark_Templar
        Hero_Zeratul = cUnitTypesEnum.Hero_Zeratul
        Hero_Tassadar_Zeratul_Archon = cUnitTypesEnum.Hero_Tassadar_Zeratul_Archon
        Hero_Fenix_Zealot = cUnitTypesEnum.Hero_Fenix_Zealot
        Hero_Fenix_Dragoon = cUnitTypesEnum.Hero_Fenix_Dragoon
        Hero_Tassadar = cUnitTypesEnum.Hero_Tassadar
        Hero_Mojo = cUnitTypesEnum.Hero_Mojo
        Hero_Warbringer = cUnitTypesEnum.Hero_Warbringer
        Hero_Gantrithor = cUnitTypesEnum.Hero_Gantrithor
        Protoss_Reaver = cUnitTypesEnum.Protoss_Reaver
        Protoss_Observer = cUnitTypesEnum.Protoss_Observer
        Protoss_Scarab = cUnitTypesEnum.Protoss_Scarab
        Hero_Danimoth = cUnitTypesEnum.Hero_Danimoth
        Hero_Aldaris = cUnitTypesEnum.Hero_Aldaris
        Hero_Artanis = cUnitTypesEnum.Hero_Artanis
        Critter_Rhynadon = cUnitTypesEnum.Critter_Rhynadon
        Critter_Bengalaas = cUnitTypesEnum.Critter_Bengalaas
        Special_Cargo_Ship = cUnitTypesEnum.Special_Cargo_Ship
        Special_Mercenary_Gunship = cUnitTypesEnum.Special_Mercenary_Gunship
        Critter_Scantid = cUnitTypesEnum.Critter_Scantid
        Critter_Kakaru = cUnitTypesEnum.Critter_Kakaru
        Critter_Ragnasaur = cUnitTypesEnum.Critter_Ragnasaur
        Critter_Ursadon = cUnitTypesEnum.Critter_Ursadon
        Zerg_Lurker_Egg = cUnitTypesEnum.Zerg_Lurker_Egg
        Hero_Raszagal = cUnitTypesEnum.Hero_Raszagal
        Hero_Samir_Duran = cUnitTypesEnum.Hero_Samir_Duran
        Hero_Alexei_Stukov = cUnitTypesEnum.Hero_Alexei_Stukov
        Special_Map_Revealer = cUnitTypesEnum.Special_Map_Revealer
        Hero_Gerard_DuGalle = cUnitTypesEnum.Hero_Gerard_DuGalle
        Zerg_Lurker = cUnitTypesEnum.Zerg_Lurker
        Hero_Infested_Duran = cUnitTypesEnum.Hero_Infested_Duran
        Spell_Disruption_Web = cUnitTypesEnum.Spell_Disruption_Web
        Terran_Command_Center = cUnitTypesEnum.Terran_Command_Center
        Terran_Comsat_Station = cUnitTypesEnum.Terran_Comsat_Station
        Terran_Nuclear_Silo = cUnitTypesEnum.Terran_Nuclear_Silo
        Terran_Supply_Depot = cUnitTypesEnum.Terran_Supply_Depot
        Terran_Refinery = cUnitTypesEnum.Terran_Refinery
        Terran_Barracks = cUnitTypesEnum.Terran_Barracks
        Terran_Academy = cUnitTypesEnum.Terran_Academy
        Terran_Factory = cUnitTypesEnum.Terran_Factory
        Terran_Starport = cUnitTypesEnum.Terran_Starport
        Terran_Control_Tower = cUnitTypesEnum.Terran_Control_Tower
        Terran_Science_Facility = cUnitTypesEnum.Terran_Science_Facility
        Terran_Covert_Ops = cUnitTypesEnum.Terran_Covert_Ops
        Terran_Physics_Lab = cUnitTypesEnum.Terran_Physics_Lab
        Unused_Terran1 = cUnitTypesEnum.Unused_Terran1
        Terran_Machine_Shop = cUnitTypesEnum.Terran_Machine_Shop
        Unused_Terran2 = cUnitTypesEnum.Unused_Terran2
        Terran_Engineering_Bay = cUnitTypesEnum.Terran_Engineering_Bay
        Terran_Armory = cUnitTypesEnum.Terran_Armory
        Terran_Missile_Turret = cUnitTypesEnum.Terran_Missile_Turret
        Terran_Bunker = cUnitTypesEnum.Terran_Bunker
        Special_Crashed_Norad_II = cUnitTypesEnum.Special_Crashed_Norad_II
        Special_Ion_Cannon = cUnitTypesEnum.Special_Ion_Cannon
        Powerup_Uraj_Crystal = cUnitTypesEnum.Powerup_Uraj_Crystal
        Powerup_Khalis_Crystal = cUnitTypesEnum.Powerup_Khalis_Crystal
        Zerg_Infested_Command_Center = cUnitTypesEnum.Zerg_Infested_Command_Center
        Zerg_Hatchery = cUnitTypesEnum.Zerg_Hatchery
        Zerg_Lair = cUnitTypesEnum.Zerg_Lair
        Zerg_Hive = cUnitTypesEnum.Zerg_Hive
        Zerg_Nydus_Canal = cUnitTypesEnum.Zerg_Nydus_Canal
        Zerg_Hydralisk_Den = cUnitTypesEnum.Zerg_Hydralisk_Den
        Zerg_Defiler_Mound = cUnitTypesEnum.Zerg_Defiler_Mound
        Zerg_Greater_Spire = cUnitTypesEnum.Zerg_Greater_Spire
        Zerg_Queens_Nest = cUnitTypesEnum.Zerg_Queens_Nest
        Zerg_Evolution_Chamber = cUnitTypesEnum.Zerg_Evolution_Chamber
        Zerg_Ultralisk_Cavern = cUnitTypesEnum.Zerg_Ultralisk_Cavern
        Zerg_Spire = cUnitTypesEnum.Zerg_Spire
        Zerg_Spawning_Pool = cUnitTypesEnum.Zerg_Spawning_Pool
        Zerg_Creep_Colony = cUnitTypesEnum.Zerg_Creep_Colony
        Zerg_Spore_Colony = cUnitTypesEnum.Zerg_Spore_Colony
        Unused_Zerg1 = cUnitTypesEnum.Unused_Zerg1
        Zerg_Sunken_Colony = cUnitTypesEnum.Zerg_Sunken_Colony
        Special_Overmind_With_Shell = cUnitTypesEnum.Special_Overmind_With_Shell
        Special_Overmind = cUnitTypesEnum.Special_Overmind
        Zerg_Extractor = cUnitTypesEnum.Zerg_Extractor
        Special_Mature_Chrysalis = cUnitTypesEnum.Special_Mature_Chrysalis
        Special_Cerebrate = cUnitTypesEnum.Special_Cerebrate
        Special_Cerebrate_Daggoth = cUnitTypesEnum.Special_Cerebrate_Daggoth
        Unused_Zerg2 = cUnitTypesEnum.Unused_Zerg2
        Protoss_Nexus = cUnitTypesEnum.Protoss_Nexus
        Protoss_Robotics_Facility = cUnitTypesEnum.Protoss_Robotics_Facility
        Protoss_Pylon = cUnitTypesEnum.Protoss_Pylon
        Protoss_Assimilator = cUnitTypesEnum.Protoss_Assimilator
        Unused_Protoss1 = cUnitTypesEnum.Unused_Protoss1
        Protoss_Observatory = cUnitTypesEnum.Protoss_Observatory
        Protoss_Gateway = cUnitTypesEnum.Protoss_Gateway
        Unused_Protoss2 = cUnitTypesEnum.Unused_Protoss2
        Protoss_Photon_Cannon = cUnitTypesEnum.Protoss_Photon_Cannon
        Protoss_Citadel_of_Adun = cUnitTypesEnum.Protoss_Citadel_of_Adun
        Protoss_Cybernetics_Core = cUnitTypesEnum.Protoss_Cybernetics_Core
        Protoss_Templar_Archives = cUnitTypesEnum.Protoss_Templar_Archives
        Protoss_Forge = cUnitTypesEnum.Protoss_Forge
        Protoss_Stargate = cUnitTypesEnum.Protoss_Stargate
        Special_Stasis_Cell_Prison = cUnitTypesEnum.Special_Stasis_Cell_Prison
        Protoss_Fleet_Beacon = cUnitTypesEnum.Protoss_Fleet_Beacon
        Protoss_Arbiter_Tribunal = cUnitTypesEnum.Protoss_Arbiter_Tribunal
        Protoss_Robotics_Support_Bay = cUnitTypesEnum.Protoss_Robotics_Support_Bay
        Protoss_Shield_Battery = cUnitTypesEnum.Protoss_Shield_Battery
        Special_Khaydarin_Crystal_Form = cUnitTypesEnum.Special_Khaydarin_Crystal_Form
        Special_Protoss_Temple = cUnitTypesEnum.Special_Protoss_Temple
        Special_XelNaga_Temple = cUnitTypesEnum.Special_XelNaga_Temple
        Resource_Mineral_Field = cUnitTypesEnum.Resource_Mineral_Field
        Resource_Mineral_Field_Type_2 = cUnitTypesEnum.Resource_Mineral_Field_Type_2
        Resource_Mineral_Field_Type_3 = cUnitTypesEnum.Resource_Mineral_Field_Type_3
        Unused_Cave = cUnitTypesEnum.Unused_Cave
        Unused_Cave_In = cUnitTypesEnum.Unused_Cave_In
        Unused_Cantina = cUnitTypesEnum.Unused_Cantina
        Unused_Mining_Platform = cUnitTypesEnum.Unused_Mining_Platform
        Unused_Independant_Command_Center = cUnitTypesEnum.Unused_Independant_Command_Center
        Special_Independant_Starport = cUnitTypesEnum.Special_Independant_Starport
        Unused_Independant_Jump_Gate = cUnitTypesEnum.Unused_Independant_Jump_Gate
        Unused_Ruins = cUnitTypesEnum.Unused_Ruins
        Unused_Khaydarin_Crystal_Formation = cUnitTypesEnum.Unused_Khaydarin_Crystal_Formation
        Resource_Vespene_Geyser = cUnitTypesEnum.Resource_Vespene_Geyser
        Special_Warp_Gate = cUnitTypesEnum.Special_Warp_Gate
        Special_Psi_Disrupter = cUnitTypesEnum.Special_Psi_Disrupter
        Unused_Zerg_Marker = cUnitTypesEnum.Unused_Zerg_Marker
        Unused_Terran_Marker = cUnitTypesEnum.Unused_Terran_Marker
        Unused_Protoss_Marker = cUnitTypesEnum.Unused_Protoss_Marker
        Special_Zerg_Beacon = cUnitTypesEnum.Special_Zerg_Beacon
        Special_Terran_Beacon = cUnitTypesEnum.Special_Terran_Beacon
        Special_Protoss_Beacon = cUnitTypesEnum.Special_Protoss_Beacon
        Special_Zerg_Flag_Beacon = cUnitTypesEnum.Special_Zerg_Flag_Beacon
        Special_Terran_Flag_Beacon = cUnitTypesEnum.Special_Terran_Flag_Beacon
        Special_Protoss_Flag_Beacon = cUnitTypesEnum.Special_Protoss_Flag_Beacon
        Special_Power_Generator = cUnitTypesEnum.Special_Power_Generator
        Special_Overmind_Cocoon = cUnitTypesEnum.Special_Overmind_Cocoon
        Spell_Dark_Swarm = cUnitTypesEnum.Spell_Dark_Swarm
        Special_Floor_Missile_Trap = cUnitTypesEnum.Special_Floor_Missile_Trap
        Special_Floor_Hatch = cUnitTypesEnum.Special_Floor_Hatch
        Special_Upper_Level_Door = cUnitTypesEnum.Special_Upper_Level_Door
        Special_Right_Upper_Level_Door = cUnitTypesEnum.Special_Right_Upper_Level_Door
        Special_Pit_Door = cUnitTypesEnum.Special_Pit_Door
        Special_Right_Pit_Door = cUnitTypesEnum.Special_Right_Pit_Door
        Special_Floor_Gun_Trap = cUnitTypesEnum.Special_Floor_Gun_Trap
        Special_Wall_Missile_Trap = cUnitTypesEnum.Special_Wall_Missile_Trap
        Special_Wall_Flame_Trap = cUnitTypesEnum.Special_Wall_Flame_Trap
        Special_Right_Wall_Missile_Trap = cUnitTypesEnum.Special_Right_Wall_Missile_Trap
        Special_Right_Wall_Flame_Trap = cUnitTypesEnum.Special_Right_Wall_Flame_Trap
        Special_Start_Location = cUnitTypesEnum.Special_Start_Location
        Powerup_Flag = cUnitTypesEnum.Powerup_Flag
        Powerup_Young_Chrysalis = cUnitTypesEnum.Powerup_Young_Chrysalis
        Powerup_Psi_Emitter = cUnitTypesEnum.Powerup_Psi_Emitter
        Powerup_Data_Disk = cUnitTypesEnum.Powerup_Data_Disk
        Powerup_Khaydarin_Crystal = cUnitTypesEnum.Powerup_Khaydarin_Crystal
        Powerup_Mineral_Cluster_Type_1 = cUnitTypesEnum.Powerup_Mineral_Cluster_Type_1
        Powerup_Mineral_Cluster_Type_2 = cUnitTypesEnum.Powerup_Mineral_Cluster_Type_2
        Powerup_Protoss_Gas_Orb_Type_1 = cUnitTypesEnum.Powerup_Protoss_Gas_Orb_Type_1
        Powerup_Protoss_Gas_Orb_Type_2 = cUnitTypesEnum.Powerup_Protoss_Gas_Orb_Type_2
        Powerup_Zerg_Gas_Sac_Type_1 = cUnitTypesEnum.Powerup_Zerg_Gas_Sac_Type_1
        Powerup_Zerg_Gas_Sac_Type_2 = cUnitTypesEnum.Powerup_Zerg_Gas_Sac_Type_2
        Powerup_Terran_Gas_Tank_Type_1 = cUnitTypesEnum.Powerup_Terran_Gas_Tank_Type_1
        Powerup_Terran_Gas_Tank_Type_2 = cUnitTypesEnum.Powerup_Terran_Gas_Tank_Type_2

        none = cUnitTypesEnum.None
        AllUnits = cUnitTypesEnum.AllUnits
        Men = cUnitTypesEnum.Men
        Buildings = cUnitTypesEnum.Buildings
        Factories = cUnitTypesEnum.Factories
        Unknown = cUnitTypesEnum.Unknown
        MAX = cUnitTypesEnum.MAX
    '''
    #comment out for build speed

    Terran_Marine = UnitType(cUnitTypesEnum.Terran_Marine)
    Terran_Ghost = UnitType(cUnitTypesEnum.Terran_Ghost)
    Terran_Vulture = UnitType(cUnitTypesEnum.Terran_Vulture)
    Terran_Goliath = UnitType(cUnitTypesEnum.Terran_Goliath)
    # goliath turret 4
    Terran_Siege_Tank_Tank_Mode = UnitType(cUnitTypesEnum.Terran_Siege_Tank_Tank_Mode)
    # siege tank turret 6
    Terran_SCV = UnitType(cUnitTypesEnum.Terran_SCV)
    Terran_Wraith = UnitType(cUnitTypesEnum.Terran_Wraith)
    Terran_Science_Vessel = UnitType(cUnitTypesEnum.Terran_Science_Vessel)
    Hero_Gui_Montag = UnitType(cUnitTypesEnum.Hero_Gui_Montag)
    Terran_Dropship = UnitType(cUnitTypesEnum.Terran_Dropship)
    Terran_Battlecruiser = UnitType(cUnitTypesEnum.Terran_Battlecruiser)
    Terran_Vulture_Spider_Mine = UnitType(cUnitTypesEnum.Terran_Vulture_Spider_Mine)
    Terran_Nuclear_Missile = UnitType(cUnitTypesEnum.Terran_Nuclear_Missile)
    Terran_Civilian = UnitType(cUnitTypesEnum.Terran_Civilian)
    Hero_Sarah_Kerrigan = UnitType(cUnitTypesEnum.Hero_Sarah_Kerrigan)
    Hero_Alan_Schezar = UnitType(cUnitTypesEnum.Hero_Alan_Schezar)
    # alan turret 18
    Hero_Jim_Raynor_Vulture = UnitType(cUnitTypesEnum.Hero_Jim_Raynor_Vulture)
    Hero_Jim_Raynor_Marine = UnitType(cUnitTypesEnum.Hero_Jim_Raynor_Marine)
    Hero_Tom_Kazansky = UnitType(cUnitTypesEnum.Hero_Tom_Kazansky)
    Hero_Magellan = UnitType(cUnitTypesEnum.Hero_Magellan)
    Hero_Edmund_Duke_Tank_Mode = UnitType(cUnitTypesEnum.Hero_Edmund_Duke_Tank_Mode)
    # edmund duke turret 24
    Hero_Edmund_Duke_Siege_Mode = UnitType(cUnitTypesEnum.Hero_Edmund_Duke_Siege_Mode)
    # edmund duke turret siege mode 26
    Hero_Arcturus_Mengsk = UnitType(cUnitTypesEnum.Hero_Arcturus_Mengsk)
    Hero_Hyperion = UnitType(cUnitTypesEnum.Hero_Hyperion)
    Hero_Norad_II = UnitType(cUnitTypesEnum.Hero_Norad_II)
    Terran_Siege_Tank_Siege_Mode = UnitType(cUnitTypesEnum.Terran_Siege_Tank_Siege_Mode)
    # siege tank siege mode turret 31
    Terran_Firebat = UnitType(cUnitTypesEnum.Terran_Firebat)
    Spell_Scanner_Sweep = UnitType(cUnitTypesEnum.Spell_Scanner_Sweep)
    Terran_Medic = UnitType(cUnitTypesEnum.Terran_Medic)
    Zerg_Larva = UnitType(cUnitTypesEnum.Zerg_Larva)
    Zerg_Egg = UnitType(cUnitTypesEnum.Zerg_Egg)
    Zerg_Zergling = UnitType(cUnitTypesEnum.Zerg_Zergling)
    Zerg_Hydralisk = UnitType(cUnitTypesEnum.Zerg_Hydralisk)
    Zerg_Ultralisk = UnitType(cUnitTypesEnum.Zerg_Ultralisk)
    Zerg_Broodling = UnitType(cUnitTypesEnum.Zerg_Broodling)
    Zerg_Drone = UnitType(cUnitTypesEnum.Zerg_Drone)
    Zerg_Overlord = UnitType(cUnitTypesEnum.Zerg_Overlord)
    Zerg_Mutalisk = UnitType(cUnitTypesEnum.Zerg_Mutalisk)
    Zerg_Guardian = UnitType(cUnitTypesEnum.Zerg_Guardian)
    Zerg_Queen = UnitType(cUnitTypesEnum.Zerg_Queen)
    Zerg_Defiler = UnitType(cUnitTypesEnum.Zerg_Defiler)
    Zerg_Scourge = UnitType(cUnitTypesEnum.Zerg_Scourge)
    Hero_Torrasque = UnitType(cUnitTypesEnum.Hero_Torrasque)
    Hero_Matriarch = UnitType(cUnitTypesEnum.Hero_Matriarch)
    Zerg_Infested_Terran = UnitType(cUnitTypesEnum.Zerg_Infested_Terran)
    Hero_Infested_Kerrigan = UnitType(cUnitTypesEnum.Hero_Infested_Kerrigan)
    Hero_Unclean_One = UnitType(cUnitTypesEnum.Hero_Unclean_One)
    Hero_Hunter_Killer = UnitType(cUnitTypesEnum.Hero_Hunter_Killer)
    Hero_Devouring_One = UnitType(cUnitTypesEnum.Hero_Devouring_One)
    Hero_Kukulza_Mutalisk = UnitType(cUnitTypesEnum.Hero_Kukulza_Mutalisk)
    Hero_Kukulza_Guardian = UnitType(cUnitTypesEnum.Hero_Kukulza_Guardian)
    Hero_Yggdrasill = UnitType(cUnitTypesEnum.Hero_Yggdrasill)
    Terran_Valkyrie = UnitType(cUnitTypesEnum.Terran_Valkyrie)
    Zerg_Cocoon = UnitType(cUnitTypesEnum.Zerg_Cocoon)
    Protoss_Corsair = UnitType(cUnitTypesEnum.Protoss_Corsair)
    Protoss_Dark_Templar = UnitType(cUnitTypesEnum.Protoss_Dark_Templar)
    Zerg_Devourer = UnitType(cUnitTypesEnum.Zerg_Devourer)
    Protoss_Dark_Archon = UnitType(cUnitTypesEnum.Protoss_Dark_Archon)
    Protoss_Probe = UnitType(cUnitTypesEnum.Protoss_Probe)
    Protoss_Zealot = UnitType(cUnitTypesEnum.Protoss_Zealot)
    Protoss_Dragoon = UnitType(cUnitTypesEnum.Protoss_Dragoon)
    Protoss_High_Templar = UnitType(cUnitTypesEnum.Protoss_High_Templar)
    Protoss_Archon = UnitType(cUnitTypesEnum.Protoss_Archon)
    Protoss_Shuttle = UnitType(cUnitTypesEnum.Protoss_Shuttle)
    Protoss_Scout = UnitType(cUnitTypesEnum.Protoss_Scout)
    Protoss_Arbiter = UnitType(cUnitTypesEnum.Protoss_Arbiter)
    Protoss_Carrier = UnitType(cUnitTypesEnum.Protoss_Carrier)
    Protoss_Interceptor = UnitType(cUnitTypesEnum.Protoss_Interceptor)
    Hero_Dark_Templar = UnitType(cUnitTypesEnum.Hero_Dark_Templar)
    Hero_Zeratul = UnitType(cUnitTypesEnum.Hero_Zeratul)
    Hero_Tassadar_Zeratul_Archon = UnitType(cUnitTypesEnum.Hero_Tassadar_Zeratul_Archon)
    Hero_Fenix_Zealot = UnitType(cUnitTypesEnum.Hero_Fenix_Zealot)
    Hero_Fenix_Dragoon = UnitType(cUnitTypesEnum.Hero_Fenix_Dragoon)
    Hero_Tassadar = UnitType(cUnitTypesEnum.Hero_Tassadar)
    Hero_Mojo = UnitType(cUnitTypesEnum.Hero_Mojo)
    Hero_Warbringer = UnitType(cUnitTypesEnum.Hero_Warbringer)
    Hero_Gantrithor = UnitType(cUnitTypesEnum.Hero_Gantrithor)
    Protoss_Reaver = UnitType(cUnitTypesEnum.Protoss_Reaver)
    Protoss_Observer = UnitType(cUnitTypesEnum.Protoss_Observer)
    Protoss_Scarab = UnitType(cUnitTypesEnum.Protoss_Scarab)
    Hero_Danimoth = UnitType(cUnitTypesEnum.Hero_Danimoth)
    Hero_Aldaris = UnitType(cUnitTypesEnum.Hero_Aldaris)
    Hero_Artanis = UnitType(cUnitTypesEnum.Hero_Artanis)
    Critter_Rhynadon = UnitType(cUnitTypesEnum.Critter_Rhynadon)
    Critter_Bengalaas = UnitType(cUnitTypesEnum.Critter_Bengalaas)
    Special_Cargo_Ship = UnitType(cUnitTypesEnum.Special_Cargo_Ship)
    Special_Mercenary_Gunship = UnitType(cUnitTypesEnum.Special_Mercenary_Gunship)
    Critter_Scantid = UnitType(cUnitTypesEnum.Critter_Scantid)
    Critter_Kakaru = UnitType(cUnitTypesEnum.Critter_Kakaru)
    Critter_Ragnasaur = UnitType(cUnitTypesEnum.Critter_Ragnasaur)
    Critter_Ursadon = UnitType(cUnitTypesEnum.Critter_Ursadon)
    Zerg_Lurker_Egg = UnitType(cUnitTypesEnum.Zerg_Lurker_Egg)
    Hero_Raszagal = UnitType(cUnitTypesEnum.Hero_Raszagal)
    Hero_Samir_Duran = UnitType(cUnitTypesEnum.Hero_Samir_Duran)
    Hero_Alexei_Stukov = UnitType(cUnitTypesEnum.Hero_Alexei_Stukov)
    Special_Map_Revealer = UnitType(cUnitTypesEnum.Special_Map_Revealer)
    Hero_Gerard_DuGalle = UnitType(cUnitTypesEnum.Hero_Gerard_DuGalle)
    Zerg_Lurker = UnitType(cUnitTypesEnum.Zerg_Lurker)
    Hero_Infested_Duran = UnitType(cUnitTypesEnum.Hero_Infested_Duran)
    Spell_Disruption_Web = UnitType(cUnitTypesEnum.Spell_Disruption_Web)
    Terran_Command_Center = UnitType(cUnitTypesEnum.Terran_Command_Center)
    Terran_Comsat_Station = UnitType(cUnitTypesEnum.Terran_Comsat_Station)
    Terran_Nuclear_Silo = UnitType(cUnitTypesEnum.Terran_Nuclear_Silo)
    Terran_Supply_Depot = UnitType(cUnitTypesEnum.Terran_Supply_Depot)
    Terran_Refinery = UnitType(cUnitTypesEnum.Terran_Refinery)
    Terran_Barracks = UnitType(cUnitTypesEnum.Terran_Barracks)
    Terran_Academy = UnitType(cUnitTypesEnum.Terran_Academy)
    Terran_Factory = UnitType(cUnitTypesEnum.Terran_Factory)
    Terran_Starport = UnitType(cUnitTypesEnum.Terran_Starport)
    Terran_Control_Tower = UnitType(cUnitTypesEnum.Terran_Control_Tower)
    Terran_Science_Facility = UnitType(cUnitTypesEnum.Terran_Science_Facility)
    Terran_Covert_Ops = UnitType(cUnitTypesEnum.Terran_Covert_Ops)
    Terran_Physics_Lab = UnitType(cUnitTypesEnum.Terran_Physics_Lab)
    # starbase 119
    Terran_Machine_Shop = UnitType(cUnitTypesEnum.Terran_Machine_Shop)
    # repair bay 121
    Terran_Engineering_Bay = UnitType(cUnitTypesEnum.Terran_Engineering_Bay)
    Terran_Armory = UnitType(cUnitTypesEnum.Terran_Armory)
    Terran_Missile_Turret = UnitType(cUnitTypesEnum.Terran_Missile_Turret)
    Terran_Bunker = UnitType(cUnitTypesEnum.Terran_Bunker)
    Special_Crashed_Norad_II = UnitType(cUnitTypesEnum.Special_Crashed_Norad_II)
    Special_Ion_Cannon = UnitType(cUnitTypesEnum.Special_Ion_Cannon)
    Powerup_Uraj_Crystal = UnitType(cUnitTypesEnum.Powerup_Uraj_Crystal)
    Powerup_Khalis_Crystal = UnitType(cUnitTypesEnum.Powerup_Khalis_Crystal)
    Zerg_Infested_Command_Center = UnitType(cUnitTypesEnum.Zerg_Infested_Command_Center)
    Zerg_Hatchery = UnitType(cUnitTypesEnum.Zerg_Hatchery)
    Zerg_Lair = UnitType(cUnitTypesEnum.Zerg_Lair)
    Zerg_Hive = UnitType(cUnitTypesEnum.Zerg_Hive)
    Zerg_Nydus_Canal = UnitType(cUnitTypesEnum.Zerg_Nydus_Canal)
    Zerg_Hydralisk_Den = UnitType(cUnitTypesEnum.Zerg_Hydralisk_Den)
    Zerg_Defiler_Mound = UnitType(cUnitTypesEnum.Zerg_Defiler_Mound)
    Zerg_Greater_Spire = UnitType(cUnitTypesEnum.Zerg_Greater_Spire)
    Zerg_Queens_Nest = UnitType(cUnitTypesEnum.Zerg_Queens_Nest)
    Zerg_Evolution_Chamber = UnitType(cUnitTypesEnum.Zerg_Evolution_Chamber)
    Zerg_Ultralisk_Cavern = UnitType(cUnitTypesEnum.Zerg_Ultralisk_Cavern)
    Zerg_Spire = UnitType(cUnitTypesEnum.Zerg_Spire)
    Zerg_Spawning_Pool = UnitType(cUnitTypesEnum.Zerg_Spawning_Pool)
    Zerg_Creep_Colony = UnitType(cUnitTypesEnum.Zerg_Creep_Colony)
    Zerg_Spore_Colony = UnitType(cUnitTypesEnum.Zerg_Spore_Colony)
    # unused zerg 1 145
    Zerg_Sunken_Colony = UnitType(cUnitTypesEnum.Zerg_Sunken_Colony)
    Special_Overmind_With_Shell = UnitType(cUnitTypesEnum.Special_Overmind_With_Shell)
    Special_Overmind = UnitType(cUnitTypesEnum.Special_Overmind)
    Zerg_Extractor = UnitType(cUnitTypesEnum.Zerg_Extractor)
    Special_Mature_Chrysalis = UnitType(cUnitTypesEnum.Special_Mature_Chrysalis)
    Special_Cerebrate = UnitType(cUnitTypesEnum.Special_Cerebrate)
    Special_Cerebrate_Daggoth = UnitType(cUnitTypesEnum.Special_Cerebrate_Daggoth)
    # unused zerg 2 153
    Protoss_Nexus = UnitType(cUnitTypesEnum.Protoss_Nexus)
    Protoss_Robotics_Facility = UnitType(cUnitTypesEnum.Protoss_Robotics_Facility)
    Protoss_Pylon = UnitType(cUnitTypesEnum.Protoss_Pylon)
    Protoss_Assimilator = UnitType(cUnitTypesEnum.Protoss_Assimilator)
    # unused protoss 1 158
    Protoss_Observatory = UnitType(cUnitTypesEnum.Protoss_Observatory)
    Protoss_Gateway = UnitType(cUnitTypesEnum.Protoss_Gateway)
    # unused protoss 2 161
    Protoss_Photon_Cannon = UnitType(cUnitTypesEnum.Protoss_Photon_Cannon)
    Protoss_Citadel_of_Adun = UnitType(cUnitTypesEnum.Protoss_Citadel_of_Adun)
    Protoss_Cybernetics_Core = UnitType(cUnitTypesEnum.Protoss_Cybernetics_Core)
    Protoss_Templar_Archives = UnitType(cUnitTypesEnum.Protoss_Templar_Archives)
    Protoss_Forge = UnitType(cUnitTypesEnum.Protoss_Forge)
    Protoss_Stargate = UnitType(cUnitTypesEnum.Protoss_Stargate)
    Special_Stasis_Cell_Prison = UnitType(cUnitTypesEnum.Special_Stasis_Cell_Prison)
    Protoss_Fleet_Beacon = UnitType(cUnitTypesEnum.Protoss_Fleet_Beacon)
    Protoss_Arbiter_Tribunal = UnitType(cUnitTypesEnum.Protoss_Arbiter_Tribunal)
    Protoss_Robotics_Support_Bay = UnitType(cUnitTypesEnum.Protoss_Robotics_Support_Bay)
    Protoss_Shield_Battery = UnitType(cUnitTypesEnum.Protoss_Shield_Battery)
    Special_Khaydarin_Crystal_Form = UnitType(cUnitTypesEnum.Special_Khaydarin_Crystal_Form)
    Special_Protoss_Temple = UnitType(cUnitTypesEnum.Special_Protoss_Temple)
    Special_XelNaga_Temple = UnitType(cUnitTypesEnum.Special_XelNaga_Temple)
    Resource_Mineral_Field = UnitType(cUnitTypesEnum.Resource_Mineral_Field)
    Resource_Mineral_Field_Type_2 = UnitType(cUnitTypesEnum.Resource_Mineral_Field_Type_2)
    Resource_Mineral_Field_Type_3 = UnitType(cUnitTypesEnum.Resource_Mineral_Field_Type_3)
    # cave 179
    # cave-in 180
    # cantina 181
    # mining platform 182
    # independant command center 183
    Special_Independant_Starport = UnitType(cUnitTypesEnum.Special_Independant_Starport)
    # independant jump gate 185
    # ruins 186
    # unused khaydarin crystal formation 187
    Resource_Vespene_Geyser = UnitType(cUnitTypesEnum.Resource_Vespene_Geyser)
    Special_Warp_Gate = UnitType(cUnitTypesEnum.Special_Warp_Gate)
    Special_Psi_Disrupter = UnitType(cUnitTypesEnum.Special_Psi_Disrupter)
    # zerg marker 191
    # terran marker 192
    # protoss marker 193
    Special_Zerg_Beacon = UnitType(cUnitTypesEnum.Special_Zerg_Beacon)
    Special_Terran_Beacon = UnitType(cUnitTypesEnum.Special_Terran_Beacon)
    Special_Protoss_Beacon = UnitType(cUnitTypesEnum.Special_Protoss_Beacon)
    Special_Zerg_Flag_Beacon = UnitType(cUnitTypesEnum.Special_Zerg_Flag_Beacon)
    Special_Terran_Flag_Beacon = UnitType(cUnitTypesEnum.Special_Terran_Flag_Beacon)
    Special_Protoss_Flag_Beacon = UnitType(cUnitTypesEnum.Special_Protoss_Flag_Beacon)
    Special_Power_Generator = UnitType(cUnitTypesEnum.Special_Power_Generator)
    Special_Overmind_Cocoon = UnitType(cUnitTypesEnum.Special_Overmind_Cocoon)
    Spell_Dark_Swarm = UnitType(cUnitTypesEnum.Spell_Dark_Swarm)
    Special_Floor_Missile_Trap = UnitType(cUnitTypesEnum.Special_Floor_Missile_Trap)
    Special_Floor_Hatch = UnitType(cUnitTypesEnum.Special_Floor_Hatch)
    Special_Upper_Level_Door = UnitType(cUnitTypesEnum.Special_Upper_Level_Door)
    Special_Right_Upper_Level_Door = UnitType(cUnitTypesEnum.Special_Right_Upper_Level_Door)
    Special_Pit_Door = UnitType(cUnitTypesEnum.Special_Pit_Door)
    Special_Right_Pit_Door = UnitType(cUnitTypesEnum.Special_Right_Pit_Door)
    Special_Floor_Gun_Trap = UnitType(cUnitTypesEnum.Special_Floor_Gun_Trap)
    Special_Wall_Missile_Trap = UnitType(cUnitTypesEnum.Special_Wall_Missile_Trap)
    Special_Wall_Flame_Trap = UnitType(cUnitTypesEnum.Special_Wall_Flame_Trap)
    Special_Right_Wall_Missile_Trap = UnitType(cUnitTypesEnum.Special_Right_Wall_Missile_Trap)
    Special_Right_Wall_Flame_Trap = UnitType(cUnitTypesEnum.Special_Right_Wall_Flame_Trap)
    Special_Start_Location = UnitType(cUnitTypesEnum.Special_Start_Location)
    Powerup_Flag = UnitType(cUnitTypesEnum.Powerup_Flag)
    Powerup_Young_Chrysalis = UnitType(cUnitTypesEnum.Powerup_Young_Chrysalis)
    Powerup_Psi_Emitter = UnitType(cUnitTypesEnum.Powerup_Psi_Emitter)
    Powerup_Data_Disk = UnitType(cUnitTypesEnum.Powerup_Data_Disk)
    Powerup_Khaydarin_Crystal = UnitType(cUnitTypesEnum.Powerup_Khaydarin_Crystal)
    Powerup_Mineral_Cluster_Type_1 = UnitType(cUnitTypesEnum.Powerup_Mineral_Cluster_Type_1)
    Powerup_Mineral_Cluster_Type_2 = UnitType(cUnitTypesEnum.Powerup_Mineral_Cluster_Type_2)
    Powerup_Protoss_Gas_Orb_Type_1 = UnitType(cUnitTypesEnum.Powerup_Protoss_Gas_Orb_Type_1)
    Powerup_Protoss_Gas_Orb_Type_2 = UnitType(cUnitTypesEnum.Powerup_Protoss_Gas_Orb_Type_2)
    Powerup_Zerg_Gas_Sac_Type_1 = UnitType(cUnitTypesEnum.Powerup_Zerg_Gas_Sac_Type_1)
    Powerup_Zerg_Gas_Sac_Type_2 = UnitType(cUnitTypesEnum.Powerup_Zerg_Gas_Sac_Type_2)
    Powerup_Terran_Gas_Tank_Type_1 = UnitType(cUnitTypesEnum.Powerup_Terran_Gas_Tank_Type_1)
    Powerup_Terran_Gas_Tank_Type_2 = UnitType(cUnitTypesEnum.Powerup_Terran_Gas_Tank_Type_2)

    none = UnitType(cUnitTypesEnum.None)
    AllUnits = UnitType(cUnitTypesEnum.AllUnits)
    Men = UnitType(cUnitTypesEnum.Men)
    Buildings = UnitType(cUnitTypesEnum.Buildings)
    Factories = UnitType(cUnitTypesEnum.Factories)
    Unknown = UnitType(cUnitTypesEnum.Unknown)

