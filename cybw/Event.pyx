﻿cimport cybw.cEvent as cEvent
from cybw.constlist cimport list

cdef class Event:
    cdef cEvent.Event thisobj
    def __init__(self):
        pass

    def __richcmp__(Event self, Event other, op):
        if op == 2:
            return self.thisobj == other.thisobj
        else:
            raise NotImplementedError

    def getType(self):
        ''' returns an int '''
        return self.thisobj.getType()

    def getPosition(self):
        return PositionFactory(self.thisobj.getPosition())

    def getText(self):
        cdef string text = string(self.thisobj.getText().c_str())
        return text.decode()

    def getUnit(self):
        return UnitFactory(self.thisobj.getUnit())

    def getPlayer(self):
        return PlayerFactory(self.thisobj.getPlayer())

    def isWinner(self):
        return self.thisobj.isWinner()

cdef EventFactory(cEvent.Event cevent):
    event = Event()
    event.thisobj = cevent
    return event

cdef EventListFactory( const list[cEvent.Event]& events ):
    eventList = []
    cdef list[cEvent.Event].const_iterator iter = events.begin()
    cdef list[cEvent.Event].const_iterator end = events.end()
    while iter != end:
        eventList.append(EventFactory(deref(iter) ) )
        inc(iter)

    return eventList
