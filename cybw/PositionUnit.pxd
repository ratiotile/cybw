cimport cybw.cPositionUnit as cPositionUnit

cdef class PositionUnitConverter:
    cdef cPositionUnit.PositionOrUnit getPositionOrUnit(self)

cdef class PositionOrUnit(PositionUnitConverter):
    cdef cPositionUnit.PositionOrUnit thisobj