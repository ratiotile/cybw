﻿cimport cybw.cUnitCommandType as cUnitCommandType

cdef class UnitCommandType:
    cdef cUnitCommandType.UnitCommandType thisobj

cdef UnitCommandTypeFactory(cUnitCommandType.UnitCommandType ct)