﻿# cimport cBulletTypeEnum
from libcpp.string cimport string

cdef extern from "BWAPI/BulletType.h" namespace "BWAPI":
    cdef cppclass BulletType:
        BulletType()
        BulletType(int id)

        #inherited
        const string &getName() const

cdef extern from "BWAPI/BulletType.h" namespace "BWAPI::BulletTypes":
    const BulletType Melee
    const BulletType Fusion_Cutter_Hit
    const BulletType Gauss_Rifle_Hit
    const BulletType C_10_Canister_Rifle_Hit
    const BulletType Gemini_Missiles
    const BulletType Fragmentation_Grenade
    const BulletType Longbolt_Missile
    const BulletType ATS_ATA_Laser_Battery
    const BulletType Burst_Lasers
    const BulletType Arclite_Shock_Cannon_Hit
    const BulletType EMP_Missile
    const BulletType Dual_Photon_Blasters_Hit
    const BulletType Particle_Beam_Hit
    const BulletType Anti_Matter_Missile
    const BulletType Pulse_Cannon
    const BulletType Psionic_Shockwave_Hit
    const BulletType Psionic_Storm
    const BulletType Yamato_Gun
    const BulletType Phase_Disruptor
    const BulletType STA_STS_Cannon_Overlay
    const BulletType Sunken_Colony_Tentacle
    const BulletType Acid_Spore
    const BulletType Glave_Wurm
    const BulletType Seeker_Spores
    const BulletType Queen_Spell_Carrier
    const BulletType Plague_Cloud
    const BulletType Consume
    const BulletType Ensnare
    const BulletType Needle_Spine_Hit
    const BulletType Invisible
    const BulletType Optical_Flare_Grenade
    const BulletType Halo_Rockets
    const BulletType Subterranean_Spines
    const BulletType Corrosive_Acid_Shot
    const BulletType Neutron_Flare
    const BulletType _None
    const BulletType Unknown