﻿from cybw.cUnitType cimport UnitType as cUnitType

cdef class UnitType:
    cdef cUnitType thisobj

cdef UnitType UnitTypeFactory(cUnitType unittype)

cdef class UnitTypeSet(set):
    cdef cUnitType.set thisobj

cdef UnitTypeSet UnitTypeSetFactory(cUnitType.set cset)

cdef ConstUnitTypeSetFactory(const cUnitType.set &cset)

cdef UnitTypeListFactory(cUnitType.list &utlist)

cdef class UnitTypes:
    pass