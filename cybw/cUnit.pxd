﻿from libcpp cimport bool
from cybw.cUnitType cimport list as UnitTypeList

cdef extern from "BWAPI/Unit.h" namespace "BWAPI":
    cdef cppclass PlayerInterface:
        pass

    ctypedef PlayerInterface* Player

    cdef cppclass Order:
        pass

    cdef cppclass TechType:
        pass

    cdef cppclass UpgradeType:
        pass

    cdef cppclass RegionInterface:
        pass

    ctypedef RegionInterface *Region

    cdef cppclass UnitCommand:
        pass

    cdef cppclass UnitCommandType:
        pass

    cdef cppclass Unitset:
        pass

    cdef cppclass WeaponType:
        pass

    #cdef cppclass UnitInterface:
    #    pass

    ctypedef UnitInterface *Unit

    cdef cppclass PositionOrUnit:
        pass

    cdef cppclass UnitFilter:
        pass

    cdef cppclass UnitType:
        pass
    #cdef cppclass UnitTypeList:
    #    pass
    cdef cppclass TilePosition:
        pass
    cdef cppclass Position:
        pass


    cdef cppclass UnitInterface:
        int getID() except +
        bool exists() except +
        int getReplayID() except +
        Player getPlayer() except +
        UnitType getType() except +
        Position getPosition() except +
        TilePosition getTilePosition() except +
        double getAngle() except +
        double getVelocityX() except +
        double getVelocityY() except +
        Region getRegion() except +
        int getLeft() except +
        int getTop() except +
        int getRight() except +
        int getBottom() except +
        int getHitPoints() except +
        int getShields() except +
        int getEnergy() except +
        int getResources() except +
        int getResourceGroup() except +
        int getDistance(PositionOrUnit target) except +
        int getDistanceP "getDistance" (Position target)  except +
        int getDistanceU "getDistance" (Unit target)  except +
        bool hasPath(PositionOrUnit target) except +
        int getLastCommandFrame() except +
        UnitCommand getLastCommand() except +
        Player getLastAttackingPlayer() except +
        UnitType getInitialType() except +
        Position getInitialPosition() except +
        TilePosition getInitialTilePosition() except +
        int getInitialHitPoints() except +
        int getInitialResources() except +
        int getKillCount() except +
        int getAcidSporeCount() except +
        int getInterceptorCount() except +
        int getScarabCount() except +
        int getSpiderMineCount() except +
        int getGroundWeaponCooldown() except +
        int getAirWeaponCooldown() except +
        int getSpellCooldown() except +
        int getDefenseMatrixPoints() except +
        int getDefenseMatrixTimer() except +
        int getEnsnareTimer() except +
        int getIrradiateTimer() except +
        int getLockdownTimer() except +
        int getMaelstromTimer() except +
        int getOrderTimer() except +
        int getPlagueTimer() except +
        int getRemoveTimer() except +
        int getStasisTimer() except +
        int getStimTimer() except +
        UnitType getBuildType() except +
        UnitTypeList getTrainingQueue() except +
        TechType getTech() except +
        UpgradeType getUpgrade() except +
        int getRemainingBuildTime() except +
        int getRemainingTrainTime() except +
        int getRemainingResearchTime() except +
        int getRemainingUpgradeTime() except +
        Unit getBuildUnit() except +
        Unit getTarget() except +
        Position getTargetPosition() except +
        Order getOrder() except +
        Order getSecondaryOrder() except +
        Unit getOrderTarget() except +
        Position getOrderTargetPosition() except +
        Position getRallyPosition() except +
        Unit getRallyUnit() except +
        Unit getAddon() except +
        Unit getNydusExit() except +
        Unit getPowerUp() except +
        Unit getTransport()
        int getSpaceRemaining() except +
        Unit getCarrier() except +
        Unitset getInterceptors() except +
        Unit getHatchery() except +
        Unitset getLarva() except +
        Unitset getUnitsInRadius(int radius, const UnitFilter &pred) except +
        Unitset getUnitsInRadius(int radius) except +

        Unitset getUnitsInWeaponRange(WeaponType weapon, const UnitFilter &pred) except +
        Unitset getUnitsInWeaponRange(WeaponType weapon) except +

        Unit getClosestUnit(const UnitFilter &pred, int radius) except +
        Unit getClosestUnit() except +

        bool hasNuke() except +
        bool isAccelerating() except +
        bool isAttacking() except +
        bool isAttackFrame() except +
        bool isBeingConstructed() except +
        bool isBeingGathered() except +
        bool isBeingHealed() except +
        bool isBlind() except +
        bool isBraking() except +
        bool isBurrowed() except +
        bool isCarryingGas() except +
        bool isCarryingMinerals() except +
        bool isCloaked() except +
        bool isCompleted() except +
        bool isConstructing() except +
        bool isDefenseMatrixed() except +
        bool isDetected() except +
        bool isEnsnared() except +
        bool isFlying() except +
        bool isFollowing() except +
        bool isGatheringGas() except +
        bool isGatheringMinerals() except +
        bool isHallucination() except +
        bool isHoldingPosition() except +
        bool isIdle() except +
        bool isInterruptible() except +
        bool isInvincible() except +
        bool isInWeaponRange(Unit target) except +
        bool isIrradiated() except +
        bool isLifted() except +
        bool isLoaded() except +
        bool isLockedDown() except +
        bool isMaelstrommed() except +
        bool isMorphing() except +
        bool isMoving() except +
        bool isParasited() except +
        bool isPatrolling() except +
        bool isPlagued() except +
        bool isRepairing() except +
        bool isResearching() except +
        bool isSelected() except +
        bool isSieged() except +
        bool isStartingAttack() except +
        bool isStasised() except +
        bool isStimmed() except +
        bool isStuck() except +
        bool isTraining() except +
        bool isUnderAttack() except +
        bool isUnderDarkSwarm() except +
        bool isUnderDisruptionWeb() except +
        bool isUnderStorm() except +
        bool isPowered() except +
        bool isUpgrading() except +
        bool isVisible(Player player) except +
        bool isTargetable() except +
        bool issueCommand(UnitCommand command)

        bool attack(PositionOrUnit target, bool shiftQueueCommand) except +
        bool attack(PositionOrUnit target) except +

        bool build(UnitType type, TilePosition target) except +
        bool buildAddon(UnitType type) except +
        bool train(UnitType type) except +
        bool morph(UnitType type) except +
        bool research(TechType tech) except +
        bool upgrade(UpgradeType upgrade) except +
        bool setRallyPoint(PositionOrUnit target) except +

        bool move(Position target, bool shiftQueueCommand) except +
        bool patrol(Position target, bool shiftQueueCommand ) except +
        bool holdPosition(bool shiftQueueCommand ) except +
        bool stop(bool shiftQueueCommand) except +
        bool follow(Unit target, bool shiftQueueCommand) except +
        bool gather(Unit target, bool shiftQueueCommand) except +
        bool returnCargo(bool shiftQueueCommand) except +
        bool repair(Unit target, bool shiftQueueCommand) except +

        bool burrow() except +
        bool unburrow() except +
        bool cloak() except +
        bool decloak() except +
        bool siege() except +
        bool unsiege() except +
        bool lift() except +
        bool land(TilePosition target) except +
        bool load(Unit target, bool shiftQueueCommand) except +
        bool unload(Unit target) except +
        bool unloadAll(bool shiftQueueCommand) except +
        bool unloadAll(Position target, bool shiftQueueCommand) except +

        bool rightClick(PositionOrUnit target, bool shiftQueueCommand) except +
        bool haltConstruction() except +
        bool cancelConstruction() except +
        bool cancelAddon() except +

        bool cancelTrain(int slot) except +
        bool cancelTrain() except +

        bool cancelMorph() except +
        bool cancelResearch() except +
        bool cancelUpgrade() except +
        bool useTech(TechType tech, PositionOrUnit target) except +
        bool useTech(TechType tech) except +

        bool placeCOP(TilePosition target) except +

        bool canIssueCommand(UnitCommand command, bool checkCanUseTechPositionOnPositions, bool checkCanUseTechUnitOnUnits, bool checkCanBuildUnitType, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canIssueCommandGrouped(UnitCommand command, bool checkCanUseTechPositionOnPositions, bool checkCanUseTechUnitOnUnits, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibilityGrouped, bool checkCommandibility) except +

        bool canCommand() except +
        bool canCommandGrouped(bool checkCommandibility) except +
        bool canIssueCommandType(UnitCommandType ct, bool checkCommandibility) except +
        bool canIssueCommandTypeGrouped(UnitCommandType ct, bool checkCommandibilityGrouped, bool checkCommandibility) except +

        bool canTargetUnit(Unit targetUnit, bool checkCommandibility) except +
        bool canAttack(bool checkCommandibility) except +
        bool canAttack(PositionOrUnit target, bool checkCanTargetUnit, bool checkCanIssueCommandType , bool checkCommandibility) except +

        bool canAttackGrouped(bool checkCommandibilityGrouped, bool checkCommandibility) except +
        bool canAttackGrouped(PositionOrUnit target, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibilityGrouped, bool checkCommandibility) except +

        bool canAttackMove(bool checkCommandibility) except +
        bool canAttackMoveGrouped(bool checkCommandibilityGrouped, bool checkCommandibility) except +

        bool canAttackUnit(bool checkCommandibility) except +
        bool canAttackUnit(Unit targetUnit, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canAttackUnitGrouped(bool checkCommandibilityGrouped, bool checkCommandibility) except +
        bool canAttackUnitGrouped(Unit targetUnit, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibilityGrouped, bool checkCommandibility) except +

        bool canBuild(bool checkCommandibility) except +
        bool canBuild(UnitType uType, bool checkCanIssueCommandType , bool checkCommandibility ) except +
        bool canBuild(UnitType uType, TilePosition tilePos, bool checkTargetUnitType, bool checkCanIssueCommandType , bool checkCommandibility) except +

        bool canBuildAddon(bool checkCommandibility) except +
        bool canBuildAddon(UnitType uType, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canTrain(bool checkCommandibility) except +
        bool canTrain(UnitType uType, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canMorph(bool checkCommandibility) except +
        bool canMorph(UnitType uType, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canResearch(bool checkCommandibility) except +
        bool canResearch(TechType type, bool checkCanIssueCommandType) except +

        bool canUpgrade(bool checkCommandibility) except +
        bool canUpgrade(UpgradeType type, bool checkCanIssueCommandType) except +

        bool canSetRallyPoint(bool checkCommandibility) except +
        bool canSetRallyPoint(PositionOrUnit target, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canSetRallyPosition(bool checkCommandibility ) except +

        bool canSetRallyUnit(bool checkCommandibility) except +
        bool canSetRallyUnit(Unit targetUnit, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canMove(bool checkCommandibility) except +

        bool canMoveGrouped(bool checkCommandibilityGrouped, bool checkCommandibility) except +

        bool canPatrol(bool checkCommandibility) except +

        bool canPatrolGrouped(bool checkCommandibilityGrouped, bool checkCommandibility) except +

        bool canFollow(bool checkCommandibility) except +
        bool canFollow(Unit targetUnit, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canGather(bool checkCommandibility) except +
        bool canGather(Unit targetUnit, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canReturnCargo(bool checkCommandibility) except +

        bool canHoldPosition(bool checkCommandibility) except +

        bool canStop(bool checkCommandibility) except +

        bool canRepair(bool checkCommandibility) except +
        bool canRepair(Unit targetUnit, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canBurrow(bool checkCommandibility) except +

        bool canUnburrow(bool checkCommandibility) except +

        bool canCloak(bool checkCommandibility) except +

        bool canDecloak(bool checkCommandibility) except +

        bool canSiege(bool checkCommandibility) except +

        bool canUnsiege(bool checkCommandibility) except +

        bool canLift(bool checkCommandibility) except +

        bool canLand(bool checkCommandibility) except +
        bool canLand(TilePosition target, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canLoad(bool checkCommandibility) except +
        bool canLoad(Unit targetUnit, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canUnloadWithOrWithoutTarget(bool checkCommandibility) except +

        bool canUnloadAtPosition(Position targDropPos, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canUnload(bool checkCommandibility) except +
        bool canUnload(Unit targetUnit, bool checkCanTargetUnit, bool checkPosition, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canUnloadAll(bool checkCommandibility) except +

        bool canUnloadAllPosition(bool checkCommandibility) except +
        bool canUnloadAllPosition(Position targDropPos, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canRightClick(bool checkCommandibility) except +
        bool canRightClick(PositionOrUnit target, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canRightClickGrouped(bool checkCommandibilityGrouped, bool checkCommandibility) except +
        bool canRightClickGrouped(PositionOrUnit target, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibilityGrouped, bool checkCommandibility) except +

        bool canRightClickPosition(bool checkCommandibility) except +

        bool canRightClickPositionGrouped(bool checkCommandibilityGrouped, bool checkCommandibility) except +

        bool canRightClickUnit(bool checkCommandibility) except +
        bool canRightClickUnit(Unit targetUnit, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canRightClickUnitGrouped(bool checkCommandibilityGrouped, bool checkCommandibility) except +
        bool canRightClickUnitGrouped(Unit targetUnit, bool checkCanTargetUnit, bool checkCanIssueCommandType, bool checkCommandibilityGrouped, bool checkCommandibility ) except +

        bool canHaltConstruction(bool checkCommandibility ) except +

        bool canCancelConstruction(bool checkCommandibility) except +

        bool canCancelAddon(bool checkCommandibility) except +

        bool canCancelTrain(bool checkCommandibility) except +

        bool canCancelTrainSlot(bool checkCommandibility) except +
        bool canCancelTrainSlot(int slot, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canCancelMorph(bool checkCommandibility) except +

        bool canCancelResearch(bool checkCommandibility) except +

        bool canCancelUpgrade(bool checkCommandibility) except +

        bool canUseTechWithOrWithoutTarget(bool checkCommandibility) except +
        bool canUseTechWithOrWithoutTarget(TechType tech, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canUseTech(TechType tech)
        bool canUseTech(TechType tech, PositionOrUnit target, bool checkCanTargetUnit, bool checkTargetsType, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canUseTechWithoutTarget(TechType tech, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canUseTechUnit(TechType tech, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canUseTechUnit(TechType tech, Unit targetUnit, bool checkCanTargetUnit, bool checkTargetsUnits, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canUseTechPosition(TechType tech, bool checkCanIssueCommandType, bool checkCommandibility) except +
        bool canUseTechPosition(TechType tech, Position target, bool checkTargetsPositions, bool checkCanIssueCommandType, bool checkCommandibility) except +

        bool canPlaceCOP(bool checkCommandibility) except +
        bool canPlaceCOP(TilePosition target, bool checkCanIssueCommandType, bool checkCommandibility) except +
