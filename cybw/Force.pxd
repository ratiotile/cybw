﻿cimport cybw.cForce as cForce

cdef class Force:
    cdef cForce.Force thisobj

cdef ForceFactory(cForce.Force cforce)

cdef const_ForceFactory(const cForce.Force &cforce)