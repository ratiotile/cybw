﻿from libcpp cimport bool, deque
from libcpp.string cimport string
from .cSetContainer cimport SetContainer
#from libcpp.unordered_map import unordered_map

cimport cybw.stdio as stdio

cdef extern from "BWAPI/Type.h" namespace "BWAPI":

    #cdef cppclass Typeset[T]:
    #    Typeset()

    cdef cppclass Type[T, UnknownId]:
        #when inherit, no matching function for call Type::Type()
        Type(int id)

        #operator int() const #not supported
        int getID() const
        bool isValid() const
        const string& getName() const
        const string& toString() const
        const char* c_str() const

        stdio.ostream &operator << (stdio.ostream &out, const Type[T, UnknownId] &t)

        T getType(string name)

    #wostream &operator << (wostream &out, const Type[T, UnknownId] &t)

