﻿from libcpp.string cimport string

cdef extern from "BWAPI/Order.h" namespace "BWAPI":
    cdef cppclass Order:
        Order(int id)
        Order()

        #inherited
        const string &getName() const
        int getID() const

cdef extern from "BWAPI/Order.h" namespace "BWAPI::Orders":
    #const Order::const_set& allOrders()

    const Order Die
    const Order Stop
    const Order Guard
    const Order PlayerGuard
    const Order TurretGuard
    const Order BunkerGuard
    const Order Move
    const Order AttackUnit
    const Order AttackTile
    const Order Hover
    const Order AttackMove
    const Order InfestedCommandCenter
    const Order UnusedNothing
    const Order UnusedPowerup
    const Order TowerGuard
    const Order VultureMine
    const Order Nothing
    const Order CastInfestation
    const Order InfestingCommandCenter
    const Order PlaceBuilding
    const Order CreateProtossBuilding
    const Order ConstructingBuilding
    const Order Repair
    const Order PlaceAddon
    const Order BuildAddon
    const Order Train
    const Order RallyPointUnit
    const Order RallyPointTile
    const Order ZergBirth
    const Order ZergUnitMorph
    const Order ZergBuildingMorph
    const Order IncompleteBuilding
    const Order BuildNydusExit
    const Order EnterNydusCanal
    const Order Follow
    const Order Carrier
    const Order ReaverCarrierMove
    const Order CarrierIgnore2
    const Order Reaver
    const Order TrainFighter
    const Order InterceptorAttack
    const Order ScarabAttack
    const Order RechargeShieldsUnit
    const Order RechargeShieldsBattery
    const Order ShieldBattery
    const Order InterceptorReturn
    const Order BuildingLand
    const Order BuildingLiftOff
    const Order DroneLiftOff
    const Order LiftingOff
    const Order ResearchTech
    const Order Upgrade
    const Order Larva
    const Order SpawningLarva
    const Order Harvest1
    const Order Harvest2
    const Order MoveToGas
    const Order WaitForGas
    const Order HarvestGas
    const Order ReturnGas
    const Order MoveToMinerals
    const Order WaitForMinerals
    const Order MiningMinerals
    const Order Harvest3
    const Order Harvest4
    const Order ReturnMinerals
    const Order Interrupted
    const Order EnterTransport
    const Order PickupIdle
    const Order PickupTransport
    const Order PickupBunker
    const Order Pickup4
    const Order PowerupIdle
    const Order Sieging
    const Order Unsieging
    const Order InitCreepGrowth
    const Order SpreadCreep
    const Order StoppingCreepGrowth
    const Order GuardianAspect
    const Order ArchonWarp
    const Order CompletingArchonSummon
    const Order HoldPosition
    const Order Cloak
    const Order Decloak
    const Order Unload
    const Order MoveUnload
    const Order FireYamatoGun
    const Order CastLockdown
    const Order Burrowing
    const Order Burrowed
    const Order Unburrowing
    const Order CastDarkSwarm
    const Order CastParasite
    const Order CastSpawnBroodlings
    const Order CastEMPShockwave
    const Order NukeWait
    const Order NukeTrain
    const Order NukeLaunch
    const Order NukePaint
    const Order NukeUnit
    const Order CastNuclearStrike
    const Order NukeTrack
    const Order CloakNearbyUnits
    const Order PlaceMine
    const Order RightClickAction
    const Order CastRecall
    const Order Teleport
    const Order CastScannerSweep
    const Order Scanner
    const Order CastDefensiveMatrix
    const Order CastPsionicStorm
    const Order CastIrradiate
    const Order CastPlague
    const Order CastConsume
    const Order CastEnsnare
    const Order CastStasisField
    const Order CastHallucination
    const Order Hallucination2
    const Order ResetCollision
    const Order Patrol
    const Order CTFCOPInit
    const Order CTFCOPStarted
    const Order CTFCOP2
    const Order ComputerAI
    const Order AtkMoveEP
    const Order HarassMove
    const Order AIPatrol
    const Order GuardPost
    const Order RescuePassive
    const Order Neutral
    const Order ComputerReturn
    const Order SelfDestructing
    const Order Critter
    const Order HiddenGun
    const Order OpenDoor
    const Order CloseDoor
    const Order HideTrap
    const Order RevealTrap
    const Order EnableDoodad
    const Order DisableDoodad
    const Order WarpIn
    const Order Medic
    const Order MedicHeal
    const Order HealMove
    const Order MedicHealToIdle
    const Order CastRestoration
    const Order CastDisruptionWeb
    const Order CastMindControl
    const Order DarkArchonMeld
    const Order CastFeedback
    const Order CastOpticalFlare
    const Order CastMaelstrom
    const Order JunkYardDog
    const Order Fatal
    const Order None
    const Order Unknown