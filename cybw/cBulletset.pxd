﻿from cybw.cSetContainer cimport SetContainer

cdef extern from "BWAPI/Bulletset.h" namespace "BWAPI":
    cdef cppclass BulletInterface:
        pass
    ctypedef BulletInterface *Bullet

    cdef cppclass Bulletset(SetContainer[Bullet]):
        Bulletset()
        Bulletset(size_t initialSize)
        Bulletset(const Bulletset &other)
        Bulletset(Bulletset &&other)