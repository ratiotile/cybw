﻿cimport cybw.cColor as cColor
cimport cybw.cText as cText

cdef class Color:
    cdef cColor.Color *thisptr

cdef ColorFactory(cColor.Color *c_color)

cdef class TextWrapper:
    cdef cText.Enum thisval