﻿cdef extern from "BWAPI/UnitCommandType.h" namespace "BWAPI":
    cdef cppclass UnitCommandType:
        UnitCommandType(int id)
        UnitCommandType()


cdef extern from "BWAPI/UnitCommandType.h" namespace "BWAPI::UnitCommandTypes":
    #cdef cppclass UnitCommandTypeConstSet "UnitCommandType::const_set":
    #    pass

    #const UnitCommandTypeConstSet& allUnitCommandTypes()

    const UnitCommandType Attack_Move
    const UnitCommandType Attack_Unit
    const UnitCommandType Build
    const UnitCommandType Build_Addon
    const UnitCommandType Train
    const UnitCommandType Morph
    const UnitCommandType Research
    const UnitCommandType Upgrade
    const UnitCommandType Set_Rally_Position
    const UnitCommandType Set_Rally_Unit
    const UnitCommandType Move
    const UnitCommandType Patrol
    const UnitCommandType Hold_Position
    const UnitCommandType Stop
    const UnitCommandType Follow
    const UnitCommandType Gather
    const UnitCommandType Return_Cargo
    const UnitCommandType Repair
    const UnitCommandType Burrow
    const UnitCommandType Unburrow
    const UnitCommandType Cloak
    const UnitCommandType Decloak
    const UnitCommandType Siege
    const UnitCommandType Unsiege
    const UnitCommandType Lift
    const UnitCommandType Land
    const UnitCommandType Load
    const UnitCommandType Unload
    const UnitCommandType Unload_All
    const UnitCommandType Unload_All_Position
    const UnitCommandType Right_Click_Position
    const UnitCommandType Right_Click_Unit
    const UnitCommandType Halt_Construction
    const UnitCommandType Cancel_Construction
    const UnitCommandType Cancel_Addon
    const UnitCommandType Cancel_Train
    const UnitCommandType Cancel_Train_Slot
    const UnitCommandType Cancel_Morph
    const UnitCommandType Cancel_Research
    const UnitCommandType Cancel_Upgrade
    const UnitCommandType Use_Tech
    const UnitCommandType Use_Tech_Position
    const UnitCommandType Use_Tech_Unit
    const UnitCommandType Place_COP
    const UnitCommandType None
    const UnitCommandType Unknown