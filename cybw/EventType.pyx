﻿cimport cybw.cEventType as cEventType
from enum import IntEnum

class EventType(IntEnum):
    MatchStart = cEventType.MatchStart
    MatchEnd = cEventType.MatchEnd
    MatchFrame = cEventType.MatchFrame
    MenuFrame = cEventType.MenuFrame
    SendText = cEventType.SendText
    ReceiveText = cEventType.ReceiveText
    PlayerLeft = cEventType.PlayerLeft
    NukeDetect = cEventType.NukeDetect
    UnitDiscover = cEventType.UnitDiscover
    UnitEvade = cEventType.UnitEvade
    UnitShow = cEventType.UnitShow
    UnitHide = cEventType.UnitHide
    UnitCreate = cEventType.UnitCreate
    UnitDestroy = cEventType.UnitDestroy
    UnitMorph = cEventType.UnitMorph
    UnitRenegade = cEventType.UnitRenegade
    SaveGame = cEventType.SaveGame
    UnitComplete = cEventType.UnitComplete
    none = cEventType.None