﻿from cybw.Unitset cimport UnitsetFactory

cdef class Region:
    # already defined in pxd
    #cdef cRegion.Region thisptr
    def __init(self):
        pass

    def __hash__(self):
        return self.thisptr.getID()

    def __richcmp__(Region self, Region other, optype):
        if optype   == 0: # <
            pass
        elif optype == 2: # ==
            return self.getID() == other.getID()
        elif optype == 4: # >
            pass
        elif optype == 1: # <=
            pass
        elif optype == 3: # !=
            return self.getID() != other.getID()
        elif optype == 5: # >=
            pass
        raise Exception("not defined")

    def getID(self):
        """ get Broodwar's internal id for this region """
        return self.thisptr.getID()

    def getRegionGroupID(self):
        """ get id of connected landmass that this region is on """
        return self.thisptr.getRegionGroupID()

    def getCenter(self):
        """ returns a Position that is the center node of this region """
        return PositionFactory(self.thisptr.getCenter())

    def isHighGround(self):
        """ returns true if this region is high ground (-30% received accuracy)
        """
        return self.thisptr.isHigherGround()

    def getDefensePriority(self):
        """ returns an int that is calculated by Broodwar indicating the
        importance of this region. Values > 1 may be chokepoints """
        return self.thisptr.getDefensePriority()

    def isAccessible(self):
        """ returns True if this region is walkable """
        return self.thisptr.isAccessible()

    def getNeighbors(self):
        """ returns the RegionSet of neighboring Regions """
        return ConstRegionSetFactory(self.thisptr.getNeighbors())

    def getBoundsLeft(self):
        """ returns The x coordinate, in pixels, of the approximate left
        boundary of the region. """
        return self.thisptr.getBoundsLeft()

    def getBoundsTop(self):
        """ returns The y coordinate, in pixels, of the approximate top
        boundary of the region. """
        return self.thisptr.getBoundsTop()

    def getBoundsRight(self):
        """ returns The x coordinate, in pixels, of the approximate right
        boundary of the region. """
        return self.thisptr.getBoundsRight()

    def getBoundsBottom(self):
        """ returns The y coordinate, in pixels, of the approximate bottom
        boundary of the region. """
        return self.thisptr.getBoundsBottom()

    def getClosestAccessibleRegion(self):
        """ Retrieves the closest accessible neighbor region. """
        return RegionFactory(self.thisptr.getClosestAccessibleRegion())

    def getClosestInaccessibleRegion(self):
        """ returns The closest Region that is inaccessible. """
        return RegionFactory(self.thisptr.getClosestInaccessibleRegion())

    def getDistance(self, Region other):
        """ Given Region other, returns the center-to-center distance between
        this one and other """
        return self.thisptr.getDistance(
            <cRegion.Region>other.thisptr)  # TODO: type checking

    #Unitset getUnits(const UnitFilter &pred ) const
    def getUnits(self):
        """ get a Unitset of all units in the region """
        return UnitsetFactory(self.thisptr.getUnits())



cdef RegionFactory(cRegion.Region cregion):
    if not cregion:
        raise NullPointerException("Region")
    region = Region()
    region.thisptr = cregion
    return region
