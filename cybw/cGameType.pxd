﻿from libcpp cimport bool
from libcpp.string cimport string

cdef extern from "BWAPI/GameType.h" namespace "BWAPI":
    cdef cppclass GameType:
        GameType()
        GameType(int id)
        #inherited from type
        int getID() const
        bool isValid() const
        const string& getName() const
        
cdef extern from "BWAPI/GameType.h" namespace "BWAPI::GameTypes":
    const GameType Melee,
    const GameType Free_For_All,
    const GameType One_on_One,
    const GameType Capture_The_Flag,
    const GameType Greed,
    const GameType Slaughter,
    const GameType Sudden_Death,
    const GameType Ladder,
    const GameType Use_Map_Settings,
    const GameType Team_Melee,
    const GameType Team_Free_For_All,
    const GameType Team_Capture_The_Flag,
    const GameType Top_vs_Bottom,
    const GameType None,
    const GameType Unknown