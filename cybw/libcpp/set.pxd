from libcpp cimport pair

cdef extern from "<set>" namespace "std" nogil:
    cdef cppclass set[T]:
        cppclass iterator:
            T& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)
        iterator begin()
        iterator end()
        iterator find(T&)
        iterator lower_bound(T&)
        iterator upper_bound(const T&)
        cppclass reverse_iterator:
            T& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(reverse_iterator)
            bint operator!=(reverse_iterator)
        reverse_iterator rbegin()
        reverse_iterator rend()
        cppclass const_iterator:
            T& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)
            const_iterator begin()
            const_iterator end()
            const_iterator find(T&)
        #cppclass const_reverse_iterator(reverse_iterator):
        #    pass
        set() except +
        set(set&) except +
        #set(key_compare&)
        #set& operator=(set&)
        bint operator==(set&, set&)
        bint operator!=(set&, set&)
        bint operator<(set&, set&)
        bint operator>(set&, set&)
        bint operator<=(set&, set&)
        bint operator>=(set&, set&)


        void clear()
        size_t count(const T&)
        bint empty()


        #pair[iterator, iterator] equal_range(const T&)
        #pair[const_iterator, const_iterator] equal_range(T&)
        void erase(iterator)
        void erase(iterator, iterator)
        size_t erase(T&)


        #pair[iterator, bint] insert(const T&) except +
        iterator insert(iterator, const T&) except +
        #void insert(input_iterator, input_iterator)
        #key_compare key_comp()

        #const_iterator lower_bound(T&)
        size_t max_size()

        #const_reverse_iterator rbegin()

        #const_reverse_iterator rend()
        size_t size()
        void swap(set&)

        #const_iterator upper_bound(const T&)
        #value_compare value_comp()
