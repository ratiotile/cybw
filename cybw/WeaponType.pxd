﻿from cybw cimport cWeaponType

cdef class WeaponType:
    cdef cWeaponType.WeaponType thisobj

cdef class WeaponTypeSet(set):
    cdef cWeaponType.set thisobj

cdef WeaponTypeFactory(cWeaponType.WeaponType cweap)

cdef ConstWeaponTypeSetFactory(const cWeaponType.set &cset)

cdef class WeaponTypes:
    pass