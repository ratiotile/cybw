from cybw.cType cimport Type


cdef extern from "BWAPI/DamageType.h" namespace "BWAPI":
    cdef cppclass DamageType(Type[DamageType, int]):
        DamageType()
        DamageType(int id)


cdef extern from "BWAPI/DamageType.h" namespace "BWAPI::DamageTypes":
    const set& allDamageTypes()

    DamageType Independent
    DamageType Explosive
    DamageType Concussive
    DamageType Normal
    DamageType Ignore_Armor
    DamageType None
    DamageType Unknown

cdef extern from "BWAPI/DamageType.h" namespace "BWAPI::DamageType":
    cdef cppclass set:
        cppclass iterator:
            DamageType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)
        iterator begin()
        iterator end()

        cppclass const_iterator:
            DamageType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)
        const_iterator cbegin()
        const_iterator cend()