﻿from cybw.cPlayerset cimport Playerset as cPlayerset
from cybw.cPlayer cimport Player as cPlayer


cdef class Playerset(set):
    pass  # TODO

cdef class const_Playerset(set):
    pass  # TODO

cdef ConstPlayerSetFactory(const cPlayerset& cps):
    ''' make a list of Players from a Playerset pointer'''
    pset = const_Playerset()
    pset.thisptr = &cps
    cdef cPlayerset.const_iterator it = cps.cbegin()
    cdef cPlayerset.const_iterator end = cps.cend()
    while it != end:
        pset.add(const_PlayerFactory(deref(it)))
        inc(it)
    return pset

cdef PlayerSetFactory(cPlayerset.Playerset &ps):
    ''' make a list of Players from a Playerset pointer'''
    pset = Playerset()
    pset.thisptr = &ps
    cdef cPlayerset.iterator it = ps.begin()
    cdef cPlayerset.iterator end = ps.end()
    while it != end:
        pset.add(PlayerFactory(deref(it)))
        inc(it)
    return pset