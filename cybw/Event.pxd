cimport cybw.cEvent as cEvent
# from cybw.constlist cimport list

cdef class Event:
    cdef cEvent.Event thisobj

cdef EventFactory(cEvent.Event cevent)

cdef EventListFactory( const list[cEvent.Event]& events )