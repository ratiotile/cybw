﻿cimport cybw.cUnitSizeType as cUnitSizeType

cdef class UnitSizeType:
    cdef cUnitSizeType.UnitSizeType thisobj

cdef class UnitSizeTypeSet(set):
    cdef cUnitSizeType.set thisobj

cdef UnitSizeTypeFactory(cUnitSizeType.UnitSizeType ct)

cdef ConstUnitSizeTypeSetFactory(const cUnitSizeType.set &cset)

cdef class UnitSizeTypes:
    pass