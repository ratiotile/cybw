﻿from libcpp cimport bool
from libcpp.string cimport string
#from cybw.cPosition cimport TilePosition
#from cybw.cRace cimport Race

cdef extern from "BWAPI/Race.h" namespace "BWAPI":
    cdef cppclass Race:
        pass

cdef extern from "BWAPI/Player.h" namespace "BWAPI":
    cdef cppclass Color:
        pass
    cdef cppclass ForceInterface:
        pass
    ctypedef ForceInterface *Force
    cdef cppclass PlayerType:
        pass
    cdef cppclass TechType:
        pass
    cdef cppclass Unitset:
        pass
    cdef cppclass UpgradeType:
        pass
    cdef cppclass WeaponType:
        pass
    cdef cppclass UnitType:
        pass
    cdef cppclass TilePosition:
        pass
    cdef struct PlayerData:
        pass
    ctypedef PlayerInterface *Player
    cdef cppclass PlayerInterface:
        int getID()
        string getName() const
        const Unitset &getUnits() const
        Race getRace() const
        PlayerType getType() const
        Force getForce() const
        bool isAlly(Player player) const
        bool isEnemy(Player player) const
        bool isNeutral() const
        TilePosition getStartLocation() const
        bool isVictorious() const
        bool isDefeated() const
        bool leftGame() const
        int minerals() const
        int gas() const
        int gatheredMinerals() const
        int gatheredGas() const
        int repairedMinerals() const
        int repairedGas() const
        int refundedMinerals() const
        int refundedGas() const
        int spentMinerals() const
        int spentGas() const
        int supplyTotal() const
        int supplyTotal(Race race) const
        int supplyUsed() const
        int supplyUsed(Race race) const
        int allUnitCount() const
        int allUnitCount(UnitType unit) const
        int visibleUnitCount() const
        int visibleUnitCount(UnitType unit) const
        int completedUnitCount() const
        int completedUnitCount(UnitType unit) const
        int incompleteUnitCount() const
        int incompleteUnitCount(UnitType unit) const
        int deadUnitCount() const
        int deadUnitCount(UnitType unit) const
        int killedUnitCount() const
        int killedUnitCount(UnitType unit) const
        int getUpgradeLevel(UpgradeType upgrade) const
        bool hasResearched(TechType tech) const
        bool isResearching(TechType tech) const
        bool isUpgrading(UpgradeType upgrade) const
        Color getColor() const
        char getTextColor() const
        int maxEnergy(UnitType unit) const
        double topSpeed(UnitType unit) const
        int weaponMaxRange(WeaponType weapon) const
        int sightRange(UnitType unit) const
        int weaponDamageCooldown(UnitType unit) const
        int armor(UnitType unit) const
        int damage(WeaponType wpn) const
        int getUnitScore() const
        int getKillScore() const
        int getBuildingScore() const
        int getRazingScore() const
        int getCustomScore() const
        bool isObserver() const
        int getMaxUpgradeLevel(UpgradeType upgrade) const
        bool isResearchAvailable(TechType tech) const
        bool isUnitAvailable(UnitType unit) const
