﻿#from cType cimport Type

cdef extern from *:
    ctypedef int UnknownColor "255"
    
cdef extern from "BWAPI/Color.h" namespace "BWAPI":
    
    #cdef cppclass Color(Type[Color,UnknownColor]):
    cdef cppclass Color:
        Color()
        Color(int id)
        Color(int red, int green, int blue)
        int red() const
        int green() const
        int blue() const
		
cdef extern from "BWAPI/Color.h" namespace "BWAPI::Colors":
    const Color Red
    const Color Blue
    const Color Teal
    const Color Purple
    const Color Orange
    const Color Brown
    const Color White
    const Color Yellow
    const Color Green
    const Color Black
    const Color Grey
		
#TODO:
#std::ostream &operator << (std::ostream &out, const Text::Enum &t)