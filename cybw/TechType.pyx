﻿cimport cybw.cTechType as cTechType
cimport cybw.cTechTypesEnum as cTechTypesEnum
from enum import IntEnum
from cybw.Race cimport Race, RaceFactory
from cybw.GameClient cimport UnitType, UnitTypeFactory
from cybw.WeaponType cimport WeaponType, WeaponTypeFactory
from cybw.Order cimport Order, OrderFactory
from cybw.TechType cimport TechType, TechTypeFactory

cdef class TechType:
    def __init__(self, id = -1):
        if(id >= 0 and id < cTechTypesEnum.MAX):
            self.thisobj = cTechType.TechType(id)

    def __str__(self):
        return self.getName()

    def __richcmp__(x, y, int op):
        if op == 0:     # lt
            return x.getID() < y.getID()
        elif op == 2:   # eq
            return x.getID() == y.getID()
        elif op == 4:   # gt
            return x.getID() > y.getID()
        elif op == 1:   # lte
            return x.getID() <= y.getID()
        elif op == 3:   # ne
            return x.getID() != y.getID()
        elif op == 5:   # gte
            return x.getID() >= y.getID()
        return False

    def __hash__(self):
        return self.getID()

    def getRace(self):  # const
        return RaceFactory(self.thisobj.getRace())

    def mineralPrice(self):  # const
        return self.thisobj.mineralPrice()

    def gasPrice(self):  # const
        return self.thisobj.gasPrice()

    def researchTime(self):  # const
        return self.thisobj.researchTime()

    def energyCost(self):  # const
        return self.thisobj.energyCost()

    def whatResearches(self):  # const
        return UnitTypeFactory(self.thisobj.whatResearches())

    def getWeapon(self):  # const
        return WeaponTypeFactory(self.thisobj.getWeapon())

    def targetsUnit(self):  # const
        return self.thisobj.targetsUnit()

    def targetsPosition(self):  # const
        return self.thisobj.targetsPosition()

    #const UnitType::const_set& whatUses() const
    def getOrder(self):  # const
        return OrderFactory(self.thisobj.getOrder())

    # inherited
    def getName(self):
        name = self.thisobj.getName()
        pystr = name.decode()
        return pystr

    def getID(self):
        return self.thisobj.getID()


cdef TechTypeFactory(cTechType.TechType ctt):
    tech = TechType()
    tech.thisobj = ctt
    return tech


cdef TechTypeSetFactory(const SetContainer[cTechType.TechType]& tts):
    pyset = set()
    for tt in tts:
        pyset.add(TechTypeFactory(tt))
    return pyset

class TechTypes:

    @staticmethod
    def allTechTypes():
        return TechTypeSetFactory(cTechType.allTechTypes())

    Stim_Packs = TechType(cTechTypesEnum.Stim_Packs)
    Lockdown = TechType(cTechTypesEnum.Lockdown)
    EMP_Shockwave = TechType(cTechTypesEnum.EMP_Shockwave)
    Spider_Mines = TechType(cTechTypesEnum.Spider_Mines)
    Scanner_Sweep = TechType(cTechTypesEnum.Scanner_Sweep)
    Tank_Siege_Mode = TechType(cTechTypesEnum.Tank_Siege_Mode)
    Defensive_Matrix = TechType(cTechTypesEnum.Defensive_Matrix)
    Irradiate = TechType(cTechTypesEnum.Irradiate)
    Yamato_Gun = TechType(cTechTypesEnum.Yamato_Gun)
    Cloaking_Field = TechType(cTechTypesEnum.Cloaking_Field)
    Personnel_Cloaking = TechType(cTechTypesEnum.Personnel_Cloaking)
    Burrowing = TechType(cTechTypesEnum.Burrowing)
    Infestation = TechType(cTechTypesEnum.Infestation)
    Spawn_Broodlings = TechType(cTechTypesEnum.Spawn_Broodlings)
    Dark_Swarm = TechType(cTechTypesEnum.Dark_Swarm)
    Plague = TechType(cTechTypesEnum.Plague)
    Consume = TechType(cTechTypesEnum.Consume)
    Ensnare = TechType(cTechTypesEnum.Ensnare)
    Parasite = TechType(cTechTypesEnum.Parasite)
    Psionic_Storm = TechType(cTechTypesEnum.Psionic_Storm)
    Hallucination = TechType(cTechTypesEnum.Hallucination)
    Recall = TechType(cTechTypesEnum.Recall)
    Stasis_Field = TechType(cTechTypesEnum.Stasis_Field)
    Archon_Warp = TechType(cTechTypesEnum.Archon_Warp)
    Restoration = TechType(cTechTypesEnum.Restoration)
    Disruption_Web = TechType(cTechTypesEnum.Disruption_Web)
    Mind_Control = TechType(cTechTypesEnum.Mind_Control)
    Dark_Archon_Meld = TechType(cTechTypesEnum.Dark_Archon_Meld)
    Feedback = TechType(cTechTypesEnum.Feedback)
    Optical_Flare = TechType(cTechTypesEnum.Optical_Flare)
    Maelstrom = TechType(cTechTypesEnum.Maelstrom)
    Lurker_Aspect = TechType(cTechTypesEnum.Lurker_Aspect)
    Healing = TechType(cTechTypesEnum.Healing)
    none = TechType(cTechTypesEnum.None)
    Nuclear_Strike = TechType(cTechTypesEnum.Nuclear_Strike)
    Unknown = TechType(cTechTypesEnum.Unknown)

    class Enum(IntEnum):
        Stim_Packs = cTechTypesEnum.Stim_Packs
        Lockdown = cTechTypesEnum.Lockdown
        EMP_Shockwave = cTechTypesEnum.EMP_Shockwave
        Spider_Mines = cTechTypesEnum.Spider_Mines
        Scanner_Sweep = cTechTypesEnum.Scanner_Sweep
        Tank_Siege_Mode = cTechTypesEnum.Tank_Siege_Mode
        Defensive_Matrix = cTechTypesEnum.Defensive_Matrix
        Irradiate = cTechTypesEnum.Irradiate
        Yamato_Gun = cTechTypesEnum.Yamato_Gun
        Cloaking_Field = cTechTypesEnum.Cloaking_Field
        Personnel_Cloaking = cTechTypesEnum.Personnel_Cloaking
        Burrowing = cTechTypesEnum.Burrowing
        Infestation = cTechTypesEnum.Infestation
        Spawn_Broodlings = cTechTypesEnum.Spawn_Broodlings
        Dark_Swarm = cTechTypesEnum.Dark_Swarm
        Plague = cTechTypesEnum.Plague
        Consume = cTechTypesEnum.Consume
        Ensnare = cTechTypesEnum.Ensnare
        Parasite = cTechTypesEnum.Parasite
        Psionic_Storm = cTechTypesEnum.Psionic_Storm
        Hallucination = cTechTypesEnum.Hallucination
        Recall = cTechTypesEnum.Recall
        Stasis_Field = cTechTypesEnum.Stasis_Field
        Archon_Warp = cTechTypesEnum.Archon_Warp
        Restoration = cTechTypesEnum.Restoration
        Disruption_Web = cTechTypesEnum.Disruption_Web
        Mind_Control = cTechTypesEnum.Mind_Control
        Dark_Archon_Meld = cTechTypesEnum.Dark_Archon_Meld
        Feedback = cTechTypesEnum.Feedback
        Optical_Flare = cTechTypesEnum.Optical_Flare
        Maelstrom = cTechTypesEnum.Maelstrom
        Lurker_Aspect = cTechTypesEnum.Lurker_Aspect
        Healing = cTechTypesEnum.Healing
        none = cTechTypesEnum.None
        Nuclear_Strike = cTechTypesEnum.Nuclear_Strike
        Unknown = cTechTypesEnum.Unknown