cimport cybw.cDamageType as cDamageType

cdef class DamageType:
    cdef cDamageType.DamageType thisobj

    @staticmethod
    cdef create(cDamageType.DamageType o_DamageType)


cdef DamageTypeSetFactory(const cDamageType.set &cset)
