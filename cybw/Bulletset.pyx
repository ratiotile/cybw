﻿from cybw cimport cBulletset

cdef class Bulletset:
    cdef cBulletset.Bulletset *thisptr

    def __init__(self):
        pass

cdef ConstBulletSetFactory(const cBulletset.Bulletset& cbs):
    bullets = set()
    for bullet in cbs:
        bullets.add(BulletFactory(bullet))
    return bullets