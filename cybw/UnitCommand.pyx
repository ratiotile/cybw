﻿cimport cybw.cUnitCommand as cUnitCommand

cdef class UnitCommand:
    def __init__(self):
        pass


cdef UnitCommandFactory(cUnitCommand.UnitCommand cunitcommand):
    unitcommand = UnitCommand()
    unitcommand.thisobj = cunitcommand
    return unitcommand
