﻿cimport cybw.cFlag as cFlag
from enum import IntEnum

class Flag(IntEnum):
    CompleteMapInformation  = cFlag.CompleteMapInformation
    UserInput               = cFlag.UserInput
    Max                     = cFlag.Max
