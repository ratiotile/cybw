cimport cybw.cExplosionType as cExplosionType

cdef class ExplosionType:
    cdef cExplosionType.ExplosionType thisobj

    @staticmethod
    cdef create(cExplosionType.ExplosionType o_ExplosionType)


cdef ExplosionTypeSetFactory(const cExplosionType.set &cset)