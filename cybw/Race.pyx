﻿from cybw cimport cRace
from cybw cimport cRacesEnum
from cybw.GameClient cimport UnitTypeFactory
from enum import IntEnum

cdef class Race:
    def __init__(self, int id = -1):
        if(id >= 0 and id < cRace.enumMAX):
            self.thisobj = cRace.Race(id)

    def __str__(self):
        return self.getName()

    def __richcmp__(x, y, int op):
        if op == 0:     # lt
            return x.getID() < y.getID()
        elif op == 2:   # eq
            return x.getID() == y.getID()
        elif op == 4:   # gt
            return x.getID() > y.getID()
        elif op == 1:   # lte
            return x.getID() <= y.getID()
        elif op == 3:   # ne
            return x.getID() != y.getID()
        elif op == 5:   # gte
            return x.getID() >= y.getID()
        return False

    def __hash__(self):
        return self.getID()

    def getCenter(self):
        return UnitTypeFactory(self.thisobj.getCenter())

    def getID(self):
        return self.thisobj.getID()

    def getName(self):
        name = self.thisobj.getName()
        pystr = name.decode()
        return pystr

    def getRefinery(self):
        return UnitTypeFactory(self.thisobj.getRefinery())

    def getSupplyProvider(self):
        return UnitTypeFactory(self.thisobj.getSupplyProvider())

    def getTransport(self):
        return UnitTypeFactory(self.thisobj.getTransport())

    def getWorker(self):
        return UnitTypeFactory(self.thisobj.getWorker())

cdef Race RaceFactory(cRace.Race race):
    r = Race()
    r.thisobj = race
    return r

cdef ConstRaceSetFactory(const cRace.set &cset):
    rset = set()
    for i in cset:
        rset.add(RaceFactory(i))
    return rset

class Races:
    @staticmethod
    def allRaces():
        return ConstRaceSetFactory(cRace.allRaces())

    Zerg = RaceFactory(cRace.Zerg)
    Terran = RaceFactory(cRace.Terran)
    Protoss = RaceFactory(cRace.Protoss)
    Random = RaceFactory(cRace.Random)
    none = RaceFactory(cRace.None)
    Unknown = RaceFactory(cRace.Unknown)
    class Enum(IntEnum):
        Zerg = cRacesEnum.Zerg
        Terran = cRacesEnum.Terran
        Protoss = cRacesEnum.Protoss
        Random = cRacesEnum.Random
        none = cRacesEnum.None
        Unknown = cRacesEnum.Unknown