﻿cimport cybw.cRegionset as cRegionset

cdef class Regionset:
    cdef cRegionset.Regionset *thisptr

cdef ConstRegionSetFactory(const cRegionset.Regionset& crs)

cdef RegionSetFactory(cRegionset.Regionset& rs)
