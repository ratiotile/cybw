from cybw.cType cimport Type

cdef extern from "BWAPI/ExplosionType.h" namespace "BWAPI":
    cdef cppclass ExplosionType(Type[ExplosionType, int]):
        DamageType()
        DamageType(int id)

cdef extern from "BWAPI/ExplosionType.h" namespace "BWAPI::ExplosionType":
    cdef cppclass set:
        cppclass iterator:
            ExplosionType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)
        iterator begin()
        iterator end()

        cppclass const_iterator:
            ExplosionType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)
        const_iterator cbegin()
        const_iterator cend()

cdef extern from "BWAPI/ExplosionType.h" namespace "BWAPI::ExplosionTypes":
    const set& allExplosionTypes()

    ExplosionType None
    ExplosionType Normal
    ExplosionType Radial_Splash
    ExplosionType Enemy_Splash
    ExplosionType Lockdown
    ExplosionType Nuclear_Missile
    ExplosionType Parasite
    ExplosionType Broodlings
    ExplosionType EMP_Shockwave
    ExplosionType Irradiate
    ExplosionType Ensnare
    ExplosionType Plague
    ExplosionType Stasis_Field
    ExplosionType Dark_Swarm
    ExplosionType Consume
    ExplosionType Yamato_Gun
    ExplosionType Restoration
    ExplosionType Disruption_Web
    ExplosionType Corrosive_Acid
    ExplosionType Mind_Control
    ExplosionType Feedback
    ExplosionType Optical_Flare
    ExplosionType Maelstrom
    ExplosionType Air_Splash
    ExplosionType Unknown