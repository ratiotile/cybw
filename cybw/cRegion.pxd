﻿from cybw.cPosition cimport Position
from libcpp cimport bool

cdef extern from "BWAPI/Region.h" namespace "BWAPI":
    cdef cppclass Regionset:
        pass

    cdef cppclass Unitset:
        pass

    ctypedef RegionInterface *Region

    cdef cppclass RegionInterface:
        int getID()  except +
        int getRegionGroupID() except +
        Position getCenter() except +
        bool isHigherGround() except +
        int getDefensePriority() except +
        bool isAccessible() except +
        const Regionset &getNeighbors() const
        int getBoundsLeft() except +
        int getBoundsTop() except +
        int getBoundsRight() except +
        int getBoundsBottom() except +
        Region getClosestAccessibleRegion() except +
        Region getClosestInaccessibleRegion() except +
        int getDistance(Region other) except +
        #Unitset getUnits(const UnitFilter &pred ) const
        Unitset getUnits() except +