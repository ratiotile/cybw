cimport cybw.cExplosionType as cExplosionType

cdef class ExplosionType:

    @staticmethod
    cdef create(cExplosionType.ExplosionType o_ExplosionType):
        d = ExplosionType()
        d.thisobj = o_ExplosionType
        return d

    def __int__(self):
        return self.thisobj.getID()

    def __richcmp__(ExplosionType self, ExplosionType other, op):
        if op == 2:
            return int(self) == int(other)
        else:
            raise NotImplementedError

    def getName(self):
        name = self.thisobj.getName()
        return name.decode()

    def getID(self):
        return self.thisobj.getID()


cdef ExplosionTypeSetFactory(const cExplosionType.set &cset):
    dts = set()
    for i in cset:
        dts.add(ExplosionType.create(i))
    return dts

cdef class ExplosionTypes:
    @staticmethod
    def allExplosionTypes():
        return ExplosionTypeSetFactory(cExplosionType.allExplosionTypes())

    none = ExplosionType.create(cExplosionType.None)
    Normal = ExplosionType.create(cExplosionType.Normal)
    Radial_Splash = ExplosionType.create(cExplosionType.Radial_Splash)
    Enemy_Splash = ExplosionType.create(cExplosionType.Enemy_Splash)
    Lockdown = ExplosionType.create(cExplosionType.Lockdown)
    Nuclear_Missile = ExplosionType.create(cExplosionType.Nuclear_Missile)
    Parasite = ExplosionType.create(cExplosionType.Parasite)
    Broodlings = ExplosionType.create(cExplosionType.Broodlings)
    EMP_Shockwave = ExplosionType.create(cExplosionType.EMP_Shockwave)
    Irradiate = ExplosionType.create(cExplosionType.Irradiate)
    Ensnare = ExplosionType.create(cExplosionType.Ensnare)
    Plague = ExplosionType.create(cExplosionType.Plague)
    Stasis_Field = ExplosionType.create(cExplosionType.Stasis_Field)
    Dark_Swarm = ExplosionType.create(cExplosionType.Dark_Swarm)
    Consume = ExplosionType.create(cExplosionType.Consume)
    Yamato_Gun = ExplosionType.create(cExplosionType.Yamato_Gun)
    Restoration = ExplosionType.create(cExplosionType.Restoration)
    Disruption_Web = ExplosionType.create(cExplosionType.Disruption_Web)
    Corrosive_Acid = ExplosionType.create(cExplosionType.Corrosive_Acid)
    Mind_Control = ExplosionType.create(cExplosionType.Mind_Control)
    Feedback = ExplosionType.create(cExplosionType.Feedback)
    Optical_Flare = ExplosionType.create(cExplosionType.Optical_Flare)
    Maelstrom = ExplosionType.create(cExplosionType.Maelstrom)
    Air_Splash = ExplosionType.create(cExplosionType.Air_Splash)
    Unknown = ExplosionType.create(cExplosionType.Unknown)
