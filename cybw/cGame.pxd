﻿from libcpp cimport bool
from libcpp.string cimport string
from libcpp.list cimport list
from cybw.cInterface cimport Interface

from cybw.cColor cimport Color
from cybw.cPosition cimport *
from cybw.cUnit cimport Unit
from cybw.cEvent cimport Event
from cybw.cText cimport TextSizeEnum
from cybw.cUnitType cimport UnitType
from cybw.cInput cimport Key, MouseButton
from cybw.cRegionset cimport Regionset

#needed for variable args
cdef extern from "stdarg.h":
    ctypedef struct va_list:
        pass


cdef extern from "BWAPI/Game.h" namespace "BWAPI":

    cdef cppclass Bulletset:
        pass
    cdef cppclass Color:
        pass
    cdef cppclass ForceInterface:
        pass
    ctypedef ForceInterface *Force
    cdef cppclass Forceset:
        pass
    cdef cppclass GameType:
        pass
    cdef cppclass PlayerInterface:
        pass
    ctypedef PlayerInterface *Player
    cdef cppclass Playerset:
        pass
    cdef cppclass Race:
        pass

    cdef cppclass RegionInterface:
        pass
    ctypedef RegionInterface *Region

    cdef cppclass Regionset:
        pass
    cdef cppclass TechType:
        pass
    cdef cppclass UnitCommand:
        pass
    cdef cppclass Unitset:
        pass
    cdef cppclass UpgradeType:
        pass

    cdef cppclass Game(Interface[Game]):
        const Forceset& getForces() nogil
        const Playerset& getPlayers() nogil
        const Unitset& getAllUnits() nogil
        const Unitset& getMinerals() nogil
        const Unitset& getGeysers() nogil
        const Unitset& getNeutralUnits() nogil
        const Unitset& getStaticMinerals() nogil
        const Unitset& getStaticGeysers() nogil
        const Unitset& getStaticNeutralUnits() nogil
        const Bulletset& getBullets() const
        const Position_list& getNukeDots() nogil
        const list[ Event ]& getEvents() nogil
        Force getForce(int forceID) nogil
        Player getPlayer(int playerID) const
        Unit getUnit(int unitID) const
        Unit indexToUnit(int unitIndex) const
        Region getRegion(int regionID) const
        GameType getGameType() const
        int getLatency() const
        int getFrameCount() const
        int getReplayFrameCount() const
        int getFPS() const
        double getAverageFPS() const
        Position getMousePosition() const
        bool getMouseState(MouseButton button) const
        bool getKeyState(Key key) const
        Position getScreenPosition() const
        void setScreenPosition(int x, int y)
        void setScreenPosition(Position p)
        void pingMinimap(int x, int y)
        pingMinimap(Position p)
        bool isFlagEnabled(int flag) const
        void enableFlag(int flag)
        #Unitset getUnitsOnTile(int tileX, int tileY, const UnitFilter &pred = nullptr) const
        Unitset getUnitsOnTile(int tileX, int tileY) const
        #Unitset getUnitsOnTile(TilePosition tile, const UnitFilter &pred = nullptr) const
        Unitset getUnitsOnTile(TilePosition tile) const
        #Unitset getUnitsInRectangle(int left, int top, int right, int bottom, const UnitFilter &pred = nullptr) const
        Unitset getUnitsInRectangle(int left, int top, int right, int bottom) const
        #Unitset getUnitsInRectangle(Position topLeft, Position bottomRight, const UnitFilter &pred = nullptr) const
        Unitset getUnitsInRectangle(Position topLeft, Position bottomRight) const
        #Unitset getUnitsInRadius(int x, int y, int radius, const UnitFilter &pred = nullptr) const
        Unitset getUnitsInRadius(int x, int y, int radius) const
        #Unitset getUnitsInRadius(Position center, int radius, const UnitFilter &pred = nullptr) const
        Unitset getUnitsInRadius(Position center, int radius) const
        #Unit getClosestUnit(Position center, const UnitFilter &pred = nullptr, int radius = 999999) const
        Unit getClosestUnit(Position center) const
        #Unit getClosestUnitInRectangle(Position center, const UnitFilter &pred = nullptr, int left = 0, int top = 0, int right = 999999, int bottom = 999999) const
        #Unit getBestUnit(const BestUnitFilter &best, const UnitFilter &pred, Position center = Positions::Origin, int radius = 999999) const
        #Error getLastError() const
        #bool setLastError(Error e = Errors::None) const
        int mapWidth() nogil
        int mapHeight() nogil
        string mapFileName() nogil
        string mapPathName() nogil
        string mapName() nogil
        string mapHash() nogil
        bool isWalkable(int walkX, int walkY) nogil
        bool isWalkable(WalkPosition position) nogil
        int getGroundHeight(int tileX, int tileY) nogil
        int getGroundHeight(TilePosition position) nogil
        bool isBuildable(int tileX, int tileY, bool includeBuildings) nogil
        bool isBuildable(TilePosition position, bool includeBuildings) nogil
        bool isVisible(int tileX, int tileY) nogil
        bool isVisible(TilePosition position) nogil
        bool isExplored(int tileX, int tileY) nogil
        bool isExplored(TilePosition position) nogil
        bool hasCreep(int tileX, int tileY) const
        bool hasCreep(TilePosition position) const
        bool hasPowerPrecise(int x, int y, UnitType unitType) const
        bool hasPowerPrecise(Position position, UnitType unitType) const
        bool hasPower(int tileX, int tileY, UnitType unitType) const
        bool hasPower(TilePosition position, UnitType unitType) const
        bool hasPower(int tileX, int tileY, int tileWidth, int tileHeight, UnitType unitType) const
        bool hasPower(TilePosition position, int tileWidth, int tileHeight, UnitType unitType) const

        bool canBuildHere(TilePosition position, UnitType type, Unit builder, bool checkExplored)
        bool canMake(UnitType type, Unit builder) const
        bool canMake(UnitType type) const
        bool canResearch(TechType type, Unit unit, bool checkCanIssueCommandType)
        bool canResearch(TechType type, Unit unit)
        bool canResearch(TechType type)

        bool canUpgrade(UpgradeType type, Unit unit, bool checkCanIssueCommandType)
        const TilePosition_list& getStartLocations() const
        void printf(const char *format, ...)
        void vPrintf(const char *format, va_list args)
        void sendText(const char *format, ...)
        void vSendText(const char *format, va_list args)
        void sendTextEx(bool toAllies, const char *format, ...)
        void vSendTextEx(bool toAllies, const char *format, va_list args)
        bool isInGame() nogil
        bool isMultiplayer() const
        bool isBattleNet() const
        bool isPaused() const
        bool isReplay() const
        void pauseGame()
        void resumeGame()
        void leaveGame()
        void restartGame()
        void setLocalSpeed(int speed)
        bool issueCommand(const Unitset& units, UnitCommand command)
        const Unitset& getSelectedUnits() const
        Player self() const
        Player enemy() const
        Player neutral() const
        Playerset& allies()
        Playerset& enemies()
        Playerset& observers()
        void setTextSize()
        void setTextSize(TextSizeEnum size)
        #void vDrawText(CoordinateType::Enum ctype, int x, int y, const char *format, va_list arg)
        #void drawText(CoordinateType::Enum ctype, int x, int y, const char *format, ...)
        void drawTextMap(int x, int y, const char *format, ...) nogil
        void drawTextMap(Position p, const char *format, ...) nogil
        void drawTextMouse(int x, int y, const char *format, ...) nogil
        void drawTextMouse(Position p, const char *format, ...) nogil
        void drawTextScreen(int x, int y, const char *format, ...) nogil
        void drawTextScreen(Position p, const char *format, ...) nogil
        # void drawBox(CoordinateType::Enum ctype, int left, int top, int right, int bottom, Color color, bool isSolid = false) = 0
        void drawBoxMap(int left, int top, int right, int bottom,
                        Color color, bool isSolid) nogil
        void drawBoxMap(Position leftTop, Position rightBottom, Color color,
                        bool isSolid) nogil
        void drawBoxMouse(int left, int top, int right, int bottom,
                          Color color, bool isSolid) nogil
        void drawBoxMouse(Position leftTop, Position rightBottom,
                          Color color, bool isSolid) nogil
        void drawBoxScreen(int left, int top, int right, int bottom,
                           Color color, bool isSolid) nogil
        void drawBoxScreen(Position leftTop, Position rightBottom,
                           Color color, bool isSolid) nogil
        #void drawTriangle(CoordinateType::Enum ctype, int ax, int ay, int bx, int by, int cx, int cy, Color color, bool isSolid = false) = 0
        void drawTriangleMap(int ax, int ay, int bx, int _by, int cx, int cy,
                             Color color, bool isSolid) nogil
        void drawTriangleMap(Position a, Position b, Position c, Color color,
                             bool isSolid) nogil
        void drawTriangleMouse(int ax, int ay, int bx, int _by, int cx,
                               int cy, Color color, bool isSolid) nogil
        void drawTriangleMouse(Position a, Position b, Position c,
                               Color color, bool isSolid) nogil
        void drawTriangleScreen(int ax, int ay, int bx, int _by, int cx,
                                int cy, Color color, bool isSolid) nogil
        void drawTriangleScreen(Position a, Position b, Position c,
                                Color color, bool isSolid) nogil
        #void drawCircle(CoordinateType::Enum ctype, int x, int y, int radius, Color color, bool isSolid = false) = 0
        void drawCircleMap(int x, int y, int radius, Color color, bool isSolid) nogil
        void drawCircleMap(int x, int y, int radius, Color color) nogil
        void drawCircleMap(Position p, int radius, Color color, bool isSolid) nogil
        void drawCircleMap(Position p, int radius, Color color) nogil

        void drawCircleMouse(int x, int y, int radius, Color color,
                             bool isSolid) nogil
        void drawCircleMouse(Position p, int radius, Color color, bool isSolid) nogil
        void drawCircleScreen(int x, int y, int radius, Color color,
                              bool isSolid) nogil
        void drawCircleScreen(Position p, int radius, Color color,
                              bool isSolid) nogil
        #void drawEllipse(CoordinateType::Enum ctype, int x, int y, int xrad, int yrad, Color color, bool isSolid = false) = 0
        void drawEllipseMap(int x, int y, int xrad, int yrad, Color color,
                            bool isSolid) nogil
        void drawEllipseMap(Position p, int xrad, int yrad, Color color,
                            bool isSolid) nogil
        void drawEllipseMouse(int x, int y, int xrad, int yrad, Color color,
                              bool isSolid) nogil
        void drawEllipseMouse(Position p, int xrad, int yrad, Color color,
                              bool isSolid) nogil
        void drawEllipseScreen(int x, int y, int xrad, int yrad, Color color,
                               bool isSolid) nogil
        void drawEllipseScreen(Position p, int xrad, int yrad, Color color,
                               bool isSolid) nogil
        #void drawDot(CoordinateType::Enum ctype, int x, int y, Color color) = 0
        void drawDotMap(int x, int y, Color color) nogil
        void drawDotMap(Position p, Color color) nogil
        void drawDotMouse(int x, int y, Color color) nogil
        void drawDotMouse(Position p, Color color) nogil
        void drawDotScreen(int x, int y, Color color) nogil
        void drawDotScreen(Position p, Color color) nogil
        #void drawLine(CoordinateType::Enum ctype, int x1, int y1, int x2, int y2, Color color) = 0
        void drawLineMap(int x1, int y1, int x2, int y2, Color color) nogil
        void drawLineMap(Position a, Position b, Color color) nogil
        void drawLineMouse(int x1, int y1, int x2, int y2, Color color) nogil
        void drawLineMouse(Position a, Position b, Color color) nogil
        void drawLineScreen(int x1, int y1, int x2, int y2, Color color) nogil
        void drawLineScreen(Position a, Position b, Color color) nogil
        int getLatencyFrames() const
        int getLatencyTime() const
        int getRemainingLatencyFrames() const
        int getRemainingLatencyTime() const
        int getRevision() const
        bool isDebug() const
        bool isLatComEnabled() const
        void setLatCom(bool isEnabled)
        bool isGUIEnabled() const
        void setGUI(bool enabled)
        int getInstanceNumber() const
        int getAPM() const
        int getAPM(bool includeSelects) const
        bool setMap(const char *mapFileName)
        bool setMap(const string &mapFileName)
        void setFrameSkip(int frameSkip)
        bool hasPath(Position source, Position destination) const
        bool setAlliance(Player player, bool allied, bool alliedVictory)
        bool setVision(Player player, bool enabled)
        int  elapsedTime() const
        void setCommandOptimizationLevel(int level)
        int countdownTimer() const
        const Regionset &getAllRegions() const
        Region getRegionAt(int x, int y) const
        Region getRegionAt(Position position) const
        int getLastEventTime() const
        bool setRevealAll(bool reveal)
        TilePosition getBuildLocation(UnitType type, TilePosition desiredPosition, int maxRange, bool creep)
        int getDamageFrom(UnitType fromType, UnitType toType, Player fromPlayer, Player toPlayer) const
        int getDamageTo(UnitType toType, UnitType fromType, Player toPlayer, Player fromPlayer) const


    Game *BroodwarPtr

cdef extern from "BWAPI/Game.h" namespace "std":
    cdef cppclass ostream:
        pass
    ctypedef ostream (*ostream_manipulator)(ostream)

cdef extern from "<ostream>" namespace "std":
    ostream& endl(ostream& os)

cdef extern from "BWAPI/Game.h" namespace "BWAPI":
    cdef cppclass GameWrapper:
        Game* opStructDereference "operator ->" () const
        GameWrapper& operator<<( ostream_manipulator &)
        GameWrapper& operator<<( string &)
        void flush()

    GameWrapper Broodwar