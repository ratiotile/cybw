cdef extern from "BWAPI/Race.h" namespace "BWAPI::Races::Enum":
    cdef enum Enum:
        Zerg = 0,
        Terran,
        Protoss,
        Other,
        Unused,
        Select,
        Random,
        None,
        Unknown,
        MAX