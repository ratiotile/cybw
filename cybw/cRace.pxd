﻿from libcpp.string cimport string
from cybw.cUnitType cimport UnitType


cdef extern from "BWAPI/Race.h":
    cdef enum Enum:
        enumZerg "BWAPI::Races::Enum::Zerg" = 0,
        enumTerran 'BWAPI::Races::Enum::Terran',
        enumProtoss 'BWAPI::Races::Enum::Protoss',
        enumOther 'BWAPI::Races::Enum::Other',
        enumUnused 'BWAPI::Races::Enum::Unused',
        enumSelect 'BWAPI::Races::Enum::Select',
        enumRandom 'BWAPI::Races::Enum::Random',
        enumNone 'BWAPI::Races::Enum::None',
        enumUnknown 'BWAPI::Races::Enum::Unknown',
        enumMAX 'BWAPI::Races::Enum::MAX'


cdef extern from "BWAPI/Race.h" namespace "BWAPI":
    cdef cppclass UnitType:
        pass


#const Race::const_set& alLRaces()

    cdef cppclass Race:
        Race()
        Race(int id )
        UnitType getWorker() const
        UnitType getCenter() const
        UnitType getRefinery() const
        UnitType getTransport() const
        UnitType getSupplyProvider() const

        #inherited
        const string &getName() const
        int getID() const

cdef extern from "BWAPI/Race.h" namespace "BWAPI::Race":
    cdef cppclass set:
        cppclass iterator:
            Race& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)
        iterator begin()
        iterator end()

        cppclass const_iterator:
            Race& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)
        const_iterator cbegin()
        const_iterator cend()

cdef extern from "BWAPI/Race.h" namespace "BWAPI::Races":
    # why doesn't this work?
    #const set[Race]& allRaces()
    const set& allRaces()

    const Race Zerg
    const Race Terran
    const Race Protoss
    const Race Random
    const Race None
    const Race Unknown