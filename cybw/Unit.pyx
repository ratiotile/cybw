﻿from cython.operator cimport dereference as deref
from cybw.cPositionUnit cimport PositionOrUnit as cPositionOrUnit
from cybw.WeaponType cimport WeaponType
from cybw.UnitCommand cimport UnitCommand, UnitCommandFactory

from cybw.TechType cimport TechType, TechTypeFactory
from cybw.UpgradeType cimport UpgradeType, UpgradeTypeFactory
from cybw.UnitCommandType cimport UnitCommandType

from cybw.Order cimport Order, OrderFactory
from cybw.Unitset cimport Unitset, UnitsetFactory
# circular include!
#from cybw.UnitType cimport UnitTypeListFactory

cdef class Unit(PositionUnitConverter):
    """ A Python wrapper around the BWAPI Unit class. Holds a pointer to the C++ object hidden from Python code. """

    def __init__(self):
        """ We cannot pass a C++ object into a python method, and `__init__()` must be a python method, not a cdef method. """
        pass

    def __str__(self):
        """ returns a string with format `[unit id] name` """
        id = self.getID()
        name = self.getType().getName()
        return ("["+str(id)+"]"+name)

    def __hash__(self):
        return self.getID()

    def __richcmp__(Unit self, Unit other, optype):
        if optype   == 0: # <
            return self.thisptr < other.thisptr
        elif optype == 2: # ==
            return self.thisptr == other.thisptr
        elif optype == 4: # >
            return self.thisptr > other.thisptr
        elif optype == 1: # <=
            return self.thisptr <= other.thisptr
        elif optype == 3: # !=
            return self.thisptr != other.thisptr
        elif optype == 5: # >=
            return self.thisptr >= other.thisptr
        raise Exception("optype {} not defined".format(optype))

    cdef cPositionOrUnit getPositionOrUnit(self):
        """ this function used internally to convert Unit into PositionOrUnit for use in methods which take a PositionOrUnit as a workaround for supporting c++'s converting constructors """
        return cPositionOrUnit(self.thisptr)

    def getID(self):
        """ __getID()__ returns an int ID. """
        return self.thisptr.getID()

    def exists(self):
        """ __exists()__ returns a bool indicating if unit exists or not. Note that inaccessible units will always return false. """
        return self.thisptr.exists()

    def getReplayID(self):
        """ __getReplayID()__ returns int replay ID """
        return self.thisptr.getReplayID()

    def getPlayer(self):
        """ __getPlayer()__ returns a Player wrapper object. """
        return PlayerFactory(self.thisptr.getPlayer())

    def getType(self):
        """ __getType()__ returns a UnitType wrapper object. """
        return UnitTypeFactory(self.thisptr.getType())

    def getPosition(self):
        """ __getPosition()__ returns a Position wrapper object. """
        return PositionFactory(self.thisptr.getPosition())

    def getTilePosition(self):
        """ __getTilePosition()__ returns a TilePosition wrapper object. """
        return TilePositionFactory(self.thisptr.getTilePosition())

    def getAngle(self):
        """ __getAngle()__ returns a double indicating unit rotation in radians, where 0 is east. """
        return self.thisptr.getAngle()

    def getVelocityX(self):
        """ __getVelocityX()__ returns the units velocity in the X axis as a double. """
        return self.thisptr.getVelocityX()

    def getVelocityY(self):
        """ __getVelocityX()__ returns the units velocity in the Y axis as a double. """
        return self.thisptr.getVelocityY()

    def getRegion(self):
        return RegionFactory(self.thisptr.getRegion())

    def getLeft(self):
        """ __getLeft()__ returns number of pixels from unit's left boundary to the left side of the map. """
        return self.thisptr.getLeft()

    def getTop(self):
        """ __getTop()__ returns number of pixels from unit's top boundary to the top of the map. """
        return self.thisptr.getTop()

    def getRight(self):
        """ __getRight()__ returns number of pixels from unit's right boundary to the left side of the map. """
        return self.thisptr.getRight()

    def getBottom(self):
        """ __getBottom()__ returns number of pixels from unit's bottom boundary to the top of the map. """
        return self.thisptr.getBottom()

    def getHitPoints(self):
        """ __getHitPoints()__ returns the number of hit points remaining. """
        return self.thisptr.getHitPoints()

    def getShields(self):
        """ __getShields()__ returns the number of shield points this unit has remaining. Units without shields will return 0. """
        return self.thisptr.getShields()

    def getEnergy(self):
        """ __getEnergy()__ returns the number of energy points this unit currently has, used for spellcasting. """
        return self.thisptr.getEnergy()

    def getResources(self):
        """ __getResources()__ returns the amount of resources (minerals/gas) that this unit has remaining. If the unit is inaccessible, then the last known amount is returned. """
        return self.thisptr.getResources()

    def getResourceGroup(self):
        """ __getResourceGroup()__ returns an int index to the expansion that this resource container is considered a part of by Starcraft's internal AI. """
        return self.thisptr.getResourceGroup()

    def getDistance(self, PositionUnitConverter target):
        """ __getDistance(target)__ takes a Position or Unit (both descend from PositionUnitConverter) as target returns the edge-to-edge distance in the case of Unit, and the edge to point distance in the case of Position. To support taking either a Unit or a Position parameter, the target will convert itself into a PositionOrUnit object.
        """
        return self.thisptr.getDistance(target.getPositionOrUnit())

    def hasPath(self, PositionUnitConverter target):
        """ __hasPath()__ using static terrain data, determines if there is a path to the target. Returns a boolean."""
        return self.thisptr.hasPath(target.getPositionOrUnit())

    def getLastCommandFrame(self):
        """ __getLastCommandFrame()__ returns the last frame number that this unit got a successfully processed command. """
        return self.thisptr.getLastCommandFrame()

    def getLastCommand(self):
        """ __getLastCommand()__ returns a UnitCommand, the last successful command that was sent to BWAPI for this unit. """
        return UnitCommandFactory(self.thisptr.getLastCommand())

    def getLastAttackingPlayer(self):
        """ __getLastAttackingPlayer()__ returns a Player, that last attacked this unit. """
        return PlayerFactory(self.thisptr.getLastAttackingPlayer())

    def getInitialType(self):
        """ __getInitialType()__ returns a UnitType, the type that this unit starts as at the beginning of the game. This is used to access the types of static neutral units such as mineral fields when they are not visible. """
        return UnitTypeFactory(self.thisptr.getInitialType())

    def getInitialPosition(self):
        return PositionFactory(self.thisptr.getInitialPosition())

    def getInitialTilePosition(self):
        return TilePositionFactory(self.thisptr.getInitialTilePosition())

    def getInitialHitPoints(self):
        return self.thisptr.getInitialHitPoints()

    def getInitialResources(self):
        return self.thisptr.getInitialResources()

    def getKillCount(self):
        return self.thisptr.getKillCount()

    def getAcidSporeCount(self):
        return self.thisptr.getAcidSporeCount()

    def getInterceptorCount(self):
        return self.thisptr.getInterceptorCount()

    def getScarabCount(self):
        return self.thisptr.getScarabCount()

    def getSpiderMineCount(self):
        return self.thisptr.getSpiderMineCount()

    def getGroundWeaponCooldown(self):
        return self.thisptr.getGroundWeaponCooldown()

    def getAirWeaponCooldown(self):
        return self.thisptr.getAirWeaponCooldown()

    def getSpellCooldown(self):
        return self.thisptr.getSpellCooldown()

    def getDefenseMatrixPoints(self):
        return self.thisptr.getDefenseMatrixPoints()

    def getDefenseMatrixTimer(self):
        return self.thisptr.getDefenseMatrixTimer()

    def getEnsnareTimer(self):
        return self.thisptr.getEnsnareTimer()

    def getIrradiateTimer(self):
        return self.thisptr.getIrradiateTimer()

    def getLockdownTimer(self):
        return self.thisptr.getLockdownTimer()

    def getMaelstromTimer(self):
        return self.thisptr.getMaelstromTimer()

    def getOrderTimer(self):
        return self.thisptr.getOrderTimer()

    def getPlagueTimer(self):
        return self.thisptr.getPlagueTimer()

    def getRemoveTimer(self):
        return self.thisptr.getRemoveTimer()

    def getStasisTimer(self):
        return self.thisptr.getStasisTimer()

    def getStimTimer(self):
        return self.thisptr.getStimTimer()

    def getBuildType(self):
        return UnitTypeFactory(self.thisptr.getBuildType())

    def getTrainingQueue(self):
        return UnitTypeListFactory(self.thisptr.getTrainingQueue())

    def getTech(self):
        return TechTypeFactory(self.thisptr.getTech())

    def getUpgrade(self):
        return UpgradeTypeFactory(self.thisptr.getUpgrade())

    def getRemainingBuildTime(self):
        return self.thisptr.getRemainingBuildTime()

    def getRemainingTrainTime(self):
        return self.thisptr.getRemainingTrainTime()

    def getRemainingResearchTime(self):
        return self.thisptr.getRemainingResearchTime()

    def getRemainingUpgradeTime(self):
        return self.thisptr.getRemainingUpgradeTime()

    def getBuildUnit(self):
        return UnitFactory(self.thisptr.getBuildUnit())

    def getTarget(self):
        return UnitFactory(self.thisptr.getTarget())

    def getTargetPosition(self):
        return PositionFactory(self.thisptr.getTargetPosition())

    def getOrder(self):
        return OrderFactory(self.thisptr.getOrder())

    def getSecondaryOrder(self):
        return OrderFactory(self.thisptr.getSecondaryOrder())

    def getOrderTarget(self):
        return UnitFactory(self.thisptr.getOrderTarget())

    def getOrderTargetPosition(self):
        return PositionFactory(self.thisptr.getOrderTargetPosition())

    def getRallyPosition(self):
        return PositionFactory(self.thisptr.getRallyPosition())

    def getRallyUnit(self):
        return UnitFactory(self.thisptr.getRallyUnit())

    def getAddon(self):
        return UnitFactory(self.thisptr.getAddon())

    def getNydusExit(self):
        return UnitFactory(self.thisptr.getNydusExit())

    def getPowerUp(self):
        return UnitFactory(self.thisptr.getPowerUp())

    def getTransport(self):
        return UnitFactory(self.thisptr.getTransport())

    def getSpaceRemaining(self):
        return self.thisptr.getSpaceRemaining()

    def getCarrier(self):
        return UnitFactory(self.thisptr.getCarrier())

    def getInterceptors(self):
        return UnitsetFactory(self.thisptr.getInterceptors())

    def getHatchery(self):
        return UnitFactory(self.thisptr.getHatchery())

    def getLarva(self):
        return UnitsetFactory(self.thisptr.getLarva())

    def getUnitsInRadius(self, int radius):
        return UnitsetFactory(self.thisptr.getUnitsInRadius(radius))

    def getUnitsInWeaponRange(self, WeaponType weapon):
        return UnitsetFactory(self.thisptr.getUnitsInWeaponRange(weapon.thisobj))

    def getClosestUnit(self):
        return UnitFactory(self.thisptr.getClosestUnit())

    def hasNuke(self):
        return self.thisptr.hasNuke()

    def isAccelerating(self):
        return self.thisptr.isAccelerating()

    def isAttacking(self):
        return self.thisptr.isAttacking()

    def isAttackFrame(self):
        return self.thisptr.isAttackFrame()

    def isBeingConstructed(self):
        return self.thisptr.isBeingConstructed()

    def isBeingGathered(self):
        return self.thisptr.isBeingGathered()

    def isBeingHealed(self):
        return self.thisptr.isBeingHealed()

    def isBlind(self):
        return self.thisptr.isBlind()

    def isBraking(self):
        return self.thisptr.isBraking()

    def isBurrowed(self):
        return self.thisptr.isBurrowed()

    def isCarryingGas(self):
        return self.thisptr.isCarryingGas()

    def isCarryingMinerals(self):
        return self.thisptr.isCarryingMinerals()

    def isCloaked(self):
        return self.thisptr.isCloaked()

    def isCompleted(self):
        return self.thisptr.isCompleted()

    def isConstructing(self):
        return self.thisptr.isConstructing()

    def isDefenseMatrixed(self):
        return self.thisptr.isDefenseMatrixed()

    def isDetected(self):
        return self.thisptr.isDetected()

    def isEnsnared(self):
        return self.thisptr.isEnsnared()

    def isFlying(self):
        return self.thisptr.isFlying()

    def isFollowing(self):
        return self.thisptr.isFollowing()

    def isGatheringGas(self):
        return self.thisptr.isGatheringGas()

    def isGatheringMinerals(self):
        return self.thisptr.isGatheringMinerals()

    def isHallucination(self):
        return self.thisptr.isHallucination()

    def isHoldingPosition(self):
        return self.thisptr.isHoldingPosition()

    def isIdle(self):
        return self.thisptr.isIdle()

    def isInterruptible(self):
        return self.thisptr.isInterruptible()

    def isInvincible(self):
        return self.thisptr.isInvincible()

    def isInWeaponRange(self, Unit target):
        return self.thisptr.isInWeaponRange(target.thisptr)

    def isIrradiated(self):
        return self.thisptr.isIrradiated()

    def isLifted(self):
        return self.thisptr.isLifted()

    def isLoaded(self):
        return self.thisptr.isLoaded()

    def isLockedDown(self):
        return self.thisptr.isLockedDown()

    def isMaelstrommed(self):
        return self.thisptr.isMaelstrommed()

    def isMorphing(self):
        return self.thisptr.isMorphing()

    def isMoving(self):
        return self.thisptr.isMoving()

    def isParasited(self):
        return self.thisptr.isParasited()

    def isPatrolling(self):
        return self.thisptr.isPatrolling()

    def isPlagued(self):
        return self.thisptr.isPlagued()

    def isRepairing(self):
        return self.thisptr.isRepairing()

    def isResearching(self):
        return self.thisptr.isResearching()

    def isSelected(self):
        return self.thisptr.isSelected()

    def isSieged(self):
        return self.thisptr.isSieged()

    def isStartingAttack(self):
        return self.thisptr.isStartingAttack()

    def isStasised(self):
        return self.thisptr.isStasised()

    def isStimmed(self):
        return self.thisptr.isStimmed()

    def isStuck(self):
        return self.thisptr.isStuck()

    def isTraining(self):
        return self.thisptr.isTraining()

    def isUnderAttack(self):
        return self.thisptr.isUnderAttack()

    def isUnderDarkSwarm(self):
        return self.thisptr.isUnderDarkSwarm()

    def isUnderDisruptionWeb(self):
        return self.thisptr.isUnderDisruptionWeb()

    def isUnderStorm(self):
        return self.thisptr.isUnderStorm()

    def isPowered(self):
        return self.thisptr.isPowered()

    def isUpgrading(self):
        return self.thisptr.isUpgrading()

    def isVisible(self, Player player):
        return self.thisptr.isVisible(player.thisptr)

    def isTargetable(self):
        return self.thisptr.isTargetable()

    def issueCommand(self, UnitCommand command):
        return self.thisptr.issueCommand(command.thisobj)

    def attack(self, PositionUnitConverter target, bool queue = False):
        return self.thisptr.attack(target.getPositionOrUnit(), queue)

    def build(self, UnitType type, TilePosition target):
        return self.thisptr.build(type.thisobj, target.thisobj)

    def buildAddon(self, UnitType type):
        return self.thisptr.buildAddon(type.thisobj)

    def train(self, UnitType trainType):
        """ __train(unitType)__ returns a bool. """
        return self.thisptr.train(trainType.thisobj)

    def morph(self, UnitType type):
        return self.thisptr.morph(type.thisobj)

    def research(self, TechType type):
        return self.thisptr.research(type.thisobj)

    def upgrade(self, UpgradeType type):
        return self.thisptr.upgrade(type.thisobj)

    def setRallyPoint(self, PositionUnitConverter target):
        return self.thisptr.setRallyPoint(target.getPositionOrUnit())

    def move(self, Position target, bool queue = False):
        return self.thisptr.move(target.thisobj, queue)

    def patrol(self, Position target, bool queue = False):
        return self.thisptr.patrol(target.thisobj, queue)

    def holdPosition(self, bool queue = False):
        return self.thisptr.holdPosition(queue)

    def stop(self, bool queue = False):
        return self.thisptr.stop( queue)

    def follow(self, Unit target, bool queue = False):
        return self.thisptr.follow(target.thisptr, queue)

    def gather(self, Unit target, bool queue = False):
        return self.thisptr.gather(target.thisptr, queue)

    def returnCargo(self, bool queue = False):
        return self.thisptr.returnCargo( queue)

    def repair(self, Unit target, bool queue = False):
        return self.thisptr.repair(target.thisptr, queue)

    def burrow(self):
        return self.thisptr.burrow()

    def unburrow(self):
        return self.thisptr.unburrow()

    def cloak(self):
        return self.thisptr.cloak()

    def decloak(self):
        return self.thisptr.decloak()

    def siege(self):
        return self.thisptr.siege()

    def unsiege(self):
        return self.thisptr.unsiege()

    def lift(self):
        return self.thisptr.lift()

    def land(self, TilePosition target):
        return self.thisptr.land(target.thisobj)

    def load(self, Unit target, bool queue = False):
        return self.thisptr.load(target.thisptr, queue)

    def unload(self, Unit target):
        return self.thisptr.unload(target.thisptr)

    def unloadAll(self, target = None, bool queue = False):
        if target is None:
            return self.thisptr.unloadAll(queue)
        elif isinstance(target,Position):
            return self.thisptr.unloadAll((<Position>target).thisobj, queue)
        else:
            raise TypeError

    def rightClick(self, PositionUnitConverter target, bool queue = False):
        return self.thisptr.rightClick(target.getPositionOrUnit(),queue)

    def haltConstruction(self):
        return self.thisptr.haltConstruction()

    def cancelConstruction(self):
        return self.thisptr.cancelConstruction()

    def cancelAddon(self):
        return self.thisptr.cancelAddon()

    def cancelTrain(self, int slot = -2):
        return self.thisptr.cancelTrain(slot)

    def cancelMorph(self):
        return self.thisptr.cancelMorph()

    def cancelResearch(self):
        return self.thisptr.cancelResearch()

    def cancelUpgrade(self):
        return self.thisptr.cancelUpgrade()

    def useTech(self, TechType tech, target = None):
        if target is None:
            return self.thisptr.useTech(tech.thisobj)
        elif isinstance(target,PositionUnitConverter):
            return self.thisptr.useTech(tech.thisobj,(<PositionUnitConverter>target).getPositionOrUnit())
        else:
            raise TypeError

    def placeCOP(self, TilePosition target):
        return self.thisptr.placeCOP(target.thisobj)

    def canIssueCommand(self, UnitCommand command, bool checkCanUseTechPositionOnPositions = True, bool checkCanUseTechUnitOnUnits = True, bool checkCanBuildUnitType = True, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        return self.thisptr.canIssueCommand(command.thisobj,checkCanUseTechPositionOnPositions, checkCanUseTechUnitOnUnits, checkCanBuildUnitType, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)

    def canIssueCommandGrouped(self, UnitCommand command, bool checkCanUseTechPositionOnPositions = True, bool checkCanUseTechUnitOnUnits = True, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibilityGrouped = True, bool checkCommandibility = True):
        return self.thisptr.canIssueCommandGrouped(command.thisobj,checkCanUseTechPositionOnPositions, checkCanUseTechUnitOnUnits, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibilityGrouped, checkCommandibility)

    def canCommand(self):
        return self.thisptr.canCommand()

    def canCommandGrouped(self, bool checkCommandibility = True):
        return self.thisptr.canCommandGrouped(checkCommandibility)

    def canIssueCommandType(self, UnitCommandType ct, bool checkCommandibility = True):
        return self.thisptr.canIssueCommandType(ct.thisobj, checkCommandibility)

    def canIssueCommandTypeGrouped(self, UnitCommandType ct, bool checkCommandibilityGrouped = True, bool checkCommandibility = True):
        return self.thisptr.canIssueCommandTypeGrouped(ct.thisobj,  checkCommandibilityGrouped, checkCommandibility)

    def canTargetUnit(self, Unit targetUnit, bool checkCommandibility = True):
        return self.thisptr.canTargetUnit(targetUnit.thisptr, checkCommandibility)

    def canAttack(self, target = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if target is None:
            return self.thisptr.canAttack(checkCommandibility)
        elif isinstance(target,PositionUnitConverter):
            return self.thisptr.canAttack((<PositionUnitConverter>target).getPositionOrUnit(), checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canAttackGrouped(self, target = None, bool checkCommandibilityGrouped = True, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if target is None:
            return self.thisptr.canAttackGrouped(checkCommandibilityGrouped, checkCommandibility)
        elif isinstance(target,PositionUnitConverter):
            return self.thisptr.canAttackGrouped((<PositionUnitConverter>target).getPositionOrUnit(), checkCommandibilityGrouped, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canAttackMove(self, bool checkCommandibility = True):
        return self.canAttackMove(checkCommandibility)

    def canAttackMoveGrouped(self, bool checkCommandibilityGrouped = True, bool checkCommandibility = True):
        return self.canAttackMoveGrouped(checkCommandibilityGrouped, checkCommandibility)

    def canAttackUnit(self, targetUnit = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.canAttackUnit(checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.canAttackUnit(targetUnit.thisptr, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canAttackUnitGrouped(self, targetUnit = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibilityGrouped = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.canAttackUnitGrouped(checkCommandibilityGrouped, checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.canAttackUnitGrouped(targetUnit.thisptr, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibilityGrouped, checkCommandibility)
        else:
            raise TypeError

    def canBuild(self, uType = None, tilePos = None, bool checkTargetUnitType = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if uType is None:
            return self.thisptr.canBuild(checkCommandibility)
        elif tilePos is None and isinstance(uType, UnitType):
            return self.thisptr.canBuild((<UnitType>uType).thisobj, checkCanIssueCommandType, checkCommandibility)
        elif isinstance(uType, UnitType) and isinstance(tilePos, TilePosition):
            return self.thisptr.canBuild((<UnitType>uType).thisobj, (<TilePosition>tilePos).thisobj, checkTargetUnitType, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canBuildAddon(self, uType = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if uType is None:
            return self.thisptr.canBuildAddon(checkCommandibility)
        elif isinstance(uType, UnitType):
            return self.thisptr.canBuildAddon((<UnitType>uType).thisobj,checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canTrain(self, uType = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if uType is None:
            return self.thisptr.canTrain(checkCommandibility)
        elif isinstance(uType, UnitType):
            return self.thisptr.canTrain((<UnitType>uType).thisobj,checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canMorph(self, uType = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if uType is None:
            return self.thisptr.canMorph(checkCommandibility)
        elif isinstance(uType, UnitType):
            return self.thisptr.canMorph((<UnitType>uType).thisobj,checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canResearch(self, tType = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if tType is None:
            return self.thisptr.canResearch(checkCommandibility)
        elif isinstance(tType, TechType):
            return self.thisptr.canResearch((<TechType>tType).thisobj,checkCanIssueCommandType)
        else:
            raise TypeError

    def canUpgrade(self, uType = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if uType is None:
            return self.thisptr.canUpgrade(checkCommandibility)
        elif isinstance(uType, UpgradeType):
            return self.thisptr.canUpgrade((<UpgradeType>uType).thisobj,checkCanIssueCommandType)
        else:
            raise TypeError

    def canSetRallyPoint(self, target = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if target is None:
            return self.thisptr.canSetRallyPoint(checkCommandibility)
        elif isinstance(target, PositionUnitConverter):
            return self.thisptr.canSetRallyPoint((<PositionUnitConverter>target).getPositionOrUnit(), checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canSetRallyPosition(self, bool checkCommandibility = True):
        return self.thisptr.canSetRallyPosition(checkCommandibility)

    def canSetRallyUnit(self, targetUnit = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.thisptr.canSetRallyUnit(checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.thisptr.canSetRallyUnit((<Unit>targetUnit).thisptr, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canMove(self, bool checkCommandibility = True):
        return self.thisptr.canMove(checkCommandibility)

    def canMoveGrouped(self, bool checkCommandibilityGrouped = True, bool checkCommandibility = True):
        return self.thisptr.canMoveGrouped(checkCommandibilityGrouped, checkCommandibility)

    def canPatrol(self, bool checkCommandibility = True):
        return self.thisptr.canPatrol(checkCommandibility)

    def canPatrolGrouped(self, bool checkCommandibilityGrouped = True, bool checkCommandibility = True):
        return self.thisptr.canPatrolGrouped(checkCommandibilityGrouped, checkCommandibility)

    def canPatrol(self, bool checkCommandibility = True):
        return self.thisptr.canPatrol(checkCommandibility)

    def canFollow(self, targetUnit = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.thisptr.canFollow(checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.thisptr.canFollow((<Unit>targetUnit).thisptr, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canGather(self, targetUnit = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.thisptr.canGather(checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.thisptr.canGather((<Unit>targetUnit).thisptr, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canReturnCargo(self, bool checkCommandibility = True):
        return self.thisptr.canReturnCargo(checkCommandibility)

    def canHoldPosition(self, bool checkCommandibility = True):
        return self.thisptr.canHoldPosition(checkCommandibility)

    def canStop(self, bool checkCommandibility = True):
        return self.thisptr.canStop(checkCommandibility)

    def canRepair(self, targetUnit = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.thisptr.canRepair(checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.thisptr.canRepair((<Unit>targetUnit).thisptr, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canBurrow(self, bool checkCommandibility = True):
        return self.thisptr.canBurrow(checkCommandibility)

    def canUnburrow(self, bool checkCommandibility = True):
        return self.thisptr.canUnburrow(checkCommandibility)

    def canCloak(self, bool checkCommandibility = True):
        return self.thisptr.canCloak(checkCommandibility)

    def canDecloak(self, bool checkCommandibility = True):
        return self.thisptr.canDecloak(checkCommandibility)

    def canSiege(self, bool checkCommandibility = True):
        return self.thisptr.canSiege(checkCommandibility)

    def canUnsiege(self, bool checkCommandibility = True):
        return self.thisptr.canUnsiege(checkCommandibility)

    def canLift(self, bool checkCommandibility = True):
        return self.thisptr.canLift(checkCommandibility)

    def canLand(self, target = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if target is None:
            return self.thisptr.canLand(checkCommandibility)
        elif isinstance(target, TilePosition):
            return self.thisptr.canLand((<TilePosition>target).thisobj,  checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canLoad(self, targetUnit = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.thisptr.canLoad(checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.thisptr.canLoad((<Unit>targetUnit).thisptr, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canUnloadWithOrWithoutTarget(self, bool checkCommandibility = True):
        return self.thisptr.canUnloadWithOrWithoutTarget(checkCommandibility)

    def canUnloadAtPosition(self, Position targDropPos, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        return self.thisptr.canUnloadAtPosition((<Position>targDropPos).thisobj,  checkCanIssueCommandType, checkCommandibility)

    def canUnload(self, targetUnit = None, bool checkCanTargetUnit = True, bool checkPosition = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.thisptr.canUnload(checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.thisptr.canUnload((<Unit>targetUnit).thisptr, checkCanTargetUnit, checkPosition, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canUnloadAll(self, bool checkCommandibility = True):
        return self.thisptr.canUnloadAll(checkCommandibility)

    def canUnloadAllPosition(self, targDropPos = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if targDropPos is None:
            return self.thisptr.canUnloadAllPosition(checkCommandibility)
        elif isinstance(targDropPos, Position):
            return self.thisptr.canUnloadAllPosition((<Position>targDropPos).thisobj, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canRightClick(self, target = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if target is None:
            return self.thisptr.canRightClick(checkCommandibility)
        elif isinstance(target,PositionUnitConverter):
            return self.thisptr.canRightClick((<PositionUnitConverter>target).getPositionOrUnit(), checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canRightClickGrouped(self, target = None, bool checkCanTargetUnit = True, bool checkCanIssueCommandType = True, bool checkCommandibilityGrouped = True,bool checkCommandibility = True):
        if target is None:
            return self.thisptr.canRightClickGrouped(checkCommandibilityGrouped, checkCommandibility)
        elif isinstance(target,PositionUnitConverter):
            return self.thisptr.canRightClickGrouped((<PositionUnitConverter>target).getPositionOrUnit(), checkCanTargetUnit, checkCanIssueCommandType, checkCommandibilityGrouped, checkCommandibility)
        else:
            raise TypeError

    def canRightClickPosition(self, bool checkCommandibility = True):
        return self.thisptr.canRightClickPosition(checkCommandibility)

    def canRightClickPositionGrouped(self, bool checkCommandibilityGrouped = True,bool checkCommandibility = True):
        return self.thisptr.canRightClickPositionGrouped(checkCommandibilityGrouped, checkCommandibility)

    def canRightClickUnit(self, targetUnit = None, bool checkCanTargetUnit = True,  bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.thisptr.canRightClickUnit(checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.thisptr.canRightClickUnit((<Unit>targetUnit).thisptr, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canRightClickUnitGrouped(self, targetUnit = None, bool checkCanTargetUnit = True,  bool checkCanIssueCommandType = True, bool checkCommandibilityGrouped = True, bool checkCommandibility = True):
        if targetUnit is None:
            return self.thisptr.canRightClickUnitGrouped(checkCommandibilityGrouped, checkCommandibility)
        elif isinstance(targetUnit, Unit):
            return self.thisptr.canRightClickUnitGrouped((<Unit>targetUnit).thisptr, checkCanTargetUnit, checkCanIssueCommandType, checkCommandibilityGrouped, checkCommandibility)
        else:
            raise TypeError

    def canHaltConstruction(self, bool checkCommandibility = True):
        return self.thisptr.canHaltConstruction(checkCommandibility)

    def canCancelConstruction(self, bool checkCommandibility = True):
        return self.thisptr.canCancelConstruction(checkCommandibility)

    def canCancelAddon(self, bool checkCommandibility = True):
        return self.thisptr.canCancelAddon(checkCommandibility)

    def canCancelTrain(self, bool checkCommandibility = True):
        return self.thisptr.canCancelTrain(checkCommandibility)

    def canCancelTrainSlot(self, slot = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if slot is None:
            return self.thisptr.canCancelTrainSlot(checkCommandibility)
        elif isinstance(slot, int):
            return self.thisptr.canCancelTrainSlot((<int>slot), checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canCancelMorph(self, bool checkCommandibility = True):
        return self.thisptr.canCancelMorph(checkCommandibility)

    def canCancelResearch(self, bool checkCommandibility = True):
        return self.thisptr.canCancelResearch(checkCommandibility)

    def canCancelUpgrade(self, bool checkCommandibility = True):
        return self.thisptr.canCancelUpgrade(checkCommandibility)

    def canUseTechWithOrWithoutTarget(self, tech = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if tech is None:
            return self.thisptr.canUseTechWithOrWithoutTarget(checkCommandibility)
        elif isinstance(tech, TechType):
            return self.thisptr.canUseTechWithOrWithoutTarget((<TechType>tech).thisobj, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canUseTech(self, TechType tech, target = None, bool checkCanTargetUnit = True, bool checkTargetsType = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if target is None:
            return self.thisptr.canUseTech(tech.thisobj)
        elif isinstance(target, PositionUnitConverter):
            return self.thisptr.canUseTech(tech.thisobj, (<PositionUnitConverter>target).getPositionOrUnit(), checkCanTargetUnit, checkTargetsType, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canUseTechWithOrWithoutTarget(self, TechType tech, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        return self.thisptr.canUseTechWithOrWithoutTarget((<TechType>tech).thisobj, checkCanIssueCommandType, checkCommandibility)

    def canUseTechUnit(self, TechType tech, Unit targetUnit, bool checkCanTargetUnit = True, bool checkTargetsUnits = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        return self.thisptr.canUseTechUnit((<TechType>tech).thisobj,  targetUnit.thisptr, checkCanTargetUnit, checkTargetsUnits, checkCanIssueCommandType, checkCommandibility)

    def canUseTechPosition(self, TechType tech, target = None, bool checkTargetsPositions = True, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if target is None:
            return self.thisptr.canUseTechPosition(tech.thisobj, checkCanIssueCommandType, checkCommandibility)
        elif isinstance(target, Position):
            return self.thisptr.canUseTechPosition(tech.thisobj, (<Position>target).thisobj, checkTargetsPositions, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

    def canPlaceCOP(self, target = None, bool checkCanIssueCommandType = True, bool checkCommandibility = True):
        if target is None:
            return self.thisptr.canPlaceCOP(checkCommandibility)
        elif isinstance(target, TilePosition):
            return self.thisptr.canPlaceCOP((<TilePosition>target).thisobj, checkCanIssueCommandType, checkCommandibility)
        else:
            raise TypeError

cdef UnitFactory(cUnit.Unit unit):
    """ __UnitFactory(unit)__ takes a C++ unit object and returns a Python Unit wrapper. """
    if not unit:
        return None
        #raise NullPointerException("Unit")
    newunit = Unit()
    newunit.thisptr = unit
    return newunit