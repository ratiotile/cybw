﻿cdef extern from "BWAPI/UnitSizeType.h" namespace "BWAPI::UnitSizeTypes::Enum":
    cdef enum Enum:
        Independent = 0,
        Small,
        Medium,
        Large,
        None,
        Unknown,
        MAX