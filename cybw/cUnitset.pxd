﻿from libcpp cimport bool
from cybw.cSetContainer cimport SetContainer
from cybw.cPosition cimport Position, TilePosition
from cybw.cPositionUnit cimport PositionOrUnit
#from cybw.cUnit cimport Unit
#include <BWAPI/Filters.h>


cdef extern from "BWAPI/Unitset.h" namespace "BWAPI":

    cdef cppclass UnitType:
        pass

    cdef cppclass Regionset:
        pass

    cdef cppclass UnitCommand:
        pass

    cdef cppclass TechType:
        pass

    cdef cppclass Unit:
        pass


    cdef cppclass Unitset(SetContainer[Unit]):
        # A blank Unitset containing no elements. This is typically used as a return value for BWAPI
        # interface functions that have encountered an error.
        const Unitset none

        # Calculates the average of all valid Unit positions in this set.
        Position getPosition() const

        # Creates a single set containing all units that are loaded into
        # units of this set.
        Unitset getLoadedUnits() const

        # Creates a single set containing all the @Interceptors of all
        # @Carriers in this set.
        Unitset getInterceptors() const

        # Creates a single set containing all the @Larvae of all
        # @Hatcheries, @Lairs, and @Hives in this set.
        Unitset getLarva() const

        # Sets the client info for every unit in this set.
        #
        # @param clientInfo [optional]
        #   A pointer to client information, managed by the AI module, or nullptr if client
        #   information is to be cleared.
        # @param index [optional]
        #   An key value for the client info mapping so that more than one piece of data can be
        #   mapped to the same unit.
        #
        # @see UnitInterface::setClientInfo
        #void setClientInfo(void *clientInfo = nullptr, int index = 0) const

        #void setClientInfo(int clientInfo = 0, int index = 0) const

        #Unitset getUnitsInRadius(int radius, const UnitFilter &pred = nullptr) const
        Unitset getUnitsInRadius(int radius) const
        #Unit getClosestUnit(const UnitFilter &pred = nullptr, int radius = 999999) const;
        #Unit getClosestUnit(const UnitFilter &pred = nullptr) const;
        Unit getClosestUnit(int radius) const
        Unit getClosestUnit() const

        bool issueCommand(UnitCommand command) const

        #bool attack(PositionOrUnit target, bool shiftQueueCommand = false) const

        bool build(UnitType type, TilePosition target) const
        bool build(UnitType type) const

        bool buildAddon(UnitType type) const

        bool train(UnitType type) const

        bool morph(UnitType type) const

        bool setRallyPoint(PositionOrUnit target) const

        #bool move(Position target, bool shiftQueueCommand = false) const

        #bool patrol(Position target, bool shiftQueueCommand = false) const

        #bool holdPosition(bool shiftQueueCommand = false) const

        #bool follow(Unit target, bool shiftQueueCommand = false) const

        #bool gather(Unit target, bool shiftQueueCommand = false) const

        #bool returnCargo(bool shiftQueueCommand = false) const

        #bool repair(Unit target, bool shiftQueueCommand = false) const

        bool burrow() const

        bool unburrow() const

        bool cloak() const

        bool decloak() const

        bool siege() const

        bool unsiege() const

        bool lift() const

        #bool load(Unit target, bool shiftQueueCommand = false) const

        #bool unloadAll(bool shiftQueueCommand = false) const

        #bool unloadAll(Position target, bool shiftQueueCommand = false) const

        #bool rightClick(PositionOrUnit target, bool shiftQueueCommand = false) const
        bool rightClick(PositionOrUnit target) const

        bool haltConstruction() const

        bool cancelConstruction() const

        bool cancelAddon() const

        bool cancelTrain(int slot) const
        bool cancelTrain() const

        bool cancelMorph() const

        bool cancelResearch() const

        bool cancelUpgrade() const

        #bool useTech(TechType tech, PositionOrUnit target = nullptr) const
        bool useTech(TechType tech) const

        #inherited
        #size_t size() const

        cppclass iterator:
            Unit operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)

        cppclass const_iterator:
            const Unit operator*()
            const_iterator operator++()
            const_iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)