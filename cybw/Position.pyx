﻿cimport cybw.cPosition as cPosition
cimport cybw.cPositionUnit as cPositionUnit
from collections import deque as py_deque
from cython.operator cimport dereference as deref, preincrement as inc
from cybw.libcpp.deque cimport deque


cdef class Position(PositionUnitConverter):
    def __init__(self, x=-1, y=-1):
        if isinstance(x, Position):
            w = <Position>x
            self.thisobj = w.thisobj
        elif isinstance(x, WalkPosition):
            self.thisobj = cPosition_fromWalk(x)
        elif isinstance(x, TilePosition):
            self.thisobj = cPosition_fromTile(x)
        else:
            self.thisobj = cPosition.Position(x,y)

    def __bool__(self):
        if(self.thisobj.isValid()): return True
        else: return False

    def __richcmp__(Position self, Position other, optype):
        if optype     == 0: # <
            return self.thisobj < other.thisobj
        elif optype == 2: # ==
            return self.thisobj == other.thisobj
        elif optype == 4: # >
            raise Exception("not implemented")
        elif optype == 1: # <=
            raise Exception("not implemented")
        elif optype == 3: # !=
            return self.thisobj != other.thisobj
        elif optype == 5: # >=
            pass
        raise Exception("not defined")

    def __add__(Position self, Position other):
        cdef cPosition.Position sum = self.thisobj + other.thisobj
        return PositionFactory(sum)

    def __sub__(Position self, Position other):
        cdef cPosition.Position difference = self.thisobj - other.thisobj
        return PositionFactory(difference)

    def __mul__(Position self, int other):
        cdef cPosition.Position product = self.thisobj * other
        return PositionFactory(product)

    def __and__(Position self, int other):
        return PositionFactory(self.thisobj & other)

    def __or__(Position self, int other):
        return PositionFactory(self.thisobj | other)

    def __xor__(Position self, int other):
        return PositionFactory(self.thisobj ^ other)

    def __truediv__(Position self, int other):
        # workaround for broken truediv
        #return PositionFactory(self.thisobj / other)
        r = Position(self.x // other, self.y // other)
        return r

    def __mod__(Position self, int other):
        return PositionFactory(self.thisobj % other)

    def __hash__(self):
        return self.x * 10000 + self.y

    cdef cPositionUnit.PositionOrUnit getPositionOrUnit(self):
        return cPositionUnit.PositionOrUnit(self.thisobj)

    def __str__(self):
        return "(" + str(self.thisobj.x) + "," + str(self.thisobj.y) + ")"

    def __repr__(self):
        return "Position" + str(self)

    def isValid(self):
        return self.thisobj.isValid()

    def makeValid(self):
        self.thisobj.makeValid()
        return self

    def getLength(self):
        return self.thisobj.getLength()

    def getDistance(self, Position position):
        return self.thisobj.getDistance(position.thisobj)

    def getApproxDistance(self, Position position):
        return self.thisobj.getApproxDistance(position.thisobj)

    #def setMax(self, Position maxPoint):
    #    self.thisobj = self.thisobj.setMax(maxPoint.thisobj)
    #    return self

    #def setMin(self, Position minPoint):
    #    self.thisobj = self.thisobj.setMin(minPoint.thisobj)
    #    return self

    def getXY(self):
        return (self.x, self.y)

    def getX(self):
        return self.thisobj.x

    def setX(self, int y):
        self.thisobj.x = y

    def getY(self):
        return self.thisobj.y

    def setY(self, int y):
        self.thisobj.y = y

    x = property(getX, setX)
    y = property(getY, setY)


cdef class WalkPosition:
    def __init__(self, x=-1, y=-1):
        if isinstance(x, WalkPosition):
            w = <WalkPosition>x
            self.thisobj = w.thisobj
        elif isinstance(x, Position):
            self.thisobj = cWalkPosition_fromPos(x)
        elif isinstance(x, TilePosition):
            self.thisobj = cWalkPosition_fromTile(x)
        else:
            self.thisobj = cPosition.WalkPosition(x,y)

    def __bool__(self):
        if(self.thisobj.isValid()): return True
        else: return False

    def __richcmp__(WalkPosition self, WalkPosition other, optype):
        if optype     == 0: # <
            return self.thisobj < other.thisobj
        elif optype == 2: # ==
            return self.thisobj == other.thisobj
        elif optype == 4: # >
            raise Exception("not implemented")
        elif optype == 1: # <=
            raise Exception("not implemented")
        elif optype == 3: # !=
            return self.thisobj != other.thisobj
        elif optype == 5: # >=
            pass
        raise Exception("not defined")

    def __add__(WalkPosition self, WalkPosition other):
        cdef cPosition.WalkPosition sum = self.thisobj + other.thisobj
        return WalkPositionFactory(sum)

    def __sub__(WalkPosition self, WalkPosition other):
        return WalkPositionFactory(self.thisobj - other.thisobj)

    def __mul__(WalkPosition self, int other):
        return WalkPositionFactory(self.thisobj * other)

    def __and__(WalkPosition self, int other):
        return WalkPositionFactory(self.thisobj & other)

    def __or__(WalkPosition self, int other):
        return WalkPositionFactory(self.thisobj | other)

    def __xor__(WalkPosition self, int other):
        return WalkPositionFactory(self.thisobj ^ other)

    def __truediv__(WalkPosition self, int other):
        # workaround since new cython version broke truediv
        # WalkPositionFactory(self.thisobj / other)
        r = WalkPosition(self.x // other, self.y // other)
        return r


    def __mod__(WalkPosition self, int other):
        return WalkPositionFactory(self.thisobj % other)

    def __hash__(WalkPosition self):
        return (1+self.x * 10000) + self.y

    def __str__(WalkPosition self):
        return "(" + str(self.thisobj.x) + "," + str(self.thisobj.y) + ")"

    def isValid(WalkPosition self):
        return self.thisobj.isValid()

    def makeValid(WalkPosition self):
        self.thisobj.makeValid()
        return self

    def getLength(WalkPosition self):
        return self.thisobj.getLength()

    def getDistance(WalkPosition self, WalkPosition position):
        return self.thisobj.getDistance(position.thisobj)

    def getApproxDistance(WalkPosition self, WalkPosition position):
        return self.thisobj.getApproxDistance(position.thisobj)

    #def setMax(self, WalkPosition maxPoint):
    #    self.thisobj = self.thisobj.setMax(maxPoint.thisobj)
    #    return self

    #def setMin(self, WalkPosition minPoint):
    #    self.thisobj = self.thisobj.setMin(minPoint.thisobj)
    #    return self

    def getXY(self):
        return (self.x, self.y)

    def getX(WalkPosition self):
        return self.thisobj.x

    def setX(WalkPosition self, int y):
        self.thisobj.x = y

    def getY(WalkPosition self):
        return self.thisobj.y

    def setY(WalkPosition self, int y):
        self.thisobj.y = y

    x = property(getX, setX)
    y = property(getY, setY)

cdef class TilePosition:
    def __init__(self, x = 4000, y = 4000):
        if isinstance(x, TilePosition):
            w = <TilePosition>x
            self.thisobj = w.thisobj
        elif isinstance(x, Position):
            self.thisobj = cTilePosition_fromPos(x)
        elif isinstance(x, WalkPosition):
            self.thisobj = cTilePosition_fromWalk(x)
        else:
            self.thisobj = cPosition.TilePosition(x,y)

    def __bool__(self):    # quick hack!!
        if(self.thisobj.isValid()): return True
        else: return False

    def __richcmp__(TilePosition self, TilePosition other, optype):
        if optype     == 0: # <
            return self.thisobj < other.thisobj
        elif optype == 2: # ==
            return self.thisobj == other.thisobj
        elif optype == 4: # >
            raise Exception("not implemented")
        elif optype == 1: # <=
            raise Exception("not implemented")
        elif optype == 3: # !=
            return self.thisobj != other.thisobj
        elif optype == 5: # >=
            pass
        raise Exception("not defined")

    def __add__(TilePosition self, TilePosition other):
        cdef cPosition.TilePosition sum = self.thisobj + other.thisobj
        return TilePositionFactory(sum)

    def __sub__(TilePosition self, TilePosition other):
        return TilePositionFactory(self.thisobj - other.thisobj)

    def __mul__(TilePosition self, int other):
        return TilePositionFactory(self.thisobj * other)

    def __and__(TilePosition self, int other):
        return TilePositionFactory(self.thisobj & other)

    def __or__(TilePosition self, int other):
        return TilePositionFactory(self.thisobj | other)

    def __xor__(TilePosition self, int other):
        return TilePositionFactory(self.thisobj ^ other)

    def __truediv__(TilePosition self, int other):
        r = TilePosition(self.x // other, self.y // other)
        return r

    def __mod__(TilePosition self, int other):
        return TilePositionFactory(self.thisobj % other)

    def __hash__(self):
        return self.x * 1000 + self.y

    def __str__(self):
        return "(" + str(self.thisobj.x) + "," + str(self.thisobj.y) + ")"

    def isValid(self):
        return self.thisobj.isValid()

    def makeValid(self):
        self.thisobj.makeValid()
        return self

    def getLength(self):
        return self.thisobj.getLength()

    def getDistance(self, TilePosition position):
        return self.thisobj.getDistance(position.thisobj)

    def getApproxDistance(self, TilePosition position):
        return self.thisobj.getApproxDistance(position.thisobj)

    #def setMax(self, TilePosition maxPoint):
    #    self.thisobj = self.thisobj.setMax(maxPoint.thisobj)
    #    return self

    #def setMin(self, TilePosition minPoint):
    #    self.thisobj = self.thisobj.setMin(minPoint.thisobj)
    #    return self

    def getXY(self):
        return (self.x, self.y)

    def getX(self):
        return self.thisobj.x

    def setX(self, int y):
        self.thisobj.x = y

    def getY(self):
        return self.thisobj.y

    def setY(self, int y):
        self.thisobj.y = y

    x = property(getX, setX)
    y = property(getY, setY)

cdef cPosition.WalkPosition cWalkPosition_fromTile(TilePosition t):
    return cPosition.WalkPosition(t.thisobj)

cdef cPosition.WalkPosition cWalkPosition_fromPos(Position t):
    return cPosition.WalkPosition(t.thisobj)

cdef cPosition.Position cPosition_fromTile(TilePosition t):
    return cPosition.Position(t.thisobj)

cdef cPosition.Position cPosition_fromWalk(WalkPosition t):
    return cPosition.Position(t.thisobj)

cdef cPosition.TilePosition cTilePosition_fromWalk(WalkPosition t):
    return cPosition.TilePosition(t.thisobj)

cdef cPosition.TilePosition cTilePosition_fromPos( Position t):
    return cPosition.TilePosition(t.thisobj)

cdef PositionFactory(cPosition.Position positionptr):
    position = Position()
    position.thisobj = positionptr
    return position

cdef WalkPositionFactory(cPosition.WalkPosition walkptr):
    wp = WalkPosition()
    wp.thisobj = walkptr
    return wp


cdef TilePositionFactory(cPosition.TilePosition tileptr):
    Tile = TilePosition()
    Tile.thisobj = tileptr
    return Tile

cdef ConstPositionListFactory(const cPosition.Position_list &c_set):
    ''' make a list of Position from referenced Position_list'''
    d = py_deque()
    cdef size_t length = c_set.size()
    for i in range(length):
        d.append(PositionFactory(c_set[i]))
    return d

cdef PositionListFactory(cPosition.Position_list &c_set):
    ''' make a list of Position from referenced Position_list'''
    d = py_deque()
    cdef deque[cPosition.Position].iterator it = c_set.begin()
    cdef cPosition.Position p
    while it != c_set.end():
        p = deref(it)
        d.append(PositionFactory(p))
        inc(it)
    return d

cdef TilePositionListFactory(cPosition.TilePosition_list &c_set):
    ''' make a list of TilePositions from referenced TilePosition_list'''
    d = py_deque()
    for p in c_set:
        d.append(TilePositionFactory(p))
    return d

class Positions:
    Invalid = PositionFactory(cPosition.Positions_Invalid)
    none = PositionFactory(cPosition.Positions_None)
    Unknown = PositionFactory(cPosition.Positions_Unknown)
    Origin = PositionFactory(cPosition.Positions_Origin)

class WalkPositions:
    Invalid = WalkPositionFactory(cPosition.WalkPositions_Invalid)
    none = WalkPositionFactory(cPosition.WalkPositions_None)
    Unknown = WalkPositionFactory(cPosition.WalkPositions_Unknown)
    Origin = WalkPositionFactory(cPosition.WalkPositions_Origin)

class TilePositions:
    Invalid = TilePositionFactory(cPosition.TilePositions_Invalid)
    none = TilePositionFactory(cPosition.TilePositions_None)
    Unknown = TilePositionFactory(cPosition.TilePositions_Unknown)
    Origin = TilePositionFactory(cPosition.TilePositions_Origin)