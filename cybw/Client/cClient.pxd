﻿from libcpp cimport bool

cdef extern from "BWAPI/Client/Client.h" namespace "BWAPI":
    cdef cppclass Client:
        Client() nogil

        bool isConnected() nogil
        bool connect() nogil
        void disconnect() nogil
        void update() nogil

        #GameData *data
    Client BWAPIClient