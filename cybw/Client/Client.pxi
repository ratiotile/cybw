﻿cimport cybw.Client.cClient as cClient

cdef class Client:
    cdef cClient.Client* thisptr

    def __cinit__(self):
        self.thisptr = new cClient.Client()

    def isConnected(self):
        return self.thisptr.isConnected()

    def connect(self):
        return self.thisptr.connect()

    def disconnect(self):
        self.thisptr.disconnect()

    def update(self):
        with nogil:
            self.thisptr.update()

    cdef setPtr(self, cClient.Client *ptr):
        self.thisptr = ptr

# create the global client wrapper and pass it the reference
cdef class GlobalClient(Client):
    def __cinit__(self):
        self.thisptr = &cClient.BWAPIClient

BWAPIClient = GlobalClient()