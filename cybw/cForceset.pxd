﻿from cybw.cSetContainer cimport SetContainer

cdef extern from "BWAPI/Forceset.h" namespace "BWAPI":
    cdef cppclass ForceInterface:
        pass

    ctypedef ForceInterface *Force

    cdef cppclass Playerset:
        pass

    cdef cppclass Forceset(SetContainer[Force]):
        Playerset getPlayers() const

        cppclass const_iterator:
            const Force& operator*()
            const_iterator operator++()
            const_iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)