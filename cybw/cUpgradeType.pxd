﻿from .cUnitType cimport UnitType
from .cType cimport Type
from .cSetContainer cimport SetContainer

cdef extern from "BWAPI/UpgradeType.h" namespace "BWAPI":
    cdef cppclass Race:
        pass

    ctypedef int enum_unknown "UpgradeTypes::Enum::Unknown"
    cdef cppclass UpgradeType(Type[UpgradeType, enum_unknown]):
        UpgradeType(int id)
        UpgradeType()

        Race getRace() const

        int mineralPrice(int level) const
        int mineralPrice() const

        int mineralPriceFactor() const

        int gasPrice(int level) const
        int gasPrice() const

        int gasPriceFactor() const

        int upgradeTime(int level) const
        int upgradeTime() const

        int upgradeTimeFactor() const

        int maxRepeats() const

        UnitType whatUpgrades() const

        UnitType whatsRequired(int level) const
        UnitType whatsRequired() const

        #const UnitType::const_set& whatUses() const

"""
    cdef cppclass set:
        cppclass iterator:
            UpgradeType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)
        iterator begin()
        iterator end()

        cppclass const_iterator:
            UpgradeType& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)
        const_iterator cbegin()
        const_iterator cend()
"""
cdef extern from "BWAPI/UpgradeType.h" namespace "BWAPI::UpgradeType":
    ctypedef SetContainer[UpgradeType] set


cdef extern from "BWAPI/UpgradeType.h" namespace "BWAPI::UpgradeTypes":
    const set& allUpgradeTypes()

    const UpgradeType Terran_Infantry_Armor
    const UpgradeType Terran_Vehicle_Plating
    const UpgradeType Terran_Ship_Plating
    const UpgradeType Zerg_Carapace
    const UpgradeType Zerg_Flyer_Carapace
    const UpgradeType Protoss_Ground_Armor
    const UpgradeType Protoss_Air_Armor
    const UpgradeType Terran_Infantry_Weapons
    const UpgradeType Terran_Vehicle_Weapons
    const UpgradeType Terran_Ship_Weapons
    const UpgradeType Zerg_Melee_Attacks
    const UpgradeType Zerg_Missile_Attacks
    const UpgradeType Zerg_Flyer_Attacks
    const UpgradeType Protoss_Ground_Weapons
    const UpgradeType Protoss_Air_Weapons
    const UpgradeType Protoss_Plasma_Shields
    const UpgradeType U_238_Shells
    const UpgradeType Ion_Thrusters
    const UpgradeType Titan_Reactor
    const UpgradeType Ocular_Implants
    const UpgradeType Moebius_Reactor
    const UpgradeType Apollo_Reactor
    const UpgradeType Colossus_Reactor
    const UpgradeType Ventral_Sacs
    const UpgradeType Antennae
    const UpgradeType Pneumatized_Carapace
    const UpgradeType Metabolic_Boost
    const UpgradeType Adrenal_Glands
    const UpgradeType Muscular_Augments
    const UpgradeType Grooved_Spines
    const UpgradeType Gamete_Meiosis
    const UpgradeType Metasynaptic_Node
    const UpgradeType Singularity_Charge
    const UpgradeType Leg_Enhancements
    const UpgradeType Scarab_Damage
    const UpgradeType Reaver_Capacity
    const UpgradeType Gravitic_Drive
    const UpgradeType Sensor_Array
    const UpgradeType Gravitic_Boosters
    const UpgradeType Khaydarin_Amulet
    const UpgradeType Apial_Sensors
    const UpgradeType Gravitic_Thrusters
    const UpgradeType Carrier_Capacity
    const UpgradeType Khaydarin_Core
    const UpgradeType Argus_Jewel
    const UpgradeType Argus_Talisman
    const UpgradeType Caduceus_Reactor
    const UpgradeType Chitinous_Plating
    const UpgradeType Anabolic_Synthesis
    const UpgradeType Charon_Boosters
    const UpgradeType None
    const UpgradeType Unknown