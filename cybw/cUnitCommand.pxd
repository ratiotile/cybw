﻿from libcpp cimport bool
#from cybw.cPosition cimport Position, TilePosition
#from cybw.cUnitCommandType cimport UnitCommandType

cdef extern from "BWAPI/UnitCommand.h" namespace "BWAPI":

    cdef cppclass UnitType:
        pass

    cdef cppclass TechType:
        pass

    cdef cppclass UpgradeType:
        pass

    cdef cppclass PositionOrUnit:
        pass

    cdef cppclass UnitCommandType:
        pass
    cdef cppclass Position:
        pass
    cdef cppclass TilePosition:
        pass
    cdef cppclass UnitInterface:
        pass

    ctypedef UnitInterface *Unit

    cdef cppclass UnitCommand:
        UnitCommand()
        UnitCommand(Unit _unit, UnitCommandType _type, Unit _target, int _x, int _y, int _extra)

        UnitCommandType getType() const
        Unit            getUnit() const
        Unit            getTarget() const
        Position        getTargetPosition() const
        TilePosition    getTargetTilePosition() const
        UnitType        getUnitType() const
        TechType        getTechType() const
        UpgradeType     getUpgradeType() const
        int             getSlot() const
        bool            isQueued() const

        bool operator==(const UnitCommand& other) const
        bool operator!=(const UnitCommand& other) const

# statics
cdef extern from "BWAPI/UnitCommand.h" namespace "BWAPI::UnitCommand":
    UnitCommand attack(Unit unit, PositionOrUnit target, bool shiftQueueCommand)
    UnitCommand attack(Unit unit, PositionOrUnit target)

    UnitCommand build(Unit unit, TilePosition target, UnitType type)
    UnitCommand buildAddon(Unit unit, UnitType type)
    UnitCommand train(Unit unit, UnitType type)
    UnitCommand morph(Unit unit, UnitType type)
    UnitCommand research(Unit unit, TechType tech)
    UnitCommand upgrade(Unit unit, UpgradeType upgrade)
    UnitCommand setRallyPoint(Unit unit, PositionOrUnit target)
    UnitCommand move(Unit unit, Position target, bool shiftQueueCommand)
    UnitCommand move(Unit unit, Position target)

    UnitCommand patrol(Unit unit, Position target, bool shiftQueueCommand)
    UnitCommand patrol(Unit unit, Position target)

    UnitCommand holdPosition(Unit unit, bool shiftQueueCommand)
    UnitCommand holdPosition(Unit unit)

    UnitCommand stop(Unit unit, bool shiftQueueCommand)
    UnitCommand stop(Unit unit)

    UnitCommand follow(Unit unit, Unit target, bool shiftQueueCommand)
    UnitCommand follow(Unit unit, Unit target)

    UnitCommand gather(Unit unit, Unit target, bool shiftQueueCommand)
    UnitCommand gather(Unit unit, Unit target)

    UnitCommand returnCargo(Unit unit, bool shiftQueueCommand)
    UnitCommand returnCargo(Unit unit)

    UnitCommand repair(Unit unit, Unit target, bool shiftQueueCommand)
    UnitCommand repair(Unit unit, Unit target)

    UnitCommand burrow(Unit unit)
    UnitCommand unburrow(Unit unit)
    UnitCommand cloak(Unit unit)
    UnitCommand decloak(Unit unit)
    UnitCommand siege(Unit unit)
    UnitCommand unsiege(Unit unit)
    UnitCommand lift(Unit unit)
    UnitCommand land(Unit unit, TilePosition target)
    UnitCommand load(Unit unit, Unit target, bool shiftQueueCommand)
    UnitCommand load(Unit unit, Unit target)

    UnitCommand unload(Unit unit, Unit target)
    UnitCommand unloadAll(Unit unit, bool shiftQueueCommand)
    UnitCommand unloadAll(Unit unit)
    UnitCommand unloadAll(Unit unit, Position target, bool shiftQueueCommand)
    UnitCommand unloadAll(Unit unit, Position target)

    UnitCommand rightClick(Unit unit, PositionOrUnit target, bool shiftQueueCommand)
    UnitCommand rightClick(Unit unit, PositionOrUnit target)

    UnitCommand haltConstruction(Unit unit)
    UnitCommand cancelConstruction(Unit unit)
    UnitCommand cancelAddon(Unit unit)
    UnitCommand cancelTrain(Unit unit, int slot)
    UnitCommand cancelTrain(Unit unit)

    UnitCommand cancelMorph(Unit unit)
    UnitCommand cancelResearch(Unit unit)
    UnitCommand cancelUpgrade(Unit unit)
    UnitCommand useTech(Unit unit,TechType tech)
    UnitCommand useTech(Unit unit,TechType tech, PositionOrUnit target)
    UnitCommand placeCOP(Unit unit, TilePosition target)