﻿from cybw.Force cimport ForceFactory
from cybw.Race cimport RaceFactory, Race
from cybw.Unitset cimport ConstUnitsetFactory
from cybw.Color cimport ColorFactory
from cybw cimport cColor

cdef class Player:
    def __init__(self):
        self.thisptr = NULL

    def __str__(self):
        return self.getName()

    def __hash__(self):
        return self.getID()

    def __richcmp__(Player self, Player other, op):
        if op == 2:
            return self.thisptr == other.thisptr
        else:
            raise NotImplementedError

    def getID(self):
        if self.thisptr == NULL:
            print("get id: Player is null")
            return -1
        return self.thisptr.getID()

    def getName(self):
        if self.thisptr == NULL:
            print("get name: Player is null")
            return ""
        return self.thisptr.getName().decode()

    def getUnits(self):
        return ConstUnitsetFactory(self.thisptr.getUnits())

    def getRace(self):
        if self.thisptr == NULL:
            print("get race: Player is null")
            return "None"
        race = RaceFactory(self.thisptr.getRace())
        return race

    def getType(self):
        raise NotImplemented("TODO")

    def getForce(self):
        return ForceFactory(self.thisptr.getForce())

    def isAlly(self, Player player):
        return self.thisptr.isAlly(player.thisptr)

    def isEnemy(self, Player player):
        return self.thisptr.isEnemy(player.thisptr)

    def isNeutral(self, Player player):
        return self.thisptr.isNeutral()

    def getStartLocation(self):
        return TilePositionFactory(self.thisptr.getStartLocation())

    def isVictorious(self):
        return self.thisptr.isVictorious()

    def isDefeated(self):
        return self.thisptr.isDefeated()

    def leftGame(self):
        return self.thisptr.leftGame()

    def minerals(self):
        return self.thisptr.minerals()

    def gas(self):
        return self.thisptr.gas()

    def gatheredGas(self):
        return self.thisptr.gatheredGas()

    def gatheredMinerals(self):
        return self.thisptr.gatheredMinerals()

    def repairedMinerals(self):
        return self.thisptr.repairedMinerals()

    def repairedGas(self):
        return self.thisptr.repairedGas()

    def spentMinerals(self):
        return self.thisptr.spentMinerals()

    def spentGas(self):
        return self.thisptr.spentGas()

    def supplyTotal(self, race=None):
        from cybw.Race import Races
        if race == None:
            return self.thisptr.supplyTotal()
        else:
            if isinstance(race, Race):
                return self.thisptr.supplyTotal((<Race>race).thisobj)
            raise TypeError("race must be an instance of Race")

    def supplyUsed(self, race=None):
        from cybw.Race import Races  # fuck you Python, where is my forward declare?
        if race == None:
            return self.thisptr.supplyUsed()
        else:
            if isinstance(race, Race):
                return self.thisptr.supplyUsed((<Race>race).thisobj)
            raise TypeError("race must be an instance of Race")

    def allUnitCount(self, UnitType unit=UnitTypes.none):  #  = UnitTypes.AllUnits
        if unit == UnitTypes.none:
            return self.thisptr.allUnitCount()
        return self.thisptr.allUnitCount(unit.thisobj)

    def visibleUnitCount(self, UnitType unit=UnitTypes.none):  #  = UnitTypes.AllUnits
        if unit == UnitTypes.none:
            return self.thisptr.visibleUnitCount()
        return self.thisptr.visibleUnitCount(unit.thisobj)

    def completedUnitCount(self, UnitType unit=UnitTypes.none):  #  = UnitTypes.AllUnits
        if unit == UnitTypes.none:
            return self.thisptr.completedUnitCount()
        return self.thisptr.completedUnitCount(unit.thisobj)

    def incompleteUnitCount(self, UnitType unit=UnitTypes.none):  #  = UnitTypes.AllUnits
        if unit == UnitTypes.none:
            return self.thisptr.incompleteUnitCount()
        return self.thisptr.incompleteUnitCount(unit.thisobj)

    def deadUnitCount(self, UnitType unit=UnitTypes.none):  #  = UnitTypes.AllUnits
        if unit == UnitTypes.none:
            return self.thisptr.deadUnitCount()
        return self.thisptr.deadUnitCount(unit.thisobj)

    def killedUnitCount(self, UnitType unit=UnitTypes.none):  #  = UnitTypes.AllUnits
        if unit == UnitTypes.none:
            return self.thisptr.killedUnitCount()
        return self.thisptr.killedUnitCount(unit.thisobj)

    def getUpgradeLevel(self, UpgradeType upgrade):
        return self.thisptr.getUpgradeLevel(upgrade.thisobj)

    def hasResearched(self, TechType tech):
        return self.thisptr.hasResearched(tech.thisobj)

    def isResearching(self, TechType tech):
        return self.thisptr.isResearching(tech.thisobj)

    def isUpgrading(self, UpgradeType upgrade):
        return self.thisptr.isUpgrading(upgrade.thisobj)

    def getColor(Player self):
        cdef cColor.Color c = self.thisptr.getColor()
        return ColorFactory(&c)

    def getTextColor(self):
        return self.thisptr.getTextColor()

    def maxEnergy(Player self, UnitType unit):
        return self.thisptr.maxEnergy(unit.thisobj)

    def topSpeed(self, UnitType unit):
        return self.thisptr.topSpeed(unit.thisobj)

    def weaponMaxRange(self, WeaponType weapon):
        return self.thisptr.weaponMaxRange(weapon.thisobj)

    def sightRange(self, UnitType unit):
        return self.thisptr.sightRange(unit.thisobj)

    def weaponDamageCooldown(self, UnitType unit):
        return self.thisptr.weaponDamageCooldown(unit.thisobj)

    def armor(self, UnitType unit):
        return self.thisptr.armor(unit.thisobj)

    def damage(self, WeaponType wpn):
        return self.thisptr.damage(wpn.thisobj)

    def getUnitScore(self):
        return self.thisptr.getUnitScore()

    def getKillScore(self):
        return self.thisptr.getKillScore()

    def getBuildingScore(self):
        return self.thisptr.getBuildingScore()

    def getRazingScore(self):
        return self.thisptr.getRazingScore()

    def getCustomScore(self):
        return self.thisptr.getCustomScore()

    def isObserver(self):
        return self.thisptr.isObserver()

    def getMaxUpgradeLevel(self, UpgradeType upgrade):
        return self.thisptr.getMaxUpgradeLevel(upgrade.thisobj)

    def isResearchAvailable(self, TechType tech):
        return self.thisptr.isResearchAvailable(tech.thisobj)

    def isUnitAvailable(self, UnitType unit):
        return self.thisptr.isUnitAvailable(unit.thisobj)

    # crutch used when RaceFactory didn't work
    def getRaceName(self):
        race = self.getRace()
        racename = race.getName()
        return racename


cdef PlayerFactory(cPlayer.Player pointer):
    if not pointer:
        raise NullPointerException("Player")
    p = Player()
    p.thisptr = pointer
    #print("PF Created Player "+p.getName())
    return p

cdef const_PlayerFactory(const cPlayer.Player pointer):
    if not pointer:
        raise NullPointerException("const Player")
    p = Player()
    p.thisptr = pointer
    return p
