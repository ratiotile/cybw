﻿cimport cybw.cForce as cForce
from cybw.GameClient cimport PlayerSetFactory

cdef class Force:
    def __init__(self):
        pass

    def getName(self):
        return self.thisobj.getName().decode()

    def getPlayers(self):
        return PlayerSetFactory(self.thisobj.getPlayers())

cdef ForceFactory(cForce.Force cforce):
    force = Force()
    force.thisobj = cforce
    return force

cdef const_ForceFactory(const cForce.Force &cforce):
    force = Force()
    force.thisobj = cforce
    return force
