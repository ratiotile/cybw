﻿cimport cybw.cUnitSizeType as cUnitSizeType

cdef class UnitSizeType:
    def __init__(self):
        pass

    def __str__(self):
       return self.getName()

    #def getName(self):
    #    name = self.thisobj.getName()
    #    pystr = name.decode()
    #    return pystr
    def getName(self):
        name = self.thisobj.getName()
        pystr = name.decode()
        return pystr

    def getID(self):
        return self.thisobj.getID()

cdef class UnitSizeTypeSet(set):
    def __init__(self):
        super().__init__()

cdef UnitSizeTypeFactory(cUnitSizeType.UnitSizeType ct):
    sizetype = UnitSizeType()
    sizetype.thisobj = ct
    return sizetype

cdef ConstUnitSizeTypeSetFactory(const cUnitSizeType.set &cset):
    ust_set = UnitSizeTypeSet()
    ust_set.thisobj = cset
    for i in cset:
        ust_set.add(UnitSizeTypeFactory(i))
    return ust_set

cdef class UnitSizeTypes:
    @staticmethod
    def allUnitSizeTypes():
        return ConstUnitSizeTypeSetFactory(cUnitSizeType.allUnitSizeTypes())

    Independent = UnitSizeTypeFactory(cUnitSizeType.Independent)
    Small = UnitSizeTypeFactory(cUnitSizeType.Small)
    Medium = UnitSizeTypeFactory(cUnitSizeType.Medium)
    Large = UnitSizeTypeFactory(cUnitSizeType.Large)
    none = UnitSizeTypeFactory(cUnitSizeType.None)
    Unknown = UnitSizeTypeFactory(cUnitSizeType.Unknown)