""" Used to wrap convenience functions for BWAPI """
#from libcpp cimport bool

cdef extern from "BWAPI/SetContainer.h" namespace "BWAPI":
    ctypedef initializer_list "std::initializer_list"

    cdef cppclass SetContainer[T]:
        SetContainer()
        #SetContainer(SetContainer &other)
        #SetContainer(SetContainer &&other)

        #SetContainer(initializer_list[T] ilist)

        IterT SetContainer[IterT](IterT _begin, IterT _end)

        # Iterates the set and erases each element x where pred(x) returns true.
        #
        # @param pred
        #     Predicate for removing elements.

        void erase_if[Pred](const Pred& pred)

        # Checks if this set contains a specific value.
        #
        # @param value
        #     Value to search for.
        bint contains(T &value) const

        cppclass iterator:
            T& operator*()
            iterator operator++()
            iterator operator--()
            bint operator==(iterator)
            bint operator!=(iterator)

        cppclass const_iterator:
            const T& operator*()
            const_iterator operator++()
            const_iterator operator--()
            bint operator==(const_iterator)
            bint operator!=(const_iterator)

        iterator begin()
        iterator end()
        const_iterator cbegin()
        const_iterator cend()
