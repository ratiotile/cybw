﻿def checkThisPtr(func):
    def _checkThisPtr(self,*args,**kwargs):
        if self.isNull():
            print("thisptr is NULL!")
            return None
        else:
            return func(self,*args,**kwargs)
    return _checkThisPtr