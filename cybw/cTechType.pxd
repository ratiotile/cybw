﻿from cybw.cUnitType cimport UnitType
from cybw.cType cimport Type
from .cSetContainer cimport SetContainer

cdef extern from "BWAPI/TechType.h" namespace "BWAPI":
    cdef cppclass WeaponType:
        pass

    cdef cppclass Order:
        pass

    cdef cppclass Race:
        pass

    ctypedef int enum_unknown "TechTypes::Enum::Unknown"
    cdef cppclass TechType(Type[TechType, enum_unknown]):
        TechType(int id)
        TechType()

        Race getRace() const

        int mineralPrice() const
        int gasPrice() const
        int researchTime() const
        int energyCost() const

        UnitType whatResearches() const
        WeaponType getWeapon() const

        bint targetsUnit() const
        bint targetsPosition() const
        #const UnitType::const_set& whatUses() const
        Order getOrder() const

cdef extern from "BWAPI/TechType.h" namespace "BWAPI::TechType":
    ctypedef SetContainer[TechType] set

cdef extern from "BWAPI/TechType.h" namespace "BWAPI::TechTypes":
    const set& allTechTypes()

    const TechType Stim_Packs
    const TechType Lockdown
    const TechType EMP_Shockwave
    const TechType Spider_Mines
    const TechType Scanner_Sweep
    const TechType Tank_Siege_Mode
    const TechType Defensive_Matrix
    const TechType Irradiate
    const TechType Yamato_Gun
    const TechType Cloaking_Field
    const TechType Personnel_Cloaking
    const TechType Burrowing
    const TechType Infestation
    const TechType Spawn_Broodlings
    const TechType Dark_Swarm
    const TechType Plague
    const TechType Consume
    const TechType Ensnare
    const TechType Parasite
    const TechType Psionic_Storm
    const TechType Hallucination
    const TechType Recall
    const TechType Stasis_Field
    const TechType Archon_Warp
    const TechType Restoration
    const TechType Disruption_Web
    const TechType Mind_Control
    const TechType Dark_Archon_Meld
    const TechType Feedback
    const TechType Optical_Flare
    const TechType Maelstrom
    const TechType Lurker_Aspect
    const TechType Healing
    const TechType None
    const TechType Nuclear_Strike
    const TechType Unknown

