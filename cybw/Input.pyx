﻿cimport cybw.cInput as cInput

cdef class MouseButton:
    def __init__(self, cInput.MouseButton button):
        self.thisenum = button


cdef class Key:
    def __init__(self, cInput.Key key):
        self.thisenum = key