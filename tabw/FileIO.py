import pickle
import gzip

class MapFile:

    def __init__(self, filename):
        self.filename = filename

    def save(self, data):
        output = gzip.GzipFile(self.filename, 'wb')
        pickle.dump(data, output, -1)
        output.close()

    def load(self):
        input = gzip.GzipFile(self.filename, 'rb')
        data = pickle.load(input)
        input.close()
        return data
