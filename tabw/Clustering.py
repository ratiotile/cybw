from collections import defaultdict
from copy import copy
import ipdb

class ScanInfo:

    def __init__(self):
        self.visited = False
        self.cluster_id = 0
        self.new_group = 0

class Cluster:

    def __init__(self, id):
        self.id = id
        self.units = set()

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return self.id

class ClusterManager:

    def __init__(self, radius, threshold, broodwarptr):
        self.current_id = 0
        self.to_visit = []
        self.ui_scan = {}  # unit to ScanInfo
        self.radius = radius
        self.threshold = threshold
        self.used_ids = set()
        self.bw = broodwarptr
        self.clusters = {}
        self.next_id = 0

    def createClusters(self, population):
        self.ui_scan = {}
        for unit in population:
            self.ui_scan[unit] = ScanInfo()

        for unit in self.ui_scan:
            info = self.ui_scan[unit]  # avoid cache
            if self.ui_scan[unit].visited is False:
                info.visited = True
                self.current_id = 0
                self.to_visit = []
                self.visit(population, unit)

                # visit all in to_visit
                # print("to_visit has {}".format(len(self.to_visit)))
                for visiting in self.to_visit:
                    if self.ui_scan[visiting].visited:
                        continue
                    else:
                        # appends to self.to_visit
                        # print("visiting {}".format(visiting.getID()))
                        self.ui_scan[visiting].visited = True
                        self.visit(population, visiting)
                # print("outside loop")
                # self.groupSize(self.current_id)
                # ipdb.set_trace()
        # self.applyCids()
        self.processClusters()
        self.printStats()
        return self.clusters, self.ui_scan

    def visit(self, population, vunit):
        curr_scan = self.ui_scan[vunit]
        curr_scan.visited = True
        neighbors = self.getUnitsInRadius(population, vunit, self.radius)
        if len(neighbors) > self.threshold:
            if self.current_id == 0:  # try to start new group
                c_id = copy(self.ui_scan[vunit].cluster_id)
                if c_id == 0 or c_id in self.used_ids:
                    self.current_id = copy(self.getNextId())
                else:
                    self.current_id = copy(c_id)

            self.used_ids.add(copy(self.current_id))
            self.ui_scan[vunit].cluster_id = copy(self.current_id)
            # print("set vunit {} cluster to {}".format(vunit.getID(), self.current_id))
            count = 0
            for neighbor in neighbors:
                if self.ui_scan[neighbor].visited is False:
                    count += 1
                    self.to_visit.append(neighbor)
            # print("appended {} to to_visit".format(count))
            # return neighbors

        #self.groupSize(self.current_id)

    def getNextId(self):
        self.next_id += 1
        return self.next_id

    def applyCids(self):
        for scan_info in self.ui_scan.values():
            old_cid = scan_info.cluster_id
            new_cid = scan_info.new_group
            if old_cid != new_cid:
                # was used to emit signal
                scan_info.cluster_id = new_cid

    def getUnitsInRadius(self, population, targetunit, radius):
        neighbors = []
        for unit in population:
            if targetunit == unit:
                continue
            pos = unit.getInitialPosition()
            targpos = targetunit.getInitialPosition()
            dist = pos.getApproxDistance(targpos)
            # print("dist {}, radius {}".format(dist, radius))
            if dist <= radius:
                neighbors.append(unit)
        # print("found {} neighbors".format(len(neighbors)))
        return neighbors

    def processClusters(self):
        self.clusters = defaultdict(set)
        for unit, info in self.ui_scan.items():
            self.clusters[info.cluster_id].add(unit)

    def printStats(self):
        for cid, cluster in self.clusters.items():
            print("cluster {}, size {}".format(cid, len(cluster)))

        print("{} clusters in total.".format(len(self.clusters)))

    def groupSize(self, cid):
        count = 0
        for unit, info in self.ui_scan.items():
            if info.cluster_id == cid:
                count += 1
        print("group {} is size {}".format(cid, count))
