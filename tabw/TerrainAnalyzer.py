from .Draw import MapImage
import heapq
from collections import defaultdict
from cybw import (Position, TilePosition, WalkPosition, WalkPositions,
    UnitTypes)
from cybw import (getTileBounds, getBoundingBox, BtoP, PtoB, WtoP,
    cropToMap, floorTile)
from math import floor
import math
import sys
import ipdb
from .Clustering import ClusterManager
import logging
from .FileIO import MapFile
import itertools
import time

# logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
###_these log lines make asyncio hide exceptions
#logging.basicConfig(filename='tabw.log', filemode='w', level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class open_tile:

    def __init__(self, xy, v):
        try:
            self.x, self.y = xy
        except TypeError:
            self.x = xy.x
            self.y = xy.y
        self.v = v

    def __lt__(self, other):
        try:
            return self.v < other.v
        except AttributeError:
            ipdb.set_trace()

    def __eq__(self, other):
        try:
            return self.x == other.x and self.y == other.y
        except AttributeError:
            ipdb.set_trace()

    def unpack(self):
        return (self.x, self.y, self.v)

class TileHeap:

    """ Max Heap for open_tile objects """

    def __init__(self, is_min_heap=False):
        self.heap = []
        self.is_min_heap = is_min_heap

    def __len__(self):
        return len(self.heap)

    def push(self, tile):
        sign = 1 if self.is_min_heap else -1
        tile.v = sign * tile.v
        heapq.heappush(self.heap, tile)

    def pop(self):
        sign = 1 if self.is_min_heap else -1
        tile = heapq.heappop(self.heap)
        tile.v = sign * tile.v
        return tile

    def remove(self, to_erase):
        try:
            self.heap.remove(to_erase)
            heapq._siftup(self.heap, 0)
        except (ValueError, IndexError):
            pass


class HeapItem:

    def __init__(self, obj, priority):
        self.obj = obj
        self.priority = priority

    def __lt__(self, other):
        return self.priority < other.priority

    def __eq__(self, other):
        return self.obj == other.obj

    def unpack(self, obj, priority):
        return (self.obj, self.priority)


class Heap:

    """ Heap for generic objects """

    def __init__(self, is_min_heap=False):
        self.heap = []
        self.is_min_heap = is_min_heap

    def __len__(self):
        return len(self.heap)

    def push(self, obj, priority):
        sign = 1 if self.is_min_heap else -1
        priority = sign * priority
        heapq.heappush(self.heap, HeapItem(obj, priority))

    def pop(self):
        sign = 1 if self.is_min_heap else -1
        item = heapq.heappop(self.heap)
        item.priority = sign * item.priority
        return (item.obj, item.priority)

    def remove(self, to_erase):
        try:
            item = HeapItem(to_erase, 0)
            self.heap.remove(item)
            if len(self.heap) > 0:
                heapq._siftup(self.heap, 0)
        except (ValueError, IndexError):
            pass

class Vector:

    def __init__(self, *args):
        self.list = []
        for x in args:
            self.list.append(x)

    def __len__(self):
        return len(self.list)

    def magnitude(self):
        v = self.list
        return math.sqrt(sum(v[i]*v[i] for i in range(len(v))))

    def normalize(self):
        mag = self.magnitude()
        if mag > sys.float_info.epsilon:
            v = self.list
            self.list = [v[i]/mag for i in range(len(v))]

    def dot(self, u):
        v = self.list
        return sum(v[i]*u[i] for i in range(len(u)))

    def __getitem__(self, i):
        return self.list[i]

class Border:

    """ A strip 2 tiles wide, between two adjacent regions. Contains 1 tile from
    each region, takes up no space on the map. Defines a line between two
    regions. """

    def __init__(self):
        # neighboring regions
        self.region1 = None
        self.region2 = None
        self.walktiles = []


class Choke(Border):

    """ A choke is a border which is narrow enough to give the defender a
    significant advantage. Unlike Borders, Chokes have area - it extends until the width gets
    over a threshold """

    newid = itertools.count()

    def __init__(self, side1, side2, size):
        super(Choke, self).__init__()
        self.size = size
        self.id = next(Choke.newid)
        self.side1 = side1
        self.side2 = side2
        self.center = WalkPosition(
            (side1.x+side2.x)/2, (side1.y+side2.y)/2 )

    def __getstate__(self):
        """ custom pickle """
        return {
            'id': self.id,
            'side1': self.side1.getXY(),
            'side2': self.side2.getXY(),
            'center': self.center.getXY(),
            'size': self.size,
            # note: circular dependency breaks pickle!
            #'region1': self.region1,
            #'region2': self.region2,
        }

    def __setstate__(self, state):
        self.id = state['id']
        x, y = state['side1']
        self.side1 = WalkPosition(x, y)
        x, y = state['side2']
        self.side2 = WalkPosition(x, y)
        self.region1 = None
        self.region2 = None
        x, y = state['center']
        self.center = WalkPosition(x, y)
        self.size = state['size']

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return self.id

class Region:

    """ A region is a continuous area of walktiles of a particular elevation """

    newid = itertools.count()

    def __init__(self, center, clearance, connectivity, elevation=0):
        self.center = center  # Walkposition
        self.elevation = elevation  # 0 for low, 2 for high, 5=vhigh doodad
        self.connectivity = connectivity  # int id
        self.clearance = clearance  # int distance from impassable
        self.chokes = set()
        self.id = next(Region.newid)
        self.resource_clusters = set()

    def __getstate__(self):
        state = self.__dict__.copy()
        state['center'] = self.center.getXY()
        return state

    def __setstate__(self, state):
        self.__dict__ = state
        x, y = self.center
        self.center = WalkPosition(x, y)

    def __eq__(self, other):
        return isinstance(other, Region) and self.id == other.id

    def __hash__(self):
        return self.id

    def addChoke(self, choke):
        self.chokes.add(choke)

    def addResourceCluster(self, cluster):
        self.resource_clusters.add(cluster)

class ResourceCluster:

    def __init__(self, units, region, cid):
        self.resources = units
        self.region = region
        self.depot_position = None
        self.id = cid

    def __hash__(self):
        return self.id

    def __getstate__(self):
        s_res = []
        for res in self.resources:
            s_res.append(res.getInitialTilePosition().getXY())
        state = self.__dict__.copy()
        del state["region"]  # avoid circular ref
        state['resources'] = s_res
        state['depot_position'] = self.depot_position.getXY()
        return state

    def __setstate__(self, state):
        """ need find resource units from saved positions in TerrainAnalyzer """
        self.__dict__ = state
        x, y = state['depot_position']
        self.depot_position = TilePosition(x, y)

    def rebuild(self, bw):
        """ called in TerrainAnalyzer.load to rebuild resource units from pos """
        d_res = set()
        for res in self.resources:
            x, y = res
            saved_tp = TilePosition(x, y)
            x *= 32
            y *= 32
            width = UnitTypes.Resource_Mineral_Field.width()
            height = UnitTypes.Resource_Mineral_Field.height()
            found = bw.getUnitsInRectangle(left=x, top=y,
                right=x + width, bottom=y + height)
            if len(found) == 0:
                logger.error("load: No unit found at {},{}".format(x, y))
            else:
                if len(found) > 1:
                    logger.warn("load: more than 1 unit found at {},{}".format(
                        x, y))
            match = None
            for unit in found:
                if(unit.getInitialTilePosition() == saved_tp and
                  unit.getType().isResourceContainer()):
                    match = unit
                    break
            if match is None:
                logger.error("load: no resource found for {},{}".format(x, y))
            else:
                d_res.add(match)
        self.resources = d_res

class WrongVersionError(Exception):
    pass

class TerrainAnalyzer:

    def __init__(self, bw):
        """ take Broodwar pointer """
        self.bw = bw
        self.version = 7
        self.reinitialize()

    def reinitialize(self):
        """ when playing multiple games, need to clear old map data """
        # no need to save
        self.w_bt = self.bw.mapWidth()
        self.h_bt = self.bw.mapHeight()
        self.w = self.bw.mapWidth() * 4
        self.h = self.bw.mapHeight() * 4
        # heap used to calculate clearance
        self.open_tiles = []
        self.resources = set()
        self.lonely_res = set()
        self.mDepotScore = None
        self.without_closest = []
        self.id_clusters = None  # int to set of Units

        # save

        # accessibility array, use RegionGroupID
        self.access = None
        self.max_access = 0  # highest access_id in self.access
        # clearance vector
        self.clearance = None
        # ignored access ids: small obstacles
        self.ignored = set()
        self.closest_obstacle = None  # 2d vector of walkposition
        # access id to bool impassable
        self.access_id_impassable = None
        # map walktiles to region, None initially
        self.tile_to_region = None
        self.regions = set()  # set of Regions

        # reconstruct on load
        Choke.newid = itertools.count()
        Region.newid = itertools.count()
        self.chokes = set()  # set of Chokes
        self.resource_clusters = set()  # set of ResourceClusters

    def getFilename(self):
        """ filename to save/load map data """
        # note: use .mapHash() for release
        return "tabw-data/{}.ptabw".format(self.bw.mapFileName())

    def __getstate__(self):
        return {
            'connectivity': self.access,
            'max_conn_id': self.max_access,
            'clearance': self.clearance,
            'small_obstacles': self.ignored,
            'closest_obstacle': self.closest_obstacle,
            'conn_id_impassable': self.access_id_impassable,
            'walkpos_to_region': self.tile_to_region,
            'regions': self.regions,
            'version': self.version,
        }

    def __setstate__(self, loadData):
        begin_time = time.clock()
        if loadData['version'] != self.version:
            logger.info("wrong version to load!")
            raise WrongVersionError
        else:
            self.access = loadData['connectivity']
            self.max_access = loadData['max_conn_id']
            self.clearance = loadData['clearance']
            self.ignored = loadData['small_obstacles']
            self.closest_obstacle = loadData['closest_obstacle']
            self.access_id_impassable = loadData['conn_id_impassable']
            self.tile_to_region = loadData['walkpos_to_region']
            self.regions = loadData['regions']
            self.rebuild_after_load()
            logger.info("Deserialized and rebuilt in {}s".format(
                time.clock() - begin_time))

    def save(self):
        """ pickles and saves calculated data to file. Need to have the
        directory existing or else pickle will fail with error 'No such file
        or directory'! """
        logger.info("Saving to file")
        saveData = self.__getstate__()
        file = MapFile(self.getFilename())
        file.save(saveData)
        return saveData

    def load(self):
        """ unpickles and loads data from file. Run rebuild_after_load next """
        logger.info("loading from file")
        file = MapFile(self.getFilename())
        loadData = file.load()
        self.__setstate__(loadData)
        return loadData

    def rebuild_after_load(self):
        """ rebuild choke region references broken in order to pickle. Also need
        to rebuild self.chokes and self.resource_clusters """
        self.resource_clusters = set()
        self.chokes = set()
        for region in self.regions:
            for choke in region.chokes:
                if choke.region1 is None:
                    choke.region1 = region
                elif choke.region2 is None:
                    choke.region2 = region
                else:
                    logger.error("load: Choke in more than 2 regions!")
                self.chokes.add(choke)
            for cluster in region.resource_clusters:
                # just assume no problems with one belonging to multiple regions
                cluster.rebuild(self.bw)
                cluster.region = region
                self.resource_clusters.add(cluster)

        Choke.newid = itertools.count(len(self.chokes))
        Region.newid = itertools.count(len(self.regions))

        # test for chokes to only one region
        for choke in self.chokes:
            if choke.region2 is None:
                choke.region2 = choke.region1
                logger.warn("load: Choke has both regions the same!")
        logger.info("Finished loading map data!")

    def save_test(self):
        self.save()
        file = MapFile(self.getFilename())
        loadData = file.load()
        assert loadData['version'] == self.version
        assert loadData['regions'] == self.regions
        assert loadData['chokes'] == self.chokes
        assert loadData['bt_width'] == self.w_bt
        assert loadData['clearance'] == self.clearance
        assert loadData['walkpos_to_region'] == self.tile_to_region

        self.load()

    def calc_access(self):
        """ 1. prime clearance at the same time, and also store small obstacles
        to ignore """
        w = self.w
        h = self.h
        self.access = [[0 for y in range(h)] for x in range(w)]
        access_id_tile_count = defaultdict(int)  # map ids to num tiles in landmass
        self.access_id_impassable = {}
        walkpos = WalkPosition(0, 449)
        print("map is {},{}".format(w,h))
        for x in range(w):
            for y in range(h):
                walkpos = WalkPosition(x, y)
                position = Position(walkpos)
                #id = 1
                id = self.landmass_id(position)  # the only place this should be used
                access_id_tile_count[id] += 1
                self.access[x][y] = id
                if id > self.max_access:
                    self.max_access = id
                if self.is_accessible(position):
                    #if not self.bw.isWalkable(walkpos):
                    # wavefront will propogate from impassables
                    # need to check for ignored later
                    if id not in self.access_id_impassable:
                        self.access_id_impassable[id] = True
                else:
                    if id not in self.access_id_impassable:
                        self.access_id_impassable[id] = False

        # go through and sort out small obstacles
        for id, size in access_id_tile_count.items():
            small_obstacle_ignore_threshold = 112
            if (self.access_id_impassable[id] and
              size < small_obstacle_ignore_threshold):
                self.ignored.add(id)

    def calculate_clearances(self):
        """ 2. propagate initial clearances and nearest impassable tile to all
        tiles on the map. """
        def propagate(self, x, y, v):
            """ set distance and closest obstacle on all nearby tiles.
            x, y: the walkpos coordinates of parent tile.
            v: the clearance of parent tile.  """
            closest_obs = self.closest_obstacle[x][y]
            assert closest_obs != WalkPositions.Invalid
            assert isinstance(closest_obs, WalkPosition)
            for i in range(x-1, x+2):
                for j in range(y-1, y+2):
                    # skip self
                    if i == x and j == y:
                        continue
                    # should forbid all out of range
                    # if self.bw.isWalkable(i, j):
                    if(i < 0 or j < 0 or i >= self.w or j >= self.h):
                        continue
                    tc = self.clearance[i][j]
                    dist = v + 10  # 10 for orthogonals
                    dx = i - x
                    dy = j - y
                    if abs(dx) and abs(dy):  # 14 for diagonals
                        assert i == x-1 or x+1
                        assert j == y-1 or y+1
                        dist += 4
                    # propogate only lesser distances
                    if tc == -1 or tc > dist:
                        self.clearance[i][j] = dist
                        self.closest_obstacle[i][j] = closest_obs
                        self.open_tiles.push(open_tile((i, j), dist))

        self.open_tiles = TileHeap(True)
        # -1 for uninitialized, 0 for impassable
        self.clearance = [[-1 for y in range(self.h)] for x in range(self.w)]
        self.closest_obstacle = [[WalkPositions.Invalid for y in range(self.h)] for x in range(self.w)]
        # go through a second pass to set closest obstacles.
        for x in range(self.w):
            for y in range(self.h):
                walkpos = WalkPosition(x, y)
                position = Position(walkpos)
                #access_id = self.landmass_id(position)
                access_id = self.getConnectivityID(walkpos)
                if self.is_accessible(position):
                    self.clearance[x][y] = 0
                    self.closest_obstacle[x][y] = walkpos
                    if access_id not in self.ignored:
                        self.open_tiles.push(open_tile((x, y), 0))

                # treat map edges as impassable obstacles
                elif x == 0 or y == 0 or x == self.w-1 or y == self.h-1:
                    self.clearance[x][y] = 10
                    self.open_tiles.push(open_tile((x, y), 10))
                    self.closest_obstacle[x][y] = WalkPosition(
                        -1 if x == 0 else (self.w if x == self.w-1 else x),
                        -1 if y == 0 else (self.h if y == self.h-1 else y))

        # limit iterations
        maptiles = len(self.clearance) * len(self.clearance[0])
        max_iter = maptiles * 4
        iterations = 0
        # process open tiles
        while len(self.open_tiles) > 0:
            tile = self.open_tiles.pop()  # walktile
            x, y, v = tile.unpack()
            walkpos = WalkPosition(x, y)
            position = Position(walkpos)
            #id = self.landmass_id(position)
            id = self.getConnectivityID(walkpos)
            # skip tiles which are part of small obstacles
            if id in self.ignored:
                continue
            propagate(self, x, y, v)
            if iterations > max_iter:
                print("exceeded {} iterations".format(max_iter))
                break
            else:
                iterations += 1

        without_closest = []
        for x in range(self.w):
            for y in range(self.h):
                if (self.closest_obstacle[x][y] == WalkPositions.Invalid):
                    without_closest.append((x, y))

        print("There are {} tiles without closest obstacle set".format(len(without_closest)))
        self.without_closest = without_closest

    def create_regions(self):
        """ 3. Divide map up into regions. Only handle passable regions, for the
        impassable areas, rely on BWAPI's internal regions. """

        def largest_unclaimed_maximum(self, choke_tiles,
                                      border_tiles=set()):
            """ find largest local maximum not part of a region.
            self.tile_to_region is 2d vector representing region ids.
            choke_tiles is a dict of WalkPos to choke ids.
            border_tiles is a set of WalkPos border tiles (assign ids later).
             """
            max_clearance = 0
            max_tile = None
            for x in range(len(self.tile_to_region)):
                for y in range(len(self.tile_to_region[x])):
                    tile = WalkPosition(x, y)
                    if(self.tile_to_region[x][y] or
                       tile in choke_tiles or
                       tile in border_tiles):  # belongs to a region
                        continue
                    else:
                        if max_clearance < self.clearance[x][y]:
                            max_clearance = self.clearance[x][y]
                            max_tile = tile
            return max_tile, max_clearance

        def find_choke_sides(self, center):
            """ given center of choke, find two sides """
            side1 = self.closest_obstacle[center.x][center.y]
            # print("center {},{} side1:{}".format(center.x, center.y, str(side1)))
            assert isinstance(side1, WalkPosition )

            if(side1 == center):
                print("warn, the side1 is same as center of choke.")
                return (side1, side1)

            side1_direction = Vector(side1.x - center.x,
                                     side1.y - center.y)
            side1_direction.normalize()

            x0 = side1.x
            y0 = side1.y

            x1 = center.x
            y1 = center.y

            dx = abs(x1 - x0)
            dy = abs(y1 - y0)

            sx = 1 if x0 < x1 else -1
            sy = 1 if y0 < y1 else -1

            x0 = x1
            y0 = y1

            error = dx - dy

            while(True):
                if(x0 < 0 or y0 < 0 or x0 >= self.w or y0 >= self.h or
                   not self.bw.isWalkable(x0, y0) ):
                    print("warn, did not reach side2")
                    return (side1, WalkPosition(x0, y0))

                side2 = self.closest_obstacle[x0][y0]

                side2_direction = Vector(side2.x - center.x,
                                         side2.y - center.y)
                side2_direction.normalize()

                dot = side2_direction.dot(side1_direction)
                if dot > 1:
                    dot = 1
                elif dot < -1:
                    dot = -1
                try:
                    angle = math.acos(dot)
                except ValueError:
                    ipdb.set_trace()
                if(angle > 2.0):
                    return (side1, side2)

                e2 = error*2
                if(e2 > -dy):
                    error -= dy
                    x0 += sx

                if(e2 < dx):
                    error += dx
                    y0 += sy

        mapW = self.w
        mapH = self.h
        self.tile_to_region = [[None for y in range(mapH)] for x in range(mapW)]
        choke_tiles = {}  # (x,y) walktiles to choke objects
        border_tiles = set()  # (x,y) walktiles that are part of borders
        while(True):
            # find largest clearance LM and then test outwards for boundaries
            # formerly currentRegionTile
            current_region_center, current_region_clearance = (
                largest_unclaimed_maximum(self, choke_tiles, border_tiles))
            if current_region_clearance == 0:
                break  # no tiles left to assign
            # each tile we visit becomes part of this region
            tilePos = TilePosition(current_region_center.x,
                                       current_region_center.x)
            current_region = Region(current_region_center,
                current_region_clearance,
                self.getConnectivityID(current_region_center),
                self.bw.getGroundHeight(tilePos)
                )
            print("created region {}".format(current_region.id))
            self.regions.add(current_region)
            tile_to_last_min = {}  # WalkPos to WalkPos
            tile_to_children = defaultdict(list)  # (x,y) to [(x,y)]
            unvisited_tiles = TileHeap(False)  # heap to extract max OpenTiles first
            unvisited_tiles.push(open_tile(current_region_center,
                                           current_region_clearance)
                                )
            tile_to_last_min[current_region_center] = current_region_center
            while len(unvisited_tiles) > 0:
                x, y, curr_tile_clearance = unvisited_tiles.pop().unpack()
                curr_tile = WalkPosition(x, y)
                if curr_tile in choke_tiles:  # current tile is choke
                    choke = choke_tiles[curr_tile]
                    if choke.region2 and choke.region2 != current_region:
                        # print("warn: Touched choke saved to another region")
                        pass
                    elif choke.region1 != current_region:
                        # touch choke already created by another region
                        current_region.addChoke(choke)
                        choke.region2 = current_region
                    continue
                # if region already set for this tile, no choke
                if self.tile_to_region[x][y] is not None:
                    # print("2 regions connected without choke. TODO: borders")
                    continue
                # look for choke
                last_min = tile_to_last_min[curr_tile]
                choke_size = self.clearance[last_min.x][last_min.y]
                found_chokepoint = False
                # minimum must have less than this percent of region max clearance
                region_threshold = 0.90
                # minimum must have less than this percent of tile clearance
                tile_threshold = 0.91
                # minimum Walktile distance from region center for chokes
                min_dist_threshold = 32
                last_min_dist = abs(x - last_min.x) + abs(y - last_min.y)
                region_center_dist = (
                    abs(current_region_center.x - last_min.x) +
                    abs(current_region_center.y - last_min.y))
                # test for chokepoint
                if(choke_size < floor(current_region_clearance * region_threshold)
                   and choke_size < floor(curr_tile_clearance * tile_threshold)):
                    found_chokepoint = True
                if found_chokepoint:
                    if last_min_dist < min_dist_threshold:
                        found_chokepoint = False
                    elif region_center_dist < min_dist_threshold:
                        found_chokepoint = False
                    # curr tile must be at least 10 walk tiles clearence
                    elif curr_tile_clearance < 120:
                        found_chokepoint = False

                if found_chokepoint:
                    # print("""choke size {}, current_region_clearance {},
                    #    curr_tile_clearance {}, region_center_dist {}""".format(
                    #        choke_size, current_region_clearance,
                    #        curr_tile_clearance, region_center_dist))
                    # now find sides of ckoke
                    choke_sides = find_choke_sides(self, last_min)
                    current_choke = Choke(choke_sides[0], choke_sides[1],
                        choke_size)
                    # print("created choke {}, {}{}".format(current_choke.id,
                    #    x, y))
                    current_choke.region1 = current_region
                    current_region.addChoke(current_choke)
                    self.chokes.add(current_choke)

                    x0 = choke_sides[1].x
                    y0 = choke_sides[1].y
                    x1 = choke_sides[0].x
                    y1 = choke_sides[0].y

                    dx = abs(x1 - x0)
                    dy = abs(y1 - y0)

                    sx = 1 if x0 < x1 else -1
                    sy = 1 if y0 < y1 else -1

                    error = dx - dy

                    choke_children = set()

                    while(True):
                        # check map bounds, obstacles, and region
                        if(x0 >= 0 and y0 >= 0 and
                          x0 < self.w and y0 < self.h and
                          self.clearance[x0][y0] != 0 and
                          not self.tile_to_region[x0][y0]):
                            choke_tile = WalkPosition(x0, y0)
                            # mark current tile as part of choke
                            tile_to_children[current_region_center].append(
                                choke_tile)
                            choke_tiles[choke_tile] = current_choke
                            choke_children.add(choke_tile)
                        # exit loop once other side of choke is reached
                        if x0 == x1 and y0 == y1:
                            break

                        e2 = error * 2
                        if e2 > -dy:
                            error -= dy
                            x0 += sx
                        if e2 < dx:
                            error += dx
                            y0 += sy
                        # clean up
                        while(len(choke_children) > 0):
                            temp_tile = choke_children.pop()
                            tile_to_last_min.pop(temp_tile, 0)
                            unvisited_tiles.remove(open_tile(temp_tile, 0))
                            for child in tile_to_children[temp_tile]:
                                choke_children.add(child)
                            tile_to_children.pop(temp_tile, 0)  # erase if exist
                            try:
                                choke_children.remove(temp_tile)
                            except KeyError:
                                pass
                else:  # not a chokepoint
                    x = curr_tile.x
                    y = curr_tile.y
                    if self.clearance[x][y] < choke_size:
                        last_min = curr_tile
                    # test 4 adjacent tiles
                    # w_pos = WalkPosition(x, y)
                    # t_pos = TilePosition(w_pos)
                    adjacent = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
                    for a_tile in adjacent:
                        i, j = a_tile
                        if(i < 0 or j < 0 or i >= mapW or j >= mapH or  # bounds check
                           self.clearance[i][j] == 0):  # passability check
                            continue
                        next_tile = WalkPosition(i, j)
                        if next_tile not in tile_to_last_min:
                            # not visited yet
                            tile_to_last_min[next_tile] = last_min
                            tile_to_children[curr_tile].append(next_tile)
                            clearance = self.clearance[i][j]
                            unvisited_tiles.push(open_tile(next_tile, clearance))
                    # different elevation, set it & our adjacent tiles as border
                    """
                    if self.bw.getGroundHeight(t_pos) != r_gh:
                        self.border_tiles.add((x, y))
                        for a_tile in adjacent:
                            i, j = a_tile
                            if self.tile_to_region[i][j] == r_id:
                                self.border_tiles.add(a_tile)
                    """
                    # detect chokepoint if clearance at local minimum
                    # if choke, claim choke tiles nearby to wall off region

                    # if it is a border, and unclaimed, set to our region
                    # else set tile is in this region, add unclaimed neighbors to set.

                    #
                    # print("added {},{} to region".format(x,y))

            # end while len(unvisited_tiles) > 0
            t_steps_set = set()
            t_steps_set.add(current_region_center)
            region_size = 0
            while(len(t_steps_set) > 0):  # count each tile in the region
                current_tile = t_steps_set.pop()
                self.tile_to_region[current_tile.x][current_tile.y] = current_region
                region_size += 1
                for next_tile in tile_to_children[current_tile]:
                    t_steps_set.add(next_tile)

            # print("region size {}".format(region_size))
            current_region.size = region_size
        self.assign_inaccessible_regions()

    def assign_inaccessible_regions(self):
        """ run after create_regions. The remaining unregioned area should be
        divided up according to its builtin-region. We will remap those ids, and
        create our own region objects for them """
        logger.info("Assigning inaccessible regions")
        rid_to_tile = defaultdict(set)  # initially with builtin region IDs
        for x in range(self.w):  # walkposition resolution
            for y in range(self.h):
                # check that it does not exist
                if self.tile_to_region[x][y] is None:
                    position = Position(WtoP(x), WtoP(y))
                    assert position.isValid(), "invalid position will cause crash"
                    region = self.bw.getRegionAt(position)
                    assert not region.isAccessible(), "these regions should be inaccessible"
                    rid = region.getID()
                    rid_to_tile[rid].add(WalkPosition(x, y))
        # go through and create new region for each previous impassable
        for rid, tileset in rid_to_tile.items():
            try:
                center = WalkPosition(self.bw.getRegion(rid).getCenter())
                connectivity_id = self.getConnectivityID(center)
                if connectivity_id not in self.access_id_impassable:
                        self.access_id_impassable[id] = True
            except TypeError:
                ipdb.set_trace()
            elevation = self.bw.getGroundHeight(TilePosition(center))
            region = Region(center, 0, connectivity_id, elevation)
            self.regions.add(region)
            for tile in tileset:  # set region for each tile
                x, y = tile.getXY()
                self.tile_to_region[x][y] = region

    def create_resource_clusters(self):
        # first get all static resources
        static_minerals = self.bw.getStaticMinerals()
        resources = set()
        for mineral in static_minerals:
            if mineral.getResources() > 250:
                resources.add(mineral)
        static_geysers = self.bw.getStaticGeysers()
        for geyser in static_geysers:
            resources.add(geyser)
        # now cluster them.
        thresh = 3      # num neighbors required to cluster
        radius = 448    # in pixels
        clusterer = ClusterManager(radius, thresh, self.bw)
        clusters, clusterinfo = clusterer.createClusters(resources)
        # detect and separate bridged clusters
        regionToSet = defaultdict(set)  # dict of region to clusters
        cRegion = None  # cluster's region
        CIDtoSet = defaultdict(set)  # cluster id to set of units
        cidMax = 0
        mLonelyResources = set()  # set of units which are not in a cluster
        self.resource_clusters = set()

        for unit, info in clusterinfo.items():
            if info.cluster_id == 0:
                mLonelyResources.add(unit)
            else:
                CIDtoSet[info.cluster_id].add(unit)
                cidMax = max(cidMax, info.cluster_id)

        for cid, cluster_units in clusters.items():
            # for each cluster set, detect if they are all of same region
            regionToSet = defaultdict(set)  # clear dict

            for res in cluster_units:
                pos = WalkPosition(res.getPosition())
                cRegion = self.tile_to_region[pos.x][pos.y]
                if not cRegion:
                    logger.debug("TerrainAnalyzer:NULL region found for tile [{},{}] \n".format(pos.x, pos.y))
                regionToSet[cRegion].add( res )

            # now all resources in the cluster divided by region
            if (len(regionToSet) > 1):  # more than 1 region in the cluster
                logger.debug("bridged cluster detected!")
                # clear original cluster
                #cluster_units.clear()
                CIDtoSet[cid].clear()
                original_cid = cid
                # iterate over regions the cluster is split across

                rsi = iter(regionToSet.items())
                first_region, region_units = next(rsi)  # skip past first group
                logger.debug("first region cid: {} len: {}, original {}".format(
                    cid, len(region_units), original_cid))
                # add cluster in first region back in
                for unit in region_units:
                    #cluster_units.add(unit)
                    CIDtoSet[cid].add(unit)

                # reassign 2nd and later groups
                while True:
                    try:
                        r, region_units = next(rsi)
                        cidMax += 1  # get new cid for this cluster
                        logger.debug("next region cid: {} len: {}".format(
                            cidMax, len(region_units)))
                        for unit in region_units:
                            clusterinfo[unit].cluster_id = cidMax
                            CIDtoSet[cidMax].add(unit)
                    except StopIteration:
                        break

        # add clusters to regions.
        for cid, units in CIDtoSet.items():
            cReg = self.getRegion(next(iter(units)).getPosition())
            res_cluster = ResourceCluster(units, cReg, cid)
            self.resource_clusters.add(res_cluster)
            cReg.addResourceCluster(res_cluster)
        self.resources = resources
        self.lonely_res = mLonelyResources
        self.id_clusters = CIDtoSet

    def addDepotScore(self, val, left, top, right, bot, tooClose):
        for x in range(left, right):
            for y in range(top, bot):
                try:
                    if( self.mDepotScore[x][y] >= 0 ):
                        self.mDepotScore[x][y] += val
                    if(tooClose or not self.bw.isBuildable(tileX=x, tileY=y) ):
                        self.mDepotScore[x][y] = -1
                except IndexError:
                    ipdb.set_trace()

    def get_optimal_depot_spots(self):
        # find optimal resource depot construction locations
        mMBTWidth = self.w//4    # 4x4 walk tile = 1 build tile
        mMBTHeight = self.h//4
        self.mDepotScore = [[0 for y in range(mMBTHeight)] for x in range(mMBTWidth)]
        # resource depots are 4x3 build tiles. Traverse only within 7 tiles.
        # visit each resource besides lonely ones, ignore those
        rLeft, rRight, rTop, rBot = (0, 0, 0, 0)  # resource box temp
        # for minerals we add 1, but geysers are more valuable
        gasBias = 5
        resources = self.resources
        mLonelyResources = self.lonely_res
        logger.debug("resources size {}".format(len(resources)))
        logger.debug("mLonelyResources size {}".format(len(mLonelyResources)))
        for res in resources:
            if res in mLonelyResources:
                continue
            val = 1
            if( res.getType() == UnitTypes.Resource_Vespene_Geyser ):
                val = gasBias
            for radius in range(7, 2, -1):
                mwp = self.bw.mapWidth() * 32
                mhp = self.bw.mapHeight() * 32
                radPx = BtoP(radius)
                rLeft, rTop, rRight, rBot = getBoundingBox(res, mwp, mhp, radPx)
                try:
                    assert rRight < mwp
                    assert rBot < mhp
                except AssertionError:
                    ipdb.set_trace()
                # convert pixel coords to BuildTile
                rLeft = floorTile(rLeft)
                rTop = floorTile(rTop)
                rRight = floorTile(rRight)
                rBot = floorTile(rBot)
                try:
                    assert rRight < self.bw.mapWidth(), "rRight is " + str(rRight)
                    assert rBot < self.bw.mapHeight(), "rBot is " + str(rBot)
                except AssertionError:
                    ipdb.set_trace()
                tooClose = radius <= 3
                # parameters are still too large
                self.addDepotScore( val, rLeft, rTop, rRight, rBot, tooClose )
        for region in self.regions:
            for rCluster in region.resource_clusters:
                # iterate over each cluster
                # l,t,r,b of search area
                cLeft = -1
                cTop = -1
                cRight = -1
                cBottom = -1
                for resource in rCluster.resources:
                    # find bounds of resources cluster area
                    if( cLeft > resource.getLeft() or cLeft == -1 ):
                        cLeft = resource.getLeft()
                    if( cTop > resource.getTop() or cTop == -1 ):
                        cTop = resource.getTop()
                    if( cRight < resource.getRight() or cRight == -1 ):
                        cRight = resource.getRight()
                    if( cBottom < resource.getBottom() or cBottom == -1 ):
                        cBottom = resource.getBottom()

                # convert bounds to Build Tile units, extend and crop
                # remember depot is 4x3, and must build 3 tiles away
                cLeft = PtoB(cLeft) - 7
                cTop = PtoB(cTop) - 6
                cRight = PtoB(cRight) + 5
                cBottom = PtoB(cBottom) + 4
                cropToMap( cLeft, cTop, cRight, cBottom, mMBTWidth, mMBTHeight )
                # scan over area and find maximum score
                best = None  # tilePosition
                maxScore = 0
                score = 0
                for x in range(cLeft, cRight+1):
                    for y in range(cTop, cBottom+1):
                        score = self.calcDepotScore(x, y, region)

                        if( score > maxScore ):
                            maxScore = score
                            best = TilePosition( x, y )

                # we should have the best location now.
                rCluster.depot_position = best
        # print stats
        total_unset = 0
        for rc in self.resource_clusters:
            if rc.depot_position is None:
                total_unset += 1
        if total_unset > 0:
            logger.debug("{} resource clusters did not get a depot position.".format(
                total_unset))

    def calcDepotScore(self, leftBT, topBT, region):
        rightBT = leftBT + 4
        bottomBT = topBT + 3
        if( rightBT > self.w_bt ):
            return -1
        if( bottomBT > self.h_bt ):
            return -1
        if( leftBT < 0 ):
            return -1
        if( topBT < 0 ):
            return -1
        score = 0
        for x in range(leftBT, rightBT):
            for y in range(topBT, bottomBT):
                assert self.tile_to_region[x*4][y*4] is not None, "all tiles should have been assigned regions"
                if(self.mDepotScore[x][y] == -1 or
                  self.tile_to_region[x*4][y*4] != region):
                    return -1
                score += self.mDepotScore[x][y]

        return score

    def getConnectivityID(self, walktile):
        x = walktile.x
        y = walktile.y
        return self.access[x][y]

    def analyze(self):
        analyze_start = time.clock()
        self.reinitialize()
        reinitialize_time = time.clock()
        logger.info("reinitialize data took {}s.".format(reinitialize_time - analyze_start))
        changed = False
        try:
            begin_load = time.clock()
            self.load()
            done_load = time.clock()
            logger.info("load data took {}s.".format(done_load - begin_load))
        except (FileNotFoundError, WrongVersionError):
            logger.info("Map file not found, analyzing...")
        # 1. calculate accessible tile classes
            begin_calc = time.clock()
            self.calc_access()
            begin_clear = time.clock()
            logger.info("Calculated accessibility, {}s.".format(begin_clear - begin_calc))
            # 2. flood fill map with distance to unwalkable terrain
            self.calculate_clearances()
            # self.generate_clearance_img()
            # 3. create regions and chokes
            begin_region = time.clock()
            logger.info("Calculated clearance, {}s.".format(begin_region - begin_calc))
            self.create_regions()
            end_region = time.clock()
            self.save()
            logger.info("Created Regions, {}s.".format(end_region - begin_region))
        # self.generate_region_img("regions")
        # create resource clusters
        if len(self.resource_clusters) == 0:
            changed = True
            begin_cluster = time.clock()
            self.create_resource_clusters()
            end_cluster = time.clock()
            logger.info("Clustered resources, {}s.".format(end_cluster - begin_cluster))
            # find base locations
            begin_depot = time.clock()
            self.get_optimal_depot_spots()
            end_depot = time.clock()
            logger.info("Found depot spots, {}s.".format(end_depot - begin_depot))
        self.generate_access_img()
        self.generate_composite_img()
        if changed:
            self.save()
        logger.info("overall took {}s.".format(time.clock() - analyze_start))

    def is_accessible(self, position):
        """ takes and checks using BWAPI.region """
        return not self.bw.getRegionAt(position).isAccessible()

    def landmass_id(self, position):
        """ takes Position, returns regionGroupID at position """
        region = self.bw.getRegionAt(position)
        rgid = region.getRegionGroupID()
        return rgid

    def getRegion(self, position):
        if not isinstance(position, WalkPositions):
            position = WalkPosition(position)
        return self.tile_to_region[position.x][position.y]

    def getHeatMapColor(self, num, n_max):
        """ assumes min to be 0 """
        n = num / n_max
        b = max(1 - 3 * abs(n - 0.33), 0)
        g = max(1 - 3 * abs(n - 0.66), 0)
        r = max(1 - 3 * abs(n - 1), 0)

        return (floor(255 * r), floor(255 * g), floor(255 * b))

    def generate_access_img(self):
        rows = len(self.access)
        cols = len(self.access[0])
        img = MapImage(8, self.bw)
        avg_access = 0
        for x in range(rows):
            for y in range(cols):
                avg_access += self.access[x][y]
        avg_access //= (rows * cols)
        print("avg access {}".format(avg_access))
        for x in range(rows):
            for y in range(cols):
                c = self.getHeatMapColor(
                    (5 + self.access[x][y] * 8) % avg_access,
                    avg_access)
                img.draw_point(x, y, c)
        img.draw_resources()
        img.save_image("mapimg/{}_access.bmp".format(self.bw.mapFileName()))

    def generate_access_impassable_img(self):
        """ create bmp with impassables in red , and passable areas in blue,
        with small obstacles black. """
        print("ignored", self.ignored)
        rows = len(self.access)
        cols = len(self.access[0])
        img = MapImage(8, self.bw)
        for x in range(rows):
            for y in range(cols):
                id = self.access[x][y]
                cn = id * 13 + 60 % 256
                if id in self.ignored:
                    color = (0, 0, 0)
                elif self.access_id_impassable[id]:
                    color = (cn, 0, 0)
                else:
                    color = (0, 0, cn)
                img.draw_point(x, y, color)
        # img.draw_resources()
        img.save_image("mapimg/{}_access_passable.bmp".format(self.bw.mapFileName()))

    def generate_clearance_img(self):
        rows = len(self.access)
        cols = len(self.access[0])
        img = MapImage(8, self.bw)
        max_clearance = 0
        for x in range(rows):
            for y in range(cols):
                clearance = self.clearance[x][y]
                if clearance > max_clearance:
                    max_clearance = clearance

        for x in range(rows):
            for y in range(cols):
                clearance = self.clearance[x][y]
                c = self.getHeatMapColor(clearance, max_clearance)
                if self.closest_obstacle[x][y] == WalkPositions.Invalid:
                    c = (155, 255, 255)
                img.draw_point(x, y, c)
        # img.draw_resources()
        img.save_image("mapimg/{}_clearance.gif".format(self.bw.mapFileName()))

    def generate_region_img(self, name):
        rows = len(self.access)
        cols = len(self.access[0])
        img = MapImage(8, self.bw)
        num_regions = len(self.regions)
        num_tiles = rows * cols
        num_no_region = 0
        for x in range(rows):
            for y in range(cols):
                region = self.tile_to_region[x][y]
                if region is None:
                    # print("tile {},{} has no region!".format(x, y))
                    num_no_region += 1
                    continue
                r_id = region.id
                c = self.getHeatMapColor(r_id+10, num_regions+10)
                img.draw_point(x, y, c)
        for choke in self.chokes:
            color = (255, 255, 255)
            img.draw_line(choke.side1.getXY(), choke.side2.getXY(), color)
            img.draw_circle(choke.center.getXY(), 2, color)

        img.save_image("mapimg/{}_{}.gif".format(self.bw.mapFileName(), name))
        print("{}% of tiles had no region".format(100.0*num_no_region/num_tiles))

    def generate_depot_score_img(self):
        mMBTWidth = self.w_bt
        mMBTHeight = self.h_bt
        img = MapImage(32, self.bw)
        filename = "mapimg/{}_dscore.gif".format(self.bw.mapFileName())
        maxVal_ = 0
        minVal_ = -1
        for x in range(mMBTWidth):
            # get max value
            for y in range(mMBTHeight):
                if ( self.mDepotScore[x][y] > maxVal_ ):
                    maxVal_ = self.mDepotScore[x][y]

        for x in range(mMBTWidth):
            for y in range(mMBTHeight):
                if ( self.mDepotScore[x][y] != -1 ):
                    c = self.getHeatMapColor(self.mDepotScore[x][y], maxVal_ - minVal_)
                    img.draw_point(x, y, c)

        for cluster in self.resource_clusters:
            depot = cluster.depot_position
            if depot is not None:
                c = (100, 200, 200)
                for x in range(4):
                    for y in range(3):
                        img.draw_point(depot.x + x, depot.y + y, c)

        img.draw_resources()
        img.save_image(filename)

    def generate_composite_img(self):
        """ regions with minerals and baseLocations """
        img = MapImage(8, self.bw)
        filename = "mapimg/{}_compositeload.gif".format(self.bw.mapFileName())
        num_regions = 0  # num accessible
        for region in self.regions:
            conn_id = self.getConnectivityID(region.center)
            #print("rid {}".format(region.id))
            if conn_id != region.connectivity:
                print("conn_id's don't match: {} vs {}".format(
                    conn_id, region.connectivity))
            if not self.access_id_impassable[conn_id]:
                num_regions += 1
        num_no_region = 0
        for x in range(self.w):
            for y in range(self.h):
                region = self.tile_to_region[x][y]
                if region is None:
                    print("walktile {},{} has no region!".format(x, y))
                    num_no_region += 1
                    continue
                #elif self.access_id_impassable[region.connectivity]:
                #    c = (0, 0, 0)
                else:
                    r_id = region.id
                    c = self.getHeatMapColor(r_id+15, len(self.regions)+15)
                img.draw_point(x, y, c)
        for choke in self.chokes:
            color = (255, 255, 255)
            img.draw_line(choke.side1.getXY(), choke.side2.getXY(), color)

        for cluster in self.resource_clusters:
            depot = cluster.depot_position
            if depot is not None:
                c = (100, 200, 200)
                for x in range(4*4):
                    for y in range(3*4):
                        img.draw_point(depot.x*4 + x, depot.y*4 + y, c)
            for res in cluster.resources:
                c = (255, 255, 255)
                pos = res.getInitialTilePosition()
                if res.getType() == UnitTypes.Resource_Vespene_Geyser:
                    for x in range(3*4):
                        for y in range(2*4):
                            img.draw_point(pos.x*4 + x, pos.y*4 + y, c)
                else:
                    for x in range(2*4):
                        for y in range(1*4):
                            img.draw_point(pos.x*4 + x, pos.y*4 + y, c)

        img.save_image(filename)
