from PIL import Image, ImageDraw

class MapImage:

    def __init__(self, scale, bw):
        """ 1 is Position, 8 WalkPosition, 32 TilePosition """
        self.scale = scale
        self.bw = bw
        h = self.bw.mapHeight() * 32 // scale
        w = self.bw.mapWidth() * 32 // scale
        print(h, w)
        self.im = Image.new("RGB", (w, h))
        self.draw = ImageDraw.Draw(self.im)

    def draw_point(self, x, y, color):
        self.draw.point((x, y), color)

    def draw_line(self, p1, p2, color):
        self.draw.line([p1, p2], fill=color)

    def draw_circle(self, center, radius, color):
        x, y = center
        tl = (x-radius, y-radius)
        br = (x + radius, y + radius)
        self.draw.ellipse((tl, br), fill=color)

    def save_image(self, pathname):
        try:
            self.im.save(pathname)

        except Exception as e:
            print("error, {}".format(str(e)))

    def draw_resources(self):
        geysers = self.bw.getStaticGeysers()
        minerals = self.bw.getStaticMinerals()
        for mineral in minerals:
            x = mineral.getLeft() // self.scale
            x_r = (mineral.getRight()+self.scale) // self.scale
            y = mineral.getTop() // self.scale
            y_d = (mineral.getBottom()+self.scale) // self.scale
            for xi in range(x, x_r):
                for yi in range(y, y_d):
                    self.draw_point(xi, yi, (100, 150, 255))

        for geyser in geysers:
            x = geyser.getLeft() // self.scale
            x_r = (geyser.getRight()) // self.scale
            y = geyser.getTop() // self.scale
            y_d = (geyser.getBottom()+self.scale) // self.scale
            for xi in range(x, x_r):
                for yi in range(y, y_d):
                    self.draw_point(xi, yi, (200, 150, 40))
