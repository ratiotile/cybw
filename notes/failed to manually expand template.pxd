
    # Try to manually expand template
    '''
    cdef cppclass WalkPosition:
        WalkPosition()
        WalkPosition(const Position &pt)
        WalkPosition(const TilePosition &pt)
        WalkPosition(const WalkPosition &pt)
        WalkPosition(int x, int y)
        bint opbool "operator bool" () nogil
        bint operator == (const WalkPosition &pos) const
        bint operator != (const WalkPosition &pos) const
        bint operator  < (const WalkPosition &position) const

        # WalkPosition &operator += (const WalkPosition &p)
        WalkPosition operator + (const WalkPosition &pos) const
        # WalkPosition &operator -= (const WalkPosition &p)
        WalkPosition operator - (const WalkPosition &pos) const
        # WalkPosition &operator *= (const WalkPosition &p)
        WalkPosition operator * (const int &val) const

        # WalkPosition &operator |= (const WalkPosition &p)
        WalkPosition operator | (const int &val) const
        # WalkPosition &operator &= (const WalkPosition &p)
        WalkPosition operator & (const int &val) const
        # WalkPosition &operator ^= (const WalkPosition &p)
        WalkPosition operator ^ (const int &val) const
        # WalkPosition &operator /= (const WalkPosition &p)
        WalkPosition operator / (const int &val) const
        WalkPosition operator % (const int &val) const
        # WalkPosition &operator %= (const WalkPosition &p)

        #friend std::ostream &operator << (std::ostream &out, const Point<T,Scale> &pt)
        #friend std::istream &operator >> (std::istream &in, Point<T,Scale> &pt)

        bint isValid() const
        Point &makeValid()
        double getDistance(const WalkPosition &position) const
        double getLength() const
        int getApproxDistance(const WalkPosition &position) const
        Point &setMax(int max_x, int max_y)
        Point &setMax(const WalkPosition &max)
        Point &setMin(int min_x, int min_y)
        Point &setMin(const WalkPosition &min)
        int x
        int y
    '''